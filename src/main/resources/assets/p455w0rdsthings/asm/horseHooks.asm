list n_entityInit
RETURN

list i_entityInit
ALOAD 0
INVOKESTATIC p455w0rd/p455w0rdsthings/asm/PHooks.onInitHorse (Lnet/minecraft/entity/passive/EntityHorse;)V

list i_getHorseArmorType
ALOAD 0
INVOKESTATIC p455w0rd/p455w0rdsthings/asm/PHooks.getCustomHorseArmor (Lnet/minecraft/entity/passive/EntityHorse;)Lnet/minecraft/entity/passive/HorseArmorType;
ARETURN

list n_setHorseArmorStack
ALOAD 0
GETFIELD net/minecraft/entity/passive/EntityHorse.field_70180_af : Lnet/minecraft/network/datasync/EntityDataManager;
GETSTATIC net/minecraft/entity/passive/EntityHorse.field_184791_bI : Lnet/minecraft/network/datasync/DataParameter;
ALOAD 2
INVOKEVIRTUAL net/minecraft/entity/passive/HorseArmorType.func_188579_a ()I
INVOKESTATIC java/lang/Integer.valueOf (I)Ljava/lang/Integer;
INVOKEVIRTUAL net/minecraft/network/datasync/EntityDataManager.func_187227_b (Lnet/minecraft/network/datasync/DataParameter;Ljava/lang/Object;)V


list r_setHorseArmorStack
ALOAD 0
ALOAD 2
INVOKESTATIC p455w0rd/p455w0rdsthings/asm/PHooks.setHorseArmorStack (Lnet/minecraft/entity/passive/EntityHorse;Lnet/minecraft/entity/passive/HorseArmorType;)V

list i_getByItem
ALOAD 0
INVOKESTATIC p455w0rd/p455w0rdsthings/asm/PHooks.getByItem (Lnet/minecraft/item/Item;)Lnet/minecraft/entity/passive/HorseArmorType;
ARETURN

list i_getByItemStack
ALOAD 0
INVOKESTATIC p455w0rd/p455w0rdsthings/asm/PHooks.getByItemStack (Lnet/minecraft/item/ItemStack;)Lnet/minecraft/entity/passive/HorseArmorType;
ARETURN

list i_isHorseArmor
ALOAD 0
INSTANCEOF p455w0rd/p455w0rdsthings/api/IHorseArmor
IFEQ LELSE
ICONST_1
IRETURN
LELSE

