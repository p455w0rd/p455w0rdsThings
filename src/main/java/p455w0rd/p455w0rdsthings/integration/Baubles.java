package p455w0rd.p455w0rdsthings.integration;

import java.util.List;

import com.google.common.collect.Lists;

import baubles.api.BaubleType;
import baubles.api.cap.BaublesCapabilities;
import baubles.api.cap.IBaublesItemHandler;
import baubles.common.container.SlotBauble;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * @author p455w0rd
 *
 */
public class Baubles {

	public static ItemStack getBauble(EntityPlayer player, int index) {
		return getHandler(player).getStackInSlot(index);
	}

	public static IBaublesItemHandler getHandler(EntityPlayer player) {
		return player.hasCapability(BaublesCapabilities.CAPABILITY_BAUBLES, null) ? player.getCapability(BaublesCapabilities.CAPABILITY_BAUBLES, null) : null;
	}

	public static ItemStack getBauble(EntityPlayer player, BaubleType type) {
		return getBauble(player, type.ordinal());
	}

	public static BaubleType getHeadBauble() {
		return BaubleType.HEAD;
	}

	public static ItemStack getHeadBaubleStack(EntityPlayer player) {
		return getBauble(player, getHeadBauble());
	}

	public static Item getHeadBaubleItem(EntityPlayer player) {
		return getHeadBaubleStack(player) == null ? null : getHeadBaubleStack(player).getItem();
	}

	public static List<ItemStack> getEquippedBaubles(EntityPlayer player) {
		List<ItemStack> baubleList = Lists.newArrayList();
		if (getHandler(player) != null) {
			for (int i = 0; i < getHandler(player).getSlots(); i++) {
				baubleList.add(getHandler(player).getStackInSlot(i));
			}
		}
		return baubleList;
	}

	public static int getBaubleSlotIndex(EntityPlayer player, ItemStack stack) {
		if (getHandler(player) != null) {
			IBaublesItemHandler baubles = getHandler(player);
			for (int i = 0; i < baubles.getSlots(); i++) {
				if (baubles.getStackInSlot(i) == null) {
					continue;
				}
				if (baubles.getStackInSlot(i) == stack) {
					return i;
				}
			}
		}
		return -1;
	}

	public static void setBaublesItemStack(EntityPlayer player, int slot, ItemStack stack) {
		if (getHandler(player) != null) {
			getHandler(player).setStackInSlot(slot, stack);
		}
	}

	public static boolean isBaubleSlot(Slot slot) {
		return slot instanceof SlotBauble;
	}

}
