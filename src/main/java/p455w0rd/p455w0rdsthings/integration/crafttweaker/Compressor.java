package p455w0rd.p455w0rdsthings.integration.crafttweaker;

import static com.blamejared.mtlib.helpers.InputHelper.toIItemStack;
import static com.blamejared.mtlib.helpers.InputHelper.toStack;
import static com.blamejared.mtlib.helpers.StackHelper.matches;

import java.util.LinkedList;
import java.util.List;

import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;

import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

/**
 * @author p455w0rd
 *
 */
@ZenClass("mods.p455w0rdsthings.Compressor")
public class Compressor {

	private static final String NAME = "Compressor";

	@ZenMethod
	public static void addRecipe(IItemStack input, IItemStack output, @Optional IItemStack output2, double chance2, int energy) {

		MineTweakerAPI.apply(new Add(new RecipeCompressor(toStack(input), toStack(output), toStack(output2), chance2, energy)));
	}

	private static class Add extends BaseListAddition<RecipeCompressor> {
		public Add(RecipeCompressor recipe) {
			super(NAME, RecipeCompressor.getRecipes());

			recipes.add(recipe);
		}

		@Override
		public String getRecipeInfo(RecipeCompressor recipe) {
			return LogHelper.getStackDescription(recipe.getOutput());
		}
	}

	@ZenMethod
	public static void remove(IIngredient input) {
		List<RecipeCompressor> recipes = new LinkedList<RecipeCompressor>();

		if (input == null) {
			LogHelper.logError(String.format("Required parameters missing for %s Recipe.", NAME));
			return;
		}

		for (RecipeCompressor recipe : RecipeCompressor.getRecipes()) {
			if (matches(input, toIItemStack(recipe.getInput()))) {
				recipes.add(recipe);
			}
		}

		if (!recipes.isEmpty()) {
			MineTweakerAPI.apply(new Remove(recipes));
		}
		else {
			LogHelper.logWarning(String.format("No %s Recipe found for output %s. Command ignored!", NAME, input.toString()));
		}

	}

	private static class Remove extends BaseListRemoval<RecipeCompressor> {
		public Remove(List<RecipeCompressor> recipes) {
			super(NAME, RecipeCompressor.getRecipes(), recipes);
		}

		@Override
		protected String getRecipeInfo(RecipeCompressor recipe) {
			return LogHelper.getStackDescription(recipe.getInput());
		}
	}

}
