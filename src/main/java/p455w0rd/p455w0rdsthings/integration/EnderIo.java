package p455w0rd.p455w0rdsthings.integration;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.collect.Lists;

import crazypants.enderio.ModObject;
import crazypants.enderio.machine.obelisk.aversion.TileAversionObelisk;
import crazypants.enderio.machine.obelisk.spawn.ISpawnCallback.Result;
import crazypants.enderio.material.Material;
import crazypants.enderio.power.IInternalPoweredItem;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.init.ModRecipes;

public class EnderIo {

	public static void init() {
		ModLogger.info(Mods.ENDERIO.getName() + " Integation: Enabled");
		addSoulBinderRecipe("SoulBinderEnderCystalRecipe2", 150000, 6, "p455w0rdsthings.Enderman2", Material.VIBRANT_CYSTAL.getStack(), Material.ENDER_CRYSTAL.getStack());
	}

	public static void addSoulBinderRecipe(String recipeID, int energy, int xp, String soulTypes, ItemStack input, ItemStack output) {

		NBTTagCompound root = new NBTTagCompound();

		root.setString("recipeUID", recipeID);
		root.setInteger("requiredEnergyRF", energy);
		root.setInteger("requiredXP", xp);
		root.setString("entityTypes", soulTypes);
		writeItemStack(root, "inputStack", input);
		writeItemStack(root, "outputStack", output);

		FMLInterModComms.sendMessage("EnderIO", "recipe:soulbinder", root);
	}

	public static List<TileAversionObelisk> getNearbyAversionObelisks(World world, BlockPos pos) {
		List<TileAversionObelisk> obeliskList = Lists.<TileAversionObelisk>newArrayList();
		Iterable<BlockPos> searchableArea = BlockPos.getAllInBox(pos.north(128).west(128).up(128), pos.south(128).east(128).down(128));
		Iterator<BlockPos> searchIterator = searchableArea.iterator();
		while (searchIterator.hasNext()) {
			TileEntity te = world.getTileEntity(searchIterator.next());
			if (te != null && te instanceof TileAversionObelisk) {
				obeliskList.add((TileAversionObelisk) te);
			}
		}
		return obeliskList;
	}

	public static List<TileAversionObelisk> getAversionObelisksInRangeOfEntityApplicable(@Nonnull EntityLivingBase entity) {
		List<TileAversionObelisk> obeliskReturnList = Lists.<TileAversionObelisk>newArrayList();
		List<TileAversionObelisk> obeliskList = getNearbyAversionObelisks(entity.getEntityWorld(), entity.getPosition());
		if (obeliskList.size() > 0) {
			for (int i = 0; i < obeliskList.size(); i++) {
				TileAversionObelisk obelisk = obeliskList.get(i);
				if (obelisk.isSpawnPrevented(entity) == Result.DENY) {
					obeliskReturnList.add(obelisk);
				}
			}
		}
		return obeliskReturnList;
	}

	public static boolean isEntityAverted(@Nonnull EntityLivingBase entity) {
		return getAversionObelisksInRangeOfEntityApplicable(entity).size() > 0;
	}

	private static void writeItemStack(NBTTagCompound nbt, String tagName, ItemStack stack) {
		if (stack != null) {
			NBTTagCompound stackTag = new NBTTagCompound();
			stack.writeToNBT(stackTag);
			nbt.setTag(tagName, stackTag);
		}
	}

	public static ItemStack getEndermanSkull() {
		return new ItemStack(ModObject.blockEndermanSkull.getBlock());
	}

	public static void registerOreDictionaryItems() {
		OreDictionary.registerOre("itemEndermanSkull", getEndermanSkull());
	}

	public static boolean isEndeIOEnergyItem(ItemStack stack) {
		return Mods.ENDERIO.isLoaded() && stack.getItem() instanceof IInternalPoweredItem;
	}

	public static IInternalPoweredItem getPoweredItem(ItemStack stack) {
		return (IInternalPoweredItem) stack.getItem();
	}

	public static void addRecipes() {
		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().eio_armorUpgradeEnderVision = new ShapedOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_ENDERVISION), new Object[] {
				" a ",
				"bcd",
				" e ",
				'a',
				new ItemStack(ModItems.NIGHT_VISION_GOGGLES),
				'b',
				getEndermanSkull(),
				'c',
				new ItemStack(ModItems.ARMOR_UPGRADE_BASE),
				'd',
				new ItemStack(ModItems.SKULL_ENDERMAN2),
				'e',
				new ItemStack(ModItems.SKULL_FRIENDERMAN)
		}));

		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().eio_skull1 = new ShapelessOreRecipe(getEndermanSkull(), new Object[] {
				new ItemStack(ModItems.SKULL_ENDERMAN2)
		}));

		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().eio_skull2 = new ShapelessOreRecipe(getEndermanSkull(), new Object[] {
				new ItemStack(ModItems.SKULL_ENDERMAN)
		}));

		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().eio_skull3 = new ShapelessOreRecipe(getEndermanSkull(), new Object[] {
				new ItemStack(ModItems.SKULL_FRIENDERMAN)
		}));
	}

}
