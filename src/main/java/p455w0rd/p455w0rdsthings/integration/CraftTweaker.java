package p455w0rd.p455w0rdsthings.integration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.google.common.collect.Lists;

import minetweaker.MineTweakerAPI;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.integration.crafttweaker.Compressor;

/**
 * @author p455w0rd
 *
 */
public class CraftTweaker {

	public static void preInit() {
		ModLogger.info(Mods.PROJECTE.getName() + " Integation: Enabled");
		generateDefaultScript();
	}

	public static void init() {
		MineTweakerAPI.registerClass(Compressor.class);
	}

	private static void generateDefaultScript() {
		File parent = new File("scripts");
		if (!parent.isDirectory()) {
			parent.mkdir();
		}
		File script = new File(parent, "p455w0rdsthings_default.zs");
		try {
			if (!script.exists()) {
				Files.write(script.toPath(), generateLines());
			}
		}
		catch (IOException ex) {
			ModLogger.warn("Failed to write sample script");
		}
	}

	private static List<String> generateLines() {
		List<String> defaultLines = Lists.<String>newArrayList();
		defaultLines.add("import mods.p455w0rdsthings.Compressor;");
		defaultLines.add("");
		defaultLines.add("#Example script for p455w0rd's Things Compressor");
		defaultLines.add("");
		defaultLines.add("#IMPORTANT (Recipe Removal):");
		defaultLines.add("#Since multiple compressor recipes can have the same output, recipes are removed");
		defaultLines.add("#by their input.");
		defaultLines.add("");
		defaultLines.add("#Example Recipe Removal:");
		defaultLines.add("#Compressor.remove(<minecraft:cobblestone>);");
		defaultLines.add("");
		defaultLines.add("#Format (Add Recipe):");
		defaultLines.add("#Compressor.addRecipe(input, output, 2ndOutput(Optional), 2ndOutputChance, RF)");
		defaultLines.add("#2ndOutput is optional, to omit, set it to null and set 2ndOutputChance to 0.0");
		defaultLines.add("#2ndOutputChance is a percentage on a scale from 0.0 (0%) to 1.0 (100%) so 0.5");
		defaultLines.add("#would be 50%. if you imot 2ndOutput, you still must set 2ndOutputChance to 0.0");
		defaultLines.add("#Stacksizes required are calculated in the ItemStack, so should be specified in");
		defaultLines.add("#traditional MineTweaker format (<modid:item:damage> * stacksize)");
		defaultLines.add("");
		defaultLines.add("#Example Recipe Addition:");
		defaultLines.add("#Compressor.addRecipe(<minecraft:cobblestone> * 64, <minecraft:coal>, null, 0.0, 10000);");
		defaultLines.add("");
		return defaultLines;
	}

}
