package p455w0rd.p455w0rdsthings.integration;

import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileDankNullDock;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineUpgrade;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileTrashCan;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.integration.waila.WAILADankNullDockProvider;
import p455w0rd.p455w0rdsthings.integration.waila.WAILAInterchangerProvider;
import p455w0rd.p455w0rdsthings.integration.waila.WAILAMachineUpgradeProvider;
import p455w0rd.p455w0rdsthings.integration.waila.WAILAProviderCompressor;
import p455w0rd.p455w0rdsthings.integration.waila.WAILAProviderFrienderman;
import p455w0rd.p455w0rdsthings.integration.waila.WAILAProviderTrashCan;

public class WAILA {

	public static String toolTipEnclose = TextFormatting.BOLD + "" + TextFormatting.GREEN + "=====================";
	public static String doSneak = TextFormatting.BOLD + "" + TextFormatting.ITALIC + "<Sneak for Info> ";

	public static void init() {
		ModLogger.info("Waila Integation: Enabled");
		FMLInterModComms.sendMessage(Mods.WAILA.getId(), "register", WAILA.class.getName() + ".callbackRegister");
	}

	public static void callbackRegister(IWailaRegistrar registrar) {
		registrar.registerBodyProvider(new WAILAProviderTrashCan(), TileTrashCan.class);
		registrar.registerBodyProvider(new WAILAProviderCompressor(), TileCompressor.class);
		registrar.registerBodyProvider(new WAILAProviderFrienderman(), EntityFrienderman.class);
		registrar.registerStackProvider(new WAILAInterchangerProvider(), TileInterchanger.class);
		registrar.registerStackProvider(new WAILADankNullDockProvider(), TileDankNullDock.class);
		registrar.registerBodyProvider(new WAILADankNullDockProvider(), TileDankNullDock.class);
		registrar.registerBodyProvider(new WAILAMachineUpgradeProvider(), TileMachineUpgrade.class);
	}

}
