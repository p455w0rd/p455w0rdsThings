package p455w0rd.p455w0rdsthings.integration.waila;

import java.util.List;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.integration.WAILA;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;

/**
 * @author p455w0rd
 *
 */
public class WAILAProviderCompressor implements IWailaDataProvider {

	@Override
	public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return null;
	}

	@Override
	public List<String> getWailaHead(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return null;
	}

	@Override
	public List<String> getWailaBody(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		EntityPlayer player = accessor.getPlayer();
		TileCompressor compressor = (TileCompressor) accessor.getTileEntity();
		currenttip.add(WAILA.toolTipEnclose);
		if (player.isSneaking()) {
			currenttip.add("RF: " + compressor.getEnergy());
			if (compressor.isCompressing()) {
				ItemStack currentStack = compressor.getCurrentRecipe().getInput();
				currenttip.add(currentStack.getDisplayName() + " => " + RecipeCompressor.getRecipeFromInput(currentStack).getOutput().getDisplayName());
				currenttip.add(compressor.getPercentComplete() + "% complete");
			}
		}
		else {
			currenttip.add(WAILA.doSneak);
		}
		currenttip.add(WAILA.toolTipEnclose);
		return currenttip;
	}

	@Override
	public List<String> getWailaTail(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return null;
	}

	@Override
	public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound tag, World world, BlockPos pos) {
		return null;
	}

}
