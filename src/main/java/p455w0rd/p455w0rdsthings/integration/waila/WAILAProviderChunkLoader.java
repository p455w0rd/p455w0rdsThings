package p455w0rd.p455w0rdsthings.integration.waila;

import java.util.List;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class WAILAProviderChunkLoader implements IWailaDataProvider {

	@Override
	public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return null;
	}

	@Override
	public List<String> getWailaHead(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return null;
	}

	@Override
	public List<String> getWailaBody(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		/*
		EntityPlayer player = accessor.getPlayer();
		TileChunkLoader te = (TileChunkLoader) accessor.getTileEntity();
		boolean visEnabled = te.isDisplayingBoundaries();
		currenttip.add(TextFormatting.BOLD + "" + TextFormatting.GREEN + "=====================");
		if (player.isSneaking()) {
			currenttip.add("Owner: " + te.getOwner().getName());
			currenttip.add("Radius: " + te.getRadius() + " (" + diameterToChunks(te.getDiameter()) + " Chunk" + (te.getRadius() > 1 ?  "s":"") + ")");
			currenttip.add("Pos: " + te.getPos().getX() + ", " + te.getPos().getY() + ", " + te.getPos().getZ());
			currenttip.add(TextFormatting.RESET + "Visualiser: " + TextFormatting.ITALIC  + (visEnabled ? "Enabled" : "Sneak+Right Click"));
		}
		else {
			currenttip.add(TextFormatting.BOLD + "" + TextFormatting.ITALIC + "<Sneak for Info> ");
		}
		currenttip.add(TextFormatting.BOLD + "" + TextFormatting.GREEN + "=====================");
		*/
		return currenttip;
	}

	@Override
	public List<String> getWailaTail(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return null;
	}

	@Override
	public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound tag, World world, BlockPos pos) {
		return null;
	}

	private int diameterToChunks(int diameter) {
		return diameter * diameter;
	}

}
