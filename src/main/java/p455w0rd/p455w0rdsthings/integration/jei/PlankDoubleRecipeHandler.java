/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.integration.jei;

import javax.annotation.Nonnull;

import mezz.jei.api.recipe.*;
import p455w0rd.p455w0rdsthings.recipe.RecipePlankDouble;

/**
 * @author p455w0rd
 *
 */
public class PlankDoubleRecipeHandler implements IRecipeHandler<RecipePlankDouble> {

	@Override
	public Class<RecipePlankDouble> getRecipeClass() {
		return RecipePlankDouble.class;
	}

	@Override
	public String getRecipeCategoryUid() {
		return VanillaRecipeCategoryUid.CRAFTING;
	}

	@Override
	public String getRecipeCategoryUid(@Nonnull RecipePlankDouble recipe) {
		return VanillaRecipeCategoryUid.CRAFTING;
	}

	@Override
	public IRecipeWrapper getRecipeWrapper(RecipePlankDouble recipe) {
		return new PlankDoubleRecipeWrapper();
	}

	@Override
	public boolean isRecipeValid(RecipePlankDouble recipe) {
		return true;
	}

}
