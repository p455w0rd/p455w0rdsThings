package p455w0rd.p455w0rdsthings.integration.jei;

import mezz.jei.api.IGuiHelper;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.util.ErrorUtil;
import mezz.jei.util.Log;
import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.integration.JEI;
import p455w0rd.p455w0rdsthings.recipe.ArmorUpgradeRecipe;

/**
 * @author p455w0rd
 *
 */
public class ArmorUpgradeRecipeHandler implements IRecipeHandler<ArmorUpgradeRecipe> {

	private final IGuiHelper guiHelper;

	public ArmorUpgradeRecipeHandler(IGuiHelper guiHelper) {
		this.guiHelper = guiHelper;
	}

	@Override
	public Class<ArmorUpgradeRecipe> getRecipeClass() {
		return ArmorUpgradeRecipe.class;
	}

	@Override
	public String getRecipeCategoryUid() {
		return JEI.UID.ARMORUPGRADE;
	}

	@Override
	public String getRecipeCategoryUid(ArmorUpgradeRecipe recipe) {
		return JEI.UID.ARMORUPGRADE;
	}

	@Override
	public IRecipeWrapper getRecipeWrapper(ArmorUpgradeRecipe recipe) {
		return new ArmorUpgradeRecipeWrapper(guiHelper, recipe);
	}

	@Override
	public boolean isRecipeValid(ArmorUpgradeRecipe recipe) {
		if (recipe.getRecipeOutput() == null) {
			String recipeInfo = ErrorUtil.getInfoFromRecipe(recipe, this);
			Log.error("Recipe has no output. {}", recipeInfo);
			return false;
		}
		int inputCount = 0;
		for (Object input : recipe.getInput()) {
			if (input instanceof ItemStack) {
				inputCount++;
			}
			else {
				String recipeInfo = ErrorUtil.getInfoFromRecipe(recipe, this);
				Log.error("Recipe has an input that is not an ItemStack. {}", recipeInfo);
				return false;
			}
		}
		if (inputCount > 9) {
			String recipeInfo = ErrorUtil.getInfoFromRecipe(recipe, this);
			Log.error("Recipe has too many inputs. {}", recipeInfo);
			return false;
		}
		return inputCount > 0;
	}
}