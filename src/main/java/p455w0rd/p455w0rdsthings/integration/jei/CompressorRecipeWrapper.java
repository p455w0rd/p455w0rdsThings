package p455w0rd.p455w0rdsthings.integration.jei;

import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.collect.Lists;

import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.client.gui.GuiCompressor;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;

/**
 * @author p455w0rd
 *
 */
public class CompressorRecipeWrapper extends BlankRecipeWrapper {

	private final RecipeCompressor recipe;
	private final IDrawableAnimated progress;

	public CompressorRecipeWrapper(IGuiHelper guiHelper, RecipeCompressor recipeIn) {
		recipe = recipeIn;
		IDrawableStatic progressStatic = guiHelper.createDrawable(GuiCompressor.BACKGROUND, 202, 1, 21, 13);

		progress = guiHelper.createAnimatedDrawable(progressStatic, 60, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ingredients.setInput(ItemStack.class, recipe.getInput());
		List<ItemStack> outputs = Lists.newArrayList();
		outputs.add(recipe.getOutput());
		outputs.add(recipe.getSecondOutput());
		ingredients.setOutputs(ItemStack.class, outputs);
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft, int recipeWidth, int recipeHeight) {
		super.drawAnimations(minecraft, recipeWidth, recipeHeight);
		progress.draw(minecraft, 35, 9);

		minecraft.fontRendererObj.drawStringWithShadow("RF: " + recipe.getEnergyRequired(), 5, 27, 0x17FF6D);
		if (recipe.getSecondOutput() != null) {
			GlStateManager.pushMatrix();
			GlStateManager.scale(0.5, 0.5, 0.5);
			minecraft.fontRendererObj.drawStringWithShadow("2nd Item Chance: " + (int) (recipe.getSecondOutputChance() * 100) + "%", 9, 78, 0x17FF6D);
			GlStateManager.popMatrix();
		}
	}

}
