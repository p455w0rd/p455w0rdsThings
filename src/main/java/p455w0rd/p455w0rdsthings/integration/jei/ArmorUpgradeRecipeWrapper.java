package p455w0rd.p455w0rdsthings.integration.jei;

import java.util.Collections;
import java.util.List;

import mezz.jei.api.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.plugins.vanilla.crafting.AbstractShapelessRecipeWrapper;
import mezz.jei.util.BrokenCraftingRecipeException;
import mezz.jei.util.ErrorUtil;
import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.recipe.ArmorUpgradeRecipe;

/**
 * @author p455w0rd
 *
 */
public class ArmorUpgradeRecipeWrapper extends AbstractShapelessRecipeWrapper {

	private final ArmorUpgradeRecipe recipe;

	public ArmorUpgradeRecipeWrapper(IGuiHelper guiHelper, ArmorUpgradeRecipe recipe) {
		super(guiHelper);
		this.recipe = recipe;
		for (Object input : this.recipe.getInput()) {
			if (input instanceof ItemStack) {
				ItemStack itemStack = (ItemStack) input;
				if (itemStack.stackSize != 1) {
					itemStack.stackSize = 1;
				}
			}
		}
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ItemStack recipeOutput = recipe.getRecipeOutput();

		try {
			ingredients.setInputs(ItemStack.class, recipe.getInput());
			if (recipeOutput != null) {
				ingredients.setOutput(ItemStack.class, recipeOutput);
			}
		}
		catch (RuntimeException e) {
			String info = ErrorUtil.getInfoFromBrokenCraftingRecipe(recipe, recipe.getInput(), recipeOutput);
			throw new BrokenCraftingRecipeException(info, e);
		}
	}

	@Override
	public List<ItemStack> getInputs() {
		return recipe.getInput();
	}

	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(recipe.getRecipeOutput());
	}
}