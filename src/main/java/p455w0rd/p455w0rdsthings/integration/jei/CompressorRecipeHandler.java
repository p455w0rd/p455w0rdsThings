package p455w0rd.p455w0rdsthings.integration.jei;

import javax.annotation.Nonnull;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;
import p455w0rd.p455w0rdsthings.integration.JEI;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;

/**
 * @author p455w0rd
 *
 */
public class CompressorRecipeHandler implements IRecipeHandler<RecipeCompressor> {

	@Nonnull
	private final IJeiHelpers jeiHelpers;

	public CompressorRecipeHandler(@Nonnull IJeiHelpers jeiHelpers) {
		this.jeiHelpers = jeiHelpers;
	}

	@Nonnull
	@Override
	public Class<RecipeCompressor> getRecipeClass() {
		return RecipeCompressor.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid() {
		return JEI.UID.COMPRESSOR;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(@Nonnull RecipeCompressor recipe) {
		return JEI.UID.COMPRESSOR;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull RecipeCompressor recipe) {
		return new CompressorRecipeWrapper(jeiHelpers.getGuiHelper(), recipe);
	}

	@Override
	public boolean isRecipeValid(@Nonnull RecipeCompressor recipe) {
		return true;
	}

}
