/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.integration.jei;

import java.util.*;

import com.google.common.collect.ImmutableList;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import net.minecraft.block.*;
import net.minecraft.item.*;

/**
 * @author p455w0rd
 *
 */
public class PlankDoubleRecipeWrapper extends BlankRecipeWrapper {

	@Override
	public void getIngredients(IIngredients ingredients) {
		List<ItemStack> inputAxes = new ArrayList<ItemStack>();
		List<ItemStack> inputLogs = new ArrayList<ItemStack>();
		List<ItemStack> outputs = new ArrayList<ItemStack>();
		List<Item> axeList = ImmutableList.copyOf(Item.REGISTRY);
		List<Block> blockList = ImmutableList.copyOf(Block.REGISTRY);
		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();

		for (Item axeItem : axeList) {
			if (axeItem instanceof ItemAxe) {
				inputAxes.add(new ItemStack(axeItem));
			}
		}
		for (Block blockItem : blockList) {
			if (blockItem instanceof BlockLog) {
				inputLogs.add(new ItemStack(blockItem));
				continue;
			}
			if (blockItem instanceof BlockPlanks) {
				outputs.add(new ItemStack(blockItem));
			}
		}
		inputs.add(inputAxes);
		inputs.add(inputLogs);
		ingredients.setInputLists(ItemStack.class, inputs);
		ingredients.setOutputs(ItemStack.class, outputs);
	}

}
