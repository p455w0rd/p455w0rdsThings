package p455w0rd.p455w0rdsthings.integration.jei;

import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeCategory;
import p455w0rd.p455w0rdsthings.client.gui.GuiCompressor;
import p455w0rd.p455w0rdsthings.integration.JEI;

/**
 * @author p455w0rd
 *
 */
public class CompressorRecipeCategory extends BlankRecipeCategory<CompressorRecipeWrapper> {

	private final IDrawable background;
	private static final int[] INPUT_SLOTS = {
			0
	};
	private static final int[] OUTPUT_SLOTS = {
			1, 2
	};

	public CompressorRecipeCategory(IGuiHelper guiHelper) {
		background = guiHelper.createDrawable(GuiCompressor.BACKGROUND, 0, 156, 88, 51);
	}

	@Override
	public String getUid() {
		return JEI.UID.COMPRESSOR;
	}

	@Override
	public String getTitle() {
		return "Compressor";
	}

	@Override
	public IDrawable getBackground() {
		return background;
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, CompressorRecipeWrapper recipeWrapper, IIngredients ingredients) {
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		guiItemStacks.init(INPUT_SLOTS[0], true, 6, 6);

		guiItemStacks.init(OUTPUT_SLOTS[0], false, 64, 6);
		guiItemStacks.init(OUTPUT_SLOTS[1], false, 64, 27);

		JEI.setRecipeItems(recipeLayout, ingredients, INPUT_SLOTS, OUTPUT_SLOTS, null, null);
	}

}
