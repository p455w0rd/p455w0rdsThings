package p455w0rd.p455w0rdsthings.integration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import cofh.api.energy.IEnergyContainerItem;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.ISubtypeRegistry;
import mezz.jei.api.ISubtypeRegistry.ISubtypeInterpreter;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.gui.IGuiFluidStackGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredientBlacklist;
import mezz.jei.api.ingredients.IIngredientRegistry;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.IModIngredientRegistration;
import mezz.jei.gui.recipes.RecipesGui;
import net.minecraft.client.gui.inventory.GuiCrafting;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.OreDictionary;
import p455w0rd.p455w0rdsthings.api.IArmorUpgrade;
import p455w0rd.p455w0rdsthings.api.IEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.container.ContainerCompressor;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.integration.jei.ArmorUpgradeRecipeCategory;
import p455w0rd.p455w0rdsthings.integration.jei.ArmorUpgradeRecipeHandler;
import p455w0rd.p455w0rdsthings.integration.jei.CompressorRecipeCategory;
import p455w0rd.p455w0rdsthings.integration.jei.CompressorRecipeHandler;
import p455w0rd.p455w0rdsthings.integration.jei.PlankDoubleRecipeHandler;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;

@JEIPlugin
public class JEI implements IModPlugin {

	public static IIngredientBlacklist blacklist;
	private static RecipesGui recipesGui;
	private static IIngredientRegistry ingredientRegistry;
	private static ISubtypeRegistry subtypeRegistry;

	@Override
	public void register(@Nonnull IModRegistry registry) {
		IJeiHelpers helpers = registry.getJeiHelpers();
		blacklist = helpers.getIngredientBlacklist();
		IGuiHelper guiHelper = helpers.getGuiHelper();
		ingredientRegistry = registry.getIngredientRegistry();

		blacklistItem(new ItemStack(ModItems.DANK_NULL_HOLDER, 1, OreDictionary.WILDCARD_VALUE));
		blacklistItem(new ItemStack(ModBlocks.FAKE_LAVA));
		blacklistItem(new ItemStack(ModItems.COMPRESSOR));
		blacklistItem(new ItemStack(ModBlocks.CARBON_DOUBLE_SLAB));
		if (!Mods.TINKERS.isLoaded()) {
			blacklistItem(new ItemStack(ModItems.HORSE_ARMOR_ARDITE));
			blacklistItem(new ItemStack(ModItems.HORSE_ARMOR_COBALT));
			blacklistItem(new ItemStack(ModItems.HORSE_ARMOR_MANYULLYN));
		}
		//blacklistItem(new ItemStack(ModItems.EMERALD_CARBON_JETPLATE));

		List<ItemStack> dankNulls = new ArrayList<ItemStack>();
		dankNulls.addAll(Arrays.asList(new ItemStack(ModItems.DANK_NULL, 1, 0), new ItemStack(ModItems.DANK_NULL, 1, 1), new ItemStack(ModItems.DANK_NULL, 1, 2), new ItemStack(ModItems.DANK_NULL, 1, 3), new ItemStack(ModItems.DANK_NULL, 1, 4), new ItemStack(ModItems.DANK_NULL, 1, 5)));
		registry.addDescription(dankNulls, "jei.danknull.desc");
		registry.addDescription(new ItemStack(ModItems.DANK_NULL, 1, 0), "jei.danknull.desc0");
		registry.addDescription(new ItemStack(ModItems.DANK_NULL, 1, 1), "jei.danknull.desc1");
		registry.addDescription(new ItemStack(ModItems.DANK_NULL, 1, 2), "jei.danknull.desc2");
		registry.addDescription(new ItemStack(ModItems.DANK_NULL, 1, 3), "jei.danknull.desc3");
		registry.addDescription(new ItemStack(ModItems.DANK_NULL, 1, 4), "jei.danknull.desc4");
		registry.addDescription(new ItemStack(ModItems.DANK_NULL, 1, 5), "jei.danknull.desc5");

		List<ItemStack> upgradeDescItemList = Lists.newArrayList();
		for (Item item : ModItems.getList()) {
			if (item instanceof IEmeraldCarbonArmor || item instanceof IArmorUpgrade) {
				upgradeDescItemList.add(new ItemStack(item));
			}
		}

		registry.addDescription(upgradeDescItemList, TextFormatting.BOLD + I18n.format("jei.upgradeinfo.header"), "jei.upgradeinfo.desc");
		registry.addDescription(new ItemStack(ModBlocks.DANKNULL_DOCK), "jei.danknull_dock.desc");
		registry.addDescription(new ItemStack(ModBlocks.INTERCHANGER), "jei.interchanger.desc");
		registry.addDescription(new ItemStack(ModItems.FRIENDER_PEARL), "jei.frienderpearl.desc");

		registry.addRecipeCategories(new ArmorUpgradeRecipeCategory(guiHelper), new CompressorRecipeCategory(guiHelper));
		registry.addRecipeHandlers(new PlankDoubleRecipeHandler(), new ArmorUpgradeRecipeHandler(guiHelper), new CompressorRecipeHandler(helpers));
		registry.addRecipeClickArea(GuiCrafting.class, 88, 32, 28, 23, UID.ARMORUPGRADE);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModItems.ARMOR_UPGRADE_BASE), UID.ARMORUPGRADE);
		//registry.addRecipeClickArea(GuiCompressor.class, 93, 42, 16, 16, UID.COMPRESSOR);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.COMPRESSOR), UID.COMPRESSOR);
		registry.addRecipes(RecipeCompressor.getRecipes());
		registry.getRecipeTransferRegistry().addRecipeTransferHandler(ContainerCompressor.class, UID.COMPRESSOR, 0, 1, 3, 36);

	}

	@Override
	public void onRuntimeAvailable(IJeiRuntime runtime) {
		recipesGui = (RecipesGui) runtime.getRecipesGui();
	}

	@Override
	public void registerIngredients(IModIngredientRegistration registry) {
	}

	@Override
	public void registerItemSubtypes(ISubtypeRegistry registry) {
		ISubtypeInterpreter jetplateInterpreter = stack -> {
			if (stack.getItem() == ModItems.EMERALD_CARBON_JETPLATE) {
				if (ArmorUtils.isUpgradeEnabled(stack, Upgrades.FLIGHT) && stack.getItem() instanceof IEnergyContainerItem) {
					if (((IEnergyContainerItem) stack.getItem()).getEnergyStored(stack) == ((IEnergyContainerItem) stack.getItem()).getMaxEnergyStored(stack)) {
						return "full_power";
					}
					return "no_power";
				}
			}
			return null;
		};
		registry.registerSubtypeInterpreter(ModItems.EMERALD_CARBON_JETPLATE, jetplateInterpreter);
	}

	public static IIngredientRegistry getIngredientRegistry() {
		return ingredientRegistry;
	}

	public static void showRecipes(List<String> categoryUIDs) {
		recipesGui.showCategories(categoryUIDs);
	}

	public static void blacklistItem(ItemStack stack) {
		if (Mods.JEI.isLoaded() && blacklist != null) {
			blacklist.addIngredientToBlacklist(stack);
		}
	}

	public static boolean isItemBlacklisted(ItemStack stack) {
		if (Mods.JEI.isLoaded()) {
			return blacklist.isIngredientBlacklisted(stack);
		}
		return false;
	}

	public static void whitelistItem(ItemStack stack) {
		if (Mods.JEI.isLoaded()) {
			blacklist.removeIngredientFromBlacklist(stack);
		}
	}

	public static void handleItemBlacklisting(ItemStack stack, boolean shouldBlacklist) {
		if (shouldBlacklist) {
			if (!isItemBlacklisted(stack)) {
				blacklistItem(stack);
			}
			return;
		}
		if (isItemBlacklisted(stack)) {
			whitelistItem(stack);
		}
	}

	public static void setRecipeItems(@Nonnull IRecipeLayout recipeLayout, @Nonnull IIngredients ingredients, @Nullable int[] itemInputSlots, @Nullable int[] itemOutputSlots, @Nullable int[] fluidInputSlots, @Nullable int[] fluidOutputSlots) {
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();

		if (itemInputSlots != null) {
			List<List<ItemStack>> inputs = ingredients.getInputs(ItemStack.class);
			for (int i = 0; i < inputs.size() && i < itemInputSlots.length; i++) {
				int inputSlot = itemInputSlots[i];
				guiItemStacks.set(inputSlot, inputs.get(i));
			}
		}

		if (itemOutputSlots != null) {
			List<ItemStack> outputs = ingredients.getOutputs(ItemStack.class);
			for (int i = 0; i < outputs.size() && i < itemOutputSlots.length; i++) {
				int outputSlot = itemOutputSlots[i];
				guiItemStacks.set(outputSlot, outputs.get(i));
			}
		}

		if (fluidInputSlots != null) {
			List<List<FluidStack>> fluidInputs = ingredients.getInputs(FluidStack.class);
			for (int i = 0; i < fluidInputs.size() && i < fluidInputSlots.length; i++) {
				int inputTank = fluidInputSlots[i];
				guiFluidStacks.set(inputTank, fluidInputs.get(i));
			}
		}

		if (fluidOutputSlots != null) {
			List<FluidStack> fluidOutputs = ingredients.getOutputs(FluidStack.class);
			for (int i = 0; i < fluidOutputs.size() && i < fluidOutputSlots.length; i++) {
				int outputTank = fluidOutputSlots[i];
				guiFluidStacks.set(outputTank, fluidOutputs.get(i));
			}
		}
	}

	public static class UID {

		public static final String ARMORUPGRADE = ModGlobals.MODID + ".armoupgrade";
		public static final String COMPRESSOR = ModGlobals.MODID + ".compressor";

	}

}
