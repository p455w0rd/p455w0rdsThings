package p455w0rd.p455w0rdsthings.integration;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.guide.chapter.GuideChapter;
import p455w0rd.p455w0rdsthings.guide.page.PageCraftingX2;
import p455w0rd.p455w0rdsthings.guide.page.PageEntityRandomHorse;
import p455w0rd.p455w0rdsthings.guide.page.PageTextOnly;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModFluids;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.init.ModRecipes;
import p455w0rd.p455w0rdsthings.integration.tinkers.TiCMaterials;
import p455w0rd.p455w0rdsthings.integration.tinkers.TiCModifiers;
import slimeknights.mantle.util.RecipeMatch;
import slimeknights.tconstruct.library.DryingRecipe;
import slimeknights.tconstruct.library.MaterialIntegration;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.library.materials.Material;
import slimeknights.tconstruct.library.modifiers.ModifierNBT;
import slimeknights.tconstruct.library.modifiers.TinkerGuiException;
import slimeknights.tconstruct.library.smeltery.CastingRecipe;
import slimeknights.tconstruct.library.utils.HarvestLevels;
import slimeknights.tconstruct.library.utils.TagUtil;
import slimeknights.tconstruct.library.utils.TinkerUtil;
import slimeknights.tconstruct.library.utils.ToolBuilder;
import slimeknights.tconstruct.shared.TinkerCommons;
import slimeknights.tconstruct.shared.TinkerFluids;
import slimeknights.tconstruct.smeltery.TinkerSmeltery;
import slimeknights.tconstruct.tools.TinkerMaterials;
import slimeknights.tconstruct.tools.TinkerTools;

public class TiC {

	public static TiCModifiers tinkersModifiers;
	public static TiCMaterials tinkersMaterials;
	public static final MaterialIntegration CARBON_INTEGRATION = new MaterialIntegration(TiCMaterials.CARBON, ModFluids.SUBLIMATED_CARBON);

	public static class TiCModifierName {
		public static final String JUICED = "juiced";
		public static final String SOLID = "solid";
	}

	public static class TiCColor {
		public static int CARBON = 0x696969;
	}

	public static class TiCModel {
		public static final ResourceLocation SOLID = new ResourceLocation(ModGlobals.MODID, "models/tconstruct/modifier/solid");
	}

	public static class TiCTexture {
		public static String CARBON = ModGlobals.MODID + ":blocks/carbon_block";
	}

	public static void preInit() {
		ModLogger.info(Mods.TINKERS.getName() + " Integation: Enabled");
		tinkersMaterials = new TiCMaterials();
		tinkersMaterials.setupMaterials();
		CARBON_INTEGRATION.integrate();
		CARBON_INTEGRATION.integrateRecipes();
		tinkersModifiers = new TiCModifiers();

		TinkerTools.registerToolForgeBlock("blockCarbon");
		HarvestLevels.harvestLevelNames.put(5, TextFormatting.GREEN + "Carbon");
		if (ConfigOptions.ENABLE_TIC_FLESH_TO_LEATHER_RECIPE) {
			for (DryingRecipe recipe : TinkerRegistry.getAllDryingRecipes()) {
				if (recipe.matches(new ItemStack(Items.ROTTEN_FLESH))) {
					TinkerRegistry.getAllDryingRecipes().remove(recipe);
				}
			}
			TinkerRegistry.registerDryingRecipe(new ItemStack(Items.ROTTEN_FLESH), new ItemStack(Items.LEATHER), ConfigOptions.TIC_LEATHER_DRY_TIME * 20); //20 * 60 * 5 = 5 minutes
		}

		TinkerRegistry.registerMelting(ModBlocks.CARBON_BLOCK, ModFluids.SUBLIMATED_CARBON, Material.VALUE_Block);
		TinkerRegistry.registerMelting(new ItemStack(ModItems.CARBON_INGOT), ModFluids.SUBLIMATED_CARBON, Material.VALUE_Ingot);
		TinkerRegistry.registerMelting(new ItemStack(ModItems.RAW_CARBON), ModFluids.SUBLIMATED_CARBON, Material.VALUE_Shard);

		TinkerRegistry.registerMelting(ModItems.BIT_EMERALD, TinkerFluids.emerald, Material.VALUE_Nugget);
		TinkerRegistry.registerMelting(ModBlocks.NETHER_CARBON_ORE, ModFluids.SUBLIMATED_CARBON, Material.VALUE_Ore());
		TinkerRegistry.registerMelting(ModItems.HORSE_ARMOR_CARBON, ModFluids.SUBLIMATED_CARBON, Material.VALUE_Ingot * 5);
		TinkerRegistry.registerMelting(ModItems.HORSE_ARMOR_EMERALD, TinkerFluids.emerald, Material.VALUE_Gem * 5);
		TinkerRegistry.registerMelting(ModItems.HORSE_ARMOR_ARDITE, TinkerFluids.ardite, Material.VALUE_Ingot * 5);
		TinkerRegistry.registerMelting(ModItems.HORSE_ARMOR_COBALT, TinkerFluids.cobalt, Material.VALUE_Ingot * 5);
		TinkerRegistry.registerMelting(ModItems.HORSE_ARMOR_MANYULLYN, TinkerFluids.manyullyn, Material.VALUE_Ingot * 5);
		TinkerRegistry.registerSmelteryFuel(new FluidStack(ModFluids.SUBLIMATED_CARBON, 25), 500);
		TinkerRegistry.registerTableCasting(new CastingRecipe(new ItemStack(ModItems.CARBON_INGOT), RecipeMatch.of(TinkerSmeltery.castIngot), new FluidStack(ModFluids.SUBLIMATED_CARBON, Material.VALUE_Ingot), false, false));
		TinkerRegistry.registerBasinCasting(new ItemStack(ModBlocks.CARBON_BLOCK), null, ModFluids.SUBLIMATED_CARBON, Material.VALUE_Block);
	}

	public static void init() {
	}

	public static void postInit() {
	}

	public static boolean hasModifier(ItemStack stack, String identifier) {
		return TinkerUtil.hasModifier(getTag(stack), identifier);
	}

	public static NBTTagCompound getTag(ItemStack stack) {
		return TagUtil.getTagSafe(stack);
	}

	public static int getModifierLevel(ItemStack stack, String modifier) {
		int level = 0;
		if (hasModifier(stack, modifier)) {
			ModifierNBT data = ModifierNBT.readTag(TinkerUtil.getModifierTag(stack, modifier));
			if (data.level > 0) {
				return data.level;
			}
		}
		return level;
	}

	public static boolean hasBeheading(ItemStack stack) {
		return hasModifier(stack, TiCModifiers.MOD_BEHEADING) || hasModifier(stack, TiCModifiers.MOD_BEHEADING_CLEAVER);
	}

	public static boolean hasLuck(ItemStack stack) {
		return hasModifier(stack, TiCModifiers.MOD_LUCK);
	}

	public static String getBeheadingTypeString(ItemStack stack) {
		if (hasBeheading(stack)) {
			if (hasModifier(stack, TiCModifiers.MOD_BEHEADING)) {
				return TiCModifiers.MOD_BEHEADING;
			}
			if (hasModifier(stack, TiCModifiers.MOD_BEHEADING_CLEAVER)) {
				return TiCModifiers.MOD_BEHEADING_CLEAVER;
			}
		}
		return "";
	}

	public static void addHorseArmorRecipes() {
		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().arditeHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_ARDITE), "  a", "aba", "a a", 'a', TinkerCommons.ingotArdite, 'b', new ItemStack(Items.SADDLE)));
		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().cobaltHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_COBALT), "  a", "aba", "a a", 'a', TinkerCommons.ingotCobalt, 'b', new ItemStack(Items.SADDLE)));
		ModRecipes.VANILLA_RECIPES.add(ModRecipes.getInstance().manyullynHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_MANYULLYN), "  a", "aba", "a a", 'a', TinkerCommons.ingotManyullyn, 'b', new ItemStack(Items.SADDLE)));
	}

	public static boolean isTinkersItem(ItemStack stack) {
		return stack.getItem().getRegistryName().getResourceDomain().equals(Mods.TINKERS.getId());
	}

	public static ItemStack getSword() {

		ItemStack[] parts = new ItemStack[6];
		parts[0] = TinkerTools.toolRod.getItemstackWithMaterial(TinkerMaterials.magmaslime);
		parts[1] = TinkerTools.swordBlade.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[2] = TinkerTools.wideGuard.getItemstackWithMaterial(TinkerMaterials.cactus);
		ItemStack sword1 = ToolBuilder.tryBuildTool(parts, "Vury Carbon MUCH Dank", TinkerRegistry.getToolForgeCrafting());
		ItemStack[] parts2 = new ItemStack[5];
		parts2[0] = new ItemStack(ModBlocks.CARBON_BLOCK);
		parts2[1] = new ItemStack(ModBlocks.CARBON_BLOCK);
		parts2[2] = new ItemStack(ModBlocks.CARBON_BLOCK);
		parts2[3] = new ItemStack(ModBlocks.CARBON_BLOCK);
		parts2[4] = new ItemStack(ModBlocks.CARBON_BLOCK);
		ItemStack returnSword = null;
		try {
			returnSword = ToolBuilder.tryModifyTool(parts2, sword1, true);
		}
		catch (TinkerGuiException e) {
		}
		//returnSword.setItemDamage(returnSword.getMaxDamage());
		return returnSword;
	}

	public static ItemStack getShuriken() {
		ItemStack[] parts = new ItemStack[6];
		parts[0] = TinkerTools.knifeBlade.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[1] = TinkerTools.knifeBlade.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[2] = TinkerTools.knifeBlade.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[3] = TinkerTools.knifeBlade.getItemstackWithMaterial(TiCMaterials.CARBON);
		ItemStack[] parts2 = new ItemStack[5];
		parts2[0] = TinkerCommons.matReinforcement;
		ItemStack shuriken = ToolBuilder.tryBuildTool(parts, "Vury Carbon MUCH Dank", TinkerRegistry.getToolForgeCrafting());
		ItemStack returnShuriken = null;
		try {
			returnShuriken = ToolBuilder.tryModifyTool(parts2, shuriken, false);
		}
		catch (TinkerGuiException e) {
		}
		return returnShuriken;
	}

	public static ItemStack getBow() {
		ItemStack[] parts = new ItemStack[6];
		parts[0] = TinkerTools.bowLimb.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[1] = TinkerTools.bowLimb.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[2] = TinkerTools.bowString.getItemstackWithMaterial(TinkerMaterials.slimevine_purple);
		ItemStack bow = ToolBuilder.tryBuildTool(parts, "Vury Carbon MUCH Dank", TinkerRegistry.getToolStationCrafting());
		return bow;
	}

	public static ItemStack getArrow() {
		ItemStack[] parts = new ItemStack[6];
		parts[0] = TinkerTools.arrowShaft.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[1] = TinkerTools.arrowHead.getItemstackWithMaterial(TiCMaterials.CARBON);
		parts[2] = TinkerTools.fletching.getItemstackWithMaterial(TinkerMaterials.feather);
		ItemStack arrow = ToolBuilder.tryBuildTool(parts, "Vury Arrow MUCH Dank", TinkerRegistry.getToolStationCrafting());
		return arrow;
	}

	public static void addHorseArmorToList(List<ItemStack> list) {
		list.addAll(Lists.<ItemStack>newArrayList(new ItemStack(ModItems.HORSE_ARMOR_ARDITE), new ItemStack(ModItems.HORSE_ARMOR_COBALT), new ItemStack(ModItems.HORSE_ARMOR_MANYULLYN)));
	}

	public static void addIntegratedGuidePages(IGuideEntry entryItems, List<ItemStack> horseArmorList) {
		new GuideChapter("items.horseArmor", entryItems, horseArmorList, new PageTextOnly(1), new PageEntityRandomHorse(2).setNoText(), new PageCraftingX2(3, ModRecipes.getInstance().redstoneHorseArmor, ModRecipes.getInstance().lapisHorseArmor).setNoText(), new PageCraftingX2(4, ModRecipes.getInstance().ironHorseArmor, ModRecipes.getInstance().goldenHorseArmor).setNoText(), new PageCraftingX2(5, ModRecipes.getInstance().diamondHorseArmor, ModRecipes.getInstance().emeraldHorseArmor).setNoText(), new PageCraftingX2(6, ModRecipes.getInstance().carbonHorseArmor, ModRecipes.getInstance().arditeHorseArmor).setNoText(), new PageCraftingX2(7, ModRecipes.getInstance().cobaltHorseArmor, ModRecipes.getInstance().manyullynHorseArmor).setNoText());
	}

}
