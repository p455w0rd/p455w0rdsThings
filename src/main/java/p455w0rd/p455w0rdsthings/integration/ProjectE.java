/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.integration;

import java.util.List;

import moze_intel.projecte.api.ProjectEAPI;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModLogger;

/**
 * @author p455w0rd
 *
 */
public class ProjectE {

	public static boolean firstPass = true;

	public static void init() {
		ModLogger.info(Mods.PROJECTE.getName() + " Integation: Enabled");
		List<Item> itemList = ModItems.getList();
		List<Block> blockList = ModBlocks.getList();
		for (Item item : itemList) {
			if (ConfigOptions.DISABLE_EMC) {
				ProjectEAPI.getEMCProxy().registerCustomEMC(item, 0);
			}
		}
		for (Block block : blockList) {
			if (ConfigOptions.DISABLE_EMC) {
				ProjectEAPI.getEMCProxy().registerCustomEMC(block, 0);
			}
		}
	}

}
