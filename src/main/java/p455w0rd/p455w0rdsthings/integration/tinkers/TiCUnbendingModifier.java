package p455w0rd.p455w0rdsthings.integration.tinkers;

import net.minecraft.nbt.NBTTagCompound;
import slimeknights.tconstruct.library.modifiers.Modifier;
import slimeknights.tconstruct.library.modifiers.ModifierAspect;
import slimeknights.tconstruct.library.tools.ToolNBT;
import slimeknights.tconstruct.library.utils.HarvestLevels;
import slimeknights.tconstruct.library.utils.TagUtil;

public class TiCUnbendingModifier extends Modifier {

	public TiCUnbendingModifier(String identifier) {
		super(identifier);
		//this.addAspects(new ModifierAspect.SingleAspect(this), new ModifierAspect.DataAspect(this, 0x1FFF97));
	}

	@Override
	public void applyEffect(NBTTagCompound rootCompound, NBTTagCompound modifierTag) {
		ToolNBT data = TagUtil.getToolStats(rootCompound);
		int currentHarvestLevel = data.harvestLevel;
		int newLevel = currentHarvestLevel + 1;
		String newHarvestLevelName = HarvestLevels.getHarvestLevelName(newLevel);
		if (!isInteger(newHarvestLevelName.toString())) {
			this.addAspects(new ModifierAspect.SingleAspect(this), new ModifierAspect.DataAspect(this, 0x1FFF97));
			data.harvestLevel++;
			TagUtil.setToolTag(rootCompound, data.get());
		}
	}


	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		}
		catch (NumberFormatException e) {
			return false;
		}
		catch (NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

}
