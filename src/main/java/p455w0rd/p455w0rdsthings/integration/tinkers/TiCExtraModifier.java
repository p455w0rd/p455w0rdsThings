package p455w0rd.p455w0rdsthings.integration.tinkers;

import net.minecraft.nbt.NBTTagCompound;
import slimeknights.tconstruct.library.modifiers.Modifier;
import slimeknights.tconstruct.library.modifiers.ModifierAspect;
import slimeknights.tconstruct.library.tools.ToolNBT;
import slimeknights.tconstruct.library.utils.TagUtil;

public class TiCExtraModifier extends Modifier {

	public TiCExtraModifier(String identifier) {
		super(identifier);
		addAspects(new ModifierAspect.SingleAspect(this));
		// this.addAspects(new ModifierAspect.SingleAspect(this), new
		// ModifierAspect.DataAspect(this, 0x1FFF97));
	}

	@Override
	public void applyEffect(NBTTagCompound rootCompound, NBTTagCompound modifierTag) {
		ToolNBT data = TagUtil.getToolStats(rootCompound);
		data.modifiers++;
		TagUtil.setToolTag(rootCompound, data.get());
	}

}
