package p455w0rd.p455w0rdsthings.integration.tinkers;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.integration.TiC.TiCModel;
import slimeknights.mantle.util.RecipeMatch;
import slimeknights.tconstruct.common.ModelRegisterUtil;
import slimeknights.tconstruct.library.modifiers.Modifier;
import slimeknights.tconstruct.tools.modifiers.ModSoulbound;

public class TiCModifiers {

	public static String MOD_BEHEADING = "beheading";
	public static String MOD_BEHEADING_CLEAVER = MOD_BEHEADING + "_cleaver";
	public static String MOD_LUCK = "luck";

	public Modifier carbonModifier;
	public Modifier diamondAndAppleModifier;
	public Modifier diamondAndGoldModifier;
	public Modifier soulboundModifier;
	public Modifier juicedModifier;
	public static Modifier solidModifier;
	//public Modifier unbendingModifier;

	ItemStack diamond = new ItemStack(Items.DIAMOND, 1);
	ItemStack diamondBlock = new ItemStack(Blocks.DIAMOND_BLOCK, 1);
	ItemStack goldBlock = new ItemStack(Blocks.GOLD_BLOCK, 1);
	ItemStack notchApple = new ItemStack(Items.GOLDEN_APPLE, 1, 1);
	ItemStack carbonBlock = new ItemStack(ModBlocks.CARBON_BLOCK, 1);
	ItemStack emeraldRod = new ItemStack(ModItems.CARBONROD_EMERALD, 1);
	ItemStack redStoneRod = new ItemStack(ModItems.CARBONROD_REDSTONE, 1);
	ItemStack lapisRod = new ItemStack(ModItems.CARBONROD_LAPIS, 1);
	ItemStack goldRod = new ItemStack(ModItems.CARBONROD_GOLD, 1);
	ItemStack diamondRod = new ItemStack(ModItems.CARBONROD_DIAMOND, 1);
	ItemStack rawCarbon = new ItemStack(ModItems.RAW_CARBON, 1);
	ItemStack battery = new ItemStack(ModItems.ADVANCED_BATTERY);

	public TiCModifiers() {

		carbonModifier = new TiCExtraModifier("carbonmod");
		carbonModifier.addRecipeMatch(new RecipeMatch.ItemCombination(1, carbonBlock, emeraldRod));
		diamondAndAppleModifier = new TiCExtraModifier("diamondandapplemod");
		diamondAndAppleModifier.addRecipeMatch(new RecipeMatch.ItemCombination(1, diamondBlock, notchApple));
		diamondAndGoldModifier = new TiCExtraModifier("diamondandgoldmod");
		diamondAndGoldModifier.addRecipeMatch(new RecipeMatch.ItemCombination(1, diamond, goldBlock));
		soulboundModifier = new ModSoulbound();
		soulboundModifier.addRecipeMatch(new RecipeMatch.ItemCombination(1, redStoneRod, lapisRod, goldRod, diamondRod, emeraldRod));
		soulboundModifier.addItem(Items.NETHER_STAR);
		solidModifier = new ModSolid();
		solidModifier.addRecipeMatch(new RecipeMatch.ItemCombination(1, carbonBlock, carbonBlock, carbonBlock, carbonBlock, carbonBlock));
		//juicedModifier = new ModBattery();
		//juicedModifier.addRecipeMatch(new RecipeMatch.Item(battery, 1));

		//unbendingModifier = new TiCUnbendingModifier("unbendingmod");
		//unbendingModifier.addItem(rawCarbon.getItem());
		if (FMLCommonHandler.instance().getSide().isClient()) {
			ModelRegisterUtil.registerModifierModel(solidModifier, TiCModel.SOLID);
		}
	}

}
