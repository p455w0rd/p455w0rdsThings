package p455w0rd.p455w0rdsthings.integration.tinkers;

import static slimeknights.tconstruct.library.materials.MaterialTypes.HEAD;
import static slimeknights.tconstruct.tools.TinkerTraits.hellish;
import static slimeknights.tconstruct.tools.TinkerTraits.magnetic2;

import mcjty.theoneprobe.apiimpl.providers.HarvestInfoTools;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModGlobals.HarvestLevel;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.integration.TiC.TiCColor;
import p455w0rd.p455w0rdsthings.integration.TiC.TiCTexture;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.library.client.MaterialRenderInfo;
import slimeknights.tconstruct.library.materials.ArrowShaftMaterialStats;
import slimeknights.tconstruct.library.materials.BowMaterialStats;
import slimeknights.tconstruct.library.materials.ExtraMaterialStats;
import slimeknights.tconstruct.library.materials.HandleMaterialStats;
import slimeknights.tconstruct.library.materials.HeadMaterialStats;
import slimeknights.tconstruct.library.materials.Material;
import slimeknights.tconstruct.library.utils.HarvestLevels;

public final class TiCMaterials {

	public static final Material CARBON = new Material("carbon", TiCColor.CARBON).addTrait(hellish).addTrait(magnetic2, HEAD);

	@SideOnly(Side.CLIENT)
	public static void registerMaterialRendering() {
		CARBON.setRenderInfo(TiCColor.CARBON).setTextureSuffix("contrast");
		CARBON.setRenderInfo(new MaterialRenderInfo.Default(TiCColor.CARBON));
		CARBON.setRenderInfo(new MaterialRenderInfo.BlockTexture(TiCTexture.CARBON));
	}

	public void setupMaterials() {
		HarvestLevels.harvestLevelNames.put(5, TextFormatting.GREEN + "Carbon");
		CARBON.setShard(ModItems.RAW_CARBON);
		CARBON.addItem(new ItemStack(ModItems.RAW_CARBON), 1, Material.VALUE_Shard);
		CARBON.addItem(new ItemStack(ModItems.CARBON_INGOT), 1, Material.VALUE_Ingot);
		CARBON.addItem(new ItemStack(ModBlocks.CARBON_BLOCK), 1, Material.VALUE_Block);
		float arrowModifier = 10.0f;
		TinkerRegistry.addMaterialStats(CARBON, new HeadMaterialStats(ConfigOptions.TIC_BASE_DURABILITY, ConfigOptions.TIC_MINING_SPEED, ConfigOptions.TIC_BASE_ATTACK, HarvestLevel.CARBON), new HandleMaterialStats(1.00f, ConfigOptions.TIC_HANDLE_DURABILITY), new ExtraMaterialStats(ConfigOptions.TIC_EXTRA_DURABILITY), new BowMaterialStats(ConfigOptions.TIC_BOW_DRAW_SPEED, ConfigOptions.TIC_BOW_RANGE, ConfigOptions.TIC_BOW_EXTRA_DAMAGE), new ArrowShaftMaterialStats(arrowModifier, ConfigOptions.TIC_EXTRA_BOLT_AMMO));
		if (Mods.TOP.isLoaded()) {
			ReflectionHelper.setPrivateValue(HarvestInfoTools.class, new HarvestInfoTools(), getHarvestLevelsAsArray(), "harvestLevels");
		}
	}

	private String[] getHarvestLevelsAsArray() {
		String[] list = new String[HarvestLevels.harvestLevelNames.values().size()];
		String[] harvestNames = HarvestLevels.harvestLevelNames.values().toArray(list);
		for (int i = 0; i < HarvestLevels.harvestLevelNames.values().size(); i++) {
			list[i] = harvestNames[i] + "" + TextFormatting.GOLD;
		}
		return list;
	}

}
