package p455w0rd.p455w0rdsthings.integration.tinkers;

import java.util.Random;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.integration.TiC.TiCColor;
import p455w0rd.p455w0rdsthings.integration.TiC.TiCModifierName;
import slimeknights.tconstruct.library.modifiers.ModifierAspect;
import slimeknights.tconstruct.library.modifiers.ModifierTrait;

/**
 * @author p455w0rd
 *
 */
public class ModSolid extends ModifierTrait {

	public ModSolid() {
		super(TiCModifierName.SOLID, TiCColor.CARBON);
		addAspects(new ModifierAspect.SingleAspect(this));
	}

	@Override
	public int onToolDamage(ItemStack tool, int damage, int newDamage, EntityLivingBase entity) {
		float r = new Random().nextFloat();
		if (r < 0.99f) {
			return 0;
		}
		else {
			return super.onToolDamage(tool, damage, newDamage, entity);
		}
	}
}