package p455w0rd.p455w0rdsthings.integration.tinkers;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;

/**
 * @author p455w0rd
 *
 */
public class EntityAIAttackRangedShuriken extends EntityAIBase {

	private final EntitySkeleton entity;
	private final double moveSpeedAmp;
	private int attackCooldown;
	private final float maxAttackDistance;
	private int attackTime = -1;
	private int seeTime;
	private boolean strafingClockwise;
	private boolean strafingBackwards;
	private int strafingTime = -1;

	public EntityAIAttackRangedShuriken(EntitySkeleton skeleton, double speedAmplifier, int delay, float maxDistance) {
		entity = skeleton;
		moveSpeedAmp = speedAmplifier;
		attackCooldown = delay;
		maxAttackDistance = maxDistance * maxDistance;
		setMutexBits(3);
	}

	public void setAttackCooldown(int p_189428_1_) {
		attackCooldown = p_189428_1_;
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute() {
		return entity.getAttackTarget() == null ? false : isBowInMainhand();
	}

	protected boolean isBowInMainhand() {
		return true;
		//return skele.getHeldItemMainhand() != null && ItemStack.areItemsEqualIgnoreDurability(skele.getHeldItemMainhand(), TiC.getShuriken()) && !ToolHelper.isBroken(skele.getHeldItemMainhand());
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting() {
		return (shouldExecute() || !entity.getNavigator().noPath()) && isBowInMainhand();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting() {
		super.startExecuting();
		entity.setSwingingArms(true);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask() {
		super.resetTask();
		entity.setSwingingArms(false);
		seeTime = 0;
		attackTime = -1;
		entity.resetActiveHand();
	}

	/**
	 * Updates the task
	 */
	@Override
	public void updateTask() {
		EntityLivingBase entitylivingbase = entity.getAttackTarget();

		if (entitylivingbase != null) {
			double d0 = entity.getDistanceSq(entitylivingbase.posX, entitylivingbase.getEntityBoundingBox().minY, entitylivingbase.posZ);
			boolean flag = entity.getEntitySenses().canSee(entitylivingbase);
			boolean flag1 = seeTime > 0;

			if (flag != flag1) {
				seeTime = 0;
			}

			if (flag) {
				++seeTime;
			}
			else {
				--seeTime;
			}

			if (d0 <= maxAttackDistance && seeTime >= 20) {
				entity.getNavigator().clearPathEntity();
				++strafingTime;
			}
			else {
				entity.getNavigator().tryMoveToEntityLiving(entitylivingbase, moveSpeedAmp);
				strafingTime = -1;
			}

			if (strafingTime >= 20) {
				if (entity.getRNG().nextFloat() < 0.3D) {
					strafingClockwise = !strafingClockwise;
				}

				if (entity.getRNG().nextFloat() < 0.3D) {
					strafingBackwards = !strafingBackwards;
				}

				strafingTime = 0;
			}

			if (strafingTime > -1) {
				if (d0 > maxAttackDistance * 0.75F) {
					strafingBackwards = false;
				}
				else if (d0 < maxAttackDistance * 0.25F) {
					strafingBackwards = true;
				}

				entity.getMoveHelper().strafe(strafingBackwards ? -0.5F : 0.5F, strafingClockwise ? 0.5F : -0.5F);
				entity.faceEntity(entitylivingbase, 30.0F, 30.0F);
			}
			else {
				entity.getLookHelper().setLookPositionWithEntity(entitylivingbase, 30.0F, 30.0F);
			}

			// if (this.entity.isHandActive())
			// {
			if (!flag && seeTime < -60) {
				entity.resetActiveHand();
			}
			else if (flag) {
				//int i = entity.getItemInUseMaxCount();

				//if (i >= 20) {
				entity.resetActiveHand();
				if (attackTime <= 0) {
					//entity.attackEntityWithRangedAttack(entitylivingbase, ItemBow.getArrowVelocity(20));
					entity.getHeldItemMainhand().getItem().onItemRightClick(entity.getHeldItemMainhand(), entity.getEntityWorld(), (EntityPlayer) null, EnumHand.MAIN_HAND);
					attackTime = attackCooldown;
				}
				else {
					--attackTime;
				}
				//}
			}
			// }
			// else if (--this.attackTime <= 0 && this.seeTime >= -60)
			// {
			entity.setActiveHand(EnumHand.MAIN_HAND);
			// }
		}
	}
}
