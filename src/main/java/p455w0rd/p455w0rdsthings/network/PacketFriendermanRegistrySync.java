package p455w0rd.p455w0rdsthings.network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
public class PacketFriendermanRegistrySync implements IMessage {

	Map<Integer, UUID> registry = Maps.newHashMap();

	public PacketFriendermanRegistrySync() {
	}

	public PacketFriendermanRegistrySync(Map<Integer, UUID> registry) {
		this.registry = registry;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void fromBytes(ByteBuf buf) {
		short len = buf.readShort();
		byte[] compressedBody = new byte[len];

		for (short i = 0; i < len; i++) {
			compressedBody[i] = buf.readByte();
		}

		try {
			ObjectInputStream obj = new ObjectInputStream(new GZIPInputStream(new ByteArrayInputStream(compressedBody)));
			registry = (Map<Integer, UUID>) obj.readObject();
			obj.close();
		}
		catch (Exception e) {
			Throwables.propagate(e);
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteArrayOutputStream obj = new ByteArrayOutputStream();

		try {
			GZIPOutputStream gzip = new GZIPOutputStream(obj);
			ObjectOutputStream objStream = new ObjectOutputStream(gzip);
			objStream.writeObject(registry);
			objStream.close();
		}
		catch (IOException e) {
			Throwables.propagate(e);
		}
		buf.writeShort(obj.size());
		buf.writeBytes(obj.toByteArray());
	}

	public static class Handler implements IMessageHandler<PacketFriendermanRegistrySync, IMessage> {
		@Override
		public IMessage onMessage(PacketFriendermanRegistrySync message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketFriendermanRegistrySync message, MessageContext ctx) {
			if (ctx.getClientHandler() != null) {
				ModRegistries.setTamedFriendermanRegistry(message.registry);
			}
		}
	}

}
