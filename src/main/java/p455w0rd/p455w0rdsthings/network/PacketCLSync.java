package p455w0rd.p455w0rdsthings.network;

import java.util.ArrayList;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rdslib.capabilities.CapabilityChunkLoader;
import p455w0rdslib.capabilities.CapabilityChunkLoader.ICLEntityHandler;
import p455w0rdslib.util.NetworkUtils;

public class PacketCLSync implements IMessage {

	public static ArrayList<BlockPos> blockList;

	public PacketCLSync() {
	}

	public PacketCLSync(ArrayList<BlockPos> list) {
		blockList = list;
	}

	public static class Handler implements IMessageHandler<PacketCLSync, IMessage> {
		@Override
		public IMessage onMessage(PacketCLSync message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketCLSync message, MessageContext ctx) {
			if (ctx.getServerHandler().playerEntity != null) {
				EntityPlayer player = ctx.getServerHandler().playerEntity;
				final ICLEntityHandler inst = player.getCapability(CapabilityChunkLoader.CAPABILITY_CHUNKLOADER_ENTITY, null);
				CapabilityChunkLoader.CAPABILITY_CHUNKLOADER_ENTITY.writeNBT(inst, null);
			}
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		NetworkUtils.readBlockPosList(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		NetworkUtils.writeBlockPosList(blockList, buf);
	}
}
