package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.init.ModNetworking;

/**
 * @author p455w0rd
 *
 */
public class PacketSetEntityChargedS implements IMessage {

	public long pos;
	public int dimID;
	//public EntityLivingBase entity;
	public int entityID;

	public PacketSetEntityChargedS() {
	}

	public PacketSetEntityChargedS(long posIn, int dimIDIn, int entityIDIn) {
		pos = posIn;
		//entity = entityIn;
		dimID = dimIDIn;
		entityID = entityIDIn;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		/*
		int len = buf.readInt();
		byte[] compressedBody = new byte[len];
		
		for (short i = 0; i < len; i++) {
			compressedBody[i] = buf.readByte();
		}
		
		try {
			ObjectInputStream obj = new ObjectInputStream(new GZIPInputStream(new ByteArrayInputStream(compressedBody)));
			entity = (EntityLivingBase) obj.readObject();
			obj.close();
		}
		catch (Exception e) {
			//Throwables.propagate(e);
		}
		*/
		pos = buf.readLong();
		dimID = buf.readInt();
		entityID = buf.readInt();

	}

	@Override
	public void toBytes(ByteBuf buf) {
		/*
		ByteArrayOutputStream obj = new ByteArrayOutputStream();
		
		try {
			GZIPOutputStream gzip = new GZIPOutputStream(obj);
			ObjectOutputStream objStream = new ObjectOutputStream(gzip);
			objStream.writeObject(entity);
			objStream.close();
		}
		catch (IOException e) {
			//Throwables.propagate(e);
		}
		buf.writeInt(obj.size());
		buf.writeBytes(obj.toByteArray());
		*/
		buf.writeLong(pos);
		buf.writeInt(dimID);
		buf.writeInt(entityID);
	}

	public static class Handler implements IMessageHandler<PacketSetEntityChargedS, IMessage> {
		@Override
		public IMessage onMessage(PacketSetEntityChargedS message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetEntityChargedS message, MessageContext ctx) {
			BlockPos pos = BlockPos.fromLong(message.pos);
			ModNetworking.INSTANCE.sendToAllAround(new PacketSetEntityChargedC(message.entityID), new TargetPoint(message.dimID, pos.getX(), pos.getY(), pos.getZ(), 32));
		}
	}

}
