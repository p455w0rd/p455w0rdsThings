package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rdslib.util.FluidUtils;

/**
 * @author p455w0rd
 *
 */
public class PacketFluidFill implements IMessage {

	long tilePos;
	NBTTagCompound fluidNBT;
	int dimension, fluidVolume;
	EnumFacing facing;
	String fluidName;

	public PacketFluidFill() {

	}

	public PacketFluidFill(TileEntity fluidTile, FluidStack stack, int dim, EnumFacing facingIn) {
		tilePos = fluidTile.getPos().toLong();
		fluidNBT = stack.tag;
		dimension = dim;
		facing = facingIn;
		Fluid fluid = stack.getFluid();
		fluidName = fluid.getName();
		fluidVolume = stack.amount;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		tilePos = buf.readLong();
		fluidNBT = ByteBufUtils.readTag(buf);
		dimension = buf.readInt();
		facing = EnumFacing.values()[buf.readInt()];
		fluidName = ByteBufUtils.readUTF8String(buf);
		fluidVolume = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(tilePos);
		ByteBufUtils.writeTag(buf, fluidNBT);
		buf.writeInt(dimension);
		buf.writeInt(facing.ordinal());
		ByteBufUtils.writeUTF8String(buf, fluidName);
		buf.writeInt(fluidVolume);
	}

	public static class Handler implements IMessageHandler<PacketFluidFill, IMessage> {
		@Override
		public IMessage onMessage(PacketFluidFill message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketFluidFill message, MessageContext ctx) {
			World world = DimensionManager.getWorld(message.dimension);
			if (world != null) {
				BlockPos pos = BlockPos.fromLong(message.tilePos);

				FluidStack stack;
				if (message.fluidNBT != null) {
					stack = FluidStack.loadFluidStackFromNBT(message.fluidNBT);
				}
				else {
					Fluid fluid = FluidRegistry.getFluid(message.fluidName);
					stack = new FluidStack(fluid, message.fluidVolume);
				}
				FluidUtils.fill(world.getTileEntity(pos), stack, stack.amount, message.facing);
			}
		}
	}

}
