package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rdslib.util.PotionUtils;

public class PacketSetArmor implements IMessage {

	public static int armorPart = -1;

	public PacketSetArmor() {
		this(0);
	}

	public PacketSetArmor(int which) {
		armorPart = which;
	}

	public static class Handler implements IMessageHandler<PacketSetArmor, IMessage> {
		@Override
		public IMessage onMessage(PacketSetArmor message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetArmor message, MessageContext ctx) {
			if (armorPart >= 0) {
				//EntityPlayer player = P455w0rdsThings.PROXY.getPlayer(ctx);
				EntityPlayer player = P455w0rdsThings.PROXY.getPlayer();
				switch (armorPart) {
				case 0:
					ModGlobals.CARBONHELMET_ISEQUIPPED = false;
					if (PotionUtils.isPotionActive(player, MobEffects.NIGHT_VISION)) {
						PotionUtils.clearPotionEffect(player, MobEffects.NIGHT_VISION, 1);
					}
					break;
				case 1:
					ModGlobals.CARBONCHESTPLATE_ISEQUIPPED = false;
					ModGlobals.CARBONJETPACK_ISEQUIPPED = false;
					player.capabilities.allowFlying = false;
					player.capabilities.isFlying = false;
					break;
				case 2:
					ModGlobals.CARBONLEGGINGS_ISEQUIPPED = false;
					if (PotionUtils.isPotionActive(player, MobEffects.SPEED)) {
						PotionUtils.clearPotionEffect(player, MobEffects.SPEED, 0);
					}
					break;
				case 3:
					ModGlobals.CARBONBOOTS_ISEQUIPPED = false;
					if (player.stepHeight != 0.6F) {
						player.stepHeight = 0.6F;
					}
					break;
				default:
				case -1:
					break;
				}
			}
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		armorPart = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(armorPart);
	}
}
