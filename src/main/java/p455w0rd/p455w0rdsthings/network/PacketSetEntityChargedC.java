package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
public class PacketSetEntityChargedC implements IMessage {

	//public EntityLivingBase entity;
	public int entityID;

	public PacketSetEntityChargedC() {
	}

	public PacketSetEntityChargedC(int entityIDIn) {
		entityID = entityIDIn;
	}

	/*
		public PacketSetEntityChargedC(EntityLivingBase entityIn) {
			entity = entityIn;
		}
	*/
	@Override
	public void fromBytes(ByteBuf buf) {
		/*
		short len = buf.readShort();
		byte[] compressedBody = new byte[len];

		for (short i = 0; i < len; i++) {
			compressedBody[i] = buf.readByte();
		}

		try {
			ObjectInputStream obj = new ObjectInputStream(new GZIPInputStream(new ByteArrayInputStream(compressedBody)));
			entity = (EntityLivingBase) obj.readObject();
			obj.close();
		}
		catch (Exception e) {
			//Throwables.propagate(e);
		}
		*/
		entityID = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		/*
		ByteArrayOutputStream obj = new ByteArrayOutputStream();

		try {
			GZIPOutputStream gzip = new GZIPOutputStream(obj);
			ObjectOutputStream objStream = new ObjectOutputStream(gzip);
			objStream.writeObject(entity);
			objStream.close();
		}
		catch (IOException e) {
			//Throwables.propagate(e);
		}
		buf.writeShort(obj.size());
		buf.writeBytes(obj.toByteArray());
		*/
		buf.writeInt(entityID);
	}

	public static class Handler implements IMessageHandler<PacketSetEntityChargedC, IMessage> {
		@Override
		public IMessage onMessage(PacketSetEntityChargedC message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetEntityChargedC message, MessageContext ctx) {
			ModRegistries.setCharged((EntityLivingBase) FMLClientHandler.instance().getWorldClient().getEntityByID(message.entityID));
		}
	}

}
