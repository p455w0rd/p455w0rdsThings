package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.api.IAccessible;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.AccessMode;

/**
 * @author p455w0rd
 *
 */
public class PacketSetAccessMode implements IMessage {

	long pos;
	boolean mode;
	int dimension;

	public PacketSetAccessMode() {
	}

	public PacketSetAccessMode(BlockPos posIn, boolean value, int dim) {
		pos = posIn.toLong();
		mode = value;
		dimension = dim;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		pos = buf.readLong();
		mode = buf.readBoolean();
		dimension = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(pos);
		buf.writeBoolean(mode);
		buf.writeInt(dimension);
	}

	public static class Handler implements IMessageHandler<PacketSetAccessMode, IMessage> {
		@Override
		public IMessage onMessage(PacketSetAccessMode message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetAccessMode message, MessageContext ctx) {
			World world = DimensionManager.getWorld(message.dimension);
			if (world != null) {
				TileEntity tile = world.getTileEntity(BlockPos.fromLong(message.pos));
				if (tile != null && tile instanceof IAccessible) {
					IAccessible accessibleTile = (IAccessible) tile;
					accessibleTile.setAccessMode(message.mode ? AccessMode.PUBLIC : AccessMode.PRIVATE);
				}
			}
		}
	}
}
