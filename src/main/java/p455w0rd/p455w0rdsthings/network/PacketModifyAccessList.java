package p455w0rd.p455w0rdsthings.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.api.IAccessible;

/**
 * @author p455w0rd
 *
 */
public class PacketModifyAccessList implements IMessage {

	long pos;
	boolean add;
	int dimension;
	String playerID;

	public PacketModifyAccessList() {
	}

	public PacketModifyAccessList(BlockPos posIn, boolean doAdd, int dim, UUID ID) {
		pos = posIn.toLong();
		add = doAdd;
		dimension = dim;
		playerID = ID.toString();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		pos = buf.readLong();
		add = buf.readBoolean();
		dimension = buf.readInt();
		playerID = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(pos);
		buf.writeBoolean(add);
		buf.writeInt(dimension);
		ByteBufUtils.writeUTF8String(buf, playerID);
	}

	public static class Handler implements IMessageHandler<PacketModifyAccessList, IMessage> {
		@Override
		public IMessage onMessage(PacketModifyAccessList message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketModifyAccessList message, MessageContext ctx) {
			World world = DimensionManager.getWorld(message.dimension);
			if (world != null) {
				TileEntity tile = world.getTileEntity(BlockPos.fromLong(message.pos));
				if (tile != null && tile instanceof IAccessible) {
					IAccessible accessibleTile = (IAccessible) tile;
					if (message.add) {
						accessibleTile.addPlayerToAccessList(UUID.fromString(message.playerID));
					}
					else {
						accessibleTile.removePlayerFromAccessList(UUID.fromString(message.playerID));
					}
				}
			}
		}
	}

}
