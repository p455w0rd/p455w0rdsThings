package p455w0rd.p455w0rdsthings.network;

import javax.annotation.Nonnull;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.container.ContainerDankNull;
import p455w0rd.p455w0rdsthings.inventory.InventoryDankNull;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rd.p455w0rdsthings.util.NullableList;

/**
 * @author p455w0rd
 *
 */
public class PacketSyncDankNull implements IMessage {

	private NullableList<ItemStack> itemStacks;
	private int[] stackSizes;

	@Override
	public void fromBytes(ByteBuf buf) {
		int i = buf.readShort();
		itemStacks = NullableList.<ItemStack>withSize(i);
		stackSizes = new int[i];
		for (int j = 0; j < i; ++j) {
			itemStacks.set(j, ByteBufUtils.readItemStack(buf));
		}
		for (int j = 0; j < i; ++j) {
			stackSizes[j] = buf.readInt();
		}

	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeShort(itemStacks.size());
		for (ItemStack itemstack : itemStacks) {
			ByteBufUtils.writeItemStack(buf, itemstack);
		}
		for (int i = 0; i < itemStacks.size(); i++) {
			buf.writeInt(stackSizes[i]);
		}
	}

	public PacketSyncDankNull() {
	}

	public PacketSyncDankNull(@Nonnull InventoryDankNull inv) {
		itemStacks = NullableList.<ItemStack>withSize(inv.getSizeInventory());
		stackSizes = new int[inv.getSizeInventory()];
		for (int i = 0; i < itemStacks.size(); i++) {
			ItemStack itemstack = inv.getStackInSlot(i);
			if (itemstack != null) {
				itemStacks.set(i, itemstack.copy());
				stackSizes[i] = itemstack.stackSize;
			}
			else {
				itemStacks.set(i, null);
				stackSizes[i] = 0;
			}
		}
	}

	public static class Handler implements IMessageHandler<PacketSyncDankNull, IMessage> {
		@Override
		public IMessage onMessage(PacketSyncDankNull message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSyncDankNull message, MessageContext ctx) {
			EntityPlayer player = P455w0rdsThings.PROXY.getPlayer();
			if (player != null && player.getHeldItemMainhand() != null && DankNullUtils.isDankNull(player.getHeldItemMainhand()) && player.openContainer instanceof ContainerDankNull) {
				ContainerDankNull container = (ContainerDankNull) player.openContainer;
				//InventoryDankNull inv = container.getDankNullInventory();
				for (int i = 0; i < message.itemStacks.size(); i++) {
					if (message.itemStacks.get(i) != null) {
						message.itemStacks.get(i).stackSize = message.stackSizes[i];
						//inv.setInventorySlotContents(i, message.itemStacks.get(i));
						container.putStackInSlot(i + 36, message.itemStacks.get(i));
					}
				}
			}
		}
	}

}