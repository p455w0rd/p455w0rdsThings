package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.guide.PlayerData;
import p455w0rd.p455w0rdsthings.guide.WorldData;

/**
 * @author p455w0rd
 *
 */
public class PacketPlayerDataS implements IMessage {

	NBTTagCompound playerData;

	public PacketPlayerDataS() {
	}

	public PacketPlayerDataS(NBTTagCompound data) {
		playerData = data;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		PacketBuffer buffer = new PacketBuffer(buf);
		try {
			playerData = buffer.readCompoundTag();
		}
		catch (Exception e) {
			// NOOP
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		PacketBuffer buffer = new PacketBuffer(buf);
		buffer.writeCompoundTag(playerData);
	}

	public static class Handler implements IMessageHandler<PacketPlayerDataS, IMessage> {
		@Override
		public IMessage onMessage(PacketPlayerDataS message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketPlayerDataS message, MessageContext ctx) {
			if (message.playerData != null) {
				World world = DimensionManager.getWorld(message.playerData.getInteger("World"));
				EntityPlayer player = world.getPlayerEntityByUUID(message.playerData.getUniqueId("UUID"));
				if (player != null) {
					PlayerData.PlayerSave data = PlayerData.getDataFromPlayer(player);
					data.loadBookmarks(message.playerData.getTagList("Bookmarks", 8));
					data.didBookTutorial = message.playerData.getBoolean("DidBookTutorial");
					WorldData.get(world).markDirty();
				}
			}
		}
	}

}
