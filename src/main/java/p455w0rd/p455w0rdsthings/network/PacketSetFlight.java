package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * @author p455w0rd
 *
 */
public class PacketSetFlight implements IMessage {

	private static boolean flightValue;

	public PacketSetFlight() {
	}

	public PacketSetFlight(boolean value) {
		flightValue = value;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		flightValue = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(flightValue);
	}

	public static class Handler implements IMessageHandler<PacketSetFlight, IMessage> {
		@Override
		public IMessage onMessage(PacketSetFlight message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetFlight message, MessageContext ctx) {
			//EntityPlayer player = P455w0rdsThings.PROXY.getPlayer(ctx);
			//ModRegistries.setFlight(player, flightValue);
		}
	}

}
