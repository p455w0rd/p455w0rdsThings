package p455w0rd.p455w0rdsthings.network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.base.Throwables;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.integration.JEI;

public class PacketConfigSync implements IMessage {

	public Map<String, Object> values;

	public PacketConfigSync() {
	}

	public PacketConfigSync(Map<String, Object> valuesIn) {
		values = valuesIn;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void fromBytes(ByteBuf buf) {
		short len = buf.readShort();
		byte[] compressedBody = new byte[len];

		for (short i = 0; i < len; i++) {
			compressedBody[i] = buf.readByte();
		}

		try {
			ObjectInputStream obj = new ObjectInputStream(new GZIPInputStream(new ByteArrayInputStream(compressedBody)));
			values = (Map<String, Object>) obj.readObject();
			obj.close();
		}
		catch (Exception e) {
			Throwables.propagate(e);
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteArrayOutputStream obj = new ByteArrayOutputStream();

		try {
			GZIPOutputStream gzip = new GZIPOutputStream(obj);
			ObjectOutputStream objStream = new ObjectOutputStream(gzip);
			objStream.writeObject(values);
			objStream.close();
		}
		catch (IOException e) {
			Throwables.propagate(e);
		}
		buf.writeShort(obj.size());
		buf.writeBytes(obj.toByteArray());
	}

	public static class Handler implements IMessageHandler<PacketConfigSync, IMessage> {
		@Override
		public IMessage onMessage(PacketConfigSync message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketConfigSync message, MessageContext ctx) {
			if (ctx.getClientHandler() != null) {
				ModLogger.info("Server Configs Synced Successfully");
				//ConfigOptions.MAX_CHUNKLOADERS_PER_PLAYER = (int) message.values.get("maxChunkLoaders");
				//ConfigOptions.CHUNKLOADER_MAXCHUNK_RADIUS = (int) message.values.get("maxChunkLoaderRadius");
				//ConfigOptions.ENABLE_CHUNK_LOADER = (boolean) message.values.get("enableChunkLoader");
				ConfigOptions.DISABLE_EMC = (boolean) message.values.get("disableEMC");
				ConfigOptions.MAGNET_RADIUS = (double) message.values.get("magnetRadius");
				ConfigOptions.ENABLE_WITHERLESS_UPGRADE = (boolean) message.values.get("witherlessUpgrade");
				ConfigOptions.ENABLE_LAVAWALKER_UPRGADE = (boolean) message.values.get("lavaWalkerUpgrade");
				ConfigOptions.DISABLE_VANILLA_RECIPES = (boolean) message.values.get("disableVanillaRecipes");
				ConfigOptions.ENABLE_SILKTOUCH_CHEST_HARVEST = (boolean) message.values.get("silkHarvestChests");
				if (Mods.JEI.isLoaded()) {
					JEI.handleItemBlacklisting(new ItemStack(ModItems.ARMOR_UPGRADE_UNWITHERING), !ConfigOptions.ENABLE_WITHERLESS_UPGRADE);
					JEI.handleItemBlacklisting(new ItemStack(ModItems.ARMOR_UPGRADE_LAVAWALKER), !ConfigOptions.ENABLE_LAVAWALKER_UPRGADE);
				}
				ConfigOptions.TIC_BOW_EXTRA_DAMAGE = (int) message.values.get("tic_bowExtraDamage");
				ConfigOptions.TIC_BOW_RANGE = (float) message.values.get("tic_bowRange");
				ConfigOptions.TIC_MINING_SPEED = (float) message.values.get("tic_miningSpeed");
				ConfigOptions.ENABLE_TIC_FLESH_TO_LEATHER_RECIPE = (boolean) message.values.get("tic_fleshToLeather");
				ConfigOptions.TIC_LEATHER_DRY_TIME = (int) message.values.get("tic_leatherDrytime");
				ConfigOptions.TIC_EXTRA_BOLT_AMMO = (int) message.values.get("tic_extraBoltAmmo");
				ConfigOptions.TIC_BOW_DRAW_SPEED = (float) message.values.get("tic_bowDrawSpeed");
				ConfigOptions.TIC_BASE_DURABILITY = (int) message.values.get("tic_baseDurability");
				ConfigOptions.TIC_BASE_ATTACK = (float) message.values.get("tic_baseAttack");
				ConfigOptions.TIC_HANDLE_DURABILITY = (int) message.values.get("tic_handleDurability");
				ConfigOptions.TIC_EXTRA_DURABILITY = (int) message.values.get("tic_extraDurability");
			}
		}
	}
}
