package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.guide.PlayerData;
import p455w0rd.p455w0rdsthings.init.ModLogger;

/**
 * @author p455w0rd
 *
 */
public class PacketPlayerDataC implements IMessage {

	NBTTagCompound playerData;

	public PacketPlayerDataC() {
	}

	public PacketPlayerDataC(NBTTagCompound data) {
		playerData = data;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		PacketBuffer buffer = new PacketBuffer(buf);
		try {
			playerData = buffer.readCompoundTag();
		}
		catch (Exception e) {
			// NOOP
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		PacketBuffer buffer = new PacketBuffer(buf);
		buffer.writeCompoundTag(playerData);
	}

	public static class Handler implements IMessageHandler<PacketPlayerDataC, IMessage> {
		@Override
		public IMessage onMessage(PacketPlayerDataC message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketPlayerDataC message, MessageContext ctx) {
			if (message.playerData != null) {
				NBTTagCompound data = message.playerData.getCompoundTag("Data");
				EntityPlayer player = P455w0rdsThings.PROXY.getPlayer();
				if (player != null) {
					PlayerData.getDataFromPlayer(player).readFromNBT(data, false);
					ModLogger.info("Successfully synced player data to client");
				}
				else {
					ModLogger.warn("Error syncing playerdata to client");
				}
			}
		}
	}

}