package p455w0rd.p455w0rdsthings.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
public class PacketSetNightVision implements IMessage {

	private boolean nvValue;
	private String uuid;

	public PacketSetNightVision() {
	}

	public PacketSetNightVision(boolean value, String uuidIn) {
		nvValue = value;
		uuid = uuidIn;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		nvValue = buf.readBoolean();
		uuid = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(nvValue);
		ByteBufUtils.writeUTF8String(buf, uuid);
	}

	public static class Handler implements IMessageHandler<PacketSetNightVision, IMessage> {
		@Override
		public IMessage onMessage(PacketSetNightVision message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetNightVision message, MessageContext ctx) {
			EntityPlayer player = null;
			for (WorldServer w : DimensionManager.getWorlds()) {
				for (EntityPlayer p : w.playerEntities) {
					if (p.getUniqueID().equals(UUID.fromString(message.uuid))) {
						player = p;
					}
				}
			}
			if (player != null) {
				ModRegistries.setNV(player, message.nvValue);
			}
		}
	}

}
