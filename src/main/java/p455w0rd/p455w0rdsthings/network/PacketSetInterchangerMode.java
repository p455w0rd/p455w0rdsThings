package p455w0rd.p455w0rdsthings.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import p455w0rd.p455w0rdsthings.api.IInterchanger;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.EnergyMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.FluidMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.ItemMode;

/**
 * @author p455w0rd
 *
 */
public class PacketSetInterchangerMode implements IMessage {

	long pos;
	int mode, modeType;
	int dimension;

	public PacketSetInterchangerMode() {
	}

	public PacketSetInterchangerMode(BlockPos posIn, int type, int value, int dim) {
		pos = posIn.toLong();
		modeType = type;
		mode = value;
		dimension = dim;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		pos = buf.readLong();
		modeType = buf.readInt();
		mode = buf.readInt();
		dimension = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(pos);
		buf.writeInt(modeType);
		buf.writeInt(mode);
		buf.writeInt(dimension);
	}

	public static class Handler implements IMessageHandler<PacketSetInterchangerMode, IMessage> {
		@Override
		public IMessage onMessage(PacketSetInterchangerMode message, MessageContext ctx) {
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
				handle(message, ctx);
			});
			return null;
		}

		private void handle(PacketSetInterchangerMode message, MessageContext ctx) {
			World world = DimensionManager.getWorld(message.dimension);
			if (world != null) {
				TileEntity tile = world.getTileEntity(BlockPos.fromLong(message.pos));
				if (tile != null && tile instanceof IInterchanger) {
					IInterchanger interchanger = (IInterchanger) tile;
					switch (message.modeType) {
					case 0:
						interchanger.setEnergyMode(EnergyMode.values()[message.mode]);
						break;
					case 1:
						interchanger.setItemMode(ItemMode.values()[message.mode]);
						break;
					case 2:
						interchanger.setFluidMode(FluidMode.values()[message.mode]);
						break;
					}
				}
			}
		}
	}
}