/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import p455w0rdslib.util.InventoryUtils;

/**
 * @author p455w0rd
 *
 */
public class ContainerTrashCan extends Container {

	IItemHandler playerInventory;
	IItemHandler trashInventory;
	IFluidHandler fluidInventory;

	public ContainerTrashCan(IItemHandler playerInv, IItemHandler trashInv, IFluidHandler fluidInv, EntityPlayer player) {
		playerInventory = playerInv;
		trashInventory = trashInv;
		fluidInventory = fluidInv;

		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new SlotItemHandler(playerInventory, i, i * 20 + (9 + i), 90 + (3 - 1) + (3 * 20 + 6)));
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new SlotItemHandler(playerInventory, j + i * 9 + 9, j * 20 + (9 + j), 149 + (3 - 1) + i - (6 - 3) * 20 + i * 20));
			}
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new SlotItemHandler(trashInventory, j + i * 9, j * 20 + (9 + j), 19 + i + i * 20));
			}
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		Slot clickSlot = inventorySlots.get(index);
		if (clickSlot.getHasStack()) {
			if (!isTrashSlot(index)) {
				if (InventoryUtils.getNextAvailableSlot(inventorySlots) == -1) {
					if (!InventoryUtils.moveStackWithinInventory(clickSlot.getStack(), index, inventorySlots)) {
						return null;
					}
					return clickSlot.getStack();
				}
				clickSlot.putStack((ItemStack) null);
				playerIn.inventory.markDirty();
			}
			else {
				if (InventoryUtils.moveStackToInventory(clickSlot.getStack(), inventorySlots)) {
					clickSlot.putStack(null);
				}
			}
		}
		return null;
	}

	private boolean isTrashSlot(int index) {
		return InventoryUtils.isCustomSlot(index);
	}

}
