/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.*;

/**
 * @author p455w0rd
 *
 */
public class ContainerTest extends Container {

	IItemHandler playerInventory;
	IItemHandler customInventory;

	public ContainerTest(IItemHandler playerInv) {
		playerInventory = playerInv;
		customInventory = new ItemStackHandler(27) {

			@Override
			public void setStackInSlot(int slot, ItemStack stack) {
			}

			@Override
			public ItemStack getStackInSlot(int slot) {
				return null;
			}

			@Override
			public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
				return null;
			}

			@Override
			public ItemStack extractItem(int slot, int amount, boolean simulate) {
				return null;
			}
		};

		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new SlotItemHandler(playerInventory, i, i * 20 + (9 + i), 90 + (3 - 1) + (3 * 20 + 6)));
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new SlotItemHandler(playerInventory, j + i * 9 + 9, j * 20 + (9 + j), 149 + (3 - 1) + i - (6 - 3) * 20 + i * 20));
			}
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new SlotItemHandler(customInventory, j + i * 9, j * 20 + (9 + j), 19 + i + i * 20));
			}
		}
		detectAndSendChanges();
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

}
