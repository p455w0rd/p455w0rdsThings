package p455w0rd.p455w0rdsthings.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.api.IAccessible;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.ChannelInventory;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.AccessMode;

/**
 * @author p455w0rd
 *
 */
public class ContainerInterchanger extends Container {

	TileInterchanger interchanger;
	ChannelInventory channelInventory;

	public ContainerInterchanger(EntityPlayer player, TileInterchanger tile) {
		interchanger = tile;
		channelInventory = tile.getChannelInventory();
		for (int i = 0; i < 6; i++) {
			addSlotToContainer(new Slot(channelInventory, i, 45 + i * 19, 30));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return ((IAccessible) interchanger).getAccessMode() == AccessMode.PUBLIC || (((IAccessible) interchanger).getAccessMode() == AccessMode.PRIVATE && playerIn.getUniqueID().equals(interchanger.getOwner()));
	}

	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player) {
		if (slotId >= 0 && getSlot(slotId).inventory instanceof ChannelInventory) {
			System.out.println(dragType + ":" + clickTypeIn);
			if (dragType == 0) {
				getTile().cycleChannelItem(slotId);
			}
			else {
				getTile().cycleChannelItem(slotId, false);
			}
			getTile().markDirty();
			detectAndSendChanges();
		}
		return null;
	}

	public TileInterchanger getTile() {
		return interchanger;
	}

}
