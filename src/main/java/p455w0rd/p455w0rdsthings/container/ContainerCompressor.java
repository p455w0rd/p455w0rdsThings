package p455w0rd.p455w0rdsthings.container;

import javax.annotation.Nullable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.SlotItemHandler;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.events.ItemCompressedEvent;

/**
 * @author p455w0rd
 *
 */
public class ContainerCompressor extends Container {

	TileCompressor compressor;
	IItemHandlerModifiable inventory;
	IItemHandler playerInventory;
	//IItemHandlerModifiable upgradeInventory;

	public ContainerCompressor(EntityPlayer player, TileCompressor tile) {
		compressor = tile;
		inventory = tile.getInventory();
		//upgradeInventory = tile.getUpgradeInventory();
		playerInventory = player.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

		// Input Slot
		addSlotToContainer(new SlotItemHandler(inventory, 0, 63, 21) {

			@Override
			public boolean isItemValid(ItemStack stack) {
				return compressor.getInventoryNonWrapped().isItemValidForSlot(0, stack);
			}

			@Override
			public boolean canTakeStack(EntityPlayer playerIn) {
				return true;
			}
		});

		// Output Slot
		addSlotToContainer(new SlotItemHandler(inventory, 1, 121, 21) {
			@Override
			public boolean canTakeStack(EntityPlayer playerIn) {
				return true;
			}
		});

		//Secondary Slot
		addSlotToContainer(new SlotItemHandler(inventory, 2, 121, 42) {
			@Override
			public boolean canTakeStack(EntityPlayer playerIn) {
				return true;
			}
		});
		/*
				addSlotToContainer(new SlotItemHandler(upgradeInventory, 0, 179, 31) {
					@Override
					public boolean isItemValid(ItemStack stack) {
						return compressor.getUpgradeInventoryNonWrapped().isItemValidForSlot(0, stack);
					}
		
					@Override
					public boolean canTakeStack(EntityPlayer playerIn) {
						return true;
					}
				});
		*/
		int xbase = 9;
		int ybase = 64;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new SlotItemHandler(playerInventory, j + i * 9 + 9, xbase + j * 21, ybase + i * 21));
			}
		}

		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new SlotItemHandler(playerInventory, i, xbase + i * 21, ybase + 67));
		}
	}

	@Override
	public void addListener(IContainerListener listener) {
		super.addListener(listener);
		listener.sendAllWindowProperties(this, getTile().getInventoryNonWrapped());
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

	public TileCompressor getTile() {
		return compressor;
	}

	@Override
	@Nullable
	public ItemStack slotClick(int slotNum, int dragType, ClickType clickTypeIn, EntityPlayer player) {
		ItemStack returnStack = null;
		if (slotNum == 0) {
			InventoryPlayer inv = player.inventory;
			Slot inputSlot = inventorySlots.get(slotNum);
			ItemStack heldStack = inv.getItemStack();
			if (heldStack == null && inputSlot.getHasStack() && clickTypeIn == ClickType.PICKUP) {
				ItemStack inputStack = inputSlot.getStack();
				if (dragType == 0 || inputStack.stackSize == 1) {
					inv.setItemStack(inputStack);
					inputSlot.putStack(null);
				}
				else if (dragType == 1) {
					ItemStack newStack = inputStack.copy();
					newStack.stackSize = inputStack.stackSize / 2;
					inv.setItemStack(newStack);
					ItemStack remainingStack = inputStack.copy();
					remainingStack.stackSize = inputStack.stackSize - newStack.stackSize;
					inputSlot.putStack(remainingStack);
				}
				detectAndSendChanges();
				return null;
			}

		}
		if (slotNum == 1 && inventorySlots.get(1).getHasStack()) {
			MinecraftForge.EVENT_BUS.post(new ItemCompressedEvent(player, inventorySlots.get(1).getStack()));
		}
		try {
			returnStack = super.slotClick(slotNum, dragType, clickTypeIn, player);
		}
		catch (NullPointerException e) {
		}
		detectAndSendChanges();
		return returnStack;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = null;
		Slot slot = inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index < 4) {
				if (!mergeItemStack(itemstack1, 4, 39, true)) {
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
				//getTile().markDirty();
			}
			/*
			else if (itemstack1.getItem() instanceof IMachineUpgrade && mergeItemStack(itemstack1, 3, 4, false)) {
				slot.putStack((ItemStack) null);
				return null;
			}
			*/
			else if (!mergeItemStack(itemstack1, 0, 1, false)) {
				return null;
			}

			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
				//getTile().markDirty();
			}
			else {
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(playerIn, itemstack1);
		}
		//getTile().markDirty();
		detectAndSendChanges();
		return itemstack;
	}

}
