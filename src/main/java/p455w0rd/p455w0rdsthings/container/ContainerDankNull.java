package p455w0rd.p455w0rdsthings.container;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.inventory.InventoryDankNull;
import p455w0rd.p455w0rdsthings.inventory.slot.SlotDankNull;
import p455w0rd.p455w0rdsthings.inventory.slot.SlotHotbar;
import p455w0rd.p455w0rdsthings.network.PacketSyncDankNull;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;

public class ContainerDankNull extends Container {

	InventoryDankNull inventoryDankNull;
	private final Set<EntityPlayerMP> playerList = Sets.<EntityPlayerMP>newHashSet();

	public ContainerDankNull(EntityPlayer player, InventoryDankNull inv) {
		inventoryDankNull = inv;
		InventoryPlayer playerInv = player.inventory;
		ItemStack dankNull = inv.getDankNull();
		int lockedSlot = -1;
		int numRows = dankNull.getItemDamage() + 1;
		for (int i = 0; i < playerInv.getSizeInventory(); i++) {
			ItemStack currStack = playerInv.getStackInSlot(i);
			if (currStack != null && currStack == dankNull) {
				lockedSlot = i;
			}
		}
		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new SlotHotbar(player.inventory, i, i * 20 + (9 + i), 90 + (numRows - 1) + (numRows * 20 + 6), lockedSlot == i));

		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new Slot(player.inventory, j + i * 9 + 9, j * 20 + (9 + j), 149 + (numRows - 1) + i - (6 - numRows) * 20 + i * 20));
			}
		}
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new SlotDankNull(inventoryDankNull, j + i * 9, j * 20 + (9 + j), 19 + i + i * 20));
			}
		}
	}

	@Override
	public void addListener(IContainerListener listener) {
		if (listener instanceof EntityPlayerMP) {
			EntityPlayerMP l = (EntityPlayerMP) listener;
			if (!playerList.contains(l)) {
				playerList.add(l);
				//detectAndSendChanges();
			}
		}

		if (listeners.contains(listener)) {
			throw new IllegalArgumentException("Listener already listening");
		}
		else {
			listeners.add(listener);
			//listener.sendAllContents(this, getInventory());
			detectAndSendChanges();
		}
		//super.addListener(listener);
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

	public ItemStack getDankNull() {
		return getDankNullInventory().getDankNull();
	}

	public InventoryDankNull getDankNullInventory() {
		return inventoryDankNull;
	}

	private boolean addStack(ItemStack stack) {
		boolean ret = false;
		if (stack.getItem() == ModItems.DANK_NULL) {
			return false;
		}
		if (DankNullUtils.isFiltered(getDankNullInventory(), stack) != null) {
			ret = DankNullUtils.addFilteredStackToDankNull(getDankNullInventory(), stack);
		}
		else if (getNextAvailableSlot() >= 0) {
			inventorySlots.get(getNextAvailableSlot()).putStack(stack);
			ret = true;
		}
		if (DankNullUtils.getSelectedStackIndex(getDankNullInventory()) == -1) {
			DankNullUtils.setSelectedIndexApplicable(getDankNullInventory());
		}
		return ret;
	}

	private int getNextAvailableSlot() {
		for (int i = 36; i < inventorySlots.size(); i++) {
			Slot s = inventorySlots.get(i);
			if ((s != null) && (s.getStack() == null)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		Slot clickSlot = inventorySlots.get(index);
		if (clickSlot.getHasStack()) {
			if (!isDankNullSlot(clickSlot)) {
				if (getNextAvailableSlot() == -1 && DankNullUtils.isFiltered(getDankNullInventory(), clickSlot.getStack()) != null) {
					if (addStack(clickSlot.getStack())) {
						clickSlot.putStack(null);
						playerIn.inventory.markDirty();
						DankNullUtils.setSelectedIndexApplicable(getDankNullInventory());
					}
					else if (!moveStackWithinInventory(clickSlot.getStack(), index)) {
						return null;
					}
					return clickSlot.getStack();
				}
				else {
					if (addStack(clickSlot.getStack())) {
						clickSlot.putStack(null);
						playerIn.inventory.markDirty();
						DankNullUtils.setSelectedIndexApplicable(getDankNullInventory());
					}
					else if (!moveStackWithinInventory(clickSlot.getStack(), index)) {
						return null;
					}
					return clickSlot.getStack();
				}
			}
			else {
				ItemStack newStack = clickSlot.getStack().copy();
				int realMaxStackSize = newStack.getMaxStackSize();
				int currentStackSize = newStack.stackSize;
				if (currentStackSize > realMaxStackSize) {
					newStack.stackSize = realMaxStackSize;
					if (moveStackToInventory(newStack)) {
						DankNullUtils.decrDankNullStackSize(getDankNullInventory(), clickSlot.getStack(), realMaxStackSize);
					}
				}
				else {
					newStack.stackSize = currentStackSize;
					if (moveStackToInventory(newStack)) {
						DankNullUtils.decrDankNullStackSize(getDankNullInventory(), clickSlot.getStack(), currentStackSize);
						clickSlot.putStack(null);
					}
					DankNullUtils.reArrangeStacks(getDankNullInventory());
				}
			}
		}
		//markDirty();
		return null;
	}

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		if (FMLCommonHandler.instance().getSide().isServer()) {
			for (EntityPlayerMP player : playerList) {
				ModNetworking.INSTANCE.sendTo(new PacketSyncDankNull(getDankNullInventory()), player);
			}
		}
		//
	}

	private boolean isDankNullSlot(Slot slot) {
		return slot instanceof SlotDankNull;
	}

	private boolean moveStackWithinInventory(ItemStack itemStackIn, int index) {
		if (isInHotbar(index)) {
			if (mergeItemStack(itemStackIn, 9, 37, false)) {
				return true;
			}
			for (int i = 9; i <= 36; i++) {
				Slot possiblyOpenSlot = inventorySlots.get(i);
				if (!possiblyOpenSlot.getHasStack()) {
					possiblyOpenSlot.putStack(itemStackIn);
					inventorySlots.get(index).putStack(null);
					return true;
				}
			}
		}
		else if (isInInventory(index)) {
			if (mergeItemStack(itemStackIn, 0, 9, false)) {
				return true;
			}
			for (int i = 0; i <= 8; i++) {
				Slot possiblyOpenSlot = inventorySlots.get(i);
				if (!possiblyOpenSlot.getHasStack()) {
					possiblyOpenSlot.putStack(itemStackIn);
					inventorySlots.get(index).putStack(null);
					return true;
				}
			}
		}
		return false;
	}

	private boolean isInHotbar(int index) {
		return (index >= 0) && (index <= 8);
	}

	private boolean isInInventory(int index) {
		return (index >= 9) && (index <= 36);
	}

	protected boolean moveStackToInventory(ItemStack itemStackIn) {
		for (int i = 0; i <= 36; i++) {
			Slot possiblyOpenSlot = inventorySlots.get(i);
			if (!possiblyOpenSlot.getHasStack()) {
				possiblyOpenSlot.putStack(itemStackIn);
				return true;
			}
		}
		return false;
	}

	@Override
	public ItemStack slotClick(int index, int dragType, ClickType clickTypeIn, EntityPlayer player) {
		InventoryPlayer inventoryplayer = player.inventory;
		ItemStack heldStack = inventoryplayer.getItemStack();
		if (index < 0) {
			return super.slotClick(index, dragType, clickTypeIn, player);
		}
		Slot s = inventorySlots.get(index);
		if (s.getStack() == inventoryDankNull.getDankNull()) {
			return null;
		}
		if (isDankNullSlot(s)) {
			ItemStack thisStack = s.getStack();
			if (thisStack != null && (thisStack.getItem() == ModItems.DANK_NULL)) {
				return null;
			}
			if (heldStack == null) {
				if (thisStack != null && clickTypeIn == ClickType.PICKUP) {
					int max = thisStack.getMaxStackSize();
					ItemStack newStack = thisStack.copy();

					if (thisStack.stackSize >= max) {
						newStack.stackSize = max;
					}
					if (dragType == 1) {
						int returnSize = Math.min(newStack.stackSize / 2, newStack.stackSize);
						if (getDankNullInventory() != null) {
							DankNullUtils.decrDankNullStackSize(getDankNullInventory(), thisStack, newStack.stackSize - returnSize);
							newStack.stackSize = returnSize + ((newStack.stackSize % 2 == 0) ? 0 : 1);
						}
					}
					else if (dragType == 0) {
						if (getDankNullInventory() != null) {
							DankNullUtils.decrDankNullStackSize(getDankNullInventory(), thisStack, newStack.stackSize);
							if (inventorySlots.get(index).getHasStack() && inventorySlots.get(index).getStack().stackSize <= 0) {
								inventorySlots.get(index).putStack(null);
							}
						}
					}
					inventoryplayer.setItemStack(newStack);
					DankNullUtils.reArrangeStacks(getDankNullInventory());
					return newStack;
				}
			}
			else {
				if (heldStack.getItem() == ModItems.DANK_NULL) {
					return null;
				}
				if (addStack(heldStack)) {
					inventoryplayer.setItemStack(null);
					return heldStack;
				}
				else {
					if (thisStack.stackSize <= thisStack.getMaxStackSize()) {
						inventoryplayer.setItemStack(thisStack);
						s.putStack(heldStack);
					}
					return null;
				}
			}
		}
		ItemStack ret = super.slotClick(index, dragType, clickTypeIn, player);
		return ret;

	}

}