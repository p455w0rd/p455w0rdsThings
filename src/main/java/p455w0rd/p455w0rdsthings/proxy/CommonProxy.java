package p455w0rd.p455w0rdsthings.proxy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppedEvent;
import p455w0rd.p455w0rdsthings.guide.WorldData;
import p455w0rd.p455w0rdsthings.init.ModAchievements;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModCommands;
import p455w0rd.p455w0rdsthings.init.ModConfig;
import p455w0rd.p455w0rdsthings.init.ModEntities;
import p455w0rd.p455w0rdsthings.init.ModEvents;
import p455w0rd.p455w0rdsthings.init.ModFluids;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler;
import p455w0rd.p455w0rdsthings.init.ModGuide;
import p455w0rd.p455w0rdsthings.init.ModIntegration;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.init.ModOreDictionary;
import p455w0rd.p455w0rdsthings.init.ModOreGen;
import p455w0rd.p455w0rdsthings.init.ModRecipes;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent e) {
		ModLogger.infoBegin("PreInit");
		ModConfig.init();
		ModFluids.init();
		ModBlocks.init();
		ModItems.init();
		ModIntegration.preInit();
		ModNetworking.init();
		ModRecipes.preInit();
		ModLogger.infoEnd("PreInit");
	}

	public void init(FMLInitializationEvent e) {
		ModLogger.infoBegin("Init");
		ModEvents.init();
		ModOreDictionary.addOres();
		ModRecipes.init();
		ModEntities.init();
		ModOreGen.init();
		ModAchievements.init();
		ModIntegration.init();
		ModLogger.infoEnd("Init");
	}

	public void postInit(FMLPostInitializationEvent e) {
		ModLogger.infoBegin("PostInit");
		ModGuiHandler.init();
		ModIntegration.postInit();
		ModLogger.infoEnd("PostInit");
	}

	public void serverStarting(FMLServerStartingEvent e) {
		ModCommands.init(e);
		ModGuide.loadData();
	}

	public void serverStopped(FMLServerStoppedEvent event) {
		WorldData.clear();
	}

	public EntityPlayer getPlayer() {
		return null;
	}

}
