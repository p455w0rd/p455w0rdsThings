package p455w0rd.p455w0rdsthings.proxy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModCreativeTab;
import p455w0rd.p455w0rdsthings.init.ModGuide;
import p455w0rd.p455w0rdsthings.init.ModIntegration;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModKeyBindings;
import p455w0rd.p455w0rdsthings.init.ModRendering;
import p455w0rdslib.util.EasyMappings;

public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);

		ModBlocks.preInitModels();

		ModItems.preInitModels();
		ModCreativeTab.init();
		ModGuide.preInit();
	}

	@Override
	public void init(FMLInitializationEvent e) {

		super.init(e);
		//ModBlocks.initModels();

		ModKeyBindings.init();
		ModRendering.init();
		ModIntegration.init();
	}

	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
		ModIntegration.postInit();
		//ModGuide.init();
	}

	@Override
	public void serverStarting(FMLServerStartingEvent e) {

	}

	@Override
	public EntityPlayer getPlayer() {
		return EasyMappings.player();
	}

}
