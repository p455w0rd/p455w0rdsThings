package p455w0rd.p455w0rdsthings.init;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Loader;

public class ModFluids {
	//public static Fluid xpJuice;
	public static final Fluid SUBLIMATED_CARBON = createFluid("carbon_fluid", true);

	public static void init() {
		if (!Loader.isModLoaded("EnderIO")) {
			//xpJuice = createFluid("xpjuice", true);
		}
	}

	private static Fluid createFluid(String name, boolean hasFlowIcon) {

		String texturePrefix = ModGlobals.MODID + ":blocks/";
		ResourceLocation still = new ResourceLocation(texturePrefix + name + "_still");
		ResourceLocation flowing = hasFlowIcon ? new ResourceLocation(texturePrefix + name + "_flow") : still;
		Fluid fluid = new Fluid(name, still, flowing);
		boolean useOwnFluid = FluidRegistry.registerFluid(fluid);
		if (!useOwnFluid) {
			fluid = FluidRegistry.getFluid(name);
		}

		return fluid;

	}
}
