package p455w0rd.p455w0rdsthings.init;

import java.io.File;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ModConfig {

	public static Configuration CONFIG;

	private static final String DEF_CAT = "General";
	private static final String TIC_CAT = "Tinkers Integration";
	private static final String PE_CAT = "ProjectE Integration";
	private static final String ARMOR_CAT = "Armor";
	public static boolean reloadConfigs = false;

	@SubscribeEvent
	public void onConfigChange(ConfigChangedEvent.OnConfigChangedEvent e) {
		if (e.getModID().equals(ModGlobals.MODID)) {
			init();
		}
	}

	public static void init() {
		if (CONFIG == null) {
			CONFIG = new Configuration(new File(ModGlobals.CONFIG_FILE));
			MinecraftForge.EVENT_BUS.register(new ModConfig());
		}
		//ConfigOptions.MAX_CHUNKLOADERS_PER_PLAYER = CONFIG.getInt("MaxChunkLoadersPerPlayer", DEF_CAT, 3, -1, 100, "Number of Chunk Loaders a player is allowed to place (-1 = infinite)");
		//ConfigOptions.CHUNKLOADER_MAXCHUNK_RADIUS = CONFIG.getInt("MaxChunkLoaderRadius", DEF_CAT, 3, 1, 7, "Maximum chunk loader radius");
		//ConfigOptions.ENABLE_CHUNK_LOADER = CONFIG.getBoolean("EnableChunkLoader", DEF_CAT, true, "Enable Chunk Loader");
		ConfigOptions.MAGNET_RADIUS = CONFIG.get(DEF_CAT, "MagnetRadius", 6.5, "Radius from player that magnet will scan for entities", 1, 16).getDouble();
		ConfigOptions.MAGNET_RF_PER_ITEM = CONFIG.getInt("MagnetRFPerItem", DEF_CAT, 100, 10, 1000, "Amount of RF consumed per item picked up.");
		ConfigOptions.MAGNET_RF_CAPCITY = CONFIG.getInt("MagnetCapacity", DEF_CAT, 1000000, 10, 64000000, "Amount of RF the magnet can hold.");
		ConfigOptions.WITHER_SKELES_DROP_RAW_CARBON = CONFIG.getBoolean("EnableWitherSkeletonDrops", DEF_CAT, true, "Enabled Wither Skeleton Raw Carbon Drops");
		ConfigOptions.ENABLE_NETHER_CARBON_WORLDGEN = CONFIG.getBoolean("NetherCarbonWorldGen", DEF_CAT, true, "Enable natural generation of Nether Carbon Ore");
		ConfigOptions.NV_GOGGLES_RF = CONFIG.getInt("NVGoggleRFPerTick", DEF_CAT, 10, 10, 1000, "Amount of RF consumed per tick while Night Vision Goggles are active. (Applies to Carbon Armor upgrade as well)");

		ConfigOptions.ENABLE_ENDERMAN = CONFIG.getBoolean("EnableEnderman", DEF_CAT, true, "Enable custom Enderman");
		ConfigOptions.ENDERMAN_DAY_SPAWN = CONFIG.getBoolean("EndermanSpawnIgnoreLightLevel", DEF_CAT, true, "Allow custom Enderman to spawn at any light level");
		ConfigOptions.ENABLE_FRIENDERMAN = CONFIG.getBoolean("EnableFrienderman", DEF_CAT, true, "Enable the Friendermans =]");
		ConfigOptions.ENDERMAN_PROBABILITY = CONFIG.getInt("EndermanSpawnProbability", DEF_CAT, 3, 1, 10, "Probability PWThings Enderman will spawn when game deicdes to spawn a mob");
		ConfigOptions.ENDERMAN_MAX_SPAWN = CONFIG.getInt("EndermanMaxSpawnPerGroup", DEF_CAT, 2, 1, 4, "When the game decides to spawn PWThings Enderman, what is the max that should spawn in the group?");
		ConfigOptions.DISABLE_VANILLA_RECIPES = CONFIG.getBoolean("DisableVanillaRecipes", DEF_CAT, false, "Disable all shaped/shapeless recipes-useful for mod pack authors");
		ConfigOptions.ENABLE_INTERCHANGER_AUTOCHUNKLOAD = CONFIG.getBoolean("InterchangerShouldChunkLoad", DEF_CAT, true, "Should the Interchanger automatically chunk load itself?");
		ConfigOptions.ENABLE_HORSE_ARMOR_RECIPES = CONFIG.getBoolean("EnabledHorseArmorRecipes", DEF_CAT, true, "Add recipes for horse armor (both vanilla and custom)");

		ConfigOptions.DISABLE_EMC = CONFIG.getBoolean("DisableEMC", PE_CAT, false, "Disable EMC values for items (true=items have no EMC)");

		ConfigOptions.TIC_BASE_DURABILITY = CONFIG.getInt("TiCCarbonBaseDurability", TIC_CAT, 1200, 1, 3000, "Tinker's Carbon Material base durability");
		ConfigOptions.TIC_HANDLE_DURABILITY = CONFIG.getInt("TiCCarbonHandleDurability", TIC_CAT, 250, 1, 500, "Tinker's Carbon Material handle durability");
		ConfigOptions.TIC_EXTRA_DURABILITY = CONFIG.getInt("TiCCarbonExtraDurability", TIC_CAT, 1200, 1, 3000, "Tinker's Carbon Material extra durability (added to any tool/weapon using and carbon material)");
		ConfigOptions.TIC_BASE_ATTACK = CONFIG.getFloat("TiCBaseAttack", TIC_CAT, 30.0F, 1.0F, 30.0F, "Tinker's Carbon Material Base attack damage (this * 2 = how many hearts)");
		ConfigOptions.TIC_BOW_EXTRA_DAMAGE = CONFIG.getInt("TiCBowExtraDamage", TIC_CAT, 300, 1, 1000, "Tinker's Carbon Crossbow Damage Bonus");
		ConfigOptions.TIC_BOW_RANGE = CONFIG.getFloat("TiCBowRange", TIC_CAT, 5.0F, 1.0F, 10.0F, "Tinker's Carbon Crossbow Range (Shouldn't mess with this unless you know what you're doing)");
		ConfigOptions.TIC_BOW_DRAW_SPEED = CONFIG.getFloat("TiCBowDrawSpeed", TIC_CAT, 20.0F, 1.0F, 20.0F, "Tinker's Carbon Crossbow Draw Speed");
		ConfigOptions.TIC_EXTRA_BOLT_AMMO = CONFIG.getInt("TiCBoltExtraAmmo", TIC_CAT, 150, 1, 1000, "Tinker's Carbon Crossbow Bolt extra ammo for using carbon");
		ConfigOptions.ENABLE_TIC_FLESH_TO_LEATHER_RECIPE = CONFIG.getBoolean("TiCFleshToLeatherRecipe", TIC_CAT, true, "Add TiC drying rack recipe to turn rotten flesh into leather");
		ConfigOptions.TIC_LEATHER_DRY_TIME = CONFIG.getInt("TiCFleshDryTime", TIC_CAT, 20, 1, 3600, "How long in seconds it takes to turn Rotten Flesh into Leather on TiC Drying Rack");
		ConfigOptions.TIC_MINING_SPEED = CONFIG.getFloat("TiCMiningSpeed", TIC_CAT, 200.0F, 1.0F, 500.0F, "Tinker's Carbon Tool Head default mining speed");

		ConfigOptions.ENERGY_USE_FLIGHT = CONFIG.getInt("FlightEnergyPerTick", ARMOR_CAT, 100, 1, 1000, "Energy use per tick while using a flight enabled armor piece");
		ConfigOptions.ENERGY_USE_REPULSION = CONFIG.getInt("RepulsionEnergyPerUse", ARMOR_CAT, 100, 1, 1000, "Energy consumed each time a mob is repulsed by the Repulsion armor upgrade");
		ConfigOptions.REPULSION_MAX_DAMAGE = CONFIG.getInt("MaxRepulsionDamage", ARMOR_CAT, 3, 1, 100, "Amount of hearts Repulsion armor upgrade deals to an enemy per use");
		ConfigOptions.SKELETONS_SPAWN_WITH_CARBON_ARMOR = CONFIG.getBoolean("SkeletonsSpawnWithCarbonArmor", ARMOR_CAT, true, "Should skeletons spawn with carbon armor (random)");
		ConfigOptions.ENABLE_SILKTOUCH_CHEST_HARVEST = CONFIG.getBoolean("EnableSilkChestHarvest", DEF_CAT, true, "Should chests retain their contents when havested? (Vanilla/IronChests)");

		if (CONFIG.hasChanged() || reloadConfigs) {
			CONFIG.save();
			reloadConfigs = false;
		}
	}

	public static class ConfigOptions {

		//public static int CHUNKLOADER_MAXCHUNK_RADIUS = 3;
		//public static int MAX_CHUNKLOADERS_PER_PLAYER = 3;
		//public static boolean ENABLE_CHUNK_LOADER = true; // TODO
		//public static boolean ARMORSTAND_REQUIRES_REDSTONE = false; // TODO
		public static double MAGNET_RADIUS = 6.5;
		public static int MAGNET_RF_PER_ITEM = 100;
		public static int MAGNET_RF_CAPCITY = 1000000;
		public static int NV_GOGGLES_RF = 10;
		public static boolean DISABLE_EMC = false;
		public static boolean ENABLE_ENDERMAN = true;
		public static boolean ENABLE_FRIENDERMAN = true;
		public static int ENDERMAN_PROBABILITY = 3;
		public static int ENDERMAN_MAX_SPAWN = 2;
		public static boolean ENDERMAN_DAY_SPAWN = true;
		public static boolean ENABLE_WITHERLESS_UPGRADE = true;
		public static boolean ENABLE_LAVAWALKER_UPRGADE = true;
		public static boolean DISABLE_VANILLA_RECIPES = false;
		public static boolean ENABLE_INTERCHANGER_AUTOCHUNKLOAD = true;
		public static boolean ENABLE_SILKTOUCH_CHEST_HARVEST = true;
		public static boolean ENABLE_HORSE_ARMOR_RECIPES = true;
		public static boolean SKELETONS_SPAWN_WITH_CARBON_ARMOR = true;

		public static int TIC_BOW_EXTRA_DAMAGE = 300;
		public static float TIC_BOW_RANGE = 5.0F;
		public static float TIC_MINING_SPEED = 200.0F;
		public static boolean ENABLE_TIC_FLESH_TO_LEATHER_RECIPE = true;
		public static int TIC_LEATHER_DRY_TIME = 20;
		public static int TIC_EXTRA_BOLT_AMMO = 150;
		public static float TIC_BOW_DRAW_SPEED = 20.0F;
		public static int TIC_BASE_DURABILITY = 1200;
		public static float TIC_BASE_ATTACK = 30.0F;
		public static int TIC_HANDLE_DURABILITY = 250;
		public static int TIC_EXTRA_DURABILITY = 1200;

		public static int ENERGY_USE_FLIGHT = 100;
		public static int ENERGY_USE_REPULSION = 100;
		public static int REPULSION_MAX_DAMAGE = 3;

		public static boolean ENABLE_CHUNK_OVERLAY = true;
		public static boolean ENABLE_UPDATE_CHECK = true;
		public static boolean WITHER_SKELES_DROP_RAW_CARBON = true;
		public static boolean ENABLE_NETHER_CARBON_WORLDGEN = true;

	}
}
