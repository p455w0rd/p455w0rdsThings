package p455w0rd.p455w0rdsthings.init;

import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemArmor;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;

/**
 * @author p455w0rd
 *
 */
public class ModMaterials {

	public static final Material CARBON = new Material(MapColor.BLACK) {

		@Override
		public boolean isReplaceable() {
			return false;
		}

		@Override
		public boolean isToolNotRequired() {
			return false;
		}

		@Override
		public EnumPushReaction getMobilityFlag() {
			return EnumPushReaction.BLOCK;
		}

	};

	public static final ItemArmor.ArmorMaterial CARBON_ARMOR_MATERIAL = ArmorUtils.addArmorMaterial("p455w0rdsthings:carbon", "p455w0rdsthings:carbon", 45, new int[] {
			2,
			6,
			9,
			3
	}, 25, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 2.0F);
	public static final ItemArmor.ArmorMaterial JETPACK_MATERIAL = ArmorUtils.addArmorMaterial("p455w0rdsthings:jetplate_carbon", "p455w0rdsthings:jetplate_carbon", 45, new int[] {
			0,
			0,
			2,
			0
	}, 25, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 1.0F);

	public static final ItemArmor.ArmorMaterial SKULL_MATERIAL = ArmorUtils.addArmorMaterial("p455w0rdsthings:skull", "p455w0rdsthings:skull", -1, new int[] {
			0,
			0,
			0,
			0
	}, 25, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0.0F);

	public static final Material SUBLIMATED_CARBON = new MaterialLiquid(MapColor.BLACK) {

		@Override
		public boolean blocksLight() {
			return true;
		}

		@Override
		public boolean getCanBurn() {
			return true;
		}

	};

}
