package p455w0rd.p455w0rdsthings.init;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import p455w0rd.p455w0rdsthings.integration.CraftTweaker;
import p455w0rd.p455w0rdsthings.integration.EnderIo;
import p455w0rd.p455w0rdsthings.integration.EnderStorage;
import p455w0rd.p455w0rdsthings.integration.ProjectE;
import p455w0rd.p455w0rdsthings.integration.TOP;
import p455w0rd.p455w0rdsthings.integration.TiC;
import p455w0rd.p455w0rdsthings.integration.WAILA;
import p455w0rd.p455w0rdsthings.integration.tinkers.TiCMaterials;

/**
 * @author p455w0rd
 *
 */
public class ModIntegration {

	public static void preInit() {
		if (Mods.TINKERS.isLoaded()) {
			TiC.preInit();
		}
		else {
			ModLogger.info(Mods.TINKERS.getName() + " Integation: Disabled");
		}
		if (Mods.TOP.isLoaded()) {
			TOP.init();
		}
		else {
			ModLogger.info(Mods.TOP.getName() + " Integation: Disabled");
		}
		if (Mods.MINETWEAKER.isLoaded()) {
			CraftTweaker.preInit();
		}
		else {
			ModLogger.info(Mods.PROJECTE.getName() + " Integation: Disabled");
		}
	}

	public static void init() {
		if (Mods.ENDERIO.isLoaded()) {
			EnderIo.init();
		}
		else {
			ModLogger.info(Mods.ENDERIO.getName() + " Integation: Disabled");
		}
		if (Mods.ENDERSTORAGE.isLoaded()) {
			EnderStorage.init();
		}
		else {
			ModLogger.info(Mods.ENDERSTORAGE.getName() + " Integation: Disabled");
		}
		if (FMLCommonHandler.instance().getSide().isClient()) {
			if (Mods.WAILA.isLoaded()) {
				WAILA.init();
			}
			else {
				ModLogger.info("Waila Integation: Disabled");
			}
		}
	}

	public static void postInit() {
		if (Mods.PROJECTE.isLoaded()) {
			ProjectE.init();
		}
		else {
			ModLogger.info(Mods.PROJECTE.getName() + " Integation: Disabled");
		}
		if (FMLCommonHandler.instance().getSide().isClient()) {
			if (Mods.TINKERS.isLoaded()) {
				TiCMaterials.registerMaterialRendering();
			}
		}
		if (Mods.MINETWEAKER.isLoaded()) {
			CraftTweaker.init();
		}
	}

	public static enum Mods {
			TINKERS("tconstruct", "Tinker's Construct"),
			TOP("theoneprobe", "The One Probe"),
			ENDERSTORAGE("EnderStorage", "Ender Storage"), ENDERIO("EnderIO", "EnderIO"),
			PROJECTE("ProjectE", "ProjectE"), WAILA("Waila", "WAILA"), BAUBLES("Baubles", "Baubles"),
			BAUBLESAPI("Baubles|API", "Baubles API"), IRONCHESTS("ironchest", "Iron Chests"), JEI("JEI", "JEI"),
			MINETWEAKER("MineTweaker3", "CraftTweaker");

		private String modid, name;

		Mods(String modidIn, String nameIn) {
			modid = modidIn;
			name = nameIn;
		}

		public String getId() {
			return modid;
		}

		public String getName() {
			return name;
		}

		public boolean isLoaded() {
			return Loader.isModLoaded(getId());
		}
	}

}
