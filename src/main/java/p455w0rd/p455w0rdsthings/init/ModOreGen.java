package p455w0rd.p455w0rdsthings.init;

import java.util.Random;

import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;

public class ModOreGen implements IWorldGenerator {

	private WorldGenerator genNetherCarbonOre;

	public static void init() {
		ModLogger.info("Adding Ore Generator");
		GameRegistry.registerWorldGenerator(new ModOreGen(), 0);
	}

	public ModOreGen() {
		genNetherCarbonOre = new WorldGenMinable(ModBlocks.NETHER_CARBON_ORE.getDefaultState(), 5, BlockMatcher.forBlock(Blocks.NETHERRACK));
	}

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		boolean tinkersLoaded = Loader.isModLoaded("tconstruct");
		int probability = tinkersLoaded ? 15 : 5;
		if (world.provider.getDimension() == -1 && ConfigOptions.ENABLE_NETHER_CARBON_WORLDGEN) {
			for (int i = 0; i < probability; i++) {
				int x = chunkX * 16 + random.nextInt(16);
				int y = 0 + random.nextInt(255);
				int z = chunkZ * 16 + random.nextInt(16);
				genNetherCarbonOre.generate(world, random, new BlockPos(x, y, z));
			}
		}
	}
}
