package p455w0rd.p455w0rdsthings.init;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.lwjgl.input.Keyboard;

import com.google.common.collect.Lists;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.SkeletonType;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootEntryItem;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraft.world.storage.loot.RandomValueRange;
import net.minecraft.world.storage.loot.conditions.LootCondition;
import net.minecraft.world.storage.loot.functions.LootFunction;
import net.minecraft.world.storage.loot.functions.SetCount;
import net.minecraftforge.client.event.GuiScreenEvent.MouseInputEvent;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.ThrowableImpactEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.api.IFlightItem;
import p455w0rd.p455w0rdsthings.api.IMachineUpgrade;
import p455w0rd.p455w0rdsthings.blocks.BlockSlabBase;
import p455w0rd.p455w0rdsthings.client.render.ParticleRenderer;
import p455w0rd.p455w0rdsthings.client.render.TESRDankNullDock;
import p455w0rd.p455w0rdsthings.client.render.TESRInterchanger;
import p455w0rd.p455w0rdsthings.client.render.TESRMachineUpgrade;
import p455w0rd.p455w0rdsthings.client.render.entity.LayerEntityCharge;
import p455w0rd.p455w0rdsthings.client.render.entity.LayerSkullEyes;
import p455w0rd.p455w0rdsthings.container.ContainerCompressor;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rd.p455w0rdsthings.events.ItemCompressedEvent;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModGlobals.Textures;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.integration.Baubles;
import p455w0rd.p455w0rdsthings.integration.EnderIo;
import p455w0rd.p455w0rdsthings.integration.IronChests;
import p455w0rd.p455w0rdsthings.integration.TiC;
import p455w0rd.p455w0rdsthings.integration.tinkers.TiCModifiers;
import p455w0rd.p455w0rdsthings.inventory.InventoryDankNull;
import p455w0rd.p455w0rdsthings.items.ItemDankNull;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonJetPlate;
import p455w0rd.p455w0rdsthings.items.ItemJetPack;
import p455w0rd.p455w0rdsthings.items.ItemRFArmor;
import p455w0rd.p455w0rdsthings.items.ItemSkullBase;
import p455w0rd.p455w0rdsthings.network.PacketConfigSync;
import p455w0rd.p455w0rdsthings.network.PacketSetEntityChargedS;
import p455w0rd.p455w0rdsthings.network.PacketSetNightVision;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rd.p455w0rdsthings.util.EntityUtils;
import p455w0rd.p455w0rdsthings.util.FriendermanUtils;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rd.p455w0rdsthings.util.MachineUtils;
import p455w0rdslib.util.ChestUtils;
import p455w0rdslib.util.ChunkUtils.Callback;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.ImageUtils;
import p455w0rdslib.util.ItemUtils;
import p455w0rdslib.util.MCPrivateUtils;
import p455w0rdslib.util.MathUtils;
import p455w0rdslib.util.PotionUtils;
import p455w0rdslib.util.ReflectionUtils;
import p455w0rdslib.util.RenderUtils;

public class ModEvents {

	//private static List<UUID> UUID_LIST = Lists.<UUID>newArrayList(/*(UUID.fromString("7b794048-f144-40bb-83dd-4789a5cf7cc8"), */UUID.fromString("10755ea6-9721-467a-8b5c-92adf689072c"));
	private static List<EntityLivingBase> ENTITY_LIST = Lists.<EntityLivingBase>newArrayList();
	//private static boolean HAS_TITLE_RUN = false;
	//private static boolean HAS_SPLASH_RUN = false;

	public static void init() {
		ForgeChunkManager.setForcedChunkLoadingCallback(P455w0rdsThings.INSTANCE, Callback.getInstance());
		MinecraftForge.EVENT_BUS.register(new ModEvents());
	}
	/*
		@SubscribeEvent
		public void onRespawn(PlayerEvent.Clone e) {
			List<EntityFrienderman> friendermanList = Lists.newArrayList();
			EntityPlayer newPlayer = e.getEntityPlayer();
			for (int entityID : ModRegistries.getTamedFriendermanRegistry().keySet()) {
				EntityFrienderman entity = (EntityFrienderman) e.getOriginal().getEntityWorld().getEntityByID(entityID);
				if (entity != null && ModRegistries.getTamedFriendermanRegistry().get(entity.getEntityId()) != null && ModRegistries.getTamedFriendermanRegistry().get(entity.getEntityId()).equals(e.getEntityPlayer().getUniqueID())) {
					if (entity.isTamed() && !entity.isSitting()) {
						friendermanList.add(entity);
					}
				}
			}
			for (EntityFrienderman frienderman : friendermanList) {
				frienderman.changeDimension(newPlayer.dimension);
				frienderman.setPosition(newPlayer.posX, newPlayer.posY + 2, newPlayer.posZ);
			}
		}

		@SubscribeEvent
		public void onDimensionChange(EntityTravelToDimensionEvent e) {
			List<EntityFrienderman> friendermanList = Lists.newArrayList();
			if (!(e.getEntity() instanceof EntityPlayer)) {
				return;
			}
			EntityPlayer newPlayer = (EntityPlayer) e.getEntity();
			World world = newPlayer.getEntityWorld();
			for (int entityID : ModRegistries.getTamedFriendermanRegistry().keySet()) {
				EntityFrienderman entity = (EntityFrienderman) world.getEntityByID(entityID);
				if (entity != null && ModRegistries.getTamedFriendermanRegistry().get(entity.getEntityId()) != null && ModRegistries.getTamedFriendermanRegistry().get(entity.getEntityId()).equals(newPlayer.getUniqueID())) {
					if (entity.isTamed() && !entity.isSitting()) {
						friendermanList.add(entity);
					}
				}
			}
			for (EntityFrienderman frienderman : friendermanList) {

				BlockPos playerPos = new BlockPos(newPlayer.posX, newPlayer.posY, newPlayer.posZ);
				BlockPos spawnPos = playerPos.east(3).west(3).up(2);

				for (int i = 0; i < Integer.MAX_VALUE; i++) {
					spawnPos = playerPos.east(i).west(i).down();
					IBlockState state = world.getBlockState(spawnPos);
					Block spawnBlock = state.getBlock();
					if (spawnBlock.canCreatureSpawn(state, world, spawnPos, SpawnPlacementType.ON_GROUND) && !(world.getBlockState(spawnPos.up()).getBlock() instanceof BlockPortal)) {
						break;
					}
				}

				EntityFrienderman newEntity = (EntityFrienderman) TeleportUtils.teleportEntity(frienderman, e.getDimension(), e.getDimension() == -1 ? spawnPos.getX() / 8 : spawnPos.getX(), e.getDimension() == -1 ? spawnPos.getY() / 8 : spawnPos.getY(), e.getDimension() == -1 ? spawnPos.getZ() / 8 : spawnPos.getZ(), frienderman.rotationYaw, frienderman.rotationPitch);
				if (newEntity != null) {
					ModRegistries.registerTamedFrienderman(newPlayer, newEntity);
					ModNetworking.INSTANCE.sendToAll(new PacketFriendermanRegistrySync(ModRegistries.getTamedFriendermanRegistry()));
				}
				//frienderman.changeDimension(e.getDimension());
				//p455w0rdslib.util.EntityUtils.teleportEntityToDimension(newPlayer.getEntityWorld(), frienderman, e.getDimension());
				//frienderman.setPosition(playerPos.getX(), playerPos.getY(), playerPos.getZ());
			}
		}

		@SubscribeEvent
		public void onDeath(LivingDeathEvent e) {
			if (!e.isCanceled() && !e.getEntityLiving().getEntityWorld().isRemote && e.getEntityLiving() instanceof EntityFrienderman) {
				EntityFrienderman frienderman = (EntityFrienderman) e.getEntityLiving();
				ModRegistries.unregisterTamedFrienderman(frienderman);
				ModNetworking.INSTANCE.sendToAll(new PacketFriendermanRegistrySync(ModRegistries.getTamedFriendermanRegistry()));
			}
		}
	*/

	@SubscribeEvent
	public void onDeath(LivingDeathEvent e) {
		if (!e.isCanceled() && e.getEntityLiving() instanceof EntitySkeleton) {
			if (ENTITY_LIST.contains(e.getEntityLiving())) {
				ENTITY_LIST.remove(e.getEntityLiving());
			}
		}
	}

	public static int chatX = 0;
	public static int chatY = 0;

	@SubscribeEvent
	public void getChatPos(RenderGameOverlayEvent.Chat event) {
		chatX = event.getPosX();
		chatY = event.getPosY();
	}

	/*
	@SuppressWarnings("unchecked")
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void renderGlowNames(RenderGameOverlayEvent.Post event) {
		AbstractClientPlayer player = Minecraft.getMinecraft().player;
		if (!ContributorUtils.doesPlayerHaveWings(player)) {
			//ContributorUtils.queuePlayerCosmetics(player);
			return;
		}
		GuiNewChat c = Minecraft.getMinecraft().ingameGUI.getChatGUI();
		Field f_lines;
		Field f_counter;
		if (event.getType() == ElementType.CHAT) {
			try {
				f_lines = c.getClass().getDeclaredField("drawnChatLines");
				f_lines.setAccessible(true);
				f_counter = Minecraft.getMinecraft().ingameGUI.getClass().getSuperclass().getDeclaredField("updateCounter");
				f_counter.setAccessible(true);
				List<ChatLine> chatLines;
				int updateCounter = 0;
				try {
					updateCounter = f_counter.getInt(Minecraft.getMinecraft().ingameGUI);
					chatLines = (List<ChatLine>) f_lines.get(c);
					for (int i = 0; c.getChatOpen() && i < chatLines.size() || !c.getChatOpen() && i < chatLines.size() && i < 10; i++) {
						ChatLine l = chatLines.get(i);
						String s = l.getChatComponent().getUnformattedText();
						for (int j = 0; j < s.length(); j++) {
							/*if (j < s.length()-4 && s.substring(j, j+5).compareTo(TextFormatting.RESET.toString()+"   ") == 0){
								float f = Minecraft.getMinecraft().gameSettings.chatOpacity * 0.9F + 0.1F;
								int j1 = updateCounter - l.getUpdatedCounter();
								if (j1 < 200 || c.getChatOpen()){
									double d0 = (double)j1 / 200.0D;
							        d0 = 1.0D - d0;
							        d0 = d0 * 10.0D;
							        d0 = MathHelper.clamp(d0, 0.0D, 1.0D);
							        d0 = d0 * d0;
							        int l1 = (int)(255.0D * d0);

							        if (c.getChatOpen())
							        {
							            l1 = 255;
							        }

							        l1 = (int)((float)l1 * f);
									String before = s.substring(0,j);
									double x = (double)chatX + (double)Minecraft.getMinecraft().fontRendererObj.getStringWidth(before)+5.0;
									double y = (double)chatY-((double)Minecraft.getMinecraft().fontRendererObj.FONT_HEIGHT)*((double)i)+1.0;
									GlStateManager.enableAlpha();
									GlStateManager.enableBlend();
									GlStateManager.color(1f, 1f, 1f, (float)l1/255f);
									int dfunc = GL11.glGetInteger(GL11.GL_DEPTH_FUNC);
									GlStateManager.depthFunc(GL11.GL_LEQUAL);
									int func = GL11.glGetInteger(GL11.GL_ALPHA_TEST_FUNC);
									float ref = GL11.glGetFloat(GL11.GL_ALPHA_TEST_REF);
									GlStateManager.alphaFunc(GL11.GL_ALWAYS, 0);
									GlStateManager.depthMask(false);
									GlStateManager.disableTexture2D();
									GlStateManager.color(1, 1, 1, 1);
									GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE);
									GlStateManager.shadeModel(GL11.GL_SMOOTH);
									Tessellator tess = Tessellator.getInstance();
									GlStateManager.translate(0.5f, 0.5f, 0);
									VertexBuffer b = tess.getBuffer();
									for (float k = 0; k < 8; k ++){
										float coeff = (float)(k+1.0f)/8.0f;
										b.begin(GL11.GL_TRIANGLES, DefaultVertexFormats.POSITION_COLOR);
										RenderUtil.renderHighlightCircle(b,x+2.5f,y+2.5f,(1.0f+7.0f*coeff*coeff)*((float)l1/255f));
										tess.draw();
									}
									GlStateManager.shadeModel(GL11.GL_FLAT);
									GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
									GlStateManager.enableTexture2D();
									GlStateManager.color(1f, 1f, 1f, l1/255f);
									Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("embers:textures/gui/tidbit.png"));
									drawScaledCustomSizeModalRect((double)x, (double)y, 0, 0, 8.0f, 8.0f, 8.0, 8.0, 32.0f, 32.0f);
									GlStateManager.translate(-0.5f, -0.5f, 0);
									GlStateManager.depthMask(true);
									GlStateManager.alphaFunc(func, ref);
									GlStateManager.depthFunc(dfunc);
									GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
									GlStateManager.disableBlend();
									GlStateManager.disableAlpha();
								}
							}
	if(j<s.length()-player.getDisplayNameString().length()&&s.substring(j,j+player.getDisplayNameString().length()).compareTo(player.getDisplayNameString())==0)

	{
		String before = s.substring(0, j);
		float f = Minecraft.getMinecraft().gameSettings.chatOpacity * 0.9F + 0.1F;
		int j1 = updateCounter - l.getUpdatedCounter();
		if (j1 < 200 || c.getChatOpen()) {
			double d0 = j1 / 200.0D;
			d0 = 1.0D - d0;
			d0 = d0 * 10.0D;
			d0 = MathUtils.clamp(d0, 0.0D, 1.0D);
			d0 = d0 * d0;
			int l1 = (int) (255.0D * d0);

			if (c.getChatOpen()) {
				l1 = 255;
			}

			l1 = (int) (l1 * f);
			if ((20 * l1) / 255 > 3) {

				GlStateManager.enableAlpha();
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE);
				int dfunc = GL11.glGetInteger(GL11.GL_DEPTH_FUNC);
				GlStateManager.depthFunc(GL11.GL_LEQUAL);
				int func = GL11.glGetInteger(GL11.GL_ALPHA_TEST_FUNC);
				float ref = GL11.glGetFloat(GL11.GL_ALPHA_TEST_REF);
				GlStateManager.alphaFunc(GL11.GL_ALWAYS, 0);
				GlStateManager.depthMask(false);
				int x = chatX + 2 + Minecraft.getMinecraft().fontRendererObj.getStringWidth(before);
				int y = chatY - (Minecraft.getMinecraft().fontRendererObj.FONT_HEIGHT) * i;
				GlStateManager.color(255, 255, 255, 255);
				boolean isp455w0rd = player.getUniqueID().toString().equals("7b794048-f144-40bb-83dd-4789a5cf7cc8");
				if (ContributorUtils.isContributor(player)) {
					if (isp455w0rd) {
						GuiUtils.drawTextGlowingAuraTransparent(Minecraft.getMinecraft().fontRendererObj, player.getDisplayNameString(), x, y, l1);
					}
					else {
						GuiUtils.drawTextGlowingAuraTransparent(Minecraft.getMinecraft().fontRendererObj, player.getDisplayNameString(), x, y, l1, ContributorUtils.getWingType(player).getColor());
					}
				}
				GlStateManager.depthMask(true);
				GlStateManager.alphaFunc(func, ref);
				GlStateManager.depthFunc(dfunc);
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
				GlStateManager.disableBlend();
				GlStateManager.disableAlpha();
				GlStateManager.pushMatrix();
				GuiUtils.drawText(Minecraft.getMinecraft().fontRendererObj, player.getDisplayNameString(), x, y, ContributorUtils.isContributor(player) ? isp455w0rd ? 0xFF2B9BFF : ContributorUtils.getWingType(player).getColor().darker().getRGB() : 0xFFFFFFFF);
				GlStateManager.popMatrix();

			}
		}
	}}
	//System.out.println(s);
	}}catch(IllegalArgumentException|
	IllegalAccessException e)
	{
		e.printStackTrace();
	}}catch(NoSuchFieldException|
	SecurityException e)
	{
		e.printStackTrace();
	}
	}}*/

	// keep chest contents when harvested

	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent e) {
		if (ConfigOptions.ENABLE_SILKTOUCH_CHEST_HARVEST && e.getWorld() != null && e.getWorld().getTileEntity(e.getPos()) != null) {
			Block block = e.getWorld().getBlockState(e.getPos()).getBlock();
			if (!ChestUtils.isVanillaChest(block) && (Mods.IRONCHESTS.isLoaded() && !IronChests.isIronChest(block))) {
				return;
			}
			boolean doCancel = false;
			if (ChestUtils.isVanillaChest(block) && ChestUtils.canPlayerSilkHarvestChest(e.getWorld().getTileEntity(e.getPos()), e.getPlayer())) {
				Block.spawnAsEntity(e.getWorld(), e.getPos(), ChestUtils.getStackWithInventory(e.getWorld().getTileEntity(e.getPos())));
				doCancel = true;
			}
			else if (Mods.IRONCHESTS.isLoaded() && IronChests.isIronChest(block) && IronChests.canPlayerSilkHarvestChest(e.getWorld().getTileEntity(e.getPos()), e.getPlayer())) {
				Block.spawnAsEntity(e.getWorld(), e.getPos(), IronChests.getStackWithInventory(e.getWorld().getTileEntity(e.getPos())));
				doCancel = true;
			}
			if (doCancel) {
				//e.getWorld().markTileEntityForRemoval(e.getWorld().getTileEntity(e.getPos()));
				e.getWorld().removeTileEntity(e.getPos());
				e.getWorld().setBlockToAir(e.getPos());
				e.setCanceled(true);
			}
		}
	}

	// /dank/null

	@SubscribeEvent
	public void onItemPickUp(EntityItemPickupEvent e) {
		EntityPlayer player = e.getEntityPlayer();
		ItemStack entityStack = e.getItem().getEntityItem();
		if ((entityStack == null) || (player == null)) {
			return;
		}
		//for (int i = 0; i < player.inventory.getSizeInventory(); i++) {
		//	ItemStack stack = player.inventory.getStackInSlot(i);
		ItemStack dankNull = DankNullUtils.getDankNullForStack(player, entityStack);
		if (dankNull != null) {
			InventoryDankNull inventory = DankNullUtils.getInventoryFromStack(dankNull);//DankNullUtils.getInventoryFromHeld(player);
			if (inventory != null && (DankNullUtils.addFilteredStackToDankNull(inventory, entityStack))) {
				entityStack.stackSize = 0;
				return;
			}
		}
		if (FriendermanUtils.getFirstFriendermenWithStorageSpaceInRangeForStack(player, entityStack, 2.0D) != null) {
			e.setCanceled(true);
			e.setResult(Result.DENY);
		}
		//}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onMouseEvent(MouseEvent event) {
		EntityPlayer player = EasyMappings.player();
		ItemStack dankNullItem = null;
		/**
		if (player.getHeldItem(EnumHand.MAIN_HAND) != null) {
			if ((player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemDankNull)) {
				dankNullItem = player.getHeldItem(EnumHand.MAIN_HAND);
			}
			else if ((player.getHeldItem(EnumHand.OFF_HAND) != null) && ((player.getHeldItem(EnumHand.OFF_HAND).getItem() instanceof ItemDankNull))) {
				dankNullItem = player.getHeldItem(EnumHand.OFF_HAND);
			}
		}
		else if (player.getHeldItem(EnumHand.OFF_HAND) != null) {
			if ((player.getHeldItem(EnumHand.OFF_HAND).getItem() instanceof ItemDankNull)) {
				dankNullItem = player.getHeldItem(EnumHand.OFF_HAND);
			}
			else if ((player.getHeldItem(EnumHand.MAIN_HAND) != null) && ((player.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemDankNull))) {
				dankNullItem = player.getHeldItem(EnumHand.MAIN_HAND);
			}
		}
		*/
		dankNullItem = DankNullUtils.getDankNull(player);
		InventoryDankNull inventory = DankNullUtils.getInventoryFromHeld(player);
		if (dankNullItem == null || !DankNullUtils.isDankNull(dankNullItem)) {
			return;
		}
		if ((event.getDwheel() == 0) && (event.isButtonstate())) {
			int currentIndex = DankNullUtils.getSelectedStackIndex(inventory);
			int totalSize = DankNullUtils.getItemCount(inventory);
			if ((currentIndex == -1) || (totalSize <= 1)) {
				return;
			}
			if (event.getButton() == 3) {
				DankNullUtils.setNextSelectedStack(inventory, player);
				event.setCanceled(true);
			}
			else if (event.getButton() == 4) {
				DankNullUtils.setPreviousSelectedStack(inventory, player);
				event.setCanceled(true);
			}
		}
		else if (player.isSneaking()) {
			int currentIndex = DankNullUtils.getSelectedStackIndex(inventory);
			int totalSize = DankNullUtils.getItemCount(inventory);
			if ((currentIndex == -1) || (totalSize <= 1)) {
				return;
			}
			int scrollForward = event.getDwheel();
			if (scrollForward < 0) {
				DankNullUtils.setNextSelectedStack(inventory, player);
				event.setCanceled(true);
			}
			else if (scrollForward > 0) {
				DankNullUtils.setPreviousSelectedStack(inventory, player);
				event.setCanceled(true);
			}
		}
	}

	// charged entities (charged via armor upgrade)

	@SuppressWarnings("unchecked")
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void renderLivingBase(RenderLivingEvent.Pre<?> event) {
		RenderLivingBase<EntityLivingBase> renderer = (RenderLivingBase<EntityLivingBase>) event.getRenderer();
		List<LayerRenderer<EntityLivingBase>> layers = ReflectionHelper.getPrivateValue(RenderLivingBase.class, renderer, ReflectionUtils.determineSRG("layerRenderers"));
		boolean isChargeLayerAdded = false;
		boolean isEyesLayerAdded = false;
		for (LayerRenderer<EntityLivingBase> layer : layers) {
			if (layer instanceof LayerEntityCharge) {
				isChargeLayerAdded = true;
				continue;
			}
			if (layer instanceof LayerSkullEyes) {
				isEyesLayerAdded = true;
			}
		}
		if (!isChargeLayerAdded) {
			renderer.addLayer(new LayerEntityCharge(renderer, renderer.getMainModel()));
		}
		if (!isEyesLayerAdded) {
			renderer.addLayer(new LayerSkullEyes(renderer));
		}
		if (EntityUtils.isWearingCustomSkull(event.getEntity())) {
			if (renderer.getMainModel() instanceof ModelBiped) {
				ModelBiped bipedModel = (ModelBiped) renderer.getMainModel();
				if (!bipedModel.bipedHead.isHidden || !bipedModel.bipedHeadwear.isHidden) {
					bipedModel.bipedHead.isHidden = true;
					bipedModel.bipedHeadwear.isHidden = true;
				}
			}
		}
		else {
			if (renderer.getMainModel() instanceof ModelBiped) {
				ModelBiped bipedModel = (ModelBiped) renderer.getMainModel();
				if (bipedModel.bipedHead.isHidden || bipedModel.bipedHeadwear.isHidden) {
					bipedModel.bipedHead.isHidden = false;
					bipedModel.bipedHeadwear.isHidden = false;
				}
			}
		}
	}

	// sprites

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onTextureStitch(TextureStitchEvent event) {
		event.getMap().registerSprite(Textures.GLOW_FLAME);
		event.getMap().registerSprite(TESRInterchanger.INTERCHANGER_SPRITE);
		event.getMap().registerSprite(TESRMachineUpgrade.MACHINEUPGRADE_SPRITE);
		event.getMap().registerSprite(TESRDankNullDock.DOCK_SPRITE);
		//if (Mods.GUIDEAPI.isLoaded()) {
		event.getMap().registerSprite(new ResourceLocation(ModGlobals.MODID, "gui/github"));
		event.getMap().registerSprite(new ResourceLocation(ModGlobals.MODID, "gui/patreon"));
		//}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onRenderAfterWorld(RenderWorldLastEvent event) {
		ParticleRenderer.getInstance().renderParticles(EasyMappings.player(), event.getPartialTicks());
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onTooltip(ItemTooltipEvent event) {
		if (event.getItemStack() != null && event.getItemStack().getItem() instanceof IMachineUpgrade) {
			EntityPlayer player = EasyMappings.player();
			if (player != null) {
				IMachineUpgrade upgrade = (IMachineUpgrade) event.getItemStack().getItem();
				if (upgrade.getUpgradeType() == MachineUtils.Upgrades.SPEED) {
					if (player.openContainer != null && player.openContainer instanceof ContainerCompressor) {
						ItemStack speedStack = event.getItemStack();
						//List<String> tooltip = speedStack.getTooltip(mc.thePlayer, mc.gameSettings.advancedItemTooltips);
						event.getToolTip().add("Processes additional " + (50 * (5 * speedStack.stackSize)) + " RF/t");
						//speedStack.getItem().addInformation(speedStack, mc.thePlayer, tooltip, mc.gameSettings.advancedItemTooltips);
					}
				}
			}
		}
	}

	// hacky slab stuff

	@SubscribeEvent
	public void onInteract(PlayerInteractEvent.RightClickBlock e) {
		if (e.getItemStack() != null && !(e.getWorld().getBlockState(e.getPos()).getBlock() instanceof BlockSlabBase) && Block.getBlockFromItem(e.getItemStack().getItem()) != null && Block.getBlockFromItem(e.getItemStack().getItem()) instanceof BlockSlabBase) {
			BlockPos pos = e.getPos().offset(e.getFace());
			IBlockState state = e.getWorld().getBlockState(pos);
			if (state != null && state.getBlock() instanceof BlockSlabBase) {
				if (e.getWorld().setBlockState(pos, ((BlockSlabBase) state.getBlock()).getDoubleSlab().getDefaultState())) {
					if (!e.getEntityPlayer().isCreative()) {
						if (e.getItemStack().stackSize - 1 == 0) {
							e.getEntityPlayer().setHeldItem(EnumHand.MAIN_HAND, null);
						}
						else {
							e.getItemStack().stackSize -= 1;
						}
					}
					e.setCanceled(true);
				}
			}
		}
	}

	// Endervision upgrade/Enderman skulls/skele armor

	@SubscribeEvent
	public void onLiving(LivingEvent.LivingUpdateEvent e) {
		if (e.getEntityLiving() instanceof EntityEnderman) {
			EntityEnderman enderman = (EntityEnderman) e.getEntityLiving();
			if (enderman.getAITarget() == null) {
				MCPrivateUtils.setEndermanScreaming(enderman, false);
			}
		}
		else if (e.getEntityLiving() instanceof EntitySkeleton) {
			if (!ConfigOptions.SKELETONS_SPAWN_WITH_CARBON_ARMOR) {
				return;
			}
			if (ENTITY_LIST.contains(e.getEntityLiving())) {
				return;
			}
			ENTITY_LIST.add(e.getEntityLiving());
			EntitySkeleton skeleton = (EntitySkeleton) e.getEntityLiving();
			Random r = skeleton.getEntityWorld().rand;
			float chance1 = r.nextFloat();
			float chance2 = r.nextFloat();
			float chance3 = r.nextFloat();
			float chance4 = r.nextFloat();
			float chance5 = r.nextFloat();
			if (chance1 < 0.1F && skeleton.getItemStackFromSlot(EntityEquipmentSlot.HEAD) == null) {
				ItemStack helmet = ((ItemRFArmor) ModItems.EMERALD_CARBON_HELMET).setFull(new ItemStack(ModItems.EMERALD_CARBON_HELMET));
				skeleton.setItemStackToSlot(EntityEquipmentSlot.HEAD, helmet);
			}
			if (chance2 < 0.1F && skeleton.getItemStackFromSlot(EntityEquipmentSlot.CHEST) == null) {
				ItemStack chestplate = ((ItemRFArmor) ModItems.EMERALD_CARBON_CHESTPLATE).setFull(new ItemStack(ModItems.EMERALD_CARBON_CHESTPLATE));
				skeleton.setItemStackToSlot(EntityEquipmentSlot.CHEST, chestplate);
			}
			if (chance3 < 0.1F && skeleton.getItemStackFromSlot(EntityEquipmentSlot.LEGS) == null) {
				ItemStack chestplate = ((ItemRFArmor) ModItems.EMERALD_CARBON_LEGGINGS).setFull(new ItemStack(ModItems.EMERALD_CARBON_LEGGINGS));
				skeleton.setItemStackToSlot(EntityEquipmentSlot.LEGS, chestplate);
			}
			if (chance4 < 0.1F && skeleton.getItemStackFromSlot(EntityEquipmentSlot.FEET) == null) {
				ItemStack chestplate = ((ItemRFArmor) ModItems.EMERALD_CARBON_BOOTS).setFull(new ItemStack(ModItems.EMERALD_CARBON_BOOTS));
				skeleton.setItemStackToSlot(EntityEquipmentSlot.FEET, chestplate);
			}
			if (chance5 < 0.05F && skeleton.getSkeletonType() == SkeletonType.WITHER) {
				if (Mods.TINKERS.isLoaded() && skeleton.getHeldItem(EnumHand.MAIN_HAND) != null && skeleton.getHeldItem(EnumHand.MAIN_HAND).getItem() == Items.STONE_SWORD) {
					skeleton.setHeldItem(EnumHand.MAIN_HAND, TiC.getSword());
				}
			}
		}
	}

	@SubscribeEvent
	public void onTargetSelect(LivingSetAttackTargetEvent e) {
		if (e.getEntityLiving() instanceof EntityEnderman && e.getTarget() instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) e.getTarget();
			ItemStack stack = player.inventory.armorInventory[3];
			boolean stopAttack = false;
			if (stack != null) {
				if (stack.getItem() instanceof ItemEmeraldCarbonArmor && ArmorUtils.isUpgradeEnabled(stack, Upgrades.ENDERVISION)) {
					stopAttack = true;
				}
				else if (stack.getItem() instanceof ItemSkullBase) {
					ItemSkullBase skull = (ItemSkullBase) stack.getItem();
					if (skull.isEndermanSkull()) {
						stopAttack = true;
					}
				}
				else if (Mods.ENDERIO.isLoaded()) {
					if (ItemStack.areItemStacksEqual(stack, EnderIo.getEndermanSkull())) {
						stopAttack = true;
					}
				}
			}

			if (stopAttack) {
				((EntityLiving) e.getEntityLiving()).setAttackTarget(null);
			}
		}
	}
	/*
		@SideOnly(Side.CLIENT)
		@SubscribeEvent
		public void onModelBake(ModelBakeEvent e) {
			//e.getModelManager().getBlockModelShapes()
			ModBlocks.initStateMappers();
		}
	*/
	// shield & repulsive upgrade

	@SubscribeEvent
	public void attackEvent(LivingAttackEvent e) {
		float damage = e.getAmount();
		if (!(e.getEntityLiving() instanceof EntityPlayer)) {
			return;
		}
		EntityPlayer player = (EntityPlayer) e.getEntityLiving();
		if (player.getActiveItemStack() != null) {
			ItemStack activeItemStack = player.getActiveItemStack();
			if ((damage > 0.0F) && (activeItemStack != null) && ((activeItemStack.getItem() instanceof ItemShield))) {
				int i = 1 + MathUtils.floor(damage);
				activeItemStack.damageItem(i, player);
				if (activeItemStack.stackSize <= 0) {
					EnumHand enumhand = player.getActiveHand();
					ForgeEventFactory.onPlayerDestroyItem(player, activeItemStack, enumhand);
					if (enumhand == EnumHand.MAIN_HAND) {
						player.setItemStackToSlot(EntityEquipmentSlot.MAINHAND, (ItemStack) null);
					}
					else {
						player.setItemStackToSlot(EntityEquipmentSlot.OFFHAND, (ItemStack) null);
					}
					activeItemStack = null;
					if (FMLCommonHandler.instance().getSide() == Side.CLIENT) {
						player.playSound(SoundEvents.ITEM_SHIELD_BREAK, 0.8F, 0.8F + EasyMappings.world(player).rand.nextFloat() * 0.4F);
					}
				}
			}
		}
		if (e.getSource().getEntity() instanceof EntityLivingBase) {

			ItemStack chestplate = ItemUtils.getChestplate(player);
			if (chestplate != null && ArmorUtils.isEmeraldCarbonChestPlate(chestplate)) {
				if (ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.REPULSION)) {
					if (ArmorUtils.getRFStored(chestplate) > ConfigOptions.ENERGY_USE_REPULSION) {
						EntityLivingBase attacker = (EntityLivingBase) e.getSource().getEntity();
						ModNetworking.INSTANCE.sendToServer(new PacketSetEntityChargedS(player.getPosition().toLong(), player.dimension, attacker.getEntityId()));
						//ModRegistries.setCharged(attacker);
						Random r = player.getEntityWorld().rand;
						int dmg = r.nextInt(ConfigOptions.REPULSION_MAX_DAMAGE);
						if (dmg <= 0) {
							dmg = 1;
						}
						float calulatedDmg = (float) 2 * dmg;
						attacker.attackEntityFrom(DamageSource.causePlayerDamage(player), calulatedDmg);
						EntityUtils.knockBackEntity(player, attacker);
						ArmorUtils.drainRF(chestplate, ConfigOptions.ENERGY_USE_REPULSION);
						e.setCanceled(true);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onThrowable(ThrowableImpactEvent event) {
		if (!(event.getEntity() instanceof EntityPlayer)) {
			return;
		}
		EntityPlayer player = (EntityPlayer) event.getEntity();
		if (ItemUtils.getChestplate(player) == null) {
			return;
		}
		ItemStack chestplate = ItemUtils.getChestplate(player);
		if (ArmorUtils.isEmeraldCarbonChestPlate(chestplate)) {
			if (ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.REPULSION)) {
				if (ArmorUtils.getRFStored(chestplate) > ConfigOptions.ENERGY_USE_REPULSION) {
					if (event.getEntityThrowable() instanceof EntityPotion) {
						EntityPotion potion = (EntityPotion) event.getEntityThrowable();
						List<PotionEffect> effectList = net.minecraft.potion.PotionUtils.getEffectsFromStack(potion.getPotion());
						boolean isNegativePotion = false;
						for (PotionEffect effect : effectList) {
							if (effect.getPotion().isBadEffect()) {
								isNegativePotion = true;
							}
						}
						if (isNegativePotion) {
							EntityLivingBase attacker = event.getEntityThrowable().getThrower();
							ModNetworking.INSTANCE.sendToServer(new PacketSetEntityChargedS(player.getPosition().toLong(), player.dimension, attacker.getEntityId()));
							//ModRegistries.setCharged(attacker);
							Random r = player.getEntityWorld().rand;
							int dmg = r.nextInt(ConfigOptions.REPULSION_MAX_DAMAGE);
							if (dmg <= 0) {
								dmg = 1;
							}
							float calulatedDmg = (float) 2 * dmg;
							attacker.attackEntityFrom(DamageSource.causePlayerDamage(player), calulatedDmg);
							EntityUtils.knockBackEntity(player, attacker);
							ArmorUtils.drainRF(chestplate, ConfigOptions.ENERGY_USE_REPULSION);
							event.setCanceled(true);
						}
					}
				}
			}
		}
	}

	// config sync

	@SubscribeEvent
	public void onPlayerLogin(PlayerLoggedInEvent e) {
		if (e.player instanceof EntityPlayerMP) {
			ModLogger.info("Sending Configs to " + e.player.getDisplayName().getUnformattedText());
			Map<String, Object> configs = new HashMap<String, Object>();
			//configs.put("maxChunkLoaders", ConfigOptions.MAX_CHUNKLOADERS_PER_PLAYER);
			//configs.put("maxChunkLoaderRadius", ConfigOptions.CHUNKLOADER_MAXCHUNK_RADIUS);
			//configs.put("enableChunkLoader", ConfigOptions.ENABLE_CHUNK_LOADER);
			configs.put("disableEMC", ConfigOptions.DISABLE_EMC);
			configs.put("magnetRadius", ConfigOptions.MAGNET_RADIUS);
			configs.put("witherlessUpgrade", ConfigOptions.ENABLE_WITHERLESS_UPGRADE);
			configs.put("lavaWalkerUpgrade", ConfigOptions.ENABLE_LAVAWALKER_UPRGADE);
			configs.put("tic_bowExtraDamage", ConfigOptions.TIC_BOW_EXTRA_DAMAGE);
			configs.put("tic_bowRange", ConfigOptions.TIC_BOW_RANGE);
			configs.put("tic_miningSpeed", ConfigOptions.TIC_MINING_SPEED);
			configs.put("tic_fleshToLeather", ConfigOptions.ENABLE_TIC_FLESH_TO_LEATHER_RECIPE);
			configs.put("tic_leatherDrytime", ConfigOptions.TIC_LEATHER_DRY_TIME);
			configs.put("tic_extraBoltAmmo", ConfigOptions.TIC_EXTRA_BOLT_AMMO);
			configs.put("tic_bowDrawSpeed", ConfigOptions.TIC_BOW_DRAW_SPEED);
			configs.put("tic_baseDurability", ConfigOptions.TIC_BASE_DURABILITY);
			configs.put("tic_baseAttack", ConfigOptions.TIC_BASE_ATTACK);
			configs.put("tic_handleDurability", ConfigOptions.TIC_HANDLE_DURABILITY);
			configs.put("tic_extraDurability", ConfigOptions.TIC_EXTRA_DURABILITY);
			configs.put("disableVanillaRecipes", ConfigOptions.DISABLE_VANILLA_RECIPES);
			configs.put("silkHarvestChests", ConfigOptions.ENABLE_SILKTOUCH_CHEST_HARVEST);
			configs.put("enableHorseArmorRecipes", ConfigOptions.ENABLE_HORSE_ARMOR_RECIPES);
			ModNetworking.INSTANCE.sendTo(new PacketConfigSync(configs), (EntityPlayerMP) e.player);
			//ModLogger.info("Syncing Tamed Frienderman registry to " + e.player.getDisplayName().getUnformattedText());

			//Guide info sync
			GuideUtils.syncPlayerDataToClient(e.player);
		}

		//ModNetworking.INSTANCE.sendTo(new PacketFriendermanRegistrySync(ModRegistries.getTamedFriendermanRegistry()), (EntityPlayerMP) e.player);
	}

	// custom main menu stuff
	/*
		@SubscribeEvent(priority = EventPriority.HIGHEST)
		@SideOnly(Side.CLIENT)
		public void onGuiOpen(GuiScreenEvent.DrawScreenEvent.Post e) {
			boolean doAnim = ModGlobals.TIME >= 360;
			if (e.getGui() instanceof GuiMainMenu) {
				String newMsg = "";
				String defaultMsg = ReflectionHelper.getPrivateValue(GuiMainMenu.class, (GuiMainMenu) e.getGui(), "splashText", "field_73975_c", "i");
				Session session = Minecraft.getMinecraft().getSession();
				if (session != null && UUID_LIST.contains(session.getProfile().getId()) && !HAS_SPLASH_RUN) {
					newMsg = TextUtils.rainbow("Happy Birthday " + session.getUsername() + "!");
					HAS_SPLASH_RUN = true;
				}
				if (newMsg.isEmpty() && doAnim) {
					newMsg = defaultMsg;
					//newMsg = TextEffects.makeFabulous(defaultMsg);
				}

				ReflectionHelper.setPrivateValue(GuiMainMenu.class, (GuiMainMenu) e.getGui(), newMsg, "splashText", "field_73975_c", "i");
			}
		}

			@SubscribeEvent
			@SideOnly(Side.CLIENT)
			public void onWorldLoadClient(WorldEvent.Load e) {
				if (!HAS_TITLE_RUN) {
					Session session = Minecraft.getMinecraft().getSession();
					if (session != null) {
						if (UUID_LIST.contains(session.getProfile().getId())) {
							String msg = TextUtils.rainbow("Happy Birthday " + session.getUsername() + "!", false);
							RenderUtils.renderHighlightTextTimed(150, msg, 2.0f, 40);
						}
						else {
							RenderUtils.renderHighlightTextTimed(150, TextEffects.makeFabulous("Welcome " + session.getUsername() + "!"), 2.0f, 40);
						}
					}
					HAS_TITLE_RUN = true;
				}
			}

			@SubscribeEvent
			@SideOnly(Side.SERVER)
			public void onWorldLoadServer(WorldEvent.Load e) {
				for (Entity entity : ((WorldServer) e.getWorld()).getLoadedEntityList()) {
					if (entity instanceof EntityFrienderman) {
						EntityFrienderman frienderman = (EntityFrienderman) entity;
						if (frienderman.isTamed() && frienderman.getOwner() != null && frienderman.getOwner() instanceof EntityPlayer) {
							ModRegistries.registerTamedFrienderman((EntityPlayer) frienderman.getOwner(), frienderman);
						}
					}
				}
			}
		*/
	// acheivements

	@SubscribeEvent
	public void onCompressed(ItemCompressedEvent e) {
		if (e.getItem() == ModItems.RAW_CARBON) {
			ModAchievements.triggerAch(ModAchievements.carbonAch, e.getPlayer());
		}
		else if (e.getItem() == ModItems.CARBON_NANOTUBE) {
			ModAchievements.triggerAch(ModAchievements.nanoAch, e.getPlayer());
		}
		if (e.getItem() == ModItems.CARBONROD_REDSTONE) {
			ModAchievements.triggerAch(ModAchievements.redStoneRodAch, e.getPlayer());
		}
		else if (e.getItem() == ModItems.CARBONROD_LAPIS) {
			ModAchievements.triggerAch(ModAchievements.lapisRodAch, e.getPlayer());
		}
		else if (e.getItem() == ModItems.CARBONROD_IRON) {
			ModAchievements.triggerAch(ModAchievements.ironRodAch, e.getPlayer());
		}
		else if (e.getItem() == ModItems.CARBONROD_GOLD) {
			ModAchievements.triggerAch(ModAchievements.goldRodAch, e.getPlayer());
		}
		else if (e.getItem() == ModItems.CARBONROD_DIAMOND) {
			ModAchievements.triggerAch(ModAchievements.diamondRodAch, e.getPlayer());
		}
		else if (e.getItem() == ModItems.CARBONROD_EMERALD) {
			ModAchievements.triggerAch(ModAchievements.emeraldRodAch, e.getPlayer());
		}

	}

	@SubscribeEvent
	public void onCrafted(ItemCraftedEvent e) {
		if (e.crafting.getItem() instanceof ItemDankNull) {
			for (int i = 0; i < e.craftMatrix.getSizeInventory(); ++i) {
				if (e.craftMatrix.getStackInSlot(i) != null && e.craftMatrix.getStackInSlot(i).getItem() instanceof ItemDankNull) {
					NBTTagCompound oldCompound = e.craftMatrix.getStackInSlot(i).getTagCompound();
					e.crafting.setTagCompound(oldCompound);
					break;
				}
				ItemUtils.setItem(e.crafting, e.crafting.getItem());
			}
		}
	}

	// mob drops

	@SubscribeEvent
	public void onMobDrop(LivingDropsEvent event) {
		if (event.getSource().getSourceOfDamage() instanceof EntityLivingBase) { //only respect death by living entity
			World world = event.getEntity().getEntityWorld();
			double x = event.getEntity().posX;
			double y = event.getEntity().posY;
			double z = event.getEntity().posZ;
			EntityLivingBase attacker = (EntityLivingBase) event.getSource().getSourceOfDamage();
			if ((event.getEntity() instanceof EntityDragon)) {
				ItemStack emeraldPane = new ItemStack(ModItems.PANEL_EMERALD, 4);
				ItemStack frienderPearls = new ItemStack(ModItems.FRIENDER_PEARL, 16);
				EntityItem drop = new EntityItem(world, x, y, z, emeraldPane);
				EntityItem drop2 = new EntityItem(world, x, y, z, frienderPearls);
				event.getDrops().add(drop);
				event.getDrops().add(drop2);
			}
			else if ((event.getEntity() instanceof EntityWither)) {
				double randNumber = Math.random();
				double d = randNumber * 100.0D;
				int n = (int) d;
				if (n == 50) {
					ItemStack carbonBlocks = new ItemStack(ModBlocks.CARBON_BLOCK, 3);
					EntityItem drop = new EntityItem(world, x + 5.0D, y + 2.0D, z + 5.0D, carbonBlocks);
					event.getDrops().add(drop);
				}
			}
			else if (event.getEntity() instanceof EntityEnderman || event.getEntity() instanceof EntityFrienderman) {
				if (!(event.getEntity() instanceof EntityFrienderman)) {
					ItemStack frienderPearls = new ItemStack(ModItems.FRIENDER_PEARL, MathUtils.getRandom(1, 3));
					double r = world.rand.nextDouble();
					if (r <= (0.05D * (event.getLootingLevel() * 5))) {
						event.getDrops().add(new EntityItem(world, x, y, z, frienderPearls));
					}
				}
				ItemStack skullDrop = EntityUtils.getSkullDrop(event.getEntityLiving(), Mods.TINKERS.isLoaded());

				if (attacker != null && attacker instanceof EntityLivingBase && skullDrop != null) {
					double r = Math.random();
					ItemStack attackItem = attacker.getHeldItemMainhand();
					if (attackItem != null) {
						EntityItem skullEntity = new EntityItem(world, x, y, z, skullDrop);
						if (Mods.TINKERS.isLoaded() && TiC.isTinkersItem(attackItem)) { // has beheading, so we ignore looting..might change in the future to calculate both
							if (TiC.hasBeheading(attackItem)) {
								double beheadingLevel = 0.0D;
								if (TiC.hasModifier(attackItem, TiCModifiers.MOD_BEHEADING_CLEAVER)) {
									beheadingLevel += TiC.getModifierLevel(attackItem, TiCModifiers.MOD_BEHEADING_CLEAVER);
								}
								if (TiC.hasModifier(attackItem, TiCModifiers.MOD_BEHEADING)) {
									beheadingLevel += TiC.getModifierLevel(attackItem, TiCModifiers.MOD_BEHEADING);
								}
								beheadingLevel = beheadingLevel / 10.0D;
								if (r <= beheadingLevel) {
									event.getDrops().add(skullEntity);
								}
							}
							else if (TiC.hasLuck(attackItem)) {
								double luckLevel = TiC.getModifierLevel(attackItem, TiCModifiers.MOD_LUCK);
								if (r <= 0.05D * (luckLevel * 5)) {
									event.getDrops().add(skullEntity);
								}
							}
						}
						else { //not a TiC item, so calculate looting
							if (r <= (0.05D * (event.getLootingLevel() * 5))) {
								event.getDrops().add(skullEntity);
							}
						}
					}
				}
			}
			else if (event.getEntity() instanceof EntityVillager) {
				int count = world.rand.nextInt(4);
				if (count > 0) {
					event.getDrops().add(new EntityItem(world, x, y, z, new ItemStack(ModItems.BIT_EMERALD, count)));
				}
			}
		}
	}

	@SubscribeEvent
	public void onLootTablesLoaded(LootTableLoadEvent event) {
		if (event.getName().equals(LootTableList.ENTITIES_WITHER_SKELETON)) {
			if (!ConfigOptions.WITHER_SKELES_DROP_RAW_CARBON) {
				return;
			}
			LootPool mainPool = event.getTable().getPool("main");
			if (mainPool != null) {
				mainPool.addEntry(new LootEntryItem(ModItems.RAW_CARBON, 1, 0, new LootFunction[] {
						new SetCount(new LootCondition[0], new RandomValueRange(0.0F, 2))
				}, new LootCondition[0], "p455w0rdsthings:wither_skeleton_drop"));
			}
			else {
				return;
			}
		}
		else if ((event.getName().equals(LootTableList.CHESTS_ABANDONED_MINESHAFT)) || (event.getName().equals(LootTableList.CHESTS_SIMPLE_DUNGEON)) || (event.getName().equals(LootTableList.CHESTS_DESERT_PYRAMID)) || (event.getName().equals(LootTableList.CHESTS_NETHER_BRIDGE)) || (event.getName().equals(LootTableList.CHESTS_STRONGHOLD_LIBRARY)) || (event.getName().equals(LootTableList.CHESTS_END_CITY_TREASURE))) {
			LootPool mainPool = event.getTable().getPool("main");
			if (mainPool != null) {
				if (event.getName().equals(LootTableList.CHESTS_ABANDONED_MINESHAFT) || event.getName().equals(LootTableList.CHESTS_NETHER_BRIDGE) || event.getName().equals(LootTableList.CHESTS_SIMPLE_DUNGEON)) {
					mainPool.addEntry(new LootEntryItem(ModItems.HORSE_ARMOR_EMERALD, 10, 0, new LootFunction[] {}, new LootCondition[0], "p455w0rdsthings:emerald_horse_armor_loot"));
					mainPool.addEntry(new LootEntryItem(ModItems.HORSE_ARMOR_CARBON, 10, 0, new LootFunction[] {}, new LootCondition[0], "p455w0rdsthings:carbon_horse_armor_loot"));
					mainPool.addEntry(new LootEntryItem(ModItems.FRIENDER_PEARL, 10, 0, new LootFunction[] {}, new LootCondition[0], "p455w0rdsthings:friender_pearl_loot"));
				}
				mainPool.addEntry(new LootEntryItem(ModItems.RAW_CARBON, 10, 0, new LootFunction[] {
						new SetCount(new LootCondition[0], new RandomValueRange(1.0F, 3.0F))
				}, new LootCondition[0], "p455w0rdsthings:carbon_loot"));
			}
		}

	}

	// rainbow colors

	@SubscribeEvent
	public void tickEvent(TickEvent e) {
		/*
		if (e.type == Type.SERVER) {
			List<EntityFrienderman> friendermanList = Lists.newArrayList();
			MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			if (server != null) {
				for (WorldServer world : server.worlds) {
					for (Entity entity : world.getLoadedEntityList()) {
						if (entity instanceof EntityFrienderman) {
							friendermanList.add((EntityFrienderman) entity);
						}
					}
				}
				for (EntityFrienderman frienderman : friendermanList) {
					if (frienderman.isTamed() && frienderman.getOwner() != null && !frienderman.isSitting()) {
						if (frienderman.dimension != frienderman.getOwner().dimension) {
							TeleportUtils.teleportEntity(frienderman, frienderman.getOwner().dimension, frienderman.getOwner().posX + 3, frienderman.getOwner().posY + 1, frienderman.getOwner().posZ + 3);
						}
					}
				}
			}
		}
		*/

		ModGlobals.TIME_LONG++;
		if (ModGlobals.TIME % 0.5 == 0) {
			ModGlobals.TIME2++;
		}
		if (ModGlobals.TIME2 > 360) {
			ModGlobals.TIME2 = 0;
		}
		if (ModGlobals.TURN == 0) {
			ModGlobals.GREEN++;
			if (ModGlobals.GREEN == 255) {
				ModGlobals.TURN = 1;
			}
		}
		if (ModGlobals.TURN == 1) {
			ModGlobals.RED--;
			if (ModGlobals.RED == 0) {
				ModGlobals.TURN = 2;
			}
		}
		if (ModGlobals.TURN == 2) {
			ModGlobals.BLUE++;
			if (ModGlobals.BLUE == 255) {
				ModGlobals.TURN = 3;
			}
		}
		if (ModGlobals.TURN == 3) {
			ModGlobals.GREEN--;
			if (ModGlobals.GREEN == 0) {
				ModGlobals.TURN = 4;
			}
		}
		if (ModGlobals.TURN == 4) {
			ModGlobals.RED++;
			if (ModGlobals.RED == 255) {
				ModGlobals.TURN = 5;
			}
		}
		if (ModGlobals.TURN == 5) {
			ModGlobals.BLUE--;
			if (ModGlobals.BLUE == 0) {
				ModGlobals.TURN = 0;
			}
		}
	}

	// armor stuff

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void guiMouse(MouseInputEvent.Pre event) {
		if ((event.getGui() instanceof GuiContainer)) {
			GuiContainer gui = (GuiContainer) event.getGui();
			EntityPlayer player = EasyMappings.player();
			if (player.capabilities.isFlying) {
				ItemStack chestPlate = ItemUtils.getChestplate(player);
				if (chestPlate != null && (chestPlate.getItem() instanceof ItemEmeraldCarbonJetPlate || chestPlate.getItem() instanceof ItemJetPack)) {
					if (gui != null && gui.getSlotUnderMouse() != null && gui.getSlotUnderMouse().getSlotIndex() == 38) {
						event.setCanceled(true);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onLivingFallEvent(LivingFallEvent event) {
		if ((event.getEntity() != null) && ((event.getEntity() instanceof EntityPlayer))) {
			EntityPlayer player = (EntityPlayer) event.getEntity();
			if ((player.capabilities.allowFlying) || (player.capabilities.isFlying)) {
				ItemStack chestPlate = ItemUtils.getChestplate(player);
				if (chestPlate.getItem() instanceof IFlightItem) {
					if (ArmorUtils.isUpgradeEnabled(chestPlate, Upgrades.FLIGHT)) {
						event.setCanceled(true);
					}
				}
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void getKeyPress(TickEvent.ClientTickEvent event) {
		if (EasyMappings.world() == null) {
			return;
		}
		ParticleRenderer.getInstance().updateParticles();
		EntityPlayer player = EasyMappings.player();
		if (ModKeyBindings.KEYBIND_NIGHTVISION_TOGGLE.getKeyCode() != Keyboard.CHAR_NONE) {
			if (ModKeyBindings.KEYBIND_NIGHTVISION_TOGGLE.isPressed()) {
				boolean isActive = ModRegistries.isNVEnabled(player);
				ModRegistries.setNV(player, !isActive);
				ModNetworking.INSTANCE.sendToServer(new PacketSetNightVision(!isActive, player.getUniqueID().toString()));
				if (isActive == true && PotionUtils.isPotionActive(player, MobEffects.NIGHT_VISION)) {
					PotionUtils.clearPotionEffect(player, MobEffects.NIGHT_VISION, 1);
				}
			}
		}
		else if (ModKeyBindings.KEYBIND_EXPORT_ITEM_IMAGE.isPressed()) {
			ImageUtils.saveCurrentItem();
		}
	}

	@SubscribeEvent
	public void onPlayerTick(TickEvent.PlayerTickEvent e) {
		EntityPlayer player = e.player;
		FlightHandler handler = new FlightHandler().getHandler(player);
		doFlightTick(handler, player);
		if (FMLCommonHandler.instance().getSide().isServer()) {
			return;
		}
		doNVTick(player);
	}

	private void doNVTick(EntityPlayer player) {
		if (FMLCommonHandler.instance().getSide().isServer()) {
			return;
		}
		if (ModRegistries.isNVEnabled(player)) {
			ItemStack headStack = ItemUtils.getHelmet(player);
			if (headStack != null && ((headStack.getItem() == ModItems.NIGHT_VISION_GOGGLES) || (headStack.getItem() == ModItems.EMERALD_CARBON_HELMET && ArmorUtils.hasUpgrade(headStack, Upgrades.NIGHTVISION)))) {
				Item headItem = headStack.getItem();
				PotionUtils.effectPlayer(player, MobEffects.NIGHT_VISION, 1000);
				if (!player.isSpectator() && !player.capabilities.isCreativeMode) {
					((IEnergyContainerItem) headItem).extractEnergy(headStack, ConfigOptions.NV_GOGGLES_RF, false);
					return;
				}
			}
			else if (Mods.BAUBLES.isLoaded() && Baubles.getHeadBaubleItem(player) != null && Baubles.getHeadBaubleItem(player) == ModItems.NIGHT_VISION_GOGGLES) {
				PotionUtils.effectPlayer(player, MobEffects.NIGHT_VISION, 1000);
				if (!player.isSpectator() && !player.capabilities.isCreativeMode) {
					((IEnergyContainerItem) Baubles.getHeadBaubleItem(player)).extractEnergy(Baubles.getHeadBaubleStack(player), ConfigOptions.NV_GOGGLES_RF, false);
					return;
				}
			}
			else {
				ModRegistries.setNV(player, false);
				if (PotionUtils.isPotionActive(player, MobEffects.NIGHT_VISION)) {
					PotionUtils.clearPotionEffect(player, MobEffects.NIGHT_VISION, 1);
					ModNetworking.INSTANCE.sendToServer(new PacketSetNightVision(false, player.getUniqueID().toString()));
				}
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void renderOverlayEvent(RenderGameOverlayEvent e) {
		if ((ModGlobals.GUI_DANKNULL_ISOPEN) && ((e.getType() == RenderGameOverlayEvent.ElementType.HOTBAR) || (e.getType() == RenderGameOverlayEvent.ElementType.CROSSHAIRS) || (e.getType() == RenderGameOverlayEvent.ElementType.EXPERIENCE) || (e.getType() == RenderGameOverlayEvent.ElementType.FOOD) || (e.getType() == RenderGameOverlayEvent.ElementType.HEALTH) || (e.getType() == RenderGameOverlayEvent.ElementType.ARMOR))) {
			e.setCanceled(true);
		}

	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onPostRenderOverlay(RenderGameOverlayEvent.Post e) {
		if (e.getType() == ElementType.SUBTITLES) {
			RenderUtils.renderHighlightTextTimed();

			EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
			if (Minecraft.getMinecraft().playerController.gameIsSurvivalOrAdventure()) {

				FlightHandler handler = new FlightHandler().getHandler(player);
				if (handler != null && handler.isFlightItem) {
					if (!player.onGround && player.capabilities.isFlying) {
						TextFormatting color = TextFormatting.BOLD;
						if (handler.getEnergyPercent() > 7 && handler.getEnergyPercent() < 12) {
							color = TextFormatting.GOLD;
							RenderUtils.renderHighlightText(74, color + "" + I18n.format("info.flight.energylow", handler.getFlightItemString()));
							RenderUtils.renderHighlightText(60, color + "(" + handler.getEnergyPercent() + "% Remaining)");
						}
						if (handler.getEnergyPercent() <= 7) {
							color = TextFormatting.RED;
							RenderUtils.renderHighlightText(74, color + "" + TextFormatting.BOLD + "" + I18n.format("info.flight.energydangerouslylow", handler.getFlightItemString()));
							RenderUtils.renderHighlightText(60, color + "(" + handler.getEnergyPercent() + "% Remaining)");
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onJoin(EntityJoinWorldEvent e) {
		if (e.getEntity() instanceof EntityPlayer && e.getEntity() == EasyMappings.player()) {
			ModGuide.init();
		}
	}

	public static void doFlightTick(FlightHandler handler, EntityPlayer player) {
		int energyPerTick = ConfigOptions.ENERGY_USE_FLIGHT;
		if (handler != null && handler.isFlightItem) {
			ModRegistries.setFlyingPlayer(player, true);
			if (handler.getEnergy() >= energyPerTick) {
				player.capabilities.allowFlying = true;
				if (!player.onGround && player.capabilities.isFlying) {
					if (!player.isSpectator() && !player.capabilities.isCreativeMode) {
						handler.drainEnergy(energyPerTick);
					}
				}
			}
			if (handler.getEnergy() < energyPerTick) {
				if (ModRegistries.isFlightEnabled(player) && !EasyMappings.world(player).isRemote) {
					ModRegistries.setFlyingPlayer(player, false);
					if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
						player.capabilities.allowFlying = false;
						player.capabilities.isFlying = false;
						player.sendPlayerAbilities();
					}
				}
				if (EasyMappings.world(player).isRemote && ModRegistries.isFlightEnabled(player)) {
					ModRegistries.setFlyingPlayer(player, false);
					if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
						player.capabilities.allowFlying = false;
						player.capabilities.isFlying = false;
					}
				}
			}
		}
		else {
			if (!ModRegistries.isPlayerCached(player)) {
				ModRegistries.setFlyingPlayer(player, false);
			}
			if (ModRegistries.isFlightEnabled(player) && !EasyMappings.world(player).isRemote) {
				ModRegistries.setFlyingPlayer(player, false);
				if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
					player.capabilities.allowFlying = false;
					player.capabilities.isFlying = false;
					player.sendPlayerAbilities();
				}
			}
			if (EasyMappings.world(player).isRemote && ModRegistries.isFlightEnabled(player)) {
				ModRegistries.setFlyingPlayer(player, false);
				if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
					player.capabilities.allowFlying = false;
					player.capabilities.isFlying = false;
				}
			}
		}
	}

	public static class FlightHandler {

		boolean isFlightItem = false;
		public ItemStack[] armorStacks;
		public ItemStack flightItem = null;

		public FlightHandler getHandler(EntityPlayer player) {
			ItemStack[] armorSlots = player.inventory.armorInventory;
			armorStacks = new ItemStack[armorSlots.length];

			for (int i = 0; i < armorSlots.length; i++) {
				ItemStack stack = armorSlots[i];
				if (stack != null && (stack.getItem() instanceof IFlightItem) && i == 2) {
					isFlightItem = true;
					flightItem = stack;
				}
			}
			return this;
		}

		public Item getFlightItem() {
			return flightItem.getItem();
		}

		public ItemStack getFlightItemStack() {
			return flightItem;
		}

		public int getEnergy() {
			IEnergyContainerItem item = (IEnergyContainerItem) getFlightItem();
			return item.getEnergyStored(getFlightItemStack());
		}

		public int getMaxEnergy() {
			IEnergyContainerItem item = (IEnergyContainerItem) getFlightItem();
			return item.getMaxEnergyStored(getFlightItemStack());
		}

		public int getEnergyPercent() {
			return (getEnergy() * 100) / getMaxEnergy();
		}

		public void drainEnergy(int amount) {
			IEnergyContainerItem item = (IEnergyContainerItem) getFlightItem();
			item.extractEnergy(getFlightItemStack(), amount, false);
		}

		public String getFlightItemString() {
			if (getFlightItem() instanceof ItemEmeraldCarbonJetPlate) {
				return "JetPlate";
			}
			if (getFlightItem() instanceof ItemJetPack) {
				return "JetPack";
			}
			return "";
		}

	}

}
