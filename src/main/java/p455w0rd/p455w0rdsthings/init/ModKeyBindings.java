package p455w0rd.p455w0rdsthings.init;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;

/**
 * @author p455w0rd
 *
 */
public class ModKeyBindings {

	public static KeyBinding KEYBIND_CHUNKOVERLAY, KEYBIND_NIGHTVISION_TOGGLE, KEYBIND_EXPORT_ITEM_IMAGE;

	public static void init() {
		KEYBIND_CHUNKOVERLAY = new KeyBinding("key.showChunkBorders", Keyboard.KEY_F9, "key.categories.p455w0rdsthings");
		KEYBIND_NIGHTVISION_TOGGLE = new KeyBinding("key.toggleNightVision", Keyboard.CHAR_NONE, "key.categories.p455w0rdsthings");
		KEYBIND_EXPORT_ITEM_IMAGE = new KeyBinding("key.exportItemImage", Keyboard.CHAR_NONE, "key.categories.p455w0rdsthings");
		ClientRegistry.registerKeyBinding(KEYBIND_CHUNKOVERLAY);
		ClientRegistry.registerKeyBinding(KEYBIND_NIGHTVISION_TOGGLE);
		ClientRegistry.registerKeyBinding(KEYBIND_EXPORT_ITEM_IMAGE);
	}

}
