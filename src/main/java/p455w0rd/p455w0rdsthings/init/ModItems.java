package p455w0rd.p455w0rdsthings.init;

import static p455w0rd.p455w0rdsthings.init.ModMaterials.CARBON_ARMOR_MATERIAL;
import static p455w0rd.p455w0rdsthings.init.ModMaterials.JETPACK_MATERIAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rd.p455w0rdsthings.items.ItemArmorUpgradeBase;
import p455w0rd.p455w0rdsthings.items.ItemBatteryAdvanced;
import p455w0rd.p455w0rdsthings.items.ItemBatteryBasic;
import p455w0rd.p455w0rdsthings.items.ItemCarbon;
import p455w0rd.p455w0rdsthings.items.ItemCarbonArmor;
import p455w0rd.p455w0rdsthings.items.ItemCarbonHarness;
import p455w0rd.p455w0rdsthings.items.ItemCarbonIngot;
import p455w0rd.p455w0rdsthings.items.ItemCarbonRod;
import p455w0rd.p455w0rdsthings.items.ItemCarbonShield;
import p455w0rd.p455w0rdsthings.items.ItemCarbonThruster;
import p455w0rd.p455w0rdsthings.items.ItemCompressor;
import p455w0rd.p455w0rdsthings.items.ItemDankNull;
import p455w0rd.p455w0rdsthings.items.ItemDankNullHolder;
import p455w0rd.p455w0rdsthings.items.ItemDankNullPanel;
import p455w0rd.p455w0rdsthings.items.ItemDiamondBit;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldBit;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonJetPlate;
import p455w0rd.p455w0rdsthings.items.ItemFrienderPearl;
import p455w0rd.p455w0rdsthings.items.ItemGuide;
import p455w0rd.p455w0rdsthings.items.ItemHorseArmor;
import p455w0rd.p455w0rdsthings.items.ItemJetPack;
import p455w0rd.p455w0rdsthings.items.ItemMagnet;
import p455w0rd.p455w0rdsthings.items.ItemNightVisionGoggles;
import p455w0rd.p455w0rdsthings.items.ItemPArmorStand;
import p455w0rd.p455w0rdsthings.items.ItemSkullBase;
import p455w0rd.p455w0rdsthings.util.HorseUtils;

public class ModItems {

	private static final List<Item> ITEM_LIST = new ArrayList<Item>();

	public static final ItemDankNull DANK_NULL = new ItemDankNull();
	public static final ItemDankNullHolder DANK_NULL_HOLDER = new ItemDankNullHolder();
	public static final ItemCompressor COMPRESSOR = new ItemCompressor();
	public static final ItemCarbon RAW_CARBON = new ItemCarbon();
	public static final ItemCarbonRod CARBON_NANOTUBE = new ItemCarbonRod(0);
	public static final ItemCarbonRod CARBONROD_REDSTONE = new ItemCarbonRod(1);
	public static final ItemCarbonRod CARBONROD_LAPIS = new ItemCarbonRod(2);
	public static final ItemCarbonRod CARBONROD_IRON = new ItemCarbonRod(3);
	public static final ItemCarbonRod CARBONROD_GOLD = new ItemCarbonRod(4);
	public static final ItemCarbonRod CARBONROD_DIAMOND = new ItemCarbonRod(5);
	public static final ItemCarbonRod CARBONROD_EMERALD = new ItemCarbonRod(6);
	public static final ItemDankNullPanel PANEL_REDSTONE = new ItemDankNullPanel(0);
	public static final ItemDankNullPanel PANEL_LAPIS = new ItemDankNullPanel(1);
	public static final ItemDankNullPanel PANEL_IRON = new ItemDankNullPanel(2);
	public static final ItemDankNullPanel PANEL_GOLD = new ItemDankNullPanel(3);
	public static final ItemDankNullPanel PANEL_DIAMOND = new ItemDankNullPanel(4);
	public static final ItemDankNullPanel PANEL_EMERALD = new ItemDankNullPanel(5);
	public static final ItemCarbonArmor CARBON_HELMET = new ItemCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.HEAD, "carbon_helmet");
	public static final ItemCarbonArmor CARBON_CHESTPLATE = new ItemCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.CHEST, "carbon_chestplate");
	public static final ItemCarbonArmor CARBON_LEGGINGS = new ItemCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.LEGS, "carbon_leggings");
	public static final ItemCarbonArmor CARBON_BOOTS = new ItemCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.FEET, "carbon_boots");

	public static final ItemEmeraldCarbonArmor EMERALD_CARBON_HELMET = new ItemEmeraldCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.HEAD, "emerald_carbon_helmet");
	public static final ItemEmeraldCarbonArmor EMERALD_CARBON_CHESTPLATE = new ItemEmeraldCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.CHEST, "emerald_carbon_chestplate");
	public static final ItemEmeraldCarbonArmor EMERALD_CARBON_LEGGINGS = new ItemEmeraldCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.LEGS, "emerald_carbon_leggings");
	public static final ItemEmeraldCarbonArmor EMERALD_CARBON_BOOTS = new ItemEmeraldCarbonArmor(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.FEET, "emerald_carbon_boots");
	public static final ItemEmeraldCarbonJetPlate EMERALD_CARBON_JETPLATE = new ItemEmeraldCarbonJetPlate(CARBON_ARMOR_MATERIAL, EntityEquipmentSlot.CHEST, "emerald_carbon_jetplate");

	public static final ItemCarbonShield CARBON_SHEILD = new ItemCarbonShield();
	public static final ItemMagnet MAGNET = new ItemMagnet();
	public static final ItemHorseArmor HORSE_ARMOR_EMERALD = new ItemHorseArmor("emerald", 1024);
	public static final ItemHorseArmor HORSE_ARMOR_CARBON = new ItemHorseArmor("carbon", 2048);
	public static final ItemHorseArmor HORSE_ARMOR_LAPIS = new ItemHorseArmor("lapis", 64);
	public static final ItemHorseArmor HORSE_ARMOR_REDSTONE = new ItemHorseArmor("redstone", 128);
	public static final ItemHorseArmor HORSE_ARMOR_ARDITE = new ItemHorseArmor("ardite", 640);
	public static final ItemHorseArmor HORSE_ARMOR_COBALT = new ItemHorseArmor("cobalt", 768);
	public static final ItemHorseArmor HORSE_ARMOR_MANYULLYN = new ItemHorseArmor("manyullyn", 1024);
	public static final ItemPArmorStand ARMOR_STAND = new ItemPArmorStand();
	public static final ItemJetPack JETPACK = new ItemJetPack(JETPACK_MATERIAL);
	public static final ItemNightVisionGoggles NIGHT_VISION_GOGGLES = new ItemNightVisionGoggles(JETPACK_MATERIAL);
	public static final ItemCarbonThruster CARBON_THRUSTER = new ItemCarbonThruster();
	public static final ItemCarbonIngot CARBON_INGOT = new ItemCarbonIngot();
	public static final ItemCarbonHarness CARBON_HARNESS = new ItemCarbonHarness();
	public static final ItemBatteryBasic BASIC_BATTERY = new ItemBatteryBasic();
	public static final ItemBatteryAdvanced ADVANCED_BATTERY = new ItemBatteryAdvanced();
	public static final ItemArmorUpgradeBase ARMOR_UPGRADE_BASE = new ItemArmorUpgradeBase();
	public static final ItemArmorUpgradeBase.UpgradeStepAssist ARMOR_UPGRADE_STEPASSIST = new ItemArmorUpgradeBase.UpgradeStepAssist();
	public static final ItemArmorUpgradeBase.UpgradeNightVision ARMOR_UPGRADE_NIGHTVISION = new ItemArmorUpgradeBase.UpgradeNightVision();
	public static final ItemArmorUpgradeBase.UpgradeSpeed ARMOR_UPGRADE_SPEED = new ItemArmorUpgradeBase.UpgradeSpeed();
	public static final ItemArmorUpgradeBase.UpgradeLavaWalker ARMOR_UPGRADE_LAVAWALKER = new ItemArmorUpgradeBase.UpgradeLavaWalker();
	public static final ItemArmorUpgradeBase.UpgradeFlight ARMOR_UPGRADE_FLIGHT = new ItemArmorUpgradeBase.UpgradeFlight();
	public static final ItemArmorUpgradeBase.UpgradeUnwithering ARMOR_UPGRADE_UNWITHERING = new ItemArmorUpgradeBase.UpgradeUnwithering();
	public static final ItemArmorUpgradeBase.UpgradeEnderVision ARMOR_UPGRADE_ENDERVISION = new ItemArmorUpgradeBase.UpgradeEnderVision();
	public static final ItemArmorUpgradeBase.UpgradeRepulsion ARMOR_UPGRADE_REPULSION = new ItemArmorUpgradeBase.UpgradeRepulsion();
	public static final ItemFrienderPearl FRIENDER_PEARL = new ItemFrienderPearl();
	public static final ItemEmeraldBit BIT_EMERALD = new ItemEmeraldBit();
	public static final ItemDiamondBit BIT_DIAMOND = new ItemDiamondBit();
	public static final ItemSkullBase.Enderman SKULL_ENDERMAN = new ItemSkullBase.Enderman();
	public static final ItemSkullBase.Frienderman SKULL_FRIENDERMAN = new ItemSkullBase.Frienderman();
	public static final ItemSkullBase.Enderman2 SKULL_ENDERMAN2 = new ItemSkullBase.Enderman2();
	public static final ItemGuide GUIDE = new ItemGuide();
	//public static final ItemMachineUpgradeBase.SpeedUpgrade MACHINEUPGRADE_SPEED = new ItemMachineUpgradeBase.SpeedUpgrade();
	public static final EnumRarity RARITY_EMERALD = EnumHelper.addRarity("EMERALD", TextFormatting.GREEN, "EMERALD");

	public static void init() {
		long millis = System.currentTimeMillis() % 1000;
		ModLogger.info("Registering Items");

		ITEM_LIST.addAll(Arrays.asList(DANK_NULL, DANK_NULL_HOLDER, GUIDE, RAW_CARBON, CARBON_NANOTUBE, CARBONROD_REDSTONE, CARBONROD_LAPIS, CARBONROD_IRON, CARBONROD_GOLD, CARBONROD_DIAMOND, CARBONROD_EMERALD, PANEL_REDSTONE, PANEL_LAPIS, PANEL_IRON, PANEL_GOLD, PANEL_DIAMOND, PANEL_EMERALD, CARBON_HELMET, CARBON_CHESTPLATE, CARBON_LEGGINGS, CARBON_BOOTS, BIT_EMERALD, BIT_DIAMOND, SKULL_ENDERMAN, SKULL_FRIENDERMAN, SKULL_ENDERMAN2, COMPRESSOR, EMERALD_CARBON_HELMET, EMERALD_CARBON_CHESTPLATE, EMERALD_CARBON_LEGGINGS, EMERALD_CARBON_BOOTS, EMERALD_CARBON_JETPLATE, CARBON_SHEILD, MAGNET, ARMOR_STAND, JETPACK, NIGHT_VISION_GOGGLES, CARBON_THRUSTER, CARBON_INGOT, CARBON_HARNESS, BASIC_BATTERY, ADVANCED_BATTERY, ARMOR_UPGRADE_BASE, ARMOR_UPGRADE_STEPASSIST, ARMOR_UPGRADE_NIGHTVISION, ARMOR_UPGRADE_SPEED, ARMOR_UPGRADE_LAVAWALKER, ARMOR_UPGRADE_FLIGHT, ARMOR_UPGRADE_UNWITHERING, ARMOR_UPGRADE_ENDERVISION, ARMOR_UPGRADE_REPULSION, FRIENDER_PEARL, HORSE_ARMOR_EMERALD, HORSE_ARMOR_CARBON, HORSE_ARMOR_LAPIS, HORSE_ARMOR_REDSTONE, HORSE_ARMOR_ARDITE, HORSE_ARMOR_COBALT, HORSE_ARMOR_MANYULLYN));

		CARBON_ARMOR_MATERIAL.customCraftingMaterial = RAW_CARBON;

		HorseUtils.registerHorseArmor("emerald", 1024, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_emerald.png");
		HorseUtils.registerHorseArmor("carbon", 2048, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_carbon.png");
		HorseUtils.registerHorseArmor("lapis", 64, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_lapis.png");
		HorseUtils.registerHorseArmor("ardite", 640, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_ardite.png");
		HorseUtils.registerHorseArmor("cobalt", 768, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_cobalt.png");
		HorseUtils.registerHorseArmor("manyullyn", 1024, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_manyullyn.png");

		ModLogger.info("Registering Items Complete In " + (int) ((System.currentTimeMillis() % 1000) - millis) + "ms");
	}

	@SideOnly(Side.CLIENT)
	public static void preInitModels() {
		ModLogger.info("Init adding item models");
		for (Item item : ITEM_LIST) {
			if (item instanceof IModelHolder) {
				((IModelHolder) item).initModel();
			}
			//
			ModLogger.info("Registered Model for " + item.getItemStackDisplayName(new ItemStack(item)));
		}
		ModLogger.info("Finished adding item models");
	}

	public static List<Item> getList() {
		return ITEM_LIST;
	}
}
