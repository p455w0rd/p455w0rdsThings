package p455w0rd.p455w0rdsthings.init;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidRegistry;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.items.ItemRFArmor;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rdslib.util.EnergyUtils;
import p455w0rdslib.util.ItemUtils;

public class ModCreativeTab extends CreativeTabs {

	public static CreativeTabs TAB;
	private int ticks = 0, index = 0;

	public static void init() {
		TAB = new ModCreativeTab();
	}

	public ModCreativeTab() {
		super(ModGlobals.MODID);
		setBackgroundImageName(ModGlobals.MODID + ".png");
	}

	@Override
	public ItemStack getIconItemStack() {
		List<ItemStack> itemList = Lists.newArrayList(new ItemStack(ModItems.DANK_NULL, 1, 5), new ItemStack(ModBlocks.CARBON_BLOCK), new ItemStack(ModBlocks.COMPRESSOR), new ItemStack(ModBlocks.INTERCHANGER), new ItemStack(ModBlocks.MACHINE_UPGRADE_SPEED), new ItemStack(ModBlocks.NETHER_CARBON_ORE), new ItemStack(ModBlocks.VOIDIFIER), new ItemStack(ModItems.SKULL_ENDERMAN), new ItemStack(ModItems.SKULL_FRIENDERMAN), new ItemStack(ModItems.SKULL_ENDERMAN2));
		int max = itemList.size();
		if (ticks < 250) {
			ticks++;
		}
		else {
			index++;
			if (index >= max) {
				index = 0;
			}
			ticks = 0;
		}
		return itemList.get(index);
	}

	@Override
	public void displayAllRelevantItems(List<ItemStack> items) {
		for (int i = 0; i < 6; i++) {
			items.add(new ItemStack(ModItems.DANK_NULL, 1, i));
		}
		items.add(new ItemStack(ModItems.PANEL_REDSTONE));
		items.add(new ItemStack(ModItems.PANEL_LAPIS));
		items.add(new ItemStack(ModItems.PANEL_IRON));
		items.add(new ItemStack(ModItems.PANEL_GOLD));
		items.add(new ItemStack(ModItems.PANEL_DIAMOND));
		items.add(new ItemStack(ModItems.PANEL_EMERALD));

		items.add(new ItemStack(ModItems.CARBON_NANOTUBE));
		items.add(new ItemStack(ModItems.CARBONROD_REDSTONE));
		items.add(new ItemStack(ModItems.CARBONROD_LAPIS));
		items.add(new ItemStack(ModItems.CARBONROD_IRON));
		items.add(new ItemStack(ModItems.CARBONROD_GOLD));
		items.add(new ItemStack(ModItems.CARBONROD_DIAMOND));
		items.add(new ItemStack(ModItems.CARBONROD_EMERALD));

		items.add(new ItemStack(ModItems.RAW_CARBON));

		items.add(new ItemStack(ModBlocks.NETHER_CARBON_ORE));
		items.add(new ItemStack(ModBlocks.CARBON_BLOCK));
		items.add(new ItemStack(ModBlocks.CARBON_BRICKS));
		items.add(new ItemStack(ModBlocks.CARBON_STAIRS));

		items.add(new ItemStack(ModItems.CARBON_HELMET));
		items.add(new ItemStack(ModItems.CARBON_CHESTPLATE));
		items.add(new ItemStack(ModItems.CARBON_LEGGINGS));
		items.add(new ItemStack(ModItems.CARBON_BOOTS));

		ItemStack upgradedCarbonHelm = new ItemStack(ModItems.EMERALD_CARBON_HELMET);
		items.add(upgradedCarbonHelm);
		items.add(((ItemRFArmor) upgradedCarbonHelm.getItem()).setFull(new ItemStack(ModItems.EMERALD_CARBON_HELMET)));

		ItemStack upgradedCarbonChest = new ItemStack(ModItems.EMERALD_CARBON_CHESTPLATE);
		items.add(upgradedCarbonChest);
		items.add(((ItemRFArmor) upgradedCarbonChest.getItem()).setFull(new ItemStack(ModItems.EMERALD_CARBON_CHESTPLATE)));

		ItemStack carbonJetplate = new ItemStack(ModItems.EMERALD_CARBON_JETPLATE);
		items.add(ArmorUtils.enableUpgrade(carbonJetplate, EntityEquipmentSlot.CHEST, Upgrades.FLIGHT));
		items.add(((ItemRFArmor) carbonJetplate.getItem()).setFull(ArmorUtils.enableUpgrade(new ItemStack(ModItems.EMERALD_CARBON_JETPLATE), EntityEquipmentSlot.CHEST, Upgrades.FLIGHT)));

		ItemStack upgradedCarbonLegs = new ItemStack(ModItems.EMERALD_CARBON_LEGGINGS);
		items.add(upgradedCarbonLegs);
		items.add(((ItemRFArmor) upgradedCarbonLegs.getItem()).setFull(new ItemStack(ModItems.EMERALD_CARBON_LEGGINGS)));

		ItemStack upgradedCarbonBoots = new ItemStack(ModItems.EMERALD_CARBON_BOOTS);
		items.add(upgradedCarbonBoots);
		items.add(((ItemRFArmor) upgradedCarbonBoots.getItem()).setFull(new ItemStack(ModItems.EMERALD_CARBON_BOOTS)));

		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_BASE));
		if (ConfigOptions.ENABLE_LAVAWALKER_UPRGADE) {
			items.add(new ItemStack(ModItems.ARMOR_UPGRADE_LAVAWALKER));
		}
		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_NIGHTVISION));
		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_SPEED));
		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_STEPASSIST));
		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_FLIGHT));
		if (ConfigOptions.ENABLE_WITHERLESS_UPGRADE) {
			items.add(new ItemStack(ModItems.ARMOR_UPGRADE_UNWITHERING));
		}
		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_ENDERVISION));
		items.add(new ItemStack(ModItems.ARMOR_UPGRADE_REPULSION));

		items.add(new ItemStack(ModItems.CARBON_SHEILD));

		items.add(new ItemStack(ModItems.HORSE_ARMOR_REDSTONE));
		items.add(new ItemStack(ModItems.HORSE_ARMOR_LAPIS));
		items.add(new ItemStack(ModItems.HORSE_ARMOR_CARBON));
		items.add(new ItemStack(ModItems.HORSE_ARMOR_EMERALD));
		items.add(new ItemStack(ModItems.HORSE_ARMOR_ARDITE));
		items.add(new ItemStack(ModItems.HORSE_ARMOR_COBALT));
		items.add(new ItemStack(ModItems.HORSE_ARMOR_MANYULLYN));

		items.add(new ItemStack(ModItems.MAGNET));

		items.add(EnergyUtils.setFullPowerRF(new ItemStack(ModItems.MAGNET)));

		items.add(new ItemStack(ModItems.ARMOR_STAND));

		items.add(new ItemStack(ModItems.JETPACK));
		items.add(EnergyUtils.setFullPowerRF(new ItemStack(ModItems.JETPACK)));

		items.add(new ItemStack(ModItems.CARBON_THRUSTER));
		items.add(new ItemStack(ModItems.CARBON_INGOT));
		items.add(new ItemStack(ModItems.CARBON_HARNESS));
		items.add(new ItemStack(ModItems.BASIC_BATTERY));
		items.add(EnergyUtils.setFullPowerRF(new ItemStack(ModItems.BASIC_BATTERY)));
		items.add(new ItemStack(ModItems.ADVANCED_BATTERY));
		items.add(EnergyUtils.setFullPowerRF(new ItemStack(ModItems.ADVANCED_BATTERY)));
		items.add(new ItemStack(ModBlocks.VOIDIFIER));
		items.add(new ItemStack(ModBlocks.INTERCHANGER));
		items.add(new ItemStack(ModItems.FRIENDER_PEARL));
		items.add(new ItemStack(ModItems.NIGHT_VISION_GOGGLES));
		items.add(EnergyUtils.setFullPowerRF(new ItemStack(ModItems.NIGHT_VISION_GOGGLES)));
		items.add(new ItemStack(ModItems.BIT_EMERALD));
		items.add(new ItemStack(ModItems.BIT_DIAMOND));
		items.add(new ItemStack(ModItems.SKULL_ENDERMAN));
		items.add(new ItemStack(ModItems.SKULL_FRIENDERMAN));
		items.add(new ItemStack(ModItems.SKULL_ENDERMAN2));
		items.add(new ItemStack(ModBlocks.COMPRESSOR));
		items.add(new ItemStack(ModBlocks.MACHINE_UPGRADE_SPEED));
		items.add(new ItemStack(ModBlocks.DANKNULL_DOCK));
		items.add(new ItemStack(ModItems.GUIDE));
		items.add(new ItemStack(ModBlocks.SUBLIMATED_CARBON));
		items.add(new ItemStack(ModBlocks.CARBON_DOOR));
		if (FluidRegistry.isUniversalBucketEnabled()) {
			items.add(ItemUtils.getBucketWithFluid(ModFluids.SUBLIMATED_CARBON));
		}

		super.displayAllRelevantItems(items);
	}

	@Override
	public Item getTabIconItem() {
		return ModItems.DANK_NULL;
	}

}
