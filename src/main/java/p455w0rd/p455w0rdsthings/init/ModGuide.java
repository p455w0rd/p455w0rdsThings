package p455w0rd.p455w0rdsthings.init;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileDankNullDock;
import p455w0rd.p455w0rdsthings.entity.EntityEnderman2;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rd.p455w0rdsthings.entity.EntityPArmorStand;
import p455w0rd.p455w0rdsthings.guide.WorldData;
import p455w0rd.p455w0rdsthings.guide.chapter.GuideChapter;
import p455w0rd.p455w0rdsthings.guide.entry.GuideEntry;
import p455w0rd.p455w0rdsthings.guide.entry.GuideEntryAllItems;
import p455w0rd.p455w0rdsthings.guide.page.PageCompressing;
import p455w0rd.p455w0rdsthings.guide.page.PageCompressingCrafting;
import p455w0rd.p455w0rdsthings.guide.page.PageCompressingX2;
import p455w0rd.p455w0rdsthings.guide.page.PageCrafting;
import p455w0rd.p455w0rdsthings.guide.page.PageCraftingEntity;
import p455w0rd.p455w0rdsthings.guide.page.PageCraftingX2;
import p455w0rd.p455w0rdsthings.guide.page.PageEntity;
import p455w0rd.p455w0rdsthings.guide.page.PageEntityRandomHorse;
import p455w0rd.p455w0rdsthings.guide.page.PageTextOnly;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.integration.EnderStorage;
import p455w0rd.p455w0rdsthings.integration.IronChests;
import p455w0rd.p455w0rdsthings.integration.TiC;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.EasyMappings;

/**
 * @author Ellpeck - used with explicit permission - ty <3
 *
 */
public class ModGuide {

	public static final List<IGuideEntry> GUIDE_ENTRIES = new ArrayList<IGuideEntry>();
	public static final List<IGuideChapter> ALL_CHAPTERS = new ArrayList<IGuideChapter>();
	public static final List<IGuidePage> GUIDE_PAGES_WITH_ITEM_OR_FLUID_DATA = new ArrayList<IGuidePage>();
	public static List<Object> INTRO_BOOKMARKS;

	public static IGuideEntry entryGettingStarted;
	public static IGuideEntry entryItems;
	public static IGuideEntry entryBlocks;
	public static IGuideEntry entryMobs;
	//public static IGuideEntry entryVanillaTweaks;
	public static IGuideEntry entryIntegrations;
	public static IGuideEntry entryUpdatesAndInfos;
	public static IGuideEntry allAndSearch;

	public static boolean inited = false;

	public static void preInit() {
		entryGettingStarted = new GuideEntry("gettingStarted").setImportant();
		entryItems = new GuideEntry("items");
		entryBlocks = new GuideEntry("blocks");
		entryMobs = new GuideEntry("mobs");
		//entryVanillaTweaks = new GuideEntry("vanillaTweaks");
		entryIntegrations = new GuideEntry("integrations");
		//entryUpdatesAndInfos = new GuideEntry("updatesAndInfos").setSpecial();
		allAndSearch = new GuideEntryAllItems("allAndSearch").setImportant();
	}

	public static void loadData() {
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		if (server != null) {
			World world = server.getEntityWorld();
			if (world != null && !world.isRemote) {
				WorldData.get(world, true).markDirty();
			}
		}
	}

	public static void init() {
		if (inited) {
			return;
		}
		initChapters();

		int chapCount = 0;
		int pageCount = 0;
		int infoCount = 0;
		for (IGuideEntry entry : GUIDE_ENTRIES) {
			if (entry == entryIntegrations) {
				continue;
			}
			for (IGuideChapter chapter : entry.getAllChapters()) {
				if (!ALL_CHAPTERS.contains(chapter)) {
					ALL_CHAPTERS.add(chapter);
					chapCount++;

					for (IGuidePage page : chapter.getAllPages()) {
						pageCount++;

						List<ItemStack> items = new ArrayList<ItemStack>();
						page.getItemStacksForPage(items);
						List<FluidStack> fluids = new ArrayList<FluidStack>();
						page.getFluidStacksForPage(fluids);

						if ((items != null && !items.isEmpty()) || (fluids != null && !fluids.isEmpty())) {
							if (!GUIDE_PAGES_WITH_ITEM_OR_FLUID_DATA.contains(page)) {
								GUIDE_PAGES_WITH_ITEM_OR_FLUID_DATA.add(page);
								infoCount++;
							}
						}
					}
				}
			}
		}

		Collections.sort(GUIDE_ENTRIES, (entry1, entry2) -> {
			Integer prio1 = entry1.getSortingPriority();
			Integer prio2 = entry2.getSortingPriority();
			return prio2.compareTo(prio1);
		});
		Collections.sort(ALL_CHAPTERS, (chapter1, chapter2) -> {
			Integer prio1 = chapter1.getSortingPriority();
			Integer prio2 = chapter2.getSortingPriority();
			return prio2.compareTo(prio1);
		});
		Collections.sort(GUIDE_PAGES_WITH_ITEM_OR_FLUID_DATA, (page1, page2) -> {
			Integer prio1 = page1.getSortingPriority();
			Integer prio2 = page2.getSortingPriority();
			return prio2.compareTo(prio1);
		});

	}

	private static void initChapters() {
		inited = true;
		IGuideChapter chap1 = new GuideChapter("bookTutorial", entryGettingStarted, new ItemStack(ModItems.GUIDE), new PageTextOnly(1), new PageTextOnly(2), new PageTextOnly(3), new PageCrafting(4, ModRecipes.getInstance().guide).setNoText());
		IGuideChapter chap2 = new GuideChapter("intro", entryGettingStarted, new ItemStack(ModItems.GUIDE), new PageTextOnly(1), new PageTextOnly(2), new PageTextOnly(3));
		INTRO_BOOKMARKS = Lists.<Object>newArrayList(chap1.getAllPages()[0], chap2.getAllPages()[0], GuideUtils.createEntry("items"), GuideUtils.createEntry("blocks"), GuideUtils.createEntry("mobs")/*, GuideUtils.createEntryGui("vanillaTweaks"), GuideUtils.createEntryGui("integrations") */);

		//Items
		new GuideChapter("items.batteries", entryItems, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.BASIC_BATTERY, 1, 2), new ItemStack(ModItems.ADVANCED_BATTERY, 1, 2)), new PageTextOnly(1), new PageCraftingX2(2, ModRecipes.getInstance().basicBattery, ModRecipes.getInstance().advancedBattery));
		new GuideChapter("items.rods", entryItems, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.CARBON_NANOTUBE), new ItemStack(ModItems.CARBONROD_REDSTONE), new ItemStack(ModItems.CARBONROD_LAPIS), new ItemStack(ModItems.CARBONROD_IRON), new ItemStack(ModItems.CARBONROD_GOLD), new ItemStack(ModItems.CARBONROD_DIAMOND), new ItemStack(ModItems.CARBONROD_EMERALD)), new PageTextOnly(1), new PageCraftingX2(2, ModRecipes.getInstance().carbonIngotToCarbonRod, ModRecipes.getInstance().carbonRodRedstone), new PageCraftingX2(3, ModRecipes.getInstance().carbonRodLapis, ModRecipes.getInstance().carbonRodIron), new PageCraftingX2(4, ModRecipes.getInstance().carbonRodGold, ModRecipes.getInstance().carbonRodDiamond), new PageCrafting(5, ModRecipes.getInstance().carbonRodEmerald).setNoText());
		new GuideChapter("items.carbon", entryItems, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.RAW_CARBON), new ItemStack(ModItems.CARBON_INGOT)), new PageTextOnly(1), new PageCompressingX2(2, ModRecipes.getInstance().coalToRawCarbon, ModRecipes.getInstance().rawCarbonToIngot));
		new GuideChapter("items.armor", entryItems, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.CARBON_BOOTS), new ItemStack(ModItems.CARBON_LEGGINGS), new ItemStack(ModItems.CARBON_HARNESS), new ItemStack(ModItems.CARBON_THRUSTER), new ItemStack(ModItems.CARBON_CHESTPLATE), new ItemStack(ModItems.CARBON_HELMET), new ItemStack(ModItems.EMERALD_CARBON_BOOTS), new ItemStack(ModItems.EMERALD_CARBON_LEGGINGS), new ItemStack(ModItems.EMERALD_CARBON_CHESTPLATE), new ItemStack(ModItems.EMERALD_CARBON_JETPLATE), new ItemStack(ModItems.EMERALD_CARBON_HELMET), new ItemStack(ModItems.JETPACK), new ItemStack(ModItems.EMERALD_CARBON_JETPLATE)), new PageTextOnly(1), new PageCraftingX2(2, ModRecipes.getInstance().carbonHelmet, ModRecipes.getInstance().carbonChestplate), new PageCraftingX2(2, ModRecipes.getInstance().carbonLeggings, ModRecipes.getInstance().carbonBoots), new PageCraftingX2(2, ModRecipes.getInstance().emeraldCarbonHelmet, ModRecipes.getInstance().emeraldCarbonChestplate), new PageCraftingX2(2, ModRecipes.getInstance().emeraldCarbonLeggings, ModRecipes.getInstance().emeraldCarbonBoots), new PageCraftingX2(2, ModRecipes.getInstance().jetpack, ModRecipes.getInstance().nightVisionGoggles));
		new GuideChapter("items.armorUpgrades", entryItems, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.ARMOR_UPGRADE_BASE), new ItemStack(ModItems.ARMOR_UPGRADE_ENDERVISION), new ItemStack(ModItems.ARMOR_UPGRADE_FLIGHT), new ItemStack(ModItems.ARMOR_UPGRADE_LAVAWALKER), new ItemStack(ModItems.ARMOR_UPGRADE_NIGHTVISION), new ItemStack(ModItems.ARMOR_UPGRADE_REPULSION), new ItemStack(ModItems.ARMOR_UPGRADE_SPEED), new ItemStack(ModItems.ARMOR_UPGRADE_STEPASSIST), new ItemStack(ModItems.ARMOR_UPGRADE_UNWITHERING)), new PageTextOnly(1), new PageCrafting(2, ModRecipes.getInstance().armorUpgradeBase), new PageCrafting(3, ModRecipes.getInstance().amorUpgradeEnderVision), new PageCrafting(4, ModRecipes.getInstance().armorUpgradeFlight), new PageCrafting(5, ModRecipes.getInstance().armorUpgradeLavaWalker), new PageCrafting(6, ModRecipes.getInstance().armorUpgradeNightVision), new PageCrafting(7, ModRecipes.getInstance().armorUpgradeRepulsion), new PageCrafting(8, ModRecipes.getInstance().armorUpgradeSpeed), new PageCrafting(9, ModRecipes.getInstance().armorUpgradeStepAssist));
		new GuideChapter("items.dankNulls", entryItems, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.DANK_NULL), new ItemStack(ModItems.DANK_NULL, 1, 1), new ItemStack(ModItems.DANK_NULL, 1, 2), new ItemStack(ModItems.DANK_NULL, 1, 3), new ItemStack(ModItems.DANK_NULL, 1, 4), new ItemStack(ModItems.DANK_NULL, 1, 5)), new PageTextOnly(1), new PageCompressing(2, ModRecipes.getInstance().redstonePanelToDankNull), new PageCompressingCrafting(3, ModRecipes.getInstance().lapisPanelToDankNull, ModRecipes.getInstance().lapisDankNullUpgrade), new PageCompressingCrafting(4, ModRecipes.getInstance().ironPanelToDankNull, ModRecipes.getInstance().ironDankNullUpgrade), new PageCompressingCrafting(6, ModRecipes.getInstance().goldPanelToDankNull, ModRecipes.getInstance().goldDankNullUpgrade), new PageCompressingCrafting(7, ModRecipes.getInstance().diamondPanelToDankNull, ModRecipes.getInstance().diamondDankNullUpgrade), new PageCompressingCrafting(8, ModRecipes.getInstance().emeraldPanelToDankNull, ModRecipes.getInstance().emeraldDankNullUpgrade));

		List<ItemStack> horseArmorList = Lists.<ItemStack>newArrayList(new ItemStack(ModItems.HORSE_ARMOR_REDSTONE), new ItemStack(ModItems.HORSE_ARMOR_LAPIS), new ItemStack(Items.IRON_HORSE_ARMOR), new ItemStack(Items.GOLDEN_HORSE_ARMOR), new ItemStack(Items.DIAMOND_HORSE_ARMOR), new ItemStack(ModItems.HORSE_ARMOR_EMERALD), new ItemStack(ModItems.HORSE_ARMOR_CARBON));

		if (Mods.TINKERS.isLoaded()) {
			TiC.addHorseArmorToList(horseArmorList);
			TiC.addIntegratedGuidePages(entryItems, horseArmorList);
		}
		else {
			new GuideChapter("items.horseArmor", entryItems, horseArmorList, new PageTextOnly(1), new PageEntityRandomHorse(2).setNoText(), new PageCraftingX2(3, ModRecipes.getInstance().redstoneHorseArmor, ModRecipes.getInstance().lapisHorseArmor).setNoText(), new PageCraftingX2(4, ModRecipes.getInstance().ironHorseArmor, ModRecipes.getInstance().goldenHorseArmor).setNoText(), new PageCraftingX2(5, ModRecipes.getInstance().diamondHorseArmor, ModRecipes.getInstance().emeraldHorseArmor).setNoText(), new PageCrafting(6, ModRecipes.getInstance().carbonHorseArmor).setNoText());
		}

		//Blocks
		new GuideChapter("blocks.armorStand", entryBlocks, new ItemStack(ModItems.ARMOR_STAND), new PageTextOnly(1), new PageCraftingEntity(2, new EntityPArmorStand(EasyMappings.world()), ModRecipes.getInstance().advancedArmorStand).setNoText());
		new GuideChapter("blocks.carbonDeco", entryBlocks, Lists.<ItemStack>newArrayList(new ItemStack(ModBlocks.CARBON_DOOR), new ItemStack(ModBlocks.CARBON_STAIRS), new ItemStack(ModBlocks.CARBON_BRICKS), new ItemStack(ModBlocks.CARBON_SLAB)), new PageTextOnly(1), new PageCraftingEntity(2, new EntityItem(EasyMappings.world(), 0, 0, 0, new ItemStack(ModBlocks.CARBON_DOOR)), 50, ModRecipes.getInstance().carbonDoor).setScale(1.5f).setNoText(), new PageCraftingEntity(2, new EntityItem(EasyMappings.world(), 0, 0, 0, new ItemStack(ModBlocks.CARBON_STAIRS)), ModRecipes.getInstance().carbonStairs).setNoText(), new PageCraftingEntity(2, new EntityItem(EasyMappings.world(), 0, 0, 0, new ItemStack(ModBlocks.CARBON_BRICKS)), ModRecipes.getInstance().carbonBricks).setScale(1.0f).setNoText(), new PageCraftingEntity(2, new EntityItem(EasyMappings.world(), 0, 0, 0, new ItemStack(ModBlocks.CARBON_SLAB)), ModRecipes.getInstance().carbonSlab).setNoText());
		new GuideChapter("blocks.carbon", entryBlocks, Lists.<ItemStack>newArrayList(new ItemStack(ModBlocks.NETHER_CARBON_ORE)), new PageTextOnly(1), new PageEntity(0, new EntityItem(EasyMappings.world(), 0, 0, 0, new ItemStack(ModBlocks.NETHER_CARBON_ORE)), 0, 2.0F));

		ItemStack dankNullDockWithDankNull = new ItemStack(ModBlocks.DANKNULL_DOCK);
		NBTTagCompound dankNullDockNBT = new NBTTagCompound();
		NBTTagCompound itemNBT = new NBTTagCompound();
		ItemStack dankNull = new ItemStack(ModItems.DANK_NULL, 1, 5);
		dankNull.writeToNBT(itemNBT);
		dankNullDockNBT.setTag(TileDankNullDock.TAG_ITEMSTACK, itemNBT);
		dankNullDockWithDankNull.setTagInfo("BlockEntityTag", dankNullDockNBT);
		new GuideChapter("blocks.machine", entryBlocks, Lists.<ItemStack>newArrayList(new ItemStack(ModBlocks.COMPRESSOR), new ItemStack(ModBlocks.MACHINE_UPGRADE_SPEED), new ItemStack(ModBlocks.VOIDIFIER), new ItemStack(ModBlocks.DANKNULL_DOCK), new ItemStack(ModBlocks.MACHINE_FRAMEWORK), new ItemStack(ModBlocks.INTERCHANGER)), new PageTextOnly(1), new PageCraftingX2(2, ModRecipes.getInstance().machineFramework, ModRecipes.getInstance().compressor).setNoText(), new PageCrafting(3, ModRecipes.getInstance().machineUpgradeSpeed).setNoText(), new PageTextOnly(4), new PageCrafting(5, ModRecipes.getInstance().voidifer), new PageCrafting(6, ModRecipes.getInstance().dankNullDock), new PageCrafting(7, ModRecipes.getInstance().interchanger));

		//Mobs
		new GuideChapter("mobs.enderman2", entryMobs, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.SKULL_ENDERMAN2)), new PageTextOnly(1), new PageEntity(0, new EntityEnderman2(EasyMappings.world()), 2.0F));
		List<Entity> friendermanList = Lists.<Entity>newArrayList();
		EntityFrienderman friendermanWithChest = new EntityFrienderman(EasyMappings.world());
		friendermanWithChest.setHeldItemStack(new ItemStack(Item.getItemFromBlock(Blocks.CHEST)));
		EntityFrienderman friendermanWithEnderChest = new EntityFrienderman(EasyMappings.world());
		friendermanWithEnderChest.setHeldItemStack(new ItemStack(Item.getItemFromBlock(Blocks.ENDER_CHEST)));
		friendermanList.add(new EntityFrienderman(EasyMappings.world()));
		friendermanList.add(friendermanWithChest);
		friendermanList.add(friendermanWithEnderChest);
		if (Mods.ENDERSTORAGE.isLoaded()) {
			EntityFrienderman friendermanWithEnderStorageChest = new EntityFrienderman(EasyMappings.world());
			friendermanWithEnderStorageChest.setHeldItemStack(EnderStorage.getEnderStorageStack());
			friendermanList.add(friendermanWithEnderStorageChest);
		}
		if (Mods.IRONCHESTS.isLoaded()) {
			for (int i = 0; i < 6; i++) {
				EntityFrienderman friendermanWithIronChest = new EntityFrienderman(EasyMappings.world());
				friendermanWithIronChest.setHeldItemStack(IronChests.getStackByMeta(i));
				friendermanList.add(friendermanWithIronChest);
			}
		}
		new GuideChapter("mobs.frienderman", entryMobs, new ItemStack(ModItems.SKULL_FRIENDERMAN), new PageTextOnly(1), new PageEntity(0, friendermanList, 2.0F), new PageTextOnly(2), new PageTextOnly(3));

		//Vanilla Tweaks - not now...let's get this damn update pushed
		//new GuideChapter("vanillaTweaks", entryVanillaTweaks, new ItemStack(Blocks.DIRT), new PageTextOnly(1));

		//Integrations
		new GuideChapter("integrations.jei", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.baubles", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.crafttweaker", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.enderio", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.enderstorage", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.ironchests", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.tinkers", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.top", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));
		new GuideChapter("integrations.waila", entryIntegrations, Lists.<ItemStack>newArrayList(new ItemStack(ModItems.FRIENDER_PEARL)));

	}

}
