package p455w0rd.p455w0rdsthings.init;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import p455w0rd.p455w0rdsthings.network.PacketCLSync;
import p455w0rd.p455w0rdsthings.network.PacketConfigSync;
import p455w0rd.p455w0rdsthings.network.PacketFluidFill;
import p455w0rd.p455w0rdsthings.network.PacketFriendermanRegistrySync;
import p455w0rd.p455w0rdsthings.network.PacketModifyAccessList;
import p455w0rd.p455w0rdsthings.network.PacketPlayerDataC;
import p455w0rd.p455w0rdsthings.network.PacketPlayerDataS;
import p455w0rd.p455w0rdsthings.network.PacketSetAccessMode;
import p455w0rd.p455w0rdsthings.network.PacketSetArmor;
import p455w0rd.p455w0rdsthings.network.PacketSetEntityChargedC;
import p455w0rd.p455w0rdsthings.network.PacketSetEntityChargedS;
import p455w0rd.p455w0rdsthings.network.PacketSetInterchangerMode;
import p455w0rd.p455w0rdsthings.network.PacketSetNightVision;
import p455w0rd.p455w0rdsthings.network.PacketSetRedstoneMode;
import p455w0rd.p455w0rdsthings.network.PacketSetSelectedItem;
import p455w0rd.p455w0rdsthings.network.PacketSyncDankNull;

public class ModNetworking {

	private static int packetId = 0;
	public static SimpleNetworkWrapper INSTANCE = null;

	private static int nextID() {
		return packetId++;
	}

	public static void init() {
		ModLogger.info("Registering Packet Messages");
		INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(ModGlobals.MODID);
		INSTANCE.registerMessage(PacketSetSelectedItem.Handler.class, PacketSetSelectedItem.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketSetNightVision.Handler.class, PacketSetNightVision.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketSetArmor.Handler.class, PacketSetArmor.class, nextID(), Side.CLIENT);
		INSTANCE.registerMessage(PacketCLSync.Handler.class, PacketCLSync.class, nextID(), Side.CLIENT);
		INSTANCE.registerMessage(PacketConfigSync.Handler.class, PacketConfigSync.class, nextID(), Side.CLIENT);
		INSTANCE.registerMessage(PacketSetEntityChargedS.Handler.class, PacketSetEntityChargedS.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketSetEntityChargedC.Handler.class, PacketSetEntityChargedC.class, nextID(), Side.CLIENT);
		INSTANCE.registerMessage(PacketSetAccessMode.Handler.class, PacketSetAccessMode.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketSetInterchangerMode.Handler.class, PacketSetInterchangerMode.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketSetRedstoneMode.Handler.class, PacketSetRedstoneMode.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketFluidFill.Handler.class, PacketFluidFill.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketModifyAccessList.Handler.class, PacketModifyAccessList.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketFriendermanRegistrySync.Handler.class, PacketFriendermanRegistrySync.class, nextID(), Side.CLIENT);
		INSTANCE.registerMessage(PacketPlayerDataS.Handler.class, PacketPlayerDataS.class, nextID(), Side.SERVER);
		INSTANCE.registerMessage(PacketPlayerDataC.Handler.class, PacketPlayerDataC.class, nextID(), Side.CLIENT);
		INSTANCE.registerMessage(PacketSyncDankNull.Handler.class, PacketSyncDankNull.class, nextID(), Side.CLIENT);
	}

}
