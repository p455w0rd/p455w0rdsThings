package p455w0rd.p455w0rdsthings.init;

import codechicken.lib.CodeChickenLib;
import net.minecraft.util.ResourceLocation;

public class ModGlobals {
	public static final String MODID = "p455w0rdsthings";
	public static final String VERSION = "2.2.32";
	public static final String NAME = "p455w0rd's Things";
	public static final String SERVER_PROXY = "p455w0rd.p455w0rdsthings.proxy.CommonProxy";
	public static final String CLIENT_PROXY = "p455w0rd.p455w0rdsthings.proxy.ClientProxy";
	public static final String GUI_FACTORY = "p455w0rd.p455w0rdsthings.client.gui.GuiFactory";
	public static final String CONFIG_FILE = "config/p455w0rdsThings.cfg";
	public static final String DEPENDANCIES = "after:guideapi;after:Baubles;after:EnderStorage;after:ironchest;after:JEI;after:theoneprobe;after:Waila;after:mantle@[1.10.2-1.1.3,);after:tconstruct@[1.10.2-2.6.1.464,);after:enderio;after:ProjectE;after:MineTweaker3;required-after:Forge@[12.18.2.2115,);required-after:p455w0rdslib@[1.0.3,);required-after:CodeChickenLib@[" + CodeChickenLib.version + ",)";

	public static boolean GUI_DANKNULL_ISOPEN = false;
	public static boolean CARBONHELMET_ISEQUIPPED = false;
	public static boolean CARBONCHESTPLATE_ISEQUIPPED = false;
	public static boolean CARBONJETPLATE_ISEQUIPPED = false;
	public static boolean CARBONJETPACK_ISEQUIPPED = false;
	public static boolean CARBONLEGGINGS_ISEQUIPPED = false;
	public static boolean CARBONBOOTS_ISEQUIPPED = false;
	public static float TIME = 0.0F;
	public static float TIME2 = 0.0F;
	public static long TIME_LONG = 0L;
	public static int ALPHA = 255;
	public static int RED = 255;
	public static int GREEN = 0;
	public static int BLUE = 0;
	public static int TURN = 0;
	public static int TIMER = 0;

	public static class HarvestLevel {
		public static int NONE = 0;
		public static int WOOD, GOLD = 1;
		public static int IRON = 2;
		public static int DIAMOND = 3;
		public static int COBALT = 4;
		public static int CARBON = 5;
	}

	public static class Textures {

		public static final ResourceLocation GLOW_FLAME = new ResourceLocation(ModGlobals.MODID, "particle/particle_glow_flame4");

	}
}
