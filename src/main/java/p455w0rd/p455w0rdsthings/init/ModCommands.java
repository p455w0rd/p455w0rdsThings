/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.init;

import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import p455w0rd.p455w0rdsthings.commands.CommandKickMyOwnAss;

/**
 * @author p455w0rd
 *
 */
public class ModCommands {

	public static void init(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandKickMyOwnAss());
	}

}
