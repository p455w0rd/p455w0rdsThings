package p455w0rd.p455w0rdsthings.init;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.init.Items;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.RecipeSorter;
import net.minecraftforge.oredict.RecipeSorter.Category;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import p455w0rd.p455w0rdsthings.api.IArmorUpgrade;
import p455w0rd.p455w0rdsthings.api.ICompressorRecipe;
import p455w0rd.p455w0rdsthings.api.IEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.integration.EnderIo;
import p455w0rd.p455w0rdsthings.integration.TiC;
import p455w0rd.p455w0rdsthings.recipe.ArmorUpgradeRecipe;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;
import p455w0rd.p455w0rdsthings.recipe.RecipePlankDouble;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rdslib.util.PotionUtils;

public class ModRecipes {

	public static final EntityEquipmentSlot HEAD = EntityEquipmentSlot.HEAD;
	public static final EntityEquipmentSlot CHEST = EntityEquipmentSlot.CHEST;
	public static final EntityEquipmentSlot LEGS = EntityEquipmentSlot.LEGS;
	public static final EntityEquipmentSlot FEET = EntityEquipmentSlot.FEET;

	private static final ModRecipes INSTANCE = new ModRecipes();

	public static final List<IRecipe> VANILLA_RECIPES = Lists.<IRecipe>newLinkedList();

	public ICompressorRecipe cobbleToCoal;
	public ICompressorRecipe charcoalToCoal;
	public ICompressorRecipe coalToRawCarbon;
	public ICompressorRecipe rawCarbonToIngot;
	//public ICompressorRecipe carbonIngotToCarbonRod;
	public ICompressorRecipe coalBlockToCarbonBlock;
	public ICompressorRecipe endermanSkullToEnderPearl;
	public ICompressorRecipe enderman2SkullToEnderPearl;
	public ICompressorRecipe friendermanSkullToEnderPearl;
	public ICompressorRecipe emeraldBitToEmerald;
	public ICompressorRecipe redstonePanelToDankNull;
	public ICompressorRecipe lapisPanelToDankNull;
	public ICompressorRecipe ironPanelToDankNull;
	public ICompressorRecipe goldPanelToDankNull;
	public ICompressorRecipe diamondPanelToDankNull;
	public ICompressorRecipe emeraldPanelToDankNull;
	public ICompressorRecipe diamondBitToDiamond;
	public ICompressorRecipe blazePowderToBlazeRod;
	public ICompressorRecipe skeleSkullToWitherSkeleSkull;
	public ICompressorRecipe carbonBlockToDiamond;
	public ICompressorRecipe carbonSlabToCarbonBlock;

	public IRecipe ironHorseArmor;
	public IRecipe goldenHorseArmor;
	public IRecipe diamondHorseArmor;
	public IRecipe emeraldHorseArmor;
	public IRecipe lapisHorseArmor;
	public IRecipe redstoneHorseArmor;
	public IRecipe carbonHorseArmor;
	public IRecipe arditeHorseArmor;
	public IRecipe cobaltHorseArmor;
	public IRecipe manyullynHorseArmor;
	public IRecipe carbonSlab;
	public IRecipe witherlessUpgrade;
	public IRecipe interchanger;
	public IRecipe dankNullDock;
	public IRecipe machineUpgradeSpeed;
	public IRecipe compressor;
	public IRecipe armorUpgradeRepulsion;
	public IRecipe machineFramework;
	public IRecipe amorUpgradeEnderVision;
	public IRecipe eio_armorUpgradeEnderVision;
	public IRecipe eio_skull1;
	public IRecipe eio_skull2;
	public IRecipe eio_skull3;
	public IRecipe armorUpgradeNightVision;
	public IRecipe emeraldBitsToEmerald;
	public IRecipe nightVisionGoggles;
	public IRecipe armorUpgradeBase;
	public IRecipe voidifer;
	public IRecipe carbonStairs;
	public IRecipe carbonBricks;
	public IRecipe basicBattery;
	public IRecipe advancedBattery;
	public IRecipe carbonHarness;
	public IRecipe jetpack;
	public IRecipe carbonThruster;
	public IRecipe carbonRodRedstone;
	public IRecipe carbonRodLapis;
	public IRecipe carbonRodIron;
	public IRecipe carbonRodGold;
	public IRecipe carbonRodDiamond;
	public IRecipe carbonRodEmerald;
	public IRecipe panelRedstone;
	public IRecipe panelLapis;
	public IRecipe panelIron;
	public IRecipe panelGold;
	public IRecipe panelDiamond;
	public IRecipe panelEmerald;
	public IRecipe carbonHelmet;
	public IRecipe carbonChestplate;
	public IRecipe carbonLeggings;
	public IRecipe carbonBoots;
	public IRecipe emeraldCarbonHelmet;
	public IRecipe emeraldCarbonChestplate;
	public IRecipe emeraldCarbonLeggings;
	public IRecipe emeraldCarbonBoots;
	public IRecipe carbonShield;
	public IRecipe magnet;
	public IRecipe advancedArmorStand;
	public IRecipe lapisDankNullUpgrade;
	public IRecipe ironDankNullUpgrade;
	public IRecipe goldDankNullUpgrade;
	public IRecipe diamondDankNullUpgrade;
	public IRecipe emeraldDankNullUpgrade;
	public IRecipe armorUpgradeFlight;
	public IRecipe armorUpgradeStepAssist;
	public IRecipe armorUpgradeLavaWalker;
	public IRecipe armorUpgradeSpeed;
	public IRecipe saddle;
	public IRecipe notchApple;
	public IRecipe nameTag;
	public IRecipe guide;
	public IRecipe carbonIngotToCarbonRod;
	public IRecipe carbonDoor;
	public IRecipe emeraldToEmeraldBits;
	public IRecipe diamondToDiamondBIts;

	public static ModRecipes getInstance() {
		return INSTANCE;
	}

	public static void preInit() {
		RecipeSorter.register(ModGlobals.MODID + ":ArmorUpgrade", ArmorUpgradeRecipe.class, Category.SHAPELESS, "after:minecraft:shapeless");
		RecipeSorter.register(ModGlobals.MODID + ":PlankDouble", RecipePlankDouble.class, Category.SHAPELESS, "after:minecraft:shapeless");
	}

	public static void init() {
		ModLogger.info("Adding Recipes");
		//MinecraftForge.EVENT_BUS.register(new ModRecipes());
		getInstance().addRecipes();
	}

	private void addRecipes() {

		ItemStack coalBlock = new ItemStack(Blocks.COAL_BLOCK);
		ItemStack rawCarbon = new ItemStack(ModItems.RAW_CARBON);
		ItemStack carbonRod = new ItemStack(ModItems.CARBON_NANOTUBE);
		ItemStack glassPane = new ItemStack(Blocks.GLASS_PANE);
		ItemStack redPane = new ItemStack(Blocks.STAINED_GLASS_PANE, 1, 14);
		ItemStack bluePane = new ItemStack(Blocks.STAINED_GLASS_PANE, 1, 3);
		ItemStack whitePane = new ItemStack(Blocks.STAINED_GLASS_PANE, 1, 0);
		ItemStack yellowPane = new ItemStack(Blocks.STAINED_GLASS_PANE, 1, 4);
		ItemStack cyanPane = new ItemStack(Blocks.STAINED_GLASS_PANE, 1, 9);
		ItemStack limePane = new ItemStack(Blocks.STAINED_GLASS_PANE, 1, 5);
		ItemStack dankNull0 = new ItemStack(ModItems.DANK_NULL, 1, 0);
		ItemStack dankNull1 = new ItemStack(ModItems.DANK_NULL, 1, 1);
		ItemStack dankNull2 = new ItemStack(ModItems.DANK_NULL, 1, 2);
		ItemStack dankNull3 = new ItemStack(ModItems.DANK_NULL, 1, 3);
		ItemStack dankNull4 = new ItemStack(ModItems.DANK_NULL, 1, 4);
		ItemStack dankNull5 = new ItemStack(ModItems.DANK_NULL, 1, 5);
		ItemStack carbonBlock = new ItemStack(ModBlocks.CARBON_BLOCK);
		//ItemStack carbonBlockStack = new ItemStack(ModBlocks.carbonBlock, 9);
		ItemStack diamond = new ItemStack(Items.DIAMOND);
		ItemStack string = new ItemStack(Items.STRING);
		ItemStack paper = new ItemStack(Items.PAPER);
		ItemStack leather = new ItemStack(Items.LEATHER);
		ItemStack goldBlock = new ItemStack(Blocks.GOLD_BLOCK);
		ItemStack apple = new ItemStack(Items.APPLE);
		ItemStack carbonHelmetUpgraded = new ItemStack(ModItems.EMERALD_CARBON_HELMET);
		ItemStack carbonChestplateUpgraded = new ItemStack(ModItems.EMERALD_CARBON_CHESTPLATE);
		ItemStack carbonLeggingsUpgraded = new ItemStack(ModItems.EMERALD_CARBON_LEGGINGS);
		ItemStack carbonBootsUpgraded = new ItemStack(ModItems.EMERALD_CARBON_BOOTS);
		ItemStack shield = new ItemStack(Items.SHIELD);
		ItemStack ironIngot = new ItemStack(Items.IRON_INGOT);
		ItemStack stoneSlab = new ItemStack(Blocks.STONE_SLAB);
		ItemStack blazePowder = new ItemStack(Items.BLAZE_POWDER);
		ItemStack carbonIngot = new ItemStack(ModItems.CARBON_INGOT);
		ItemStack redstoneBlock = new ItemStack(Blocks.REDSTONE_BLOCK);
		ItemStack lavaBucket = new ItemStack(Items.LAVA_BUCKET);
		ItemStack obsidian = new ItemStack(Blocks.OBSIDIAN);
		ItemStack frienderPearl = new ItemStack(ModItems.FRIENDER_PEARL);
		ItemStack swiftnessPotion = PotionUtils.createSplashPotionStack("minecraft:swiftness");
		ItemStack netherStar = new ItemStack(Items.NETHER_STAR);
		ItemStack witherSkull = new ItemStack(Items.SKULL, 1, 1);
		ItemStack emeraldBit = new ItemStack(ModItems.BIT_EMERALD);
		ItemStack emerald = new ItemStack(Items.EMERALD);

		GameRegistry.addSmelting(coalBlock, rawCarbon, 0.7F);
		//GameRegistry.addSmelting(rawCarbon, carbonRod, 0.85F);
		GameRegistry.addRecipe(new RecipePlankDouble());
		GameRegistry.addRecipe(new ArmorUpgradeRecipe());

		if (Mods.JEI.isLoaded()) {
			for (Item armorItem : ModItems.getList()) {
				if (armorItem instanceof IEmeraldCarbonArmor) {
					IEmeraldCarbonArmor armor = (IEmeraldCarbonArmor) armorItem;
					for (Item upgradeItem : ModItems.getList()) {
						if (upgradeItem instanceof IArmorUpgrade) {
							IArmorUpgrade upgrade = (IArmorUpgrade) upgradeItem;
							if (upgrade.getUpgradeType() == null) {
								continue;
							}
							if (armorItem == ModItems.EMERALD_CARBON_JETPLATE && upgradeItem == ModItems.ARMOR_UPGRADE_FLIGHT) {
								continue;
							}
							if (armorItem == ModItems.EMERALD_CARBON_CHESTPLATE && upgradeItem == ModItems.ARMOR_UPGRADE_FLIGHT) {
								ItemStack upgradedStack = ArmorUtils.enableUpgrade(new ItemStack(ModItems.EMERALD_CARBON_JETPLATE), armor.getArmorType(), upgrade.getUpgradeType());
								ItemStack armorStack = new ItemStack(armorItem);
								GameRegistry.addRecipe(new ArmorUpgradeRecipe(upgradedStack, new ItemStack[] {
										armorStack,
										new ItemStack(upgradeItem)
								}));
							}
							else {
								if (upgrade.getUpgradeType().getArmorTypes().contains(armor.getArmorType())) {
									ItemStack upgradedStack = new ItemStack(armorItem);
									ItemStack armorStack = new ItemStack(armorItem);
									if (armorItem == ModItems.EMERALD_CARBON_JETPLATE) {
										upgradedStack = ArmorUtils.enableUpgrade(new ItemStack(armorItem), armor.getArmorType(), Upgrades.FLIGHT);
										armorStack = ArmorUtils.enableUpgrade(new ItemStack(armorItem), armor.getArmorType(), Upgrades.FLIGHT);
									}
									GameRegistry.addRecipe(new ArmorUpgradeRecipe(ArmorUtils.enableUpgrade(upgradedStack, armor.getArmorType(), upgrade.getUpgradeType()), new ItemStack[] {
											armorStack,
											new ItemStack(upgradeItem)
									}));
								}
							}
						}
					}
				}
			}
		}

		cobbleToCoal = RecipeCompressor.addRecipe(new ItemStack(Blocks.COBBLESTONE, 18), new ItemStack(Items.COAL), new ItemStack(Items.COAL), 0.1D, 10000);
		charcoalToCoal = RecipeCompressor.addRecipe(new ItemStack(Items.COAL, 3, 1), new ItemStack(Items.COAL), new ItemStack(Items.COAL), 0.05D, 10000);
		coalToRawCarbon = RecipeCompressor.addRecipe(new ItemStack(Items.COAL, 9), rawCarbon, null, 0.0D, 10000);
		rawCarbonToIngot = RecipeCompressor.addRecipe(new ItemStack(ModItems.RAW_CARBON, 2), carbonIngot, null, 0.0D, 2500);
		//carbonIngotToCarbonRod = RecipeCompressor.addRecipe(new ItemStack(ModItems.CARBON_INGOT, 3), carbonRod, new ItemStack(ModItems.BIT_EMERALD), 0.05D, 5000);
		coalBlockToCarbonBlock = RecipeCompressor.addRecipe(new ItemStack(ModItems.CARBON_INGOT, 9), carbonBlock, new ItemStack(ModItems.BIT_DIAMOND), 0.05D, 100000);
		endermanSkullToEnderPearl = RecipeCompressor.addRecipe(new ItemStack(ModItems.SKULL_ENDERMAN, 16), new ItemStack(Items.ENDER_PEARL), frienderPearl, 0.05D, 25000);
		enderman2SkullToEnderPearl = RecipeCompressor.addRecipe(new ItemStack(ModItems.SKULL_ENDERMAN2, 16), new ItemStack(Items.ENDER_PEARL), frienderPearl, 0.05D, 25000);
		friendermanSkullToEnderPearl = RecipeCompressor.addRecipe(new ItemStack(ModItems.SKULL_ENDERMAN, 16), new ItemStack(Items.ENDER_PEARL), frienderPearl, 0.05D, 25000);
		emeraldBitToEmerald = RecipeCompressor.addRecipe(new ItemStack(ModItems.BIT_EMERALD, 9), emerald, null, 0.0D, 1500);
		redstonePanelToDankNull = RecipeCompressor.addRecipe(new ItemStack(ModItems.PANEL_REDSTONE, 5), dankNull0, new ItemStack(Items.REDSTONE), 0.5, 1000);
		lapisPanelToDankNull = RecipeCompressor.addRecipe(new ItemStack(ModItems.PANEL_LAPIS, 5), dankNull1, new ItemStack(Items.DYE, 1, 4), 0.5, 2500);
		ironPanelToDankNull = RecipeCompressor.addRecipe(new ItemStack(ModItems.PANEL_IRON, 5), dankNull2, new ItemStack(Items.IRON_INGOT), 0.5, 6000);
		goldPanelToDankNull = RecipeCompressor.addRecipe(new ItemStack(ModItems.PANEL_GOLD, 5), dankNull3, new ItemStack(Items.GOLD_NUGGET), 0.5, 13000);
		diamondPanelToDankNull = RecipeCompressor.addRecipe(new ItemStack(ModItems.PANEL_DIAMOND, 5), dankNull4, new ItemStack(ModItems.BIT_DIAMOND), 0.5, 20000);
		emeraldPanelToDankNull = RecipeCompressor.addRecipe(new ItemStack(ModItems.PANEL_EMERALD, 5), dankNull5, new ItemStack(ModItems.BIT_EMERALD), 0.5, 100000);
		diamondBitToDiamond = RecipeCompressor.addRecipe(new ItemStack(ModItems.BIT_DIAMOND, 9), new ItemStack(Items.DIAMOND), null, 0.0D, 1000);
		blazePowderToBlazeRod = RecipeCompressor.addRecipe(new ItemStack(Items.BLAZE_POWDER, 4), new ItemStack(Items.BLAZE_ROD), null, 0.0D, 2000);
		skeleSkullToWitherSkeleSkull = RecipeCompressor.addRecipe(new ItemStack(Items.SKULL, 64), new ItemStack(Items.SKULL, 1, 1), null, 0.0D, 200000);
		carbonBlockToDiamond = RecipeCompressor.addRecipe(new ItemStack(ModBlocks.CARBON_BLOCK, 9), diamond, new ItemStack(ModItems.BIT_DIAMOND), 0.1D, 20000);
		carbonSlabToCarbonBlock = RecipeCompressor.addRecipe(new ItemStack(ModBlocks.CARBON_SLAB, 2), carbonBlock, null, 0.0D, 50000);

		Map<Enchantment, Integer> infinityEnchantment = Maps.newHashMap();
		Map<Enchantment, Integer> knockbackEnchantment = Maps.newHashMap();
		infinityEnchantment.put(Enchantments.INFINITY, 1);
		knockbackEnchantment.put(Enchantments.KNOCKBACK, 2);
		ItemStack infinityBow = new ItemStack(Items.BOW);
		ItemStack knockBackBow = new ItemStack(Items.BOW);
		EnchantmentHelper.setEnchantments(infinityEnchantment, infinityBow);
		EnchantmentHelper.setEnchantments(knockbackEnchantment, knockBackBow);

		GameRegistry.addSmelting(ModBlocks.NETHER_CARBON_ORE, rawCarbon, 10f);
		if (ConfigOptions.ENABLE_HORSE_ARMOR_RECIPES) {
			if (Mods.TINKERS.isLoaded()) {
				TiC.addHorseArmorRecipes();
			}
			VANILLA_RECIPES.add(ironHorseArmor = new ShapedOreRecipe(new ItemStack(Items.IRON_HORSE_ARMOR), "  a", "aba", "a a", 'a', new ItemStack(Items.IRON_INGOT), 'b', new ItemStack(Items.SADDLE)));
			VANILLA_RECIPES.add(goldenHorseArmor = new ShapedOreRecipe(new ItemStack(Items.GOLDEN_HORSE_ARMOR), "  a", "aba", "a a", 'a', new ItemStack(Items.GOLD_INGOT), 'b', new ItemStack(Items.SADDLE)));
			VANILLA_RECIPES.add(diamondHorseArmor = new ShapedOreRecipe(new ItemStack(Items.DIAMOND_HORSE_ARMOR), "  a", "aba", "a a", 'a', new ItemStack(Items.DIAMOND), 'b', new ItemStack(Items.SADDLE)));
			VANILLA_RECIPES.add(emeraldHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_EMERALD), "  a", "aba", "a a", 'a', new ItemStack(Items.EMERALD), 'b', new ItemStack(Items.SADDLE)));
			VANILLA_RECIPES.add(lapisHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_LAPIS), "  a", "aba", "a a", 'a', "gemLapis", 'b', new ItemStack(Items.SADDLE)));
			VANILLA_RECIPES.add(redstoneHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_REDSTONE), "  a", "aba", "a a", 'a', new ItemStack(Items.REDSTONE), 'b', new ItemStack(Items.SADDLE)));
			VANILLA_RECIPES.add(carbonHorseArmor = new ShapedOreRecipe(new ItemStack(ModItems.HORSE_ARMOR_CARBON), "  a", "aba", "a a", 'a', carbonIngot, 'b', new ItemStack(Items.SADDLE)));
		}
		if (Mods.ENDERIO.isLoaded()) {
			EnderIo.addRecipes();
		}

		VANILLA_RECIPES.add(carbonSlab = new ShapedOreRecipe(new ItemStack(ModBlocks.CARBON_SLAB, 6), "aaa", 'a', new ItemStack(ModBlocks.CARBON_BLOCK)));
		VANILLA_RECIPES.add(witherlessUpgrade = new ShapedOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_UNWITHERING), "aba", "bcb", "aba", 'a', witherSkull, 'b', netherStar, 'c', new ItemStack(ModItems.ARMOR_UPGRADE_BASE)));
		VANILLA_RECIPES.add(interchanger = new ShapedOreRecipe(new ItemStack(ModBlocks.INTERCHANGER), "aba", "bcb", "aba", 'a', frienderPearl, 'b', carbonIngot, 'c', new ItemStack(ModBlocks.MACHINE_FRAMEWORK)));
		VANILLA_RECIPES.add(dankNullDock = new ShapedOreRecipe(new ItemStack(ModBlocks.DANKNULL_DOCK), "aba", "bcb", "aba", 'a', emeraldBit, 'b', new ItemStack(Items.REDSTONE), 'c', new ItemStack(ModBlocks.MACHINE_FRAMEWORK)));
		VANILLA_RECIPES.add(machineUpgradeSpeed = new ShapedOreRecipe(new ItemStack(ModBlocks.MACHINE_UPGRADE_SPEED), " a ", " b ", "ccc", 'a', emerald, 'b', carbonRod, 'c', carbonIngot));
		VANILLA_RECIPES.add(compressor = new ShapedOreRecipe(new ItemStack(ModBlocks.COMPRESSOR), "aba", "bcb", "aba", 'a', rawCarbon, 'b', emeraldBit, 'c', new ItemStack(ModBlocks.MACHINE_FRAMEWORK)));
		VANILLA_RECIPES.add(armorUpgradeRepulsion = new ShapedOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_REPULSION), "aba", "bcb", "aba", 'a', infinityBow, 'b', knockBackBow, 'c', new ItemStack(ModItems.ARMOR_UPGRADE_BASE)));
		VANILLA_RECIPES.add(machineFramework = new ShapedOreRecipe(new ItemStack(ModBlocks.MACHINE_FRAMEWORK), "aba", "b b", "aba", 'a', glassPane, 'b', coalBlock));
		VANILLA_RECIPES.add(amorUpgradeEnderVision = new ShapedOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_ENDERVISION), " a ", "bcd", " e ", 'a', new ItemStack(ModItems.NIGHT_VISION_GOGGLES), 'b', new ItemStack(ModItems.SKULL_ENDERMAN), 'c', new ItemStack(ModItems.ARMOR_UPGRADE_BASE), 'd', new ItemStack(ModItems.SKULL_ENDERMAN2), 'e', new ItemStack(ModItems.SKULL_FRIENDERMAN)));
		VANILLA_RECIPES.add(armorUpgradeNightVision = new ShapelessOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_NIGHTVISION), new ItemStack(ModItems.ARMOR_UPGRADE_BASE), new ItemStack(ModItems.NIGHT_VISION_GOGGLES)));
		VANILLA_RECIPES.add(emeraldBitsToEmerald = new ShapelessOreRecipe(new ItemStack(ModItems.BIT_EMERALD, 9), "gemEmerald"));
		VANILLA_RECIPES.add(nightVisionGoggles = new ShapedOreRecipe(new ItemStack(ModItems.NIGHT_VISION_GOGGLES), "aba", 'a', emeraldBit, 'b', carbonRod));
		VANILLA_RECIPES.add(armorUpgradeBase = new ShapedOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_BASE, 4), "aba", "b b", "aba", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', carbonRod));
		VANILLA_RECIPES.add(voidifer = new ShapedOreRecipe(new ItemStack(ModBlocks.VOIDIFIER), "aba", "bcb", "aba", 'a', rawCarbon, 'b', obsidian, 'c', lavaBucket));
		VANILLA_RECIPES.add(carbonStairs = new ShapedOreRecipe(new ItemStack(ModBlocks.CARBON_STAIRS, 4), "a  ", "aa ", "aaa", 'a', new ItemStack(ModBlocks.CARBON_BRICKS)));
		VANILLA_RECIPES.add(carbonBricks = new ShapedOreRecipe(new ItemStack(ModBlocks.CARBON_BRICKS, 4), "aa", "aa", 'a', carbonBlock));
		VANILLA_RECIPES.add(basicBattery = new ShapedOreRecipe(new ItemStack(ModItems.BASIC_BATTERY), "a", "b", "b", 'a', redstoneBlock, 'b', carbonIngot));
		VANILLA_RECIPES.add(advancedBattery = new ShapedOreRecipe(new ItemStack(ModItems.ADVANCED_BATTERY), "aba", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', new ItemStack(ModItems.BASIC_BATTERY)));
		VANILLA_RECIPES.add(carbonHarness = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_HARNESS), "a a", "aaa", "   ", 'a', "ingotCarbon"));
		VANILLA_RECIPES.add(jetpack = new ShapedOreRecipe(new ItemStack(ModItems.JETPACK), "a a", "aba", "c c", 'a', "ingotCarbon", 'b', new ItemStack(ModItems.CARBON_HARNESS), 'c', new ItemStack(ModItems.CARBON_THRUSTER)));
		VANILLA_RECIPES.add(carbonThruster = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_THRUSTER), " a ", "aba", "   ", 'a', "ingotCarbon", 'b', blazePowder));
		VANILLA_RECIPES.add(carbonRodRedstone = new ShapedOreRecipe(new ItemStack(ModItems.CARBONROD_REDSTONE), "aaa", "aba", "aaa", 'b', carbonRod, 'a', "dustRedstone"));
		VANILLA_RECIPES.add(carbonRodLapis = new ShapedOreRecipe(new ItemStack(ModItems.CARBONROD_LAPIS), "aaa", "aba", "aaa", 'a', "gemLapis", 'b', new ItemStack(ModItems.CARBONROD_REDSTONE)));
		VANILLA_RECIPES.add(carbonRodIron = new ShapedOreRecipe(new ItemStack(ModItems.CARBONROD_IRON), "aaa", "aba", "aaa", 'a', "ingotIron", 'b', new ItemStack(ModItems.CARBONROD_LAPIS)));
		VANILLA_RECIPES.add(carbonRodGold = new ShapedOreRecipe(new ItemStack(ModItems.CARBONROD_GOLD), "aaa", "aba", "aaa", 'a', "ingotGold", 'b', new ItemStack(ModItems.CARBONROD_IRON)));
		VANILLA_RECIPES.add(carbonRodDiamond = new ShapedOreRecipe(new ItemStack(ModItems.CARBONROD_DIAMOND), "aaa", "aba", "aaa", 'a', "gemDiamond", 'b', new ItemStack(ModItems.CARBONROD_GOLD)));
		VANILLA_RECIPES.add(carbonRodEmerald = new ShapedOreRecipe(new ItemStack(ModItems.CARBONROD_EMERALD), "aaa", "aba", "aaa", 'a', emeraldBit, 'b', new ItemStack(ModItems.CARBONROD_DIAMOND)));
		VANILLA_RECIPES.add(panelRedstone = new ShapedOreRecipe(new ItemStack(ModItems.PANEL_REDSTONE), "aaa", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_REDSTONE), 'b', redPane));
		VANILLA_RECIPES.add(panelLapis = new ShapedOreRecipe(new ItemStack(ModItems.PANEL_LAPIS), "aaa", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_LAPIS), 'b', bluePane));
		VANILLA_RECIPES.add(panelIron = new ShapedOreRecipe(new ItemStack(ModItems.PANEL_IRON), "aaa", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_IRON), 'b', whitePane));
		VANILLA_RECIPES.add(panelGold = new ShapedOreRecipe(new ItemStack(ModItems.PANEL_GOLD), "aaa", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_GOLD), 'b', yellowPane));
		VANILLA_RECIPES.add(panelDiamond = new ShapedOreRecipe(new ItemStack(ModItems.PANEL_DIAMOND), "aaa", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_DIAMOND), 'b', cyanPane));
		VANILLA_RECIPES.add(panelEmerald = new ShapedOreRecipe(new ItemStack(ModItems.PANEL_EMERALD), "aaa", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', limePane));
		VANILLA_RECIPES.add(carbonHelmet = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_HELMET), "aaa", "a a", 'a', carbonBlock));
		VANILLA_RECIPES.add(carbonChestplate = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_CHESTPLATE), "a a", "aaa", "aaa", 'a', carbonBlock));
		VANILLA_RECIPES.add(carbonLeggings = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_LEGGINGS), "aaa", "a a", "a a", 'a', carbonBlock));
		VANILLA_RECIPES.add(carbonBoots = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_BOOTS), "a a", "a a", 'a', carbonBlock));
		VANILLA_RECIPES.add(emeraldCarbonHelmet = new ShapedOreRecipe(carbonHelmetUpgraded, "aba", "a a", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', new ItemStack(ModItems.CARBON_HELMET)));
		VANILLA_RECIPES.add(emeraldCarbonChestplate = new ShapedOreRecipe(carbonChestplateUpgraded, "a a", "aba", "aaa", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', new ItemStack(ModItems.CARBON_CHESTPLATE)));
		VANILLA_RECIPES.add(emeraldCarbonLeggings = new ShapedOreRecipe(carbonLeggingsUpgraded, "aba", "a a", "a a", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', new ItemStack(ModItems.CARBON_LEGGINGS)));
		VANILLA_RECIPES.add(emeraldCarbonBoots = new ShapedOreRecipe(carbonBootsUpgraded, "aba", "a a", 'a', new ItemStack(ModItems.CARBONROD_EMERALD), 'b', new ItemStack(ModItems.CARBON_BOOTS)));
		VANILLA_RECIPES.add(carbonShield = new ShapedOreRecipe(new ItemStack(ModItems.CARBON_SHEILD), "aba", "aaa", " a ", 'a', rawCarbon, 'b', shield));
		VANILLA_RECIPES.add(magnet = new ShapedOreRecipe(new ItemStack(ModItems.MAGNET), "a b", "c c", "ccc", 'a', new ItemStack(ModItems.CARBONROD_LAPIS), 'b', new ItemStack(ModItems.CARBONROD_REDSTONE), 'c', ironIngot));
		VANILLA_RECIPES.add(advancedArmorStand = new ShapedOreRecipe(new ItemStack(ModItems.ARMOR_STAND), " a ", " a ", "bbb", 'a', ironIngot, 'b', stoneSlab));
		VANILLA_RECIPES.add(lapisDankNullUpgrade = new ShapedOreRecipe(dankNull1, "   ", "aba", "aaa", 'a', new ItemStack(ModItems.PANEL_LAPIS), 'b', dankNull0));
		VANILLA_RECIPES.add(ironDankNullUpgrade = new ShapedOreRecipe(dankNull2, "   ", "aba", "aaa", 'a', new ItemStack(ModItems.PANEL_IRON), 'b', dankNull1));
		VANILLA_RECIPES.add(goldDankNullUpgrade = new ShapedOreRecipe(dankNull3, "   ", "aba", "aaa", 'a', new ItemStack(ModItems.PANEL_GOLD), 'b', dankNull2));
		VANILLA_RECIPES.add(diamondDankNullUpgrade = new ShapedOreRecipe(dankNull4, "   ", "aba", "aaa", 'a', new ItemStack(ModItems.PANEL_DIAMOND), 'b', dankNull3));
		VANILLA_RECIPES.add(emeraldDankNullUpgrade = new ShapedOreRecipe(dankNull5, "   ", "aba", "aaa", 'a', new ItemStack(ModItems.PANEL_EMERALD), 'b', dankNull4));
		VANILLA_RECIPES.add(armorUpgradeFlight = new ShapelessOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_FLIGHT), new ItemStack(ModItems.ARMOR_UPGRADE_BASE), new ItemStack(ModItems.JETPACK)));
		VANILLA_RECIPES.add(armorUpgradeStepAssist = new ShapelessOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_STEPASSIST), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModItems.ARMOR_UPGRADE_BASE), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModBlocks.CARBON_STAIRS, 4), new ItemStack(ModBlocks.CARBON_STAIRS, 4)));
		VANILLA_RECIPES.add(armorUpgradeLavaWalker = new ShapelessOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_LAVAWALKER), lavaBucket, lavaBucket, lavaBucket, lavaBucket, new ItemStack(ModItems.ARMOR_UPGRADE_BASE), lavaBucket, lavaBucket, lavaBucket, lavaBucket));
		VANILLA_RECIPES.add(armorUpgradeSpeed = new ShapelessOreRecipe(new ItemStack(ModItems.ARMOR_UPGRADE_SPEED), swiftnessPotion, swiftnessPotion, swiftnessPotion, swiftnessPotion, new ItemStack(ModItems.ARMOR_UPGRADE_BASE), swiftnessPotion, swiftnessPotion, swiftnessPotion, swiftnessPotion));
		VANILLA_RECIPES.add(guide = new ShapelessOreRecipe(new ItemStack(ModItems.GUIDE), new ItemStack(Items.BOOK), new ItemStack(ModItems.RAW_CARBON)));
		VANILLA_RECIPES.add(carbonIngotToCarbonRod = new ShapedOreRecipe(carbonRod, "a", "a", "a", 'a', new ItemStack(ModItems.CARBON_INGOT)));
		VANILLA_RECIPES.add(carbonDoor = new ShapedOreRecipe(new ItemStack(ModBlocks.CARBON_DOOR), "aa", "aa", "aa", 'a', new ItemStack(ModItems.CARBON_INGOT)));
		VANILLA_RECIPES.add(emeraldToEmeraldBits = new ShapelessOreRecipe(new ItemStack(ModItems.BIT_DIAMOND, 9), diamond));
		//VANILLA_RECIPES.add(emeraldToEmeraldBits = new ShapelessOreRecipe(emerald, new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD), new ItemStack(ModItems.BIT_EMERALD)));
		//VANILLA_RECIPES.add(emeraldToEmeraldBits = new ShapelessOreRecipe(diamond, new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND), new ItemStack(ModItems.BIT_DIAMOND)));

		//Vanilla additions
		VANILLA_RECIPES.add(saddle = new ShapedOreRecipe(new ItemStack(Items.SADDLE), " a ", "aaa", "a a", 'a', leather));
		VANILLA_RECIPES.add(notchApple = new ShapedOreRecipe(new ItemStack(Items.GOLDEN_APPLE, 1, 1), "aaa", "aba", "aaa", 'a', goldBlock, 'b', apple));
		VANILLA_RECIPES.add(nameTag = new ShapelessOreRecipe(new ItemStack(Items.NAME_TAG), string, paper));

		if (!ConfigOptions.DISABLE_VANILLA_RECIPES) {
			for (IRecipe recipe : VANILLA_RECIPES) {
				GameRegistry.addRecipe(recipe);
			}
			/*
						VANILLA_RECIPES.add(new ShapedOreRecipe(diamond,
								"aaa",
								"aaa",
								"aaa",
								'a',
								carbonBlock
						));
			*/

		}

	}

}
