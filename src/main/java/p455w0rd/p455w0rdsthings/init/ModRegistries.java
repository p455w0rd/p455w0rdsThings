package p455w0rd.p455w0rdsthings.init;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import p455w0rd.p455w0rdsthings.api.IAccessible;
import p455w0rd.p455w0rdsthings.api.IInterchanger;
import p455w0rd.p455w0rdsthings.blocks.BlockSlabBase;
import p455w0rd.p455w0rdsthings.blocks.BlockSlabDoubleBase;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.AccessMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.ModeType;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;

/**
 * @author p455w0rd
 *
 */
public class ModRegistries {

	private static final Map<EntityPlayer, Boolean> NIGHTVISION_REGISTRY = Maps.newHashMap();
	private static final Map<EntityPlayer, Boolean> FLIGHT_REGISTRY = Maps.newHashMap();
	private static final Map<EntityLivingBase, Long> CHARGED_ENTITY_REGISTRY = Maps.newHashMap();
	private static final List<IInterchanger> INTERCHANGER_REGISTRY = Lists.newArrayList();
	private static Map<Integer, UUID> TAMED_FRIENDERMAN_REGISTRY = Maps.newHashMap();
	private static final Map<BlockSlabBase, BlockSlabDoubleBase> SLAB_REGISTRY = Maps.newHashMap();
	private static final Map<BlockSlabDoubleBase, BlockSlabBase> DOUBLE_SLAB_REGISTRY = Maps.newHashMap();

	public static Map<BlockSlabBase, BlockSlabDoubleBase> getSlabRegistry() {
		return SLAB_REGISTRY;
	}

	public static void registerSlab(BlockSlabBase slab, BlockSlabDoubleBase doubleSlab) {
		if (!getSlabRegistry().containsKey(slab)) {
			getSlabRegistry().put(slab, doubleSlab);
			getDoubleSlabRegistry().put(doubleSlab, slab);
		}
	}

	public static BlockSlabBase getSlab(BlockSlabDoubleBase doubleSlab) {
		return getDoubleSlabRegistry().get(doubleSlab);
	}

	public static BlockSlabDoubleBase getDoubleSlab(BlockSlabBase slab) {
		return getSlabRegistry().get(slab);
	}

	public static Map<BlockSlabDoubleBase, BlockSlabBase> getDoubleSlabRegistry() {
		return DOUBLE_SLAB_REGISTRY;
	}

	public static Map<Integer, UUID> getTamedFriendermanRegistry() {
		return TAMED_FRIENDERMAN_REGISTRY;
	}

	public static void registerTamedFrienderman(EntityPlayer player, EntityFrienderman frienderman) {
		if (!TAMED_FRIENDERMAN_REGISTRY.containsKey(frienderman)) {
			TAMED_FRIENDERMAN_REGISTRY.put(frienderman.getEntityId(), player.getGameProfile().getId());
		}
	}

	public static void unregisterTamedFrienderman(EntityFrienderman frienderman) {
		if (TAMED_FRIENDERMAN_REGISTRY.containsKey(frienderman)) {
			TAMED_FRIENDERMAN_REGISTRY.remove(frienderman);
		}
	}

	public static void setTamedFriendermanRegistry(Map<Integer, UUID> registry) {
		TAMED_FRIENDERMAN_REGISTRY = registry;
	}

	public static void registerInterchanger(IInterchanger tile) {
		if (!INTERCHANGER_REGISTRY.contains(tile)) {
			INTERCHANGER_REGISTRY.add(tile);
		}
	}

	public static List<IInterchanger> getInterchangerRegistry() {
		return INTERCHANGER_REGISTRY;
	}

	public static void unregisterInterchanger(IInterchanger interchanger) {
		if (INTERCHANGER_REGISTRY.contains(interchanger)) {
			INTERCHANGER_REGISTRY.remove(interchanger);
		}
	}

	public static List<IInterchanger> getInterchangersSending(ModeType mode) {
		List<IInterchanger> tileList = Lists.newArrayList();
		switch (mode) {
		case FLUID:
			break;
		case ITEM:
			for (IInterchanger tile : getInterchangerRegistry()) {
				if (tile.canSendItems()) {
					tileList.add(tile);
				}
			}
			break;
		case RF:
			for (IInterchanger tile : getInterchangerRegistry()) {
				if (tile.canSendEnergy()) {
					tileList.add(tile);
				}
			}
			break;
		default:
			break;
		}
		return tileList;
	}

	public static List<IInterchanger> getInterchangersReceiving(ModeType mode) {
		List<IInterchanger> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangerRegistry()) {
			switch (mode) {
			case FLUID:
				if (tile.canReceiveFluids()) {
					tileList.add(tile);
				}
				break;
			case ITEM:
				if (tile.canReceiveItems()) {
					tileList.add(tile);
				}
				break;
			case RF:
				if (tile.canReceiveEnergy()) {
					tileList.add(tile);
				}
				break;
			default:
				break;
			}
		}
		return tileList;
	}

	public static List<IAccessible> getPublicSendingInterchangers(ModeType mode) {
		List<IAccessible> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangersSending(mode)) {
			IAccessible accessibleTile = (IAccessible) tile;
			if (tile instanceof IAccessible) {
				if (accessibleTile.getAccessMode() == AccessMode.PUBLIC) {
					tileList.add(accessibleTile);
				}
			}
		}
		return tileList;
	}

	public static List<IAccessible> getPrivateSendingInterchangers(ModeType mode) {
		List<IAccessible> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangersSending(mode)) {
			IAccessible accessibleTile = (IAccessible) tile;
			if (tile instanceof IAccessible) {
				if (accessibleTile.getAccessMode() == AccessMode.PRIVATE) {
					tileList.add(accessibleTile);
				}
			}
		}
		return tileList;
	}

	public static List<IInterchanger> getPublicReceivingInterchangers(ModeType mode) {
		List<IInterchanger> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangersReceiving(mode)) {
			IAccessible accessibleTile = (IAccessible) tile;
			if (tile instanceof IAccessible) {
				if (accessibleTile.getAccessMode() == AccessMode.PUBLIC) {
					tileList.add(tile);
				}
			}
		}
		return tileList;
	}

	public static List<IInterchanger> getPrivateReceivingInterchangers(ModeType mode, UUID owner) {
		List<IInterchanger> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangersReceiving(mode)) {
			IAccessible accessibleTile = (IAccessible) tile;
			if (tile instanceof IAccessible) {
				if (accessibleTile.getAccessMode() == AccessMode.PRIVATE && accessibleTile.getOwner().equals(owner)) {
					tileList.add(tile);
				}
			}
		}
		return tileList;
	}

	public static List<IInterchanger> getSendingInterchangersWithOwner(UUID uuid, ModeType mode) {
		List<IInterchanger> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangersSending(mode)) {
			if (((IAccessible) tile).getOwner().equals(uuid)) {
				tileList.add(tile);
			}
		}
		return tileList;
	}

	public static List<IInterchanger> getReceivingInterchangersWithOwner(UUID uuid, ModeType mode) {
		List<IInterchanger> tileList = Lists.newArrayList();
		for (IInterchanger tile : getInterchangersReceiving(mode)) {
			if (((IAccessible) tile).getOwner().equals(uuid)) {
				tileList.add(tile);
			}
		}
		return tileList;
	}

	public static void setNV(EntityPlayer player, boolean value) {
		if (!NIGHTVISION_REGISTRY.containsKey(player)) {
			NIGHTVISION_REGISTRY.put(player, value);
			return;
		}
		NIGHTVISION_REGISTRY.replace(player, value);
	}

	public static boolean isNVEnabled(EntityPlayer player) {
		if (!NIGHTVISION_REGISTRY.containsKey(player)) {
			NIGHTVISION_REGISTRY.put(player, false);
		}
		return NIGHTVISION_REGISTRY.get(player);
	}

	public static void setFlyingPlayer(EntityPlayer player, boolean value) {
		if (!FLIGHT_REGISTRY.containsKey(player)) {
			FLIGHT_REGISTRY.put(player, value);
			return;
		}
		FLIGHT_REGISTRY.replace(player, value);
	}

	public static boolean isPlayerCached(EntityPlayer player) {
		return FLIGHT_REGISTRY.containsKey(player);
	}

	public static boolean isFlightEnabled(EntityPlayer player) {
		return !isPlayerCached(player) ? false : FLIGHT_REGISTRY.get(player);
	}

	public static boolean isEntityCharged(EntityLivingBase entity) {
		return CHARGED_ENTITY_REGISTRY.containsKey(entity);
	}

	public static void setCharged(EntityLivingBase entity) {
		if (!isEntityCharged(entity)) {
			CHARGED_ENTITY_REGISTRY.put(entity, System.currentTimeMillis());
		}
		else {
			CHARGED_ENTITY_REGISTRY.replace(entity, CHARGED_ENTITY_REGISTRY.get(entity), System.currentTimeMillis());
		}
	}

	public static Set<EntityLivingBase> getChargedEntityList() {
		return CHARGED_ENTITY_REGISTRY.keySet();
	}

	public static void unsetCharged(EntityLivingBase entity) {
		if (isEntityCharged(entity)) {
			CHARGED_ENTITY_REGISTRY.remove(entity);
		}
	}

	public static void updateChargedList() {
		long currentTime = System.currentTimeMillis() / 1000L;
		try {
			for (EntityLivingBase entity : getChargedEntityList()) {
				long difference = currentTime - CHARGED_ENTITY_REGISTRY.get(entity) / 1000L;
				if (difference >= 2L) {
					unsetCharged(entity);
				}
			}
		}
		catch (Exception e) {
		}
	}

}
