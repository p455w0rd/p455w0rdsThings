package p455w0rd.p455w0rdsthings.init;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraftforge.common.MinecraftForge;
import p455w0rd.p455w0rdsthings.client.render.ChunkBorderRenderer;
import p455w0rd.p455w0rdsthings.client.render.DankNullRenderer;
import p455w0rd.p455w0rdsthings.client.render.entity.RenderEnderman2;
import p455w0rd.p455w0rdsthings.client.render.entity.RenderEndermite2;
import p455w0rd.p455w0rdsthings.client.render.entity.RenderFrienderman;
import p455w0rd.p455w0rdsthings.client.render.entity.RenderPArmorStand;
import p455w0rd.p455w0rdsthings.entity.EntityEnderman2;
import p455w0rd.p455w0rdsthings.entity.EntityEndermite2;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderPearl;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rd.p455w0rdsthings.entity.EntityPArmorStand;

/**
 * @author p455w0rd
 *
 */
public class ModRendering {

	public static void init() {
		RenderManager rm = Minecraft.getMinecraft().getRenderManager();
		RenderItem itemRenderer = Minecraft.getMinecraft().getRenderItem();
		rm.entityRenderMap.put(EntityPArmorStand.class, new RenderPArmorStand(rm));
		rm.entityRenderMap.put(EntityEnderman2.class, new RenderEnderman2(rm));
		rm.entityRenderMap.put(EntityFrienderman.class, new RenderFrienderman(rm));
		rm.entityRenderMap.put(EntityFrienderPearl.class, new RenderSnowball<EntityFrienderPearl>(rm, ModItems.FRIENDER_PEARL, itemRenderer));
		rm.entityRenderMap.put(EntityEndermite2.class, new RenderEndermite2(rm));
		MinecraftForge.EVENT_BUS.register(new DankNullRenderer());
		MinecraftForge.EVENT_BUS.register(new ChunkBorderRenderer());
	}

}
