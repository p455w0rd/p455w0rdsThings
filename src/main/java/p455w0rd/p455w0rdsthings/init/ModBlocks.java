package p455w0rd.p455w0rdsthings.init;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rd.p455w0rdsthings.blocks.BlockCarbon;
import p455w0rd.p455w0rdsthings.blocks.BlockCarbonBrick;
import p455w0rd.p455w0rdsthings.blocks.BlockCarbonDoor;
import p455w0rd.p455w0rdsthings.blocks.BlockCarbonSlab;
import p455w0rd.p455w0rdsthings.blocks.BlockCarbonSlabDouble;
import p455w0rd.p455w0rdsthings.blocks.BlockCarbonStairs;
import p455w0rd.p455w0rdsthings.blocks.BlockCompressor;
import p455w0rd.p455w0rdsthings.blocks.BlockDankNullDock;
import p455w0rd.p455w0rdsthings.blocks.BlockFakeLava;
import p455w0rd.p455w0rdsthings.blocks.BlockInterchanger;
import p455w0rd.p455w0rdsthings.blocks.BlockMachineFramework;
import p455w0rd.p455w0rdsthings.blocks.BlockMachineUpgrade;
import p455w0rd.p455w0rdsthings.blocks.BlockNetherCarbonOre;
import p455w0rd.p455w0rdsthings.blocks.BlockSkullBase;
import p455w0rd.p455w0rdsthings.blocks.BlockTrashCan;
import p455w0rd.p455w0rdsthings.blocks.fluid.BlockFluidCarbon;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileBlockSkull;
import p455w0rd.p455w0rdsthings.client.render.TESRBlockSkull;

public class ModBlocks {
	private static final List<Block> BLOCK_LIST = new ArrayList<Block>();

	public static final BlockCarbon CARBON_BLOCK = new BlockCarbon();
	public static final BlockNetherCarbonOre NETHER_CARBON_ORE = new BlockNetherCarbonOre();
	public static final BlockCarbonBrick CARBON_BRICKS = new BlockCarbonBrick();
	public static final BlockCarbonStairs CARBON_STAIRS = new BlockCarbonStairs();
	public static final BlockCarbonSlab CARBON_SLAB = new BlockCarbonSlab();
	public static final BlockCarbonSlabDouble CARBON_DOUBLE_SLAB = new BlockCarbonSlabDouble();
	public static final BlockTrashCan VOIDIFIER = new BlockTrashCan();
	public static final BlockFakeLava FAKE_LAVA = new BlockFakeLava();
	public static final BlockSkullBase.Enderman ENDERMAN_SKULL = new BlockSkullBase.Enderman();
	public static final BlockSkullBase.Frienderman FRIENDERMAN_SKULL = new BlockSkullBase.Frienderman();
	public static final BlockSkullBase.Enderman2 ENDERMAN2_SKULL = new BlockSkullBase.Enderman2();
	public static final BlockCompressor COMPRESSOR = new BlockCompressor();
	public static final BlockInterchanger INTERCHANGER = new BlockInterchanger();
	public static final BlockMachineUpgrade MACHINE_UPGRADE_SPEED = new BlockMachineUpgrade.Speed();
	public static final BlockMachineFramework MACHINE_FRAMEWORK = new BlockMachineFramework();
	public static final BlockCarbonDoor CARBON_DOOR = new BlockCarbonDoor();
	// @formatter:off
	public static final BlockFluidCarbon SUBLIMATED_CARBON = new BlockFluidCarbon(
			ModFluids.SUBLIMATED_CARBON,
			Material.LAVA);
	// @formatter:on
	public static final BlockDankNullDock DANKNULL_DOCK = new BlockDankNullDock();

	public static void init() {
		GameRegistry.registerTileEntity(TileBlockSkull.class, ModGlobals.MODID + ":tile_pskull");
		BLOCK_LIST.addAll(Arrays.asList(CARBON_BLOCK, CARBON_DOOR, NETHER_CARBON_ORE, CARBON_BRICKS, CARBON_STAIRS, CARBON_SLAB, CARBON_DOUBLE_SLAB, VOIDIFIER, FAKE_LAVA, ENDERMAN_SKULL, FRIENDERMAN_SKULL, ENDERMAN2_SKULL, COMPRESSOR, INTERCHANGER, MACHINE_UPGRADE_SPEED, DANKNULL_DOCK, MACHINE_FRAMEWORK, SUBLIMATED_CARBON));
		ModLogger.info("Added Blocks");
	}

	@SideOnly(Side.CLIENT)
	public static void preInitModels() {
		CARBON_DOOR.registerStateMapper();
		for (Block block : BLOCK_LIST) {
			if (block instanceof IModelHolder) {
				((IModelHolder) block).initModel();
			}
		}
		ClientRegistry.bindTileEntitySpecialRenderer(TileBlockSkull.class, new TESRBlockSkull());

		ModLogger.info("Added Block Models");
	}

	public static List<Block> getList() {
		return BLOCK_LIST;
	}
}
