package p455w0rd.p455w0rdsthings.init;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.items.CapabilityItemHandler;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileTrashCan;
import p455w0rd.p455w0rdsthings.client.gui.GuiCompressor;
import p455w0rd.p455w0rdsthings.client.gui.GuiDankNull;
import p455w0rd.p455w0rdsthings.client.gui.GuiInterchanger;
import p455w0rd.p455w0rdsthings.client.gui.GuiTrashCan;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiGuide;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiMainPage;
import p455w0rd.p455w0rdsthings.container.ContainerCompressor;
import p455w0rd.p455w0rdsthings.container.ContainerDankNull;
import p455w0rd.p455w0rdsthings.container.ContainerInterchanger;
import p455w0rd.p455w0rdsthings.container.ContainerTrashCan;
import p455w0rd.p455w0rdsthings.guide.PlayerData;
import p455w0rd.p455w0rdsthings.items.ItemGuide;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rd.p455w0rdsthings.util.TileUtils;

public class ModGuiHandler implements IGuiHandler {

	public static void init() {
		ModLogger.info("Registering GUI Handler");
		NetworkRegistry.INSTANCE.registerGuiHandler(ModGlobals.MODID, new ModGuiHandler());
	}

	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World worldIn, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		switch (GUIType.values()[id]) {
		case DANKNULL:
			return new ContainerDankNull(player, DankNullUtils.getNewDankNullInventory(player.getHeldItemMainhand()));
		case TRASH:
			if (TileUtils.getTrashCan(worldIn, pos) != null) {
				TileTrashCan te = TileUtils.getTrashCan(worldIn, pos);
				return new ContainerTrashCan(player.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null), te.invItem, te.invFluid, player);
			}
			break;
		case COMPRESSOR:
			if (TileUtils.getCompressor(worldIn, pos) != null) {
				return new ContainerCompressor(player, TileUtils.getCompressor(worldIn, pos));
			}
			break;
		case INTERCHANGER:
			if (TileUtils.getInterchanger(worldIn, pos) != null) {
				return new ContainerInterchanger(player, TileUtils.getInterchanger(worldIn, pos));
			}
			break;
		default:
			break;
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World worldIn, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		switch (GUIType.values()[id]) {
		case DANKNULL:
			return new GuiDankNull(new ContainerDankNull(player, DankNullUtils.getNewDankNullInventory(player.getHeldItemMainhand())), player);
		case TRASH:
			TileEntity te = worldIn.getTileEntity(pos);
			if (te != null && te instanceof TileTrashCan) {
				return new GuiTrashCan(new ContainerTrashCan(player.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null), ((TileTrashCan) te).invItem, ((TileTrashCan) te).invFluid, player));
			}
			break;
		case COMPRESSOR:
			if (TileUtils.getCompressor(worldIn, pos) != null) {
				return new GuiCompressor(player, TileUtils.getCompressor(worldIn, new BlockPos(x, y, z)));
			}
			break;
		case INTERCHANGER:
			if (TileUtils.getInterchanger(worldIn, pos) != null) {
				return new GuiInterchanger(player, TileUtils.getInterchanger(worldIn, pos));
			}
			break;
		case GUIDE:
			if (ItemGuide.forcedPage != null) {
				GuiGuide gui = GuideUtils.createBookletGuiFromPage(null, ItemGuide.forcedPage);
				ItemGuide.forcedPage = null;
				return gui;
			}
			else {
				PlayerData.PlayerSave data = PlayerData.getDataFromPlayer(player);
				if (data.lastOpenBooklet != null) {
					return data.lastOpenBooklet;
				}
				else {
					return new GuiMainPage(null);
				}
			}
		default:
			break;
		}
		return null;
	}

	public static void launchGui(GUIType type, EntityPlayer playerIn, World worldIn, int x, int y, int z) {
		playerIn.openGui(P455w0rdsThings.INSTANCE, type.ordinal(), worldIn, x, y, z);
	}

	public static enum GUIType {
			DANKNULL, TRASH, COMPRESSOR, INTERCHANGER, GUIDE;
	}

}
