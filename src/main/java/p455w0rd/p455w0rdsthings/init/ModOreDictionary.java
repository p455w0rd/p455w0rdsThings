package p455w0rd.p455w0rdsthings.init;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.integration.EnderIo;

public class ModOreDictionary {

	public static void addOres() {
		OreDictionary.registerOre("rawCarbon", ModItems.RAW_CARBON);
		OreDictionary.registerOre("carbonRaw", ModItems.RAW_CARBON);
		OreDictionary.registerOre("blockCarbon", ModBlocks.CARBON_BLOCK);
		OreDictionary.registerOre("blockNetherCarbonOre", ModBlocks.NETHER_CARBON_ORE);
		OreDictionary.registerOre("ingotCarbon", ModItems.CARBON_INGOT);
		OreDictionary.registerOre("enderpearl", ModItems.FRIENDER_PEARL);
		OreDictionary.registerOre("itemSkull", ModItems.SKULL_ENDERMAN);
		OreDictionary.registerOre("itemSkull", ModItems.SKULL_ENDERMAN2);
		OreDictionary.registerOre("itemSkull", ModItems.SKULL_FRIENDERMAN);
		OreDictionary.registerOre("itemEndermanSkull", ModItems.SKULL_ENDERMAN);
		OreDictionary.registerOre("itemEndermanSkull", ModItems.SKULL_ENDERMAN2);
		OreDictionary.registerOre("itemEndermanSkull", ModItems.SKULL_FRIENDERMAN);
		OreDictionary.registerOre("nuggetDiamond", ModItems.BIT_DIAMOND);
		OreDictionary.registerOre("itemMachineChassis", ModBlocks.MACHINE_FRAMEWORK);
		OreDictionary.registerOre("itemMachineChassi", ModBlocks.MACHINE_FRAMEWORK);
		if (Mods.ENDERIO.isLoaded()) {
			EnderIo.registerOreDictionaryItems();
		}
		else {
			ItemStack skull = new ItemStack(Items.SKULL, 1, OreDictionary.WILDCARD_VALUE);
			OreDictionary.registerOre("itemSkull", skull);
		}
		ModLogger.info("Added Ore Dictionary Entries");
	}
}
