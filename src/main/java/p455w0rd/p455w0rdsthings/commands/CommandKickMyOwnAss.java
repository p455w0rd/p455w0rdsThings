/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

/**
 * @author p455w0rd
 *
 */
public class CommandKickMyOwnAss extends CommandBase {

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return sender instanceof EntityPlayer;
	}

	@Override
	public String getName() {
		return "kickme";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "command.p455w0rdsthings.kickme.usage";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (sender != null && sender instanceof EntityPlayerMP) {
			((EntityPlayerMP) sender).connection.disconnect("You may now reconnect :)");
		}
	}

}
