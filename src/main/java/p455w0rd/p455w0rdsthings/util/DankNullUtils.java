/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.util;

import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.collect.Lists;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.inventory.DankNulItemHandler;
import p455w0rd.p455w0rdsthings.inventory.InventoryDankNull;
import p455w0rd.p455w0rdsthings.items.ItemDankNull;
import p455w0rd.p455w0rdsthings.network.PacketSetSelectedItem;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.ItemUtils;

/**
 * @author p455w0rd
 *
 */
public class DankNullUtils {

	public static ItemStack getDankNull(EntityPlayer player) {
		InventoryPlayer playerInv = player.inventory;
		ItemStack dankNullItem = null;
		if (player.getHeldItemMainhand() != null) {
			if ((player.getHeldItemMainhand().getItem() instanceof ItemDankNull)) {
				dankNullItem = player.getHeldItemMainhand();
			}
			else if ((player.getHeldItemOffhand() != null) && ((player.getHeldItemOffhand().getItem() instanceof ItemDankNull))) {
				dankNullItem = player.getHeldItemOffhand();
			}
		}
		else if (player.getHeldItemOffhand() != null) {
			if ((player.getHeldItemOffhand().getItem() instanceof ItemDankNull)) {
				dankNullItem = player.getHeldItem(EnumHand.OFF_HAND);
			}
			else if ((player.getHeldItemMainhand() != null) && ((player.getHeldItemMainhand().getItem() instanceof ItemDankNull))) {
				dankNullItem = player.getHeldItemMainhand();
			}
		}
		if (dankNullItem == null) {
			int invSize = playerInv.getSizeInventory();
			if (invSize <= 0) {
				return null;
			}
			for (int i = 0; i < invSize; i++) {
				ItemStack itemStack = playerInv.getStackInSlot(i);
				if (itemStack != null) {
					if ((itemStack.getItem() instanceof ItemDankNull)) {
						dankNullItem = itemStack;
						break;
					}
				}
			}
		}
		return dankNullItem;
	}

	public static ItemStack getDankNullForStack(EntityPlayer player, ItemStack stack) {
		InventoryPlayer playerInv = player.inventory;
		ItemStack dankNullItem = null;
		int invSize = playerInv.getSizeInventory();
		if (invSize <= 0) {
			return null;
		}
		for (int i = 0; i < invSize; i++) {
			ItemStack itemStack = playerInv.getStackInSlot(i);
			if (itemStack != null) {
				if ((itemStack.getItem() instanceof ItemDankNull)) {
					if (isFiltered(getNewDankNullInventory(itemStack), stack) != null) {
						dankNullItem = itemStack;
						break;
					}
				}
			}
		}
		return dankNullItem;
	}

	public static void reArrangeStacks(InventoryDankNull inventory) {
		if (inventory != null) {
			List<ItemStack> stackList = Lists.<ItemStack>newArrayList();
			for (int i = 0; i < inventory.getSizeInventory(); i++) {
				if (inventory.getStackInSlot(i) != null) {
					stackList.add(inventory.getStackInSlot(i));
				}
			}
			if (stackList.size() == 0) {
				setSelectedStackIndex(inventory, -1);
			}
			else {
				for (int i = 0; i < stackList.size(); i++) {
					inventory.setInventorySlotContents(i, stackList.get(i));
				}
				for (int i = stackList.size(); i < inventory.getSizeInventory(); i++) {
					inventory.setInventorySlotContents(i, null);
				}
			}
			setSelectedIndexApplicable(inventory);
		}
	}

	public static ItemStack[] getInventoryListArray(InventoryDankNull inventory) {
		if (inventory != null) {
			return inventory.getStacks();
		}
		return new ItemStack[0];
	}

	public static int getSelectedStackIndex(InventoryDankNull inventory) {
		if (inventory != null && inventory.getDankNull() != null) {
			ItemStack dankNull = inventory.getDankNull();
			if (!dankNull.hasTagCompound()) {
				dankNull.setTagCompound(new NBTTagCompound());
			}
			if (!dankNull.getTagCompound().hasKey("selectedIndex")) {
				dankNull.getTagCompound().setInteger("selectedIndex", 0);
			}
			return dankNull.getTagCompound().getInteger("selectedIndex");
		}
		return -1;
	}

	public static boolean isDankNull(ItemStack stack) {
		return stack.getItem() instanceof ItemDankNull;
	}

	public static void setSelectedStackIndex(InventoryDankNull inventory, int index) {
		if (inventory != null && inventory.getDankNull() != null) {
			setSelectedStackIndex(inventory, index, true);
		}
	}

	public static void setSelectedStackIndex(InventoryDankNull inventory, int index, boolean sync) {
		if (inventory != null && inventory.getDankNull() != null) {
			ItemStack dankNull = inventory.getDankNull();
			if (!dankNull.hasTagCompound()) {
				dankNull.setTagCompound(new NBTTagCompound());
			}
			//NBTTagCompound capData = getInventory(dankNull).serializeNBT();
			dankNull.getTagCompound().setInteger("selectedIndex", index);
			//getInventory(dankNull).deserializeNBT(capData);
			if (FMLCommonHandler.instance().getSide().isClient()) {
				ModNetworking.INSTANCE.sendToServer(new PacketSetSelectedItem(index));
			}
			if (FMLCommonHandler.instance().getSide().isServer()) {
				for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
					//ModNetworking.INSTANCE.sendTo(new PacketSyncDankNull(DankNullUtils.getInventory(itemStackIn).serializeNBT()), player);
				}
			}
			else {
				//ModNetworking.INSTANCE.sendToAll(new PacketSyncDankNull(DankNullUtils.getInventory(itemStackIn).serializeNBT()));
			}
		}
	}

	public static void setNextSelectedStack(InventoryDankNull inventory) {
		if (inventory != null && inventory.getDankNull() != null) {
			setNextSelectedStack(inventory, null);
		}
	}

	public static void setNextSelectedStack(InventoryDankNull inventory, EntityPlayer player) {
		if (inventory == null || inventory.getDankNull() == null) {
			return;
		}
		int currentIndex = getSelectedStackIndex(inventory);
		int totalSize = getItemCount(inventory);
		int maxIndex = totalSize - 1;
		int newIndex = 0;
		if (totalSize > 1) {
			if (currentIndex == maxIndex) {
				//ModNetworking.INSTANCE.sendToServer(new PacketSetSelectedItem(newIndex));
				setSelectedStackIndex(inventory, newIndex);
			}
			else {
				newIndex = currentIndex + 1;
				//ModNetworking.INSTANCE.sendToServer(new PacketSetSelectedItem(newIndex));
				setSelectedStackIndex(inventory, newIndex);
			}
			if (player != null && newIndex >= 0) {
				displaySelectedMessage(inventory, player, newIndex);
			}
		}
	}

	public static void displaySelectedMessage(InventoryDankNull inventory, EntityPlayer player, int index) {
		if (getItemByIndex(inventory, index) == null) {
			reArrangeStacks(inventory);
		}
		EasyMappings.message(player, new TextComponentString(TextFormatting.BLUE + "" + TextFormatting.ITALIC + "" + getItemByIndex(inventory, index).getDisplayName() + " Selected"));
	}

	public static void setPreviousSelectedStack(InventoryDankNull inventory, EntityPlayer player) {
		int currentIndex = getSelectedStackIndex(inventory);
		int totalSize = getItemCount(inventory);
		int maxIndex = totalSize - 1;
		int newIndex = 0;
		if (totalSize > 1) {
			if (currentIndex == 0) {
				newIndex = maxIndex;
				//ModNetworking.INSTANCE.sendToServer(new PacketSetSelectedItem(newIndex));
				setSelectedStackIndex(inventory, newIndex);
			}
			else {
				newIndex = currentIndex - 1;
				//ModNetworking.INSTANCE.sendToServer(new PacketSetSelectedItem(newIndex));
				setSelectedStackIndex(inventory, newIndex);
			}
			if (player != null) {
				displaySelectedMessage(inventory, player, newIndex);
			}
		}
	}

	public static int getItemCount(InventoryDankNull inventory) {
		int count = 0;
		if (inventory != null) {
			for (int i = 0; i < inventory.getSizeInventory(); i++) {
				if (inventory.getStackInSlot(i) != null) {
					count++;
				}
			}
		}
		return count;
	}

	/*
		public static int getItemCount(ContainerDankNull container) {
			List<ItemStack> dankInventory = container.inventoryItemStacks;
			int numItems = 0;
			for (int i = 0; i < dankInventory.size(); i++) {
				if (dankInventory.get(i) != null) {
					++numItems;
				}
			}
			return numItems;
		}

		public static NBTTagList getInventoryTagList(ItemStack itemStackIn) {
			if (itemStackIn != null) {
				if ((itemStackIn.hasTagCompound()) && (itemStackIn.getTagCompound().hasKey("danknull-inventory"))) {
					return itemStackIn.getTagCompound().getTagList("danknull-inventory", 10);
				}
			}
			return null;
		}


			public static void decrStackSize(ItemStack dankNull, int index, int amount) {
				if (dankNull == null) {
					return;
				}
				ItemStack indexedStack = getItemByIndex(dankNull, index);
				int newStackSize = getStackSize(indexedStack) - amount;
				NBTTagCompound nbtTC = indexedStack.getTagCompound();
				NBTTagList tagList = dankNull.getTagCompound().getTagList("danknull-inventory", 10);
				if (newStackSize >= 1L) {
					nbtTC.setLong(InventoryDankNull.TAG_COUNT, newStackSize);
				}
				else {
					tagList.removeTag(index);
					reArrangeStacks(dankNull);
				}
			}
		**/
	public static void decrSelectedStackSize(InventoryDankNull inventory, int amount) {
		if (inventory == null || inventory.getDankNull() == null || getSelectedStack(inventory) == null) {
			return;
		}
		getSelectedStack(inventory).stackSize -= amount;
		if (getSelectedStack(inventory).stackSize <= 0) {
			inventory.setInventorySlotContents(getSelectedStackIndex(inventory), null);
		}
		reArrangeStacks(inventory);
		/*
		int newStackSize = getSelectedStackSize(dankNull) - amount;
		NBTTagCompound nbtTC = getSelectedStack(dankNull).getTagCompound();
		if (newStackSize >= 1L) {
			nbtTC.setInteger(InventoryDankNull.TAG_COUNT, newStackSize);
		}
		else {
			NBTTagList tagList = dankNull.getTagCompound().getTagList("danknull-inventory", 10);
			int index = getSelectedStackIndex(dankNull);
			if (tagList != null && tagList.getCompoundTagAt(index) != null) {
				tagList.removeTag(index);
			}
			reArrangeStacks(dankNull);
		}
		*/
	}

	public static int getSelectedStackSize(InventoryDankNull inventory) {
		if (inventory != null && inventory.getDankNull() != null) {
			ItemStack selectedStack = getSelectedStack(inventory);
			if (selectedStack != null) {
				return selectedStack.stackSize;
			}
		}
		return 0;
	}

	public static InventoryDankNull getInventoryFromHeld(EntityPlayer player) {
		if (player != null && player.getHeldItemMainhand() != null && DankNullUtils.isDankNull(player.getHeldItemMainhand())) {
			return getInventoryFromStack(player.getHeldItemMainhand());
		}
		return null;
	}

	public static ItemStack getSelectedStack(InventoryDankNull inventory) {
		if (inventory != null && inventory.getDankNull() != null) {
			ItemStack dankNull = inventory.getDankNull();
			//if ((itemStackIn.hasTagCompound()) && (itemStackIn.getTagCompound().hasKey("danknull-inventory"))) {
			if (!dankNull.hasTagCompound()) {
				setSelectedStackIndex(inventory, isEmpty(inventory) ? 1 : 0);
			}

			//if (dankNull.getTagCompound().hasKey("danknull-inventory")) {
			NBTTagCompound nbtTC = dankNull.getTagCompound();
			if (!nbtTC.hasKey("selectedIndex")) {
				nbtTC.setInteger("selectedIndex", 0);
			}
			int selectedIndex = nbtTC.getInteger("selectedIndex");
			if (selectedIndex > -1) {
				return inventory.getStackInSlot(selectedIndex);
			}
			//}
		}
		return null;
	}

	public static boolean isEmpty(InventoryDankNull inventory) {
		return inventory.isEmpty();
	}

	public static ItemStack isFiltered(InventoryDankNull inventory, ItemStack filteredStack) {
		if (inventory != null) {
			for (int i = 0; i < inventory.getSizeInventory(); i++) {
				if (inventory.getStackInSlot(i) != null) {
					if (ItemUtils.compareStacks(inventory.getStackInSlot(i), filteredStack)) {
						return filteredStack;
					}
				}
			}
		}
		return null;
	}

	public static DankNulItemHandler getHandler(ItemStack dankNull) {
		if (hasDankNullHandler(dankNull)) {
			return (DankNulItemHandler) dankNull.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
		}
		return null;
	}

	public static boolean hasDankNullHandler(ItemStack dankNull) {
		return dankNull.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
	}

	/*
		public static boolean addFilteredStackToDankNull(ItemStack itemStackIn, ItemStack newStack) {
			if (itemStackIn != null || newStack == null) {
				ItemStack filteredStack = isFiltered(itemStackIn, newStack);
				if (filteredStack != null) {
					int maxStackSize = getDankNullMaxStackSize(itemStackIn);
					int currentFilteredStackSize = getStackSize(filteredStack);
					int itemToAddStackSize = newStack.stackSize;
					if (currentFilteredStackSize + itemToAddStackSize > maxStackSize) {
						setDankNullStackSize(filteredStack, maxStackSize);
					}
					else {
						setDankNullStackSize(filteredStack, currentFilteredStackSize + itemToAddStackSize);
					}
					return true;
				}
			}
			return false;
		}
	*/
	public static boolean addFilteredStackToDankNull(InventoryDankNull inventory, ItemStack filteredStack) {
		if (getIndexForStack(inventory, filteredStack) >= 0) {
			ItemStack currentStack = getFilteredStack(inventory, filteredStack);
			currentStack.stackSize += filteredStack.stackSize;
			if (currentStack.stackSize > DankNullUtils.getDankNullMaxStackSize(inventory)) {
				currentStack.stackSize = DankNullUtils.getDankNullMaxStackSize(inventory);
			}
			inventory.setInventorySlotContents(getIndexForStack(inventory, filteredStack), currentStack);
			//getInventory(dankNull).serializeNBT();
			return true;
		}
		return false;
	}

	public static ItemStack getFilteredStack(InventoryDankNull inventory, ItemStack stack) {
		if (isFiltered(inventory, stack) != null) {
			return getItemByIndex(inventory, getIndexForStack(inventory, stack));
		}
		return null;
	}

	public static int getIndexForStack(InventoryDankNull inventory, ItemStack filteredStack) {
		if (isFiltered(inventory, filteredStack) != null) {
			for (int i = 0; i < inventory.getSizeInventory(); i++) {
				if (inventory.getStackInSlot(i) != null) {
					if (ItemUtils.compareStacks(inventory.getStackInSlot(i), filteredStack)) {
						return i;
					}
				}
			}
		}
		return -1;
	}

	public static ItemStack getItemByIndex(InventoryDankNull inventory, int index) {
		if (inventory != null && index >= 0) {
			return inventory.getStackInSlot(index);
		}
		return null;
	}

	public static void decrDankNullStackSize(InventoryDankNull inventory, ItemStack stack, int amount) {
		if (isFiltered(inventory, stack) != null) {
			ItemStack currentStack = getFilteredStack(inventory, stack);
			currentStack.stackSize -= amount;
			if (currentStack.stackSize <= 0) {
				currentStack = null;
			}
			inventory.markDirty();
		}
		/*
		int newStackSize = getStackSize(dankNullStack) - amount;
		NBTTagCompound nbtTC = dankNullStack.getTagCompound();
		if (newStackSize >= 1L) {
			nbtTC.setInteger(InventoryDankNull.TAG_COUNT, newStackSize);
		}
		else {
			if (itemStackIn.hasTagCompound() && itemStackIn.getTagCompound().hasKey("danknull-inventory")) {
				NBTTagList tagList = itemStackIn.getTagCompound().getTagList("danknull-inventory", Constants.NBT.TAG_COMPOUND);
				if (tagList != null && tagList.tagCount() > 0) {
					int index = getSelectedStackIndex(itemStackIn);
					if (index != -1 && tagList.tagCount() > index && tagList.get(index) != null) {
						tagList.removeTag(index);
					}
				}
				reArrangeStacks(itemStackIn);
				setSelectedIndexApplicable(itemStackIn);
			}
		}
		return itemStackIn;
		*/
	}

	public static InventoryDankNull getNewDankNullInventory(ItemStack stack) {
		return (stack.getItem() instanceof ItemDankNull) ? new InventoryDankNull(stack) : null;
	}

	public static InventoryDankNull getInventoryFromStack(ItemStack stack) {
		return getNewDankNullInventory(stack);
	}

	public static int getDankNullMaxStackSize(ItemStack itemStackIn) {
		int level = itemStackIn.getItemDamage() + 1;
		if (level == 6) {
			return Integer.MAX_VALUE;
		}
		return level * (128 * level);
	}

	public static int getDankNullMaxStackSize(InventoryDankNull inventory) {
		if (inventory != null && inventory.getDankNull() != null) {
			int level = inventory.getDankNull().getItemDamage() + 1;
			if (level == 6) {
				return Integer.MAX_VALUE;
			}
			return level * (128 * level);
		}
		return 0;
	}

	public static int getColor(int damage, boolean opaque) {
		switch (damage) {
		case 0:
			return opaque ? 0xFFEC4848 : 0x99EC4848;
		case 1:
			return opaque ? 0xFF4885EC : 0x994885EC;
		default:
		case 2:
			return opaque ? 0xFFFFFFFF : 0x99FFFFFF;
		case 3:
			return opaque ? 0xFFFFFF00 : 0x99FFFF00;
		case 4:
			return opaque ? 0xFF00FFFF : 0x9900FFFF;
		case 5:
			return opaque ? 0xFF17FF6D : 0x9917FF6D;
		}
	}

	public static int getSlotCount(ItemStack stack) {
		return (stack.getItemDamage() + 1) * 9;
	}

	public static int getSlotCount(InventoryDankNull inventory) {
		return inventory.getSizeInventory();
	}

	public static void setSelectedIndexApplicable(InventoryDankNull inventory) {
		if (inventory != null && inventory.getDankNull() != null) {
			if (getSelectedStackIndex(inventory) >= 0 && getItemByIndex(inventory, getSelectedStackIndex(inventory)) != null) {
				return;
			}
			boolean indexFound = false;
			for (int i = getSelectedStackIndex(inventory); i > -1; i--) {
				if (getItemByIndex(inventory, i) != null) {
					indexFound = true;
					setSelectedStackIndex(inventory, i);
					return;
				}
			}
			for (int i = getSlotCount(inventory) - 1; i > -1; i--) {
				if (getItemByIndex(inventory, i) != null) {
					indexFound = true;
					setSelectedStackIndex(inventory, i);
					return;
				}
			}
			if (!indexFound) {
				setSelectedStackIndex(inventory, -1);
			}
		}
		//getInventory(dankNull).serializeNBT();
	}
	/*
		public static ItemStack getVanillaStack(ItemStack stack) {
			if (isDankNullStack(stack)) {
				ItemStack newStack = stack.copy();
				if (newStack.hasTagCompound() && newStack.getTagCompound().hasKey(InventoryDankNull.TAG_COUNT)) {
					newStack.getTagCompound().removeTag(InventoryDankNull.TAG_COUNT);
					return newStack;
				}
			}
			return stack == null ? null : stack.copy();
		}
	*/

	public static EnumActionResult placeBlock(@Nonnull IBlockState state, World world, BlockPos pos) {
		return world.setBlockState(pos, state, 2) ? EnumActionResult.SUCCESS : EnumActionResult.FAIL;
	}
}