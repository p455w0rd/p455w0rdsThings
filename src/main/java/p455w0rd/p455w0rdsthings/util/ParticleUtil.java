package p455w0rd.p455w0rdsthings.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.Particle;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import p455w0rd.p455w0rdsthings.client.particle.ParticleGlowFlame;
import p455w0rd.p455w0rdsthings.client.particle.ParticleLove;
import p455w0rd.p455w0rdsthings.client.particle.ParticlePortal2;
import p455w0rd.p455w0rdsthings.client.render.ParticleRenderer;

/**
 * @author p455w0rd
 *
 */
public class ParticleUtil {

	public static void spawn(EnumParticles particleType, World world, double xCoordIn, double yCoordIn, double zCoordIn, double xSpeedIn, double ySpeedIn, double zSpeedIn) {
		if (world == null || FMLCommonHandler.instance().getSide().isServer()) {
			return;
		}
		Particle particle = null;
		switch (particleType) {
		case LOVE:
			particle = new ParticleLove(world, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn);
			break;
		case PORTAL_GREEN:
			particle = new ParticlePortal2(world, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn);
			break;
		case GLOW_FLAME:
			int g = Math.max(100, world.rand.nextInt(255));
			float scale = Math.max(0.45f, world.rand.nextFloat() - 0.1f);
			particle = new ParticleGlowFlame(world, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn, 255, g, Math.max(51, world.rand.nextInt(100)), scale);
			//particle = new ParticleGlowFlame(world, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn, g, 255, Math.max(51, world.rand.nextInt(100)), scale);
			break;
		default:
			break;
		}
		switch (particleType.getRenderer()) {
		case "custom":
			ParticleRenderer.getInstance().addParticle(particle);
			break;
		case "vanilla":
			Minecraft.getMinecraft().effectRenderer.addEffect(particle);
			break;
		}

	}

}