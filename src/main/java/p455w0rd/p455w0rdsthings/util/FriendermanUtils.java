package p455w0rd.p455w0rdsthings.util;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rdslib.util.InventoryUtils;

/**
 * @author p455w0rd
 *
 */
public class FriendermanUtils {

	@SuppressWarnings("unchecked")
	public static List<EntityFrienderman> getTamedFriendermenInRange(EntityPlayer player, double radius) {
		List<EntityFrienderman> friendermenInRange = Lists.<EntityFrienderman>newArrayList();
		List<EntityFrienderman> possibilities = (List<EntityFrienderman>) p455w0rdslib.util.EntityUtils.getEntitiesInRange(EntityFrienderman.class, player.getEntityWorld(), player.posX, player.posY, player.posZ, radius);
		for (EntityFrienderman frienderman : possibilities) {
			if (frienderman.isTamed()) {
				if (frienderman.getOwnerId().equals(player.getUniqueID())) {
					friendermenInRange.add(frienderman);
				}
			}
		}
		return friendermenInRange;
	}

	public static List<EntityFrienderman> getTamedFriendermenWithChestInRange(EntityPlayer player, double radius) {
		List<EntityFrienderman> tamedFriendermen = Lists.<EntityFrienderman>newArrayList();
		List<EntityFrienderman> endermanInRadius = Lists.<EntityFrienderman>newArrayList(getTamedFriendermenInRange(player, radius));
		for (EntityFrienderman frienderman : endermanInRadius) {
			if (frienderman.isHoldingChest()) {
				tamedFriendermen.add(frienderman);
			}
		}
		return tamedFriendermen;
	}

	public static IInventory getInventoryForHeldChest(EntityFrienderman frienderman) {
		if (frienderman.isHoldingChest()) {
			return frienderman.getHeldChestInventory();
		}
		return null;
	}

	public static List<EntityFrienderman> getFriendermenWithStorageSpaceInRangeForStack(EntityPlayer player, ItemStack stack, double radius) {
		List<EntityFrienderman> friendermanWithStorageSpaceForStack = Lists.<EntityFrienderman>newArrayList();
		List<EntityFrienderman> endermanInRadius = Lists.<EntityFrienderman>newArrayList(getTamedFriendermenInRange(player, radius));
		for (EntityFrienderman frienderman : endermanInRadius) {
			if (InventoryUtils.canInsertStack(frienderman.getHeldChestInventory(), stack)) {
				friendermanWithStorageSpaceForStack.add(frienderman);
			}
		}
		return friendermanWithStorageSpaceForStack;
	}

	public static EntityFrienderman getFirstFriendermenWithStorageSpaceInRangeForStack(EntityPlayer player, ItemStack stack, double radius) {
		List<EntityFrienderman> endermanInRadius = Lists.<EntityFrienderman>newArrayList(getTamedFriendermenWithChestInRange(player, radius));
		for (EntityFrienderman frienderman : endermanInRadius) {
			if (frienderman.getHeldChestInventory() != null && InventoryUtils.canInsertStack(frienderman.getHeldChestInventory(), stack)) {
				return frienderman;
			}
		}
		return null;
	}

	public static void setEntityWorld(Entity entity, World world) {
		entity.setWorld(world);
	}

	//public static List<>

	public static enum ChestType {
			VANILLA, IRONCHEST, ENDERSTORAGE;
	}

}
