package p455w0rd.p455w0rdsthings.util;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiEntry;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiMainPage;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiPage;
import p455w0rd.p455w0rdsthings.guide.PlayerData;
import p455w0rd.p455w0rdsthings.guide.PlayerData.PlayerSave;
import p455w0rd.p455w0rdsthings.init.ModGuide;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.network.PacketPlayerDataC;
import p455w0rd.p455w0rdsthings.network.PacketPlayerDataS;
import p455w0rdslib.util.ItemUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class GuideUtils {

	public static IGuideEntry allAndSearch;

	public static void addGuideEntry(IGuideEntry entry) {
		ModGuide.GUIDE_ENTRIES.add(entry);
	}

	public static IGuidePage findFirstPageForStack(ItemStack stack) {
		for (IGuidePage page : ModGuide.GUIDE_PAGES_WITH_ITEM_OR_FLUID_DATA) {
			List<ItemStack> stacks = new ArrayList<ItemStack>();
			page.getItemStacksForPage(stacks);
			if (stacks != null && !stacks.isEmpty()) {
				for (ItemStack pageStack : stacks) {
					if (ItemUtils.areItemsEqual(pageStack, stack)) {
						return page;
					}
				}
			}
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	public static GuiPage createBookletGuiFromPage(GuiScreen previousScreen, IGuidePage page) {
		GuiMainPage mainPage = new GuiMainPage(previousScreen);

		IGuideChapter chapter = page.getChapter();
		GuiEntry entry = new GuiEntry(previousScreen, mainPage, chapter.getEntry(), chapter, "", false);

		return createPageGui(previousScreen, entry, page);
	}

	@SideOnly(Side.CLIENT)
	public static GuiPage createPageGui(GuiScreen previousScreen, GuiGuideBase parentPage, IGuidePage page) {
		IGuideChapter chapter = page.getChapter();

		IGuidePage[] allPages = chapter.getAllPages();
		int pageIndex = chapter.getPageIndex(page);
		IGuidePage page1;
		IGuidePage page2;

		if (page.shouldBeOnLeftSide()) {
			page1 = page;
			page2 = pageIndex >= allPages.length - 1 ? null : allPages[pageIndex + 1];
		}
		else {
			page1 = pageIndex <= 0 ? null : allPages[pageIndex - 1];
			page2 = page;
		}

		return new GuiPage(previousScreen, parentPage, page1, page2);
	}

	public static void resetPlayerData() {
		EntityPlayer player = P455w0rdsThings.PROXY.getPlayer();
		if (player != null) {
			PlayerData.PlayerSave data = PlayerData.getDataFromPlayer(player);
			data.bookGottenAlready = false;
			data.bookmarks = new Object[12];
			data.didBookTutorial = false;
			syncPlayerDataToServer();
		}
	}

	public static void syncPlayerDataToClient(EntityPlayer player) {
		if (player instanceof EntityPlayerMP) {
			NBTTagCompound compound = new NBTTagCompound();
			NBTTagCompound data = new NBTTagCompound();
			PlayerData.getDataFromPlayer(player).writeToNBT(data, false);
			compound.setTag("Data", data);
			ModNetworking.INSTANCE.sendTo(new PacketPlayerDataC(compound), (EntityPlayerMP) player);
		}
	}

	public static void syncPlayerDataToServer() {
		EntityPlayer player = P455w0rdsThings.PROXY.getPlayer();
		if (player != null) {
			NBTTagCompound compound = new NBTTagCompound();
			compound.setInteger("World", player.getEntityWorld().provider.getDimension());
			compound.setUniqueId("UUID", player.getUniqueID());
			PlayerSave data = PlayerData.getDataFromPlayer(player);
			compound.setTag("Bookmarks", data.saveBookmarks());
			compound.setBoolean("DidBookTutorial", data.didBookTutorial);
			ModNetworking.INSTANCE.sendToServer(new PacketPlayerDataS(compound));
		}
	}

	@SideOnly(Side.CLIENT)
	public static GuiEntry createEntryGui(String desc) {
		for (IGuideEntry entry : ModGuide.GUIDE_ENTRIES) {
			if (entry.getIdentifier().equals(desc)) {
				return new GuiEntry(null, new GuiMainPage(null), entry, 0, "", false);
			}
		}
		return null;
	}

	public static IGuideEntry createEntry(String desc) {
		for (IGuideEntry entry : ModGuide.GUIDE_ENTRIES) {
			if (entry.getIdentifier().equals(desc)) {
				return entry;
			}
		}
		return null;
	}

	public static IGuidePage getBookletPageById(String id) {
		if (id != null) {
			for (IGuideChapter chapter : ModGuide.ALL_CHAPTERS) {
				for (IGuidePage page : chapter.getAllPages()) {
					if (id.equals(page.getIdentifier())) {
						return page;
					}
				}
			}
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	public static void drawHorizontalGradientRect(int left, int top, int right, int bottom, int startColor, int endColor, float zLevel) {
		float f = (startColor >> 24 & 255) / 255.0F;
		float f1 = (startColor >> 16 & 255) / 255.0F;
		float f2 = (startColor >> 8 & 255) / 255.0F;
		float f3 = (startColor & 255) / 255.0F;
		float f4 = (endColor >> 24 & 255) / 255.0F;
		float f5 = (endColor >> 16 & 255) / 255.0F;
		float f6 = (endColor >> 8 & 255) / 255.0F;
		float f7 = (endColor & 255) / 255.0F;
		GlStateManager.disableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.shadeModel(7425);
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer renderer = tessellator.getBuffer();
		renderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
		renderer.pos(left, top, zLevel).color(f1, f2, f3, f).endVertex();
		renderer.pos(left, bottom, zLevel).color(f1, f2, f3, f).endVertex();
		renderer.pos(right, bottom, zLevel).color(f5, f6, f7, f4).endVertex();
		renderer.pos(right, top, zLevel).color(f5, f6, f7, f4).endVertex();
		tessellator.draw();
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.enableTexture2D();
	}

	@SideOnly(Side.CLIENT)
	public static void renderStackToGui(ItemStack stack, int x, int y, float scale) {
		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.enableDepth();
		GlStateManager.enableRescaleNormal();
		GlStateManager.translate(x, y, 0);
		GlStateManager.scale(scale, scale, scale);

		Minecraft mc = Minecraft.getMinecraft();
		boolean flagBefore = mc.fontRendererObj.getUnicodeFlag();
		mc.fontRendererObj.setUnicodeFlag(false);
		Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(stack, 0, 0);
		//Minecraft.getMinecraft().getRenderItem().renderItemOverlayIntoGUI(mc.fontRendererObj, stack, 0, 0, null);
		mc.fontRendererObj.setUnicodeFlag(flagBefore);

		RenderHelper.disableStandardItemLighting();
		GlStateManager.popMatrix();
	}

}
