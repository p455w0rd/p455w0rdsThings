package p455w0rd.p455w0rdsthings.util;

import javax.annotation.Nullable;

import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;

/**
 * @author p455w0rd
 *
 */
public class MachineUtils {

	public static enum Upgrades {
			SPEED, CAPACITY;
	}

	public static enum InWorldUpgrades {
			SPEED(null, null);

		ResourceLocation texture;
		TextureAtlasSprite sprite;

		InWorldUpgrades(@Nullable ResourceLocation modelTexture, @Nullable TextureAtlasSprite particleSprite) {
			texture = modelTexture;
			sprite = particleSprite;
		}

		public ResourceLocation getTexture() {
			return texture;
		}

		public TextureAtlasSprite getParticleSprite() {
			return sprite;
		}
	}

}
