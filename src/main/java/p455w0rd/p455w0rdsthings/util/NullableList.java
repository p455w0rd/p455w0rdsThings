package p455w0rd.p455w0rdsthings.util;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author p455w0rd
 *
 */
public class NullableList<E> extends AbstractList<E> {
	private final List<E> delegate;
	private final E defaultElement;

	public static <E> NullableList<E> create() {
		return new NullableList<E>();
	}

	/**
	 * Creates a new NullableList prefilled null values
	 */
	public static <E> NullableList<E> withSize(int size) {
		return withSize(size, null);
	}

	/**
	 * Creates a new NullableList with <i>fixed</i> size, and filled with the object passed.
	 */
	@SuppressWarnings("unchecked")
	public static <E> NullableList<E> withSize(int size, @Nullable E fill) {
		Object[] aobject = new Object[size];
		Arrays.fill(aobject, fill);
		return new NullableList<E>(Arrays.asList((E[]) aobject), fill);
	}

	@SafeVarargs
	public static <E> NullableList<E> from(E p_193580_0_, E... p_193580_1_) {
		return new NullableList<E>(Arrays.asList(p_193580_1_), p_193580_0_);
	}

	protected NullableList() {
		this(new ArrayList<E>(), null);
	}

	protected NullableList(List<E> delegateIn, @Nullable E listType) {
		this.delegate = delegateIn;
		this.defaultElement = listType;
	}

	@Override
	@Nonnull
	public E get(int p_get_1_) {
		return this.delegate.get(p_get_1_);
	}

	@Override
	public E set(int p_set_1_, E p_set_2_) {
		return this.delegate.set(p_set_1_, p_set_2_);
	}

	@Override
	public void add(int p_add_1_, E p_add_2_) {
		this.delegate.add(p_add_1_, p_add_2_);
	}

	@Override
	public E remove(int p_remove_1_) {
		return this.delegate.remove(p_remove_1_);
	}

	@Override
	public int size() {
		return this.delegate.size();
	}

	@Override
	public void clear() {
		if (this.defaultElement == null) {
			super.clear();
		}
		else {
			for (int i = 0; i < this.size(); ++i) {
				this.set(i, this.defaultElement);
			}
		}
	}
}