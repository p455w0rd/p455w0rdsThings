package p455w0rd.p455w0rdsthings.util;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.passive.HorseArmorType;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import p455w0rdslib.util.EnumUtils;

public class HorseUtils {
	private static Map<String, HorseArmorType> TYPES = new HashMap<>();

	private static Class<?>[][] classMap = new Class<?>[][] {
			{
					HorseArmorType.class, int.class, String.class, String.class
			}
	};

	public static void registerHorseArmor(String name, int protection, String texture) {
		HorseArmorType type = EnumUtils.addEnum(classMap, HorseArmorType.class, name, protection, "", texture);
		ReflectionHelper.setPrivateValue(HorseArmorType.class, type, texture, "textureName", "field_188586_e");
		TYPES.put(name, type);
	}

	public static HorseArmorType getTypeSafe(String name, int protection, String texture) {
		if (!HorseUtils.TYPES.containsKey(name)) {
			registerHorseArmor(name, protection, texture);
		}
		return TYPES.get(name);
	}

	public static HorseArmorType gerTypeRaw(String name) {
		return TYPES.get(name);
	}
}
