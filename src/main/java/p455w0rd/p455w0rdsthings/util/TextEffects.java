package p455w0rd.p455w0rdsthings.util;

import static net.minecraft.util.text.TextFormatting.AQUA;
import static net.minecraft.util.text.TextFormatting.BLUE;
import static net.minecraft.util.text.TextFormatting.GOLD;
import static net.minecraft.util.text.TextFormatting.GREEN;
import static net.minecraft.util.text.TextFormatting.LIGHT_PURPLE;
import static net.minecraft.util.text.TextFormatting.RED;
import static net.minecraft.util.text.TextFormatting.YELLOW;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextFormatting;

/**
 * @author p455w0rd
 *
 */
public class TextEffects {

	private static final TextFormatting[] fabulousness = new TextFormatting[] {
			RED,
			GOLD,
			YELLOW,
			GREEN,
			AQUA,
			BLUE,
			LIGHT_PURPLE
	};

	public static String makeFabulous(String input) {
		return ludicrousFormatting(input, fabulousness, 80.0, 1, 1);
	}

	public static String ludicrousFormatting(String input, TextFormatting[] colours, double delay, int step, int posstep) {
		StringBuilder sb = new StringBuilder(input.length() * 3);
		if (delay <= 0) {
			delay = 0.001;
		}

		int offset = (int) Math.floor(Minecraft.getSystemTime() / delay) % colours.length;

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);

			int col = ((i * posstep) + colours.length - offset) % colours.length;

			sb.append(colours[col].toString());
			sb.append(c);
		}

		return sb.toString();
	}

}
