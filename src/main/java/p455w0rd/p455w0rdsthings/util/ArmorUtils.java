package p455w0rd.p455w0rdsthings.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.util.EnumHelper;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonJetPlate;
import p455w0rd.p455w0rdsthings.items.ItemJetPack;

public class ArmorUtils {

	public static boolean hasUpgrade(ItemStack is, Upgrades upgrade) {
		if (is.hasTagCompound()) {
			NBTTagCompound nbt = is.getTagCompound();
			return nbt.hasKey(upgrade.getNBTKey());
		}
		return false;
	}

	@Nullable
	public static ItemStack enableUpgrade(ItemStack is, EntityEquipmentSlot armor, Upgrades upgrade) {
		if (!getUpgradeTypes(armor).contains(upgrade)) {
			return is;
		}
		if (!hasUpgrade(is, upgrade)) {
			if (!is.hasTagCompound()) {
				is.setTagCompound(new NBTTagCompound());
			}
		}
		NBTTagCompound nbt = is.getTagCompound();
		nbt.setBoolean(upgrade.getNBTKey(), true);
		return is;
	}

	public static void removeUpgrade(ItemStack is, Upgrades upgrade) {
		if (is.hasTagCompound()) {
			NBTTagCompound nbt = is.getTagCompound();
			if (nbt.hasKey(upgrade.getNBTKey())) {
				nbt.removeTag(upgrade.getNBTKey());
			}
		}
	}

	public static void disableUpgrade(ItemStack is, Upgrades upgrade) {
		if (isUpgradeEnabled(is, upgrade)) {
			NBTTagCompound nbt = is.getTagCompound();
			nbt.setBoolean(upgrade.getNBTKey(), false);
		}
	}

	public static List<Upgrades> getUpgradeTypes(EntityEquipmentSlot armor) {
		List<Upgrades> upgradeList = new ArrayList<Upgrades>();
		for (Upgrades upgrade : Upgrades.values()) {
			if (upgrade.getArmorTypes().contains(armor)) {
				upgradeList.add(upgrade);
			}
		}
		/*
			List<Upgrades> list = new ArrayList<Upgrades>();
			switch (armor) {
			case CHEST:
				list.add(Upgrades.FLIGHT);
				list.add(Upgrades.UNWITHERING);
				break;
			case FEET:
				list.add(Upgrades.STEPASSIST);
				list.add(Upgrades.LAVAWALKER);
				break;
			case HEAD:
				list.add(Upgrades.NIGHTVISION);
				list.add(Upgrades.ENDERVISION);
				break;
			case LEGS:
				list.add(Upgrades.SPEED);
				break;
			case MAINHAND:
			case OFFHAND:
			default:
				break;
			}
			*/
		return upgradeList;
	}

	public static List<Upgrades> getActiveUpgrades(ItemStack stack, EntityEquipmentSlot armor) {
		List<Upgrades> activeUpgradeList = new ArrayList<Upgrades>();
		List<Upgrades> possibleTypes = getUpgradeTypes(armor);
		for (int i = 0; i < possibleTypes.size(); i++) {
			if (hasUpgrade(stack, possibleTypes.get(i))) {
				activeUpgradeList.add(possibleTypes.get(i));
			}
		}
		return activeUpgradeList;
	}

	public static boolean isApplicableUpgrade(EntityEquipmentSlot type, Upgrades upgrade) {
		return upgrade.getArmorTypes().contains(type);
	}

	public static boolean isUpgradeEnabled(ItemStack is, Upgrades upgrade) {
		if (is != null && is.hasTagCompound()) {
			NBTTagCompound nbt = is.getTagCompound();
			return (nbt.hasKey(upgrade.getNBTKey())) && (nbt.getBoolean(upgrade.getNBTKey()));

		}
		return false;
	}

	public static String getArmorTypeString(EntityEquipmentSlot slot) {
		switch (slot) {
		case CHEST:
			return "Chestplate";
		case FEET:
			return "Sabatons";
		case HEAD:
			return "Helm";
		case LEGS:
			return "Greaves";
		default:
			return "";
		}
	}

	public static boolean isFlightItem(ItemStack stack) {
		if (stack.getItem() instanceof ItemEmeraldCarbonJetPlate || stack.getItem() instanceof ItemJetPack) {
			return isUpgradeEnabled(stack, Upgrades.FLIGHT);
		}
		return false;
	}

	public static boolean isEmeraldCarbonChestPlate(ItemStack stack) {
		return (stack != null && stack.getItem() instanceof ItemEmeraldCarbonJetPlate || (stack.getItem() instanceof ItemEmeraldCarbonArmor));
	}

	public static IEnergyContainerItem getRFItem(ItemStack stack) {
		if (stack.getItem() instanceof IEnergyContainerItem) {
			return (IEnergyContainerItem) stack.getItem();
		}
		return null;
	}

	public static int getRFStored(ItemStack stack) {
		if (getRFItem(stack) != null) {
			return getRFItem(stack).getEnergyStored(stack);
		}
		return 0;
	}

	public static int drainRF(ItemStack stack, int amount) {
		if (getRFItem(stack) != null && getRFStored(stack) > amount) {
			return getRFItem(stack).extractEnergy(stack, amount, false);
		}
		return amount;
	}

	public static ItemArmor.ArmorMaterial addArmorMaterial(String enumName, String textureName, int durability, int[] reductionAmounts, int enchantability, SoundEvent soundOnEquip, float toughness) {
		return EnumHelper.addEnum(ItemArmor.ArmorMaterial.class, enumName, new Class[] {
				String.class,
				Integer.TYPE,
				int[].class,
				Integer.TYPE,
				SoundEvent.class,
				Float.TYPE
		}, new Object[] {
				textureName,
				Integer.valueOf(durability),
				reductionAmounts,
				Integer.valueOf(enchantability),
				soundOnEquip,
				Float.valueOf(toughness)
		});
	}

	public static EntityEquipmentSlot[] getAllEquipmentSlots() {
		return new EntityEquipmentSlot[] {
				EntityEquipmentSlot.HEAD,
				EntityEquipmentSlot.CHEST,
				EntityEquipmentSlot.LEGS,
				EntityEquipmentSlot.FEET
		};
	}

	public static enum Upgrades {
			STEPASSIST("Step Assist", "PStep", EntityEquipmentSlot.FEET),
			FLIGHT("JetPack", "PFlight", EntityEquipmentSlot.CHEST),
			NIGHTVISION("Night Vision", "PNVision", EntityEquipmentSlot.HEAD),
			SPEED("Speed", "PSpeed", EntityEquipmentSlot.LEGS),
			UNWITHERING("Witherless", "PWitherless", EntityEquipmentSlot.CHEST, EntityEquipmentSlot.LEGS),
			ENDERVISION("Ender Vision", "PEVision", EntityEquipmentSlot.HEAD),
			LAVAWALKER("Lava Walker", "PLWalker", EntityEquipmentSlot.FEET),
			REPULSION("Repulsion", "PLRepulse", EntityEquipmentSlot.CHEST, EntityEquipmentSlot.LEGS),
			BATTERY("Battery", "PLBattery", getAllEquipmentSlots()),
			SOULDBOUND("Soul Bound", "PSoulBound", getAllEquipmentSlots());

		private final String description;
		private final String nbtKey;
		private final EntityEquipmentSlot[] slot;

		private Upgrades(String desc, String nbtKeyString, EntityEquipmentSlot... slotIn) {
			description = desc;
			nbtKey = nbtKeyString;
			slot = slotIn;
		}

		public String getDesc() {
			return description;
		}

		public String getNBTKey() {
			return nbtKey;
		}

		public List<EntityEquipmentSlot> getArmorTypes() {
			return Arrays.asList(slot);
		}

	}

}
