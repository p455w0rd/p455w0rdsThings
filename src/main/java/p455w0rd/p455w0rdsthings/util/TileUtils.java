package p455w0rd.p455w0rdsthings.util;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileTrashCan;

/**
 * @author p455w0rd
 *
 */
public class TileUtils {

	public static TileCompressor getCompressor(World world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof TileCompressor) {
			return (TileCompressor) te;
		}
		return null;
	}

	public static TileInterchanger getInterchanger(World world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof TileInterchanger) {
			return (TileInterchanger) te;
		}
		return null;
	}

	public static TileTrashCan getTrashCan(World world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof TileTrashCan) {
			return (TileTrashCan) te;
		}
		return null;
	}

}
