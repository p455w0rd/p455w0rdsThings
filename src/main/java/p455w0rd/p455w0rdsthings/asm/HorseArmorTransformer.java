package p455w0rd.p455w0rdsthings.asm;

import codechicken.lib.asm.ASMBlock;
import codechicken.lib.asm.ASMReader;
import codechicken.lib.asm.ModularASMTransformer;
import codechicken.lib.asm.ModularASMTransformer.MethodInjector;
import codechicken.lib.asm.ModularASMTransformer.MethodReplacer;
import codechicken.lib.asm.ObfMapping;
import net.minecraft.launchwrapper.IClassTransformer;

import java.util.Map;

/**
 * Created by covers1624 on 17/10/2016.
 */
public class HorseArmorTransformer implements IClassTransformer {
	private ModularASMTransformer transformer = new ModularASMTransformer();

	public HorseArmorTransformer() {
		Map<String, ASMBlock> blocks = ASMReader.loadResource("/assets/p455w0rdsthings/asm/horseHooks.asm");

		String entityHorse = "net/minecraft/entity/passive/EntityHorse";
		String horseArmorType = "net/minecraft/entity/passive/HorseArmorType";

		ObfMapping entityInit = new ObfMapping(entityHorse, "func_70088_a", "()V");
		transformer.add(new MethodInjector(entityInit, blocks.get("n_entityInit"), blocks.get("i_entityInit"), true));

		ObfMapping getHorseArmorType = new ObfMapping(entityHorse, "func_184783_dl", "()Lnet/minecraft/entity/passive/HorseArmorType;");
		transformer.add(new MethodInjector(getHorseArmorType, blocks.get("i_getHorseArmorType"), true));

		ObfMapping setHorseArmorStack = new ObfMapping(entityHorse, "func_146086_d", "(Lnet/minecraft/item/ItemStack;)V");
		transformer.add(new MethodReplacer(setHorseArmorStack, blocks.get("n_setHorseArmorStack"), blocks.get("r_setHorseArmorStack")));

		ObfMapping getByItem = new ObfMapping(horseArmorType, "func_188576_a", "(Lnet/minecraft/item/Item;)Lnet/minecraft/entity/passive/HorseArmorType;");
		transformer.add(new MethodInjector(getByItem, blocks.get("i_getByItem"), true));

		ObfMapping getByItemStack = new ObfMapping(horseArmorType, "func_188580_a", "(Lnet/minecraft/item/ItemStack;)Lnet/minecraft/entity/passive/HorseArmorType;");
		transformer.add(new MethodInjector(getByItemStack, blocks.get("i_getByItemStack"), true));

		ObfMapping isHorseArmor = new ObfMapping(horseArmorType, "func_188577_b", "(Lnet/minecraft/item/Item;)Z");
		transformer.add(new MethodInjector(isHorseArmor, blocks.get("i_isHorseArmor"), true));

	}

	@Override
	public byte[] transform(String name, String tname, byte[] bytes) {
		return transformer.transform(name, bytes);
	}
}