package p455w0rd.p455w0rdsthings.asm;

import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.HorseArmorType;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import p455w0rd.p455w0rdsthings.api.IHorseArmor;

import static net.minecraft.entity.passive.HorseArmorType.*;

public class PHooks {

	public static final DataParameter<String> ARMOR_STRING = EntityDataManager.createKey(EntityHorse.class, DataSerializers.STRING);

	public static void onInitHorse(EntityHorse entity) {
		entity.getDataManager().register(PHooks.ARMOR_STRING, "");
	}

	public static void setHorseArmorStack(EntityHorse entityHorse, HorseArmorType armorType) {
		entityHorse.getDataManager().set(PHooks.ARMOR_STRING, armorType.name());
		if (armorType.ordinal() <= 3) {
			entityHorse.getDataManager().set(EntityHorse.HORSE_ARMOR, Integer.valueOf(armorType.getOrdinal()));
		}
	}

	public static String parseHorseArmorTypeTextureName(HorseArmorType horseArmorType, String textureName) {
		if (horseArmorType.ordinal() <= 3) {
			return "textures/entity/horse/armor/horse_armor_" + textureName + ".png";
		}
		return textureName;

	}

	public static HorseArmorType getCustomHorseArmor(EntityHorse entity) {
		String armor = entity.getDataManager().get(PHooks.ARMOR_STRING);

		if (!armor.isEmpty()) {
			for (HorseArmorType type : HorseArmorType.values()) {
				if (type.name().equals(armor)) {
					return type;
				}
			}
		}

		return null;
	}

	public static HorseArmorType getByItem(Item stack) {
		return getByItemStack(new ItemStack(stack));
	}

	public static HorseArmorType getByItemStack(ItemStack stack) {
		if (stack != null) {
			Item item = stack.getItem();
			if (item instanceof IHorseArmor) {
				return ((IHorseArmor) stack.getItem()).getArmorType(stack);
			}
			else {
				return item == Items.IRON_HORSE_ARMOR ? IRON : (item == Items.GOLDEN_HORSE_ARMOR ? GOLD : (item == Items.DIAMOND_HORSE_ARMOR ? DIAMOND : NONE));
			}

		}
		return NONE;
	}

}
