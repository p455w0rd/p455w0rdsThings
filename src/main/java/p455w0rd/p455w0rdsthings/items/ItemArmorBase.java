package p455w0rd.p455w0rdsthings.items;

import java.util.List;

import net.minecraft.block.BlockDispenser;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rdslib.util.EntityUtils;

public class ItemArmorBase extends ItemArmor implements IModelHolder {
	/*
		public static final IBehaviorDispenseItem DISPENSER_BEHAVIOR = new BehaviorDefaultDispenseItem() {
			@Override
			protected ItemStack dispenseStack(IBlockSource source, ItemStack stack) {
				ItemStack itemstack;
				if (stack.getItem() instanceof ItemCarbonArmor) {
					itemstack = ItemCarbonArmor.dispenseArmor(source, stack);
				}
				else {
					itemstack = ItemArmor.dispenseArmor(source, stack);
				}
				return itemstack != null ? itemstack : super.dispenseStack(source, stack);
			}
		};
	*/
	public ItemArmorBase(ItemArmor.ArmorMaterial materialIn, EntityEquipmentSlot slot, String itemName) {
		super(materialIn, 1, slot);
		setRegistryName(itemName);
		setUnlocalizedName(itemName);
		GameRegistry.register(this);
		setCreativeTab(CreativeTabs.COMBAT);
		BlockDispenser.DISPENSE_BEHAVIOR_REGISTRY.putObject(this, ItemArmor.DISPENSER_BEHAVIOR);
		//BlockDispenser.DISPENSE_BEHAVIOR_REGISTRY.putObject(this, DISPENSER_BEHAVIOR);

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@SuppressWarnings("unchecked")
	public static ItemStack dispenseArmor(IBlockSource blockSource, ItemStack stack) {
		BlockPos blockpos = blockSource.getBlockPos().offset(blockSource.getBlockState().getValue(BlockDispenser.FACING));
		List<Entity> list = (List<Entity>) EntityUtils.getEntitiesInRange(EntityLivingBase.class, blockSource.getWorld(), blockpos.getX(), blockpos.getY(), blockpos.getZ(), 1.0);

		if (list.isEmpty()) {
			return null;
		}
		else {
			EntityLivingBase entitylivingbase = (EntityLivingBase) list.get(0);
			EntityEquipmentSlot entityequipmentslot = EntityLiving.getSlotForItemStack(stack);
			ItemStack itemstack = stack.copy();
			itemstack.stackSize = 1;
			entitylivingbase.setItemStackToSlot(entityequipmentslot, itemstack);

			if (entitylivingbase instanceof EntityLiving) {
				((EntityLiving) entitylivingbase).setDropChance(entityequipmentslot, 2.0F);
			}

			--stack.stackSize;
			return stack;
		}
	}

}
