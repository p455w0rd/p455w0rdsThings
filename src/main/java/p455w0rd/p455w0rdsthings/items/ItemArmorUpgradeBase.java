/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.items;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IArmorUpgrade;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;

/**
 * @author p455w0rd
 *
 */
public class ItemArmorUpgradeBase extends ItemBase implements IArmorUpgrade {

	public ItemArmorUpgradeBase() {
		this("base");
	}

	/**
	 * @param name
	 */
	public ItemArmorUpgradeBase(String name) {
		super("armorupgrade_" + name);
	}

	@Override
	public Upgrades getUpgradeType() {
		return null;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		if (getUpgradeType() != null) {
			if (getUpgradeType().getArmorTypes().size() == 1 && !getUpgradeType().getArmorTypes().contains(EntityEquipmentSlot.CHEST)) {
				tooltip.add("Applicable Armor: " + ArmorUtils.getArmorTypeString(getUpgradeType().getArmorTypes().get(0)));
			}
			else {
				tooltip.add("Applicable Armor: ");
				for (EntityEquipmentSlot slot : getUpgradeType().getArmorTypes()) {
					if (slot == EntityEquipmentSlot.CHEST) {
						tooltip.add("JetPlate");
					}
					tooltip.add(ArmorUtils.getArmorTypeString(slot));
				}
			}
		}
	}

	@Override
	public Item getItem() {
		return this;
	}

	public static List<String> getArmorTypeStrings(EntityEquipmentSlot slot) {
		List<String> typeList = Lists.newArrayList();
		for (int i = 0; i < Upgrades.values().length; ++i) {
			if (Upgrades.values()[i].getArmorTypes().contains(slot)) {
				typeList.add(ArmorUtils.getArmorTypeString(slot));
			}
		}
		return typeList;
	}

	public static class UpgradeStepAssist extends ItemArmorUpgradeBase {

		private static final String name = "step_assist";

		public UpgradeStepAssist() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.STEPASSIST;
		}

	}

	public static class UpgradeNightVision extends ItemArmorUpgradeBase {

		private static final String name = "night_vision";

		public UpgradeNightVision() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.NIGHTVISION;
		}

	}

	public static class UpgradeSpeed extends ItemArmorUpgradeBase {

		private static final String name = "speed";

		public UpgradeSpeed() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.SPEED;
		}

	}

	public static class UpgradeLavaWalker extends ItemArmorUpgradeBase {

		private static final String name = "lava_walker";

		public UpgradeLavaWalker() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.LAVAWALKER;
		}

	}

	public static class UpgradeFlight extends ItemArmorUpgradeBase {

		private static final String name = "flight";

		public UpgradeFlight() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.FLIGHT;
		}

	}

	public static class UpgradeUnwithering extends ItemArmorUpgradeBase {

		private static final String name = "unwithering";

		public UpgradeUnwithering() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.UNWITHERING;
		}

	}

	public static class UpgradeEnderVision extends ItemArmorUpgradeBase {

		private static final String name = "endervision";

		public UpgradeEnderVision() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.ENDERVISION;
		}

	}

	public static class UpgradeRepulsion extends ItemArmorUpgradeBase {

		private static final String name = "repulsion";

		public UpgradeRepulsion() {
			super(name);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.REPULSION;
		}

	}

}
