/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.items;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.api.IFlightItem;
import p455w0rd.p455w0rdsthings.client.model.ModelCarbonArmor;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModMaterials;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rdslib.util.PotionUtils;

/**
 * @author p455w0rd
 *
 */
public class ItemEmeraldCarbonJetPlate extends ItemEmeraldCarbonArmor implements IFlightItem, IEmeraldCarbonArmor {

	protected Map<EntityEquipmentSlot, ModelBiped> models = null;

	public ItemEmeraldCarbonJetPlate(ItemArmor.ArmorMaterial materialIn, EntityEquipmentSlot slot, String itemName) {
		super(materialIn, slot, itemName);
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public int getMaxEnergyStored(ItemStack container) {
		return maxCapacity + (ArmorUtils.hasUpgrade(container, Upgrades.BATTERY) ? 16000000 : 0);
	}

	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot) {
		return getEnergyStored(armor) > 0 ? ModMaterials.CARBON_ARMOR_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)) : 0;
	}

	@Override
	public int getMaxDamage(ItemStack is) {
		return 0;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack stack) {
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		List<Upgrades> upgradeList = ArmorUtils.getActiveUpgrades(stack, armorType);
		tooltip.add("");
		if (upgradeList.size() > 0) {
			tooltip.add(TextFormatting.WHITE + "Active Upgrade" + (upgradeList.size() == 1 ? "" : "s") + ": ");

			for (int i = 0; i < upgradeList.size(); i++) {
				tooltip.add(TextFormatting.BLUE + "" + TextFormatting.ITALIC + "" + upgradeList.get(i).getDesc());
			}

			if (playerIn.capabilities.isFlying) {
				if (armorType == EntityEquipmentSlot.CHEST && !playerIn.capabilities.isCreativeMode && !playerIn.isSpectator() && playerIn.getItemStackFromSlot(getEquipmentSlot()) == stack) {
					tooltip.add("");
					tooltip.add(TextFormatting.RED + "" + TextFormatting.ITALIC + "Cannot remove chestplate while flying!");
				}
			}
		}
		else {
			tooltip.add(TextFormatting.WHITE + "Active Upgrades:");
			tooltip.add(TextFormatting.BLUE + "" + TextFormatting.ITALIC + "None");
		}
		tooltip.add("");
		tooltip.add(ChatFormatting.WHITE + "" + ChatFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack stack, EntityEquipmentSlot armorSlot, ModelBiped original) {
		ModelBiped model = getArmorModelForSlot(entityLiving, stack, armorSlot);
		if (model == null) {
			model = provideArmorModelForSlot(stack, armorSlot);
		}
		if (model != null) {
			model.setModelAttributes(original);
		}
		return model;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModelForSlot(EntityLivingBase entity, ItemStack stack, EntityEquipmentSlot slot) {
		if (models == null) {
			models = new EnumMap<EntityEquipmentSlot, ModelBiped>(EntityEquipmentSlot.class);
		}
		return models.get(slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped provideArmorModelForSlot(ItemStack stack, EntityEquipmentSlot slot) {
		models.put(slot, new ModelCarbonArmor(slot));
		return models.get(slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String layer) {
		return "p455w0rdsthings:textures/models/armor/model_carbon_armor.png";
	}

	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
		ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
		if (chest != null && chest.getItem().equals(this)) {
			/*
			if (getEnergyStored(chest) > 0) {
				player.capabilities.allowFlying = true;
				if (player.capabilities.isFlying && (!player.capabilities.isCreativeMode && !player.isSpectator())) {
					extractEnergy(chest, 10, false);
				}
			}
			else {
				if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
					player.capabilities.isFlying = false;
					player.capabilities.allowFlying = false;
				}
			}
			*/
			if (ArmorUtils.isUpgradeEnabled(chest, Upgrades.UNWITHERING) && getEnergyStored(chest) > 0) {
				if (PotionUtils.isPotionActive(player, MobEffects.WITHER)) {
					PotionUtils.clearPotionEffect(player, MobEffects.WITHER, 1);
				}
			}
		}
	}

	/*
		@Override
		public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
			EntityPlayer player = (EntityPlayer) entityIn;
			ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
			if ((chest == null || (chest.getItem() != ModItems.emeraldCarbonJetplate && chest.getItem() != ModItems.jetPackItem))) {
				if (Globals.CARBONJETPLATE_ISEQUIPPED || Globals.CARBONJETPACK_ISEQUIPPED) {
					Globals.CARBONJETPLATE_ISEQUIPPED = false;
					Globals.CARBONJETPACK_ISEQUIPPED = false;
					if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
						player.capabilities.isFlying = false;
						player.capabilities.allowFlying = false;
					}
				}
			}
		}
	*/
	@Override
	public EnumRarity getRarity(ItemStack stack) {
		return ModItems.RARITY_EMERALD;
	}

}