package p455w0rd.p455w0rdsthings.items;

import java.util.*;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.*;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import net.minecraftforge.fml.relauncher.*;
import p455w0rd.p455w0rdsthings.client.model.ModelCarbonArmor;

public class ItemCarbonArmor extends ItemArmorBase {

	protected Map<EntityEquipmentSlot, ModelBiped> models = null;

	public ItemCarbonArmor(ItemArmor.ArmorMaterial materialIn, EntityEquipmentSlot slot, String itemName) {
		super(materialIn, slot, itemName);
	}

	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModelForSlot(EntityLivingBase entity, ItemStack stack, EntityEquipmentSlot slot) {
		if (models == null) {
			models = new EnumMap<EntityEquipmentSlot, ModelBiped>(EntityEquipmentSlot.class);
		}
		return models.get(slot);
	}

	@SideOnly(Side.CLIENT)
	public ModelBiped provideArmorModelForSlot(ItemStack stack, EntityEquipmentSlot slot) {
		models.put(slot, new ModelCarbonArmor(slot));
		return models.get(slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String layer) {
		if (slot == EntityEquipmentSlot.LEGS) {
			return "p455w0rdsthings:textures/models/armor/carbon_layer_2.png";
		}
		return "p455w0rdsthings:textures/models/armor/carbon_layer_1.png";
	}

	public EntityEquipmentSlot getSlot(int slot) {
		switch (slot) {
		default:
		case 0:
			return EntityEquipmentSlot.FEET;
		case 1:
			return EntityEquipmentSlot.LEGS;
		case 2:
			return EntityEquipmentSlot.CHEST;
		case 3:
			return EntityEquipmentSlot.HEAD;
		}
	}
}
