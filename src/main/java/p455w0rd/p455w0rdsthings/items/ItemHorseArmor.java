package p455w0rd.p455w0rdsthings.items;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.passive.HorseArmorType;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import p455w0rd.p455w0rdsthings.api.IHorseArmor;
import p455w0rd.p455w0rdsthings.util.HorseUtils;

public class ItemHorseArmor extends ItemBase implements IHorseArmor {
	
	private String name;
	private int armorPoints;
	
    public ItemHorseArmor(String name, int protection) {
        super(name + "_horse_armor");
        this.name=name;
        this.setCreativeTab(CreativeTabs.COMBAT);
        this.setMaxStackSize(1);
        this.armorPoints = protection;
    }

    @Override
    public HorseArmorType getArmorType(ItemStack stack) {
        return HorseUtils.getTypeSafe(this.name, this.armorPoints, "p455w0rdsthings:textures/entity/horse/armor/horse_armor_" + this.name+ ".png");
    }
    
    @Override
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}
}
