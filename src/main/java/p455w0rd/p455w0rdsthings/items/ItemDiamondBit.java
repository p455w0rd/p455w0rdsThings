package p455w0rd.p455w0rdsthings.items;

import net.minecraft.creativetab.CreativeTabs;

/**
 * @author p455w0rd
 *
 */
public class ItemDiamondBit extends ItemBase {

	public ItemDiamondBit() {
		super("diamond_bit");
		setCreativeTab(CreativeTabs.MATERIALS);
	}

}
