package p455w0rd.p455w0rdsthings.items;

import java.util.List;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class ItemRF extends ItemBase implements IEnergyContainerItem {

	protected int capacity;
	protected int maxReceive;
	protected int maxExtract;

	public ItemRF(int capacity, String name) {
		this(capacity, capacity, capacity, name);
	}

	public ItemRF(int capacity, int maxTransfer, String name) {
		this(capacity, maxTransfer, maxTransfer, name);
	}

	public ItemRF(int capacity, int maxReceive, int maxExtract, String name) {
		super(name);
		this.capacity = capacity;
		this.maxReceive = maxReceive;
		this.maxExtract = maxExtract;
		setMaxStackSize(1);
	}

	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, NBTTagCompound nbt) {

		return new ICapabilityProvider() {

			@Override
			public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
				return capability == CapabilityEnergy.ENERGY;
			}

			@Override
			public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
				return capability != CapabilityEnergy.ENERGY ? null : CapabilityEnergy.ENERGY.cast(new IEnergyStorage() {

					ItemRF item = (ItemRF) stack.getItem();

					@Override
					public int receiveEnergy(int maxReceive, boolean simulate) {
						return item.receiveEnergy(stack, maxReceive, simulate);
					}

					@Override
					public int extractEnergy(int maxExtract, boolean simulate) {
						return item.extractEnergy(stack, maxExtract, simulate);
					}

					@Override
					public int getEnergyStored() {
						return item.getEnergyStored(stack);
					}

					@Override
					public int getMaxEnergyStored() {
						return item.getMaxEnergyStored(stack);
					}

					@Override
					public boolean canExtract() {
						return true;
					}

					@Override
					public boolean canReceive() {
						return true;
					}

				});
			}

		};
	}

	@Override
	public void getSubItems(Item itemIn, CreativeTabs tab, List<ItemStack> subItems) {
		subItems.add(new ItemStack(this));
		ItemStack magnet = new ItemStack(this);
		setFull(magnet);
		subItems.add(magnet);
	}

	public ItemRF setCapacity(int capacity) {
		this.capacity = capacity;
		return this;
	}

	public ItemRF setMaxTransfer(int maxTransfer) {
		setMaxReceive(maxTransfer);
		setMaxExtract(maxTransfer);
		return this;
	}

	public ItemRF setMaxReceive(int maxReceive) {
		this.maxReceive = maxReceive;
		return this;
	}

	public ItemRF setMaxExtract(int maxExtract) {
		this.maxExtract = maxExtract;
		return this;
	}

	@Override
	public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate) {
		if (!container.hasTagCompound()) {
			container.setTagCompound(new NBTTagCompound());
		}
		int energy = container.getTagCompound().getInteger("Energy");
		int energyReceived = Math.min(capacity - energy, Math.min(this.maxReceive, maxReceive));
		if (!simulate) {
			energy += energyReceived;
			container.getTagCompound().setInteger("Energy", energy);
		}
		return energyReceived;
	}

	@Override
	public int extractEnergy(ItemStack container, int maxExtract, boolean simulate) {
		if ((container.getTagCompound() == null) || (!container.getTagCompound().hasKey("Energy"))) {
			return 0;
		}
		int energy = container.getTagCompound().getInteger("Energy");
		int energyExtracted = Math.min(energy, Math.min(this.maxExtract, maxExtract));
		if (!simulate) {
			energy -= energyExtracted;
			container.getTagCompound().setInteger("Energy", energy);
		}
		return energyExtracted;
	}

	@Override
	public int getEnergyStored(ItemStack container) {
		if ((container.getTagCompound() == null) || (!container.getTagCompound().hasKey("Energy"))) {
			return 0;
		}
		return container.getTagCompound().getInteger("Energy");
	}

	@Override
	public int getMaxEnergyStored(ItemStack container) {
		return capacity;
	}

	public void setEnergyStored(ItemStack container, int energy) {
		if (container.getTagCompound() == null) {
			container.setTagCompound(new NBTTagCompound());
		}
		container.getTagCompound().setInteger("Energy", MathHelper.clamp(energy, 0, capacity));
	}

	public ItemStack setFull(ItemStack container) {
		setEnergyStored(container, capacity);
		return container;
	}

	public int getMaxExtract(ItemStack item) {
		return maxExtract;
	}

}
