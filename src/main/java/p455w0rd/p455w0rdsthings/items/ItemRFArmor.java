package p455w0rd.p455w0rdsthings.items;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemEnchantedBook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.ISpecialArmor;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemRFArmor extends ItemArmorBase implements ISpecialArmor, IEnergyContainerItem {

	public int maxCapacity;
	public int powerPerDamage;
	public int inputRate;
	public int outputRate;

	public ItemRFArmor(ItemArmor.ArmorMaterial materialIn, EntityEquipmentSlot slot, int capacity, int energyPerDmg, String itemName) {
		super(materialIn, slot, itemName);
		//BlockDispenser.DISPENSE_BEHAVIOR_REGISTRY.putObject(this, ItemArmor.DISPENSER_BEHAVIOR);
		maxCapacity = capacity;
		powerPerDamage = energyPerDmg;
		outputRate = inputRate = 1000;
	}

	@Override
	public void damageArmor(EntityLivingBase entity, ItemStack stack, DamageSource source, int damage, int slot) {
		extractEnergy(stack, powerPerDamage, false);
	}

	@Override
	public boolean getIsRepairable(ItemStack toRepair, ItemStack repair) {
		return false;
	}

	@Override
	public boolean isRepairable() {
		return false;
	}

	@Override
	public boolean isDamageable() {
		return false;
	}

	@Override
	public boolean isBookEnchantable(ItemStack stack, ItemStack book) {
		if (book.getItem() instanceof ItemEnchantedBook) {
			ItemEnchantedBook item = (ItemEnchantedBook) book.getItem();
			if (item.getEnchantments(book).tagCount() == 1) {
				Short enchID = ((NBTTagCompound) item.getEnchantments(book).get(0)).getShort("id");
				String name = Enchantment.getEnchantmentByID(enchID).getName();
				if (name.equals("enchantment.soulBound")) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		return 1D - (double) getEnergyStored(stack) / (double) getMaxEnergyStored(stack);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean showDurabilityBar(ItemStack stack) {
		return true;
	}

	public EntityEquipmentSlot getSlot(int slot) {
		switch (slot) {
		default:
		case 0:
			return EntityEquipmentSlot.FEET;
		case 1:
			return EntityEquipmentSlot.LEGS;
		case 2:
			return EntityEquipmentSlot.CHEST;
		case 3:
			return EntityEquipmentSlot.HEAD;
		}
	}

	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot) {
		return getEnergyStored(armor) > 0 ? getDamageReduction(slot) : 0;
	}

	protected int getDamageReduction(int slot) {
		return 0;
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot) {
		return null;
	}

	@Override
	public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate) {
		if (!container.hasTagCompound()) {
			container.setTagCompound(new NBTTagCompound());
		}
		int energy = container.getTagCompound().getInteger("Energy");
		int energyReceived = Math.min(getMaxEnergyStored(container) - energy, Math.min(inputRate, maxReceive));
		if (!simulate) {
			energy += energyReceived;
			container.getTagCompound().setInteger("Energy", energy);
		}
		return energyReceived;
	}

	@Override
	public int extractEnergy(ItemStack container, int maxExtract, boolean simulate) {
		if ((container.getTagCompound() == null) || (!container.getTagCompound().hasKey("Energy"))) {
			return 0;
		}
		int energy = container.getTagCompound().getInteger("Energy");
		int energyExtracted = Math.min(energy, Math.min(outputRate, maxExtract));
		if (!simulate) {
			energy -= energyExtracted;
			container.getTagCompound().setInteger("Energy", energy);
		}
		return energyExtracted;
	}

	@Override
	public int getEnergyStored(ItemStack container) {
		if ((container.getTagCompound() == null) || (!container.getTagCompound().hasKey("Energy"))) {
			return 0;
		}
		return container.getTagCompound().getInteger("Energy");
	}

	@Override
	public int getMaxEnergyStored(ItemStack container) {
		return maxCapacity;
	}

	public void setEnergyStored(ItemStack container, int energy) {
		if (container.getTagCompound() == null) {
			container.setTagCompound(new NBTTagCompound());
		}
		container.getTagCompound().setInteger("Energy", energy);
	}

	public ItemStack setFull(ItemStack container) {
		setEnergyStored(container, getMaxEnergyStored(container));
		return container;
	}

}
