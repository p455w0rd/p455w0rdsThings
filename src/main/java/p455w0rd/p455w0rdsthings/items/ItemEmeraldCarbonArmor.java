/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.items;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockStaticLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.client.model.ModelCarbonArmor;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModKeyBindings;
import p455w0rd.p455w0rdsthings.init.ModMaterials;
import p455w0rd.p455w0rdsthings.init.ModRegistries;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.EntityUtils;
import p455w0rdslib.util.ItemUtils;
import p455w0rdslib.util.MathUtils;
import p455w0rdslib.util.PotionUtils;

/**
 * @author p455w0rd
 *
 */
public class ItemEmeraldCarbonArmor extends ItemRFArmor implements IEmeraldCarbonArmor {

	protected Map<EntityEquipmentSlot, ModelBiped> models = null;

	public ItemEmeraldCarbonArmor(ItemArmor.ArmorMaterial materialIn, EntityEquipmentSlot slot, String itemName) {
		super(materialIn, slot, 1000000, 10000, itemName);
		//BlockDispenser.DISPENSE_BEHAVIOR_REGISTRY.putObject(this, ItemArmor.DISPENSER_BEHAVIOR);
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public int getMaxEnergyStored(ItemStack container) {
		return maxCapacity + (ArmorUtils.hasUpgrade(container, Upgrades.BATTERY) ? 16000000 : 0);
	}

	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot) {
		return getEnergyStored(armor) > 0 ? ModMaterials.CARBON_ARMOR_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)) : 0;
	}

	@Override
	public int getMaxDamage(ItemStack is) {
		return 0;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack stack) {
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip, boolean advanced) {
		List<Upgrades> upgradeList = ArmorUtils.getActiveUpgrades(stack, armorType);
		tooltip.add("");
		if (upgradeList.size() > 0) {
			tooltip.add(TextFormatting.WHITE + "Active Upgrade" + (upgradeList.size() == 1 ? "" : "s") + ": ");

			for (int i = 0; i < upgradeList.size(); i++) {
				tooltip.add(TextFormatting.BLUE + "" + TextFormatting.ITALIC + "" + upgradeList.get(i).getDesc());
			}

			if (ArmorUtils.hasUpgrade(stack, Upgrades.NIGHTVISION)) {
				tooltip.add(TextFormatting.DARK_GREEN + "Night Vision Mode: " + (ModRegistries.isNVEnabled(player) ? TextFormatting.WHITE + "Active" : TextFormatting.RED + "Inactive"));
				tooltip.add(TextFormatting.DARK_GREEN + "Press " + TextFormatting.GREEN + TextFormatting.BOLD + ModKeyBindings.KEYBIND_NIGHTVISION_TOGGLE.getDisplayName() + TextFormatting.RESET + TextFormatting.DARK_GREEN + " to toggle");
			}

			if (player.capabilities.isFlying) {
				if (armorType == EntityEquipmentSlot.CHEST && !player.capabilities.isCreativeMode && !player.isSpectator() && player.getItemStackFromSlot(getEquipmentSlot()) == stack) {
					tooltip.add("");
					tooltip.add(TextFormatting.ITALIC + "Cannot remove chestplate while flying!");
				}
			}
		}
		else {
			tooltip.add(TextFormatting.WHITE + "Active Upgrades:");
			tooltip.add(TextFormatting.BLUE + "" + TextFormatting.ITALIC + "None");
		}
		tooltip.add("");
		tooltip.add(ChatFormatting.WHITE + "" + ChatFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack stack, EntityEquipmentSlot armorSlot, ModelBiped original) {
		ModelBiped model = getArmorModelForSlot(entityLiving, stack, armorSlot);
		if (model == null) {
			model = provideArmorModelForSlot(stack, armorSlot);
		}
		if (model != null) {
			model.setModelAttributes(original);
		}
		return model;
	}

	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModelForSlot(EntityLivingBase entity, ItemStack stack, EntityEquipmentSlot slot) {
		if (models == null) {
			models = new EnumMap<EntityEquipmentSlot, ModelBiped>(EntityEquipmentSlot.class);
		}
		return models.get(slot);
	}

	@SideOnly(Side.CLIENT)
	public ModelBiped provideArmorModelForSlot(ItemStack stack, EntityEquipmentSlot slot) {
		models.put(slot, new ModelCarbonArmor(slot));
		return models.get(slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String layer) {
		return ModGlobals.MODID + ":textures/models/armor/model_carbon_armor.png";
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot) {
		if (ArmorUtils.getRFStored(armor) <= 0) {
			return new ArmorProperties(0, 0, 0);
		}
		return new ArmorProperties(1, ModMaterials.CARBON_ARMOR_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)), ModMaterials.CARBON_ARMOR_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)) * 10);
	}

	@Override
	protected int getDamageReduction(int slot) {
		int dmg = 0;
		switch (slot) {
		case 0:
			dmg = 5;
			break;
		case 1:
			dmg = 7;
			break;
		case 2:
			dmg = 10;
			break;
		case 3:
			dmg = 5;
			break;
		}
		return dmg;
	}

	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
		ItemStack helmet = player.getItemStackFromSlot(EntityEquipmentSlot.HEAD);
		ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
		ItemStack leggings = player.getItemStackFromSlot(EntityEquipmentSlot.LEGS);
		ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
		if (chest != null) {
			if (world.isRemote && chest.getItem() == ModItems.EMERALD_CARBON_CHESTPLATE) {
				ModGlobals.CARBONCHESTPLATE_ISEQUIPPED = true;
			}
			if (ArmorUtils.isUpgradeEnabled(chest, Upgrades.UNWITHERING) && getEnergyStored(chest) > 0) {
				if (PotionUtils.isPotionActive(player, MobEffects.WITHER)) {
					PotionUtils.clearPotionEffect(player, MobEffects.WITHER, 1);
				}
			}
		}
		if (leggings != null && leggings.getItem() == ModItems.EMERALD_CARBON_LEGGINGS) {
			if (world.isRemote) {
				ModGlobals.CARBONLEGGINGS_ISEQUIPPED = true;
			}
			if (ArmorUtils.isUpgradeEnabled(itemStack, Upgrades.SPEED)) {
				PotionUtils.effectPlayer(player, MobEffects.SPEED, 1);
			}
		}
		if (boots != null && boots.getItem() == ModItems.EMERALD_CARBON_BOOTS) {
			if (world.isRemote) {
				ModGlobals.CARBONBOOTS_ISEQUIPPED = true;
			}
			if (ArmorUtils.isUpgradeEnabled(itemStack, Upgrades.STEPASSIST)) {
				if (EasyMappings.world(player).isRemote) {
					player.stepHeight = 1.0F;
				}
			}
			if (ArmorUtils.isUpgradeEnabled(itemStack, Upgrades.LAVAWALKER)) {
				if (getEnergyStored(itemStack) > 0) {
					doLavaWalk(player, itemStack);
				}
			}
		}
	}

	@SubscribeEvent
	public void onBurn(LivingAttackEvent event) {
		if (event.getEntity() instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.getEntity();
			if (player.isCreative() || player.isSpectator()) {
				return;
			}
			ItemStack boots = ItemUtils.getBoots(player);
			if (ArmorUtils.isUpgradeEnabled(boots, Upgrades.LAVAWALKER)) {
				if (event.getSource().equals(DamageSource.lava) || event.getSource().equals(DamageSource.inFire) || event.getSource().equals(DamageSource.onFire) || event.getSource().isFireDamage()) {
					player.extinguish();
					event.setCanceled(true);
				}
			}
		}
	}

	@Override
	public EnumRarity getRarity(ItemStack stack) {
		return ModItems.RARITY_EMERALD;
	}

	private void doLavaWalk(EntityLivingBase entity, ItemStack stack) {
		if (entity instanceof EntityPlayer) {
			if (((EntityPlayer) entity).isCreative() || ((EntityPlayer) entity).isSpectator()) {
				return;
			}
		}
		Block block = EntityUtils.getBlockBelowEntity(entity, 0);
		World world = entity.getEntityWorld();
		BlockPos pos = new BlockPos(entity.posX, entity.posY, entity.posZ);

		if (block == Blocks.LAVA && EntityUtils.getBlockStateBelowEntity(entity, 0).getValue(BlockLiquid.LEVEL).intValue() == 0 && block instanceof BlockStaticLiquid) {
			BlockPos belowPos = EntityUtils.getPosBelowEntity(entity, 0);
			world.setBlockState(belowPos, ModBlocks.FAKE_LAVA.getDefaultState());
			world.scheduleUpdate(belowPos, ModBlocks.FAKE_LAVA, MathUtils.getInt(entity.getRNG(), 60, 120));
			double x = entity.posX;
			double y = entity.prevPosY;
			double z = entity.posZ;
			entity.setPosition(x, y, z);
			extractEnergy(stack, 100, false);
		}
		block = EntityUtils.getBlockBelowEntity(entity, 1);
		if (block == Blocks.LAVA && EntityUtils.getBlockStateBelowEntity(entity, 1).getValue(BlockLiquid.LEVEL).intValue() == 0 && block instanceof BlockStaticLiquid) {
			BlockPos belowPos = EntityUtils.getPosBelowEntity(entity, 1);
			entity.getEntityWorld().setBlockState(belowPos, ModBlocks.FAKE_LAVA.getDefaultState());
			entity.getEntityWorld().scheduleUpdate(belowPos, ModBlocks.FAKE_LAVA, MathUtils.getInt(entity.getRNG(), 60, 120));
			double x = entity.posX;
			double y = entity.prevPosY;
			double z = entity.posZ;
			entity.setPosition(x, y, z);
			extractEnergy(stack, 100, false);
		}
		if (entity.onGround) {
			float f = 1;
			BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos(0, 0, 0);

			for (BlockPos.MutableBlockPos blockpos$mutableblockpos1 : BlockPos.getAllInBoxMutable(pos.add((-f), -1.0D, (-f)), pos.add(f, -1.0D, f))) {
				if (blockpos$mutableblockpos1.distanceSqToCenter(entity.posX, entity.posY, entity.posZ) <= f * f) {
					blockpos$mutableblockpos.setPos(blockpos$mutableblockpos1.getX(), blockpos$mutableblockpos1.getY() + 1, blockpos$mutableblockpos1.getZ());
					IBlockState iblockstate = world.getBlockState(blockpos$mutableblockpos);

					if (iblockstate.getMaterial() == Material.AIR) {
						IBlockState iblockstate1 = world.getBlockState(blockpos$mutableblockpos1);

						if (iblockstate1.getMaterial() == Material.LAVA && iblockstate1.getValue(BlockLiquid.LEVEL).intValue() == 0 && world.canBlockBePlaced(ModBlocks.FAKE_LAVA, blockpos$mutableblockpos1, false, EnumFacing.DOWN, (Entity) null, (ItemStack) null)) {
							world.setBlockState(blockpos$mutableblockpos1, ModBlocks.FAKE_LAVA.getDefaultState());
							world.scheduleUpdate(blockpos$mutableblockpos1.toImmutable(), ModBlocks.FAKE_LAVA, MathUtils.getInt(entity.getRNG(), 60, 120));
							extractEnergy(stack, 100, false);
						}
					}
				}
			}
		}
	}

	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		EntityPlayer player = (EntityPlayer) entityIn;
		ItemStack helmet = player.getItemStackFromSlot(EntityEquipmentSlot.HEAD);
		//ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
		ItemStack leggings = player.getItemStackFromSlot(EntityEquipmentSlot.LEGS);
		ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
		if (player.capabilities.isCreativeMode || player.isSpectator()) {
			player.capabilities.allowFlying = true;
		}

		if (helmet == null || helmet.getItem() != ModItems.EMERALD_CARBON_HELMET || helmet.getItem() != ModItems.NIGHT_VISION_GOGGLES) {
			if (worldIn.isRemote && ModGlobals.CARBONHELMET_ISEQUIPPED) {
				ModGlobals.CARBONHELMET_ISEQUIPPED = false;
				if (!ArmorUtils.isUpgradeEnabled(helmet, Upgrades.NIGHTVISION) || getEnergyStored(helmet) <= 0) {
					if (PotionUtils.isPotionActive(player, MobEffects.NIGHT_VISION)) {
						PotionUtils.clearPotionEffect(player, MobEffects.NIGHT_VISION, 1);
					}
				}
			}
		}

		if (leggings == null || leggings.getItem() != ModItems.EMERALD_CARBON_LEGGINGS) {
			if (ModGlobals.CARBONLEGGINGS_ISEQUIPPED) {
				ModGlobals.CARBONLEGGINGS_ISEQUIPPED = false;
				if (!ArmorUtils.isUpgradeEnabled(leggings, Upgrades.SPEED)) {
					if (PotionUtils.isPotionActive(player, MobEffects.SPEED)) {
						PotionUtils.clearPotionEffect(player, MobEffects.SPEED, 0);
					}
				}
			}
		}

		if (boots == null || boots.getItem() != ModItems.EMERALD_CARBON_BOOTS) {
			if (ModGlobals.CARBONBOOTS_ISEQUIPPED) {
				ModGlobals.CARBONBOOTS_ISEQUIPPED = false;
				if (!ArmorUtils.isUpgradeEnabled(boots, Upgrades.SPEED)) {
					if (player.stepHeight != 0.6F) {
						player.stepHeight = 0.6F;
					}
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack stack) {
		String name = I18n.format(getUnlocalizedName() + ".name").trim();
		if (armorType == EntityEquipmentSlot.CHEST) {
			if (ArmorUtils.isUpgradeEnabled(stack, Upgrades.FLIGHT)) {
				name = name.replace("Chestplate", "Jetplate");
			}
		}
		//return (TextFormatting.GREEN + "" + TextFormatting.ITALIC + name);
		return name;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public EntityEquipmentSlot getEquipmentSlot() {
		return getArmorType();

	}

	@Override
	public EntityEquipmentSlot getArmorType() {
		return armorType;
	}
}
