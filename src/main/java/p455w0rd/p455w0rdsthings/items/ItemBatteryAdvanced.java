package p455w0rd.p455w0rdsthings.items;

import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import crazypants.enderio.capacitor.ICapacitorData;
import crazypants.enderio.capacitor.ICapacitorDataItem;
import crazypants.enderio.capacitor.ICapacitorKey;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Optional.Interface;
import net.minecraftforge.fml.common.Optional.InterfaceList;
import net.minecraftforge.fml.common.Optional.Method;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IArmorUpgrade;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;

@InterfaceList({
		@Interface(iface = "crazypants.enderio.capacitor.ICapacitorDataItem", modid = "EnderIO", striprefs = true), @Interface(iface = "crazypants.enderio.capacitor.ICapacitorData", modid = "EnderIO", striprefs = true)
})
public class ItemBatteryAdvanced extends ItemBatteryBasic implements ICapacitorDataItem, ICapacitorData, IArmorUpgrade {

	private ItemStack capStack;

	public ItemBatteryAdvanced() {
		super(16000000, 5000, "battery_advanced");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.clear();
		tooltip.add(TextFormatting.GREEN + "" + TextFormatting.ITALIC + "Emerald Infused Battery");
		tooltip.add(TextFormatting.DARK_GRAY + "Charges Armor+Inventory Items");
		tooltip.add(TextFormatting.WHITE + (stack.getItemDamage() == 0 ? "Dea" : "A") + "ctivated");
		if (Loader.isModLoaded("EnderIO")) {
			tooltip.add("");
			tooltip.add(TextFormatting.ITALIC + "16x EnderIO cap w/full charge");
			tooltip.add("");
		}
		if (getUpgradeType() != null) {
			tooltip.add("Applicable Upgrade for Armor: ");
			for (EntityEquipmentSlot slot : getUpgradeType().getArmorTypes()) {
				if (slot == EntityEquipmentSlot.CHEST) {
					tooltip.add("JetPlate");
				}
				tooltip.add(ArmorUtils.getArmorTypeString(slot));
			}
			tooltip.add("");
		}
		tooltip.add(ChatFormatting.WHITE + "" + ChatFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack stack) {
		return (TextFormatting.GREEN + I18n.format(this.getUnlocalizedName() + ".name")).trim();
	}

	@Override
	@Method(modid = "EnderIO")
	public ICapacitorData getCapacitorData(ItemStack stack) {
		capStack = stack;
		return this;
	}

	@Override
	@Method(modid = "EnderIO")
	public int getBaseLevel() {
		return 3;
	}

	@Override
	@Method(modid = "EnderIO")
	public float getUnscaledValue(ICapacitorKey key) {
		return capStack != null && getEnergyStored(capStack) >= getMaxEnergyStored(capStack) ? 5 : 1;
	}

	@Override
	@Method(modid = "EnderIO")
	public String getLocalizedName() {
		return I18n.format(getUnlocalizedName());
	}

	@Override
	public Upgrades getUpgradeType() {
		return Upgrades.BATTERY;
	}

	@Override
	public Item getItem() {
		return this;
	}

}
