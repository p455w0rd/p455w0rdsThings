package p455w0rd.p455w0rdsthings.items;

import codechicken.lib.model.ModelRegistryHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.client.render.DankNullPanelRenderer;

public class ItemDankNullPanel extends ItemBase {

	public ItemDankNullPanel(int index) {
		super("dank_null_panel_" + index);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack stack) {
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack stack) {
		return I18n.format(getUnlocalizedNameInefficiently(stack) + ".name").trim();
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void initModel() {
		ModelRegistryHelper.registerItemRenderer(this, DankNullPanelRenderer.getInstance());
	}

}
