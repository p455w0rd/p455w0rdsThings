package p455w0rd.p455w0rdsthings.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler.GUIType;
import p455w0rd.p455w0rdsthings.util.GuideUtils;

/**
 * @author p455w0rd
 *
 */
public class ItemGuide extends ItemBase {

	@SideOnly(Side.CLIENT)
	public static IGuidePage forcedPage;
	private static final String NAME = "guide";

	public ItemGuide() {
		super(NAME);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack stack, World world, EntityPlayer player, EnumHand hand) {
		if (player.isSneaking()) {
			if (world.isRemote) {
				GuideUtils.resetPlayerData();
			}
		}
		else {
			ModGuiHandler.launchGui(GUIType.GUIDE, player, world, (int) player.posX, (int) player.posY, (int) player.posZ);
		}
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
	}

}
