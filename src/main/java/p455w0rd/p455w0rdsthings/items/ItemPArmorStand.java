package p455w0rd.p455w0rdsthings.items;

import java.util.List;
import java.util.Random;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Rotations;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import p455w0rd.p455w0rdsthings.entity.EntityPArmorStand;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.MathUtils;

public class ItemPArmorStand extends ItemBase {
	public ItemPArmorStand() {
		super("armorstand");
		setCreativeTab(CreativeTabs.DECORATIONS);
	}

	@Override
	public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (facing == EnumFacing.DOWN) {
			return EnumActionResult.FAIL;
		}
		boolean flag = worldIn.getBlockState(pos).getBlock().isReplaceable(worldIn, pos);
		BlockPos blockpos = flag ? pos : pos.offset(facing);
		if (!playerIn.canPlayerEdit(blockpos, facing, stack)) {
			return EnumActionResult.FAIL;
		}
		BlockPos blockpos1 = blockpos.up();
		boolean flag1 = (!worldIn.isAirBlock(blockpos)) && (!worldIn.getBlockState(blockpos).getBlock().isReplaceable(worldIn, blockpos));
		flag1 |= ((!worldIn.isAirBlock(blockpos1)) && (!worldIn.getBlockState(blockpos1).getBlock().isReplaceable(worldIn, blockpos1)));
		if (flag1) {
			return EnumActionResult.FAIL;
		}
		double d0 = blockpos.getX();
		double d1 = blockpos.getY();
		double d2 = blockpos.getZ();
		List<Entity> list = worldIn.getEntitiesWithinAABBExcludingEntity((Entity) null, new AxisAlignedBB(d0, d1, d2, d0 + 1.0D, d1 + 2.0D, d2 + 1.0D));
		if (!list.isEmpty()) {
			return EnumActionResult.FAIL;
		}
		if (!worldIn.isRemote) {
			worldIn.setBlockToAir(blockpos);
			worldIn.setBlockToAir(blockpos1);
			EntityPArmorStand entityarmorstand = new EntityPArmorStand(worldIn, d0 + 0.5D, d1, d2 + 0.5D);

			float f = MathUtils.floor((MathHelper.wrapDegrees(playerIn.rotationYaw - 180.0F) + 22.5F) / 45.0F) * 45.0F;
			entityarmorstand.setLocationAndAngles(d0 + 0.5D, d1, d2 + 0.5D, f, 0.0F);
			applyRandomRotations(entityarmorstand, worldIn.rand);
			ItemMonsterPlacer.applyItemEntityDataToEntity(worldIn, playerIn, stack, entityarmorstand);
			EasyMappings.spawn(worldIn, entityarmorstand);
			worldIn.playSound((EntityPlayer) null, entityarmorstand.posX, entityarmorstand.posY, entityarmorstand.posZ, SoundEvents.ENTITY_ARMORSTAND_PLACE, SoundCategory.BLOCKS, 0.75F, 0.8F);
		}
		stack.stackSize -= 1;
		return EnumActionResult.SUCCESS;
	}

	@Override
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	private void applyRandomRotations(EntityPArmorStand armorStand, Random rand) {
		Rotations rotations = armorStand.getHeadRotation();
		float f = rand.nextFloat() * 5.0F;
		float f1 = rand.nextFloat() * 20.0F - 10.0F;
		Rotations rotations1 = new Rotations(rotations.getX() + f, rotations.getY() + f1, rotations.getZ());
		armorStand.setHeadRotation(rotations1);
		rotations = armorStand.getBodyRotation();
		f = rand.nextFloat() * 10.0F - 5.0F;
		rotations1 = new Rotations(rotations.getX(), rotations.getY() + f, rotations.getZ());
		armorStand.setBodyRotation(rotations1);
	}
}
