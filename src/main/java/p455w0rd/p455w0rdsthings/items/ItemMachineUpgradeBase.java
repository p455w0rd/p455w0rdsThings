package p455w0rd.p455w0rdsthings.items;

import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.api.IMachineUpgrade;
import p455w0rd.p455w0rdsthings.util.MachineUtils.Upgrades;

/**
 * @author p455w0rd
 *
 */
public abstract class ItemMachineUpgradeBase extends ItemBase implements IMachineUpgrade {

	public ItemMachineUpgradeBase(String name) {
		super(name);
	}

	public static class SpeedUpgrade extends ItemMachineUpgradeBase {

		private static final String NAME = "machineupgrade_speed";

		public SpeedUpgrade() {
			super(NAME);
		}

		@Override
		public Upgrades getUpgradeType() {
			return Upgrades.SPEED;
		}

		@Override
		public int getItemStackLimit(ItemStack stack) {
			return 5;
		}

	}

}
