/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.items;

import java.util.List;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.client.model.ModelNightVisionGoggles;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModKeyBindings;
import p455w0rd.p455w0rdsthings.init.ModMaterials;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
@Optional.Interface(iface = "baubles.api.IBauble", modid = "Baubles|API", striprefs = true)
public class ItemNightVisionGoggles extends ItemRFArmor implements IBauble {

	private static final String name = "night_vision_goggles";

	public ItemNightVisionGoggles(ItemArmor.ArmorMaterial materialIn) {
		super(materialIn, EntityEquipmentSlot.HEAD, 400000, 100, name);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, EntityEquipmentSlot armorSlot, ModelBiped original) {
		return new ModelNightVisionGoggles();
	}

	@Override
	public int getMaxDamage(ItemStack is) {
		return 0;
	}

	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot) {
		return getEnergyStored(armor) > 0 ? ModMaterials.JETPACK_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)) : 0;
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot) {
		return new ArmorProperties(0, ModMaterials.JETPACK_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)) / 25.0D, getDamageReduction(3 - slot));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add("");
		tooltip.add(TextFormatting.DARK_GREEN + "Night Vision Mode: " + (ModRegistries.isNVEnabled(playerIn) ? TextFormatting.WHITE + "Active" : TextFormatting.RED + "Inactive"));
		tooltip.add(TextFormatting.DARK_GREEN + "Press " + TextFormatting.GREEN + TextFormatting.BOLD + ModKeyBindings.KEYBIND_NIGHTVISION_TOGGLE.getDisplayName() + TextFormatting.RESET + TextFormatting.DARK_GREEN + " to toggle");
		tooltip.add(TextFormatting.WHITE + "" + TextFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getArmorTexture(ItemStack itemStack, Entity entity, EntityEquipmentSlot slot, String layer) {
		if (slot != EntityEquipmentSlot.HEAD) {
			return null;
		}
		return ModGlobals.MODID + ":textures/models/armor/night_vision_goggles.png";
	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public BaubleType getBaubleType(ItemStack itemstack) {
		return BaubleType.HEAD;
	}

	/*
		@Optional.Method(modid = Baubles.API_MODID)
		@Override
		public void onWornTick(ItemStack item, EntityLivingBase entity) {
			doMagnet(item, entity);
		}
	*/
	@Optional.Method(modid = "Baubles|API")
	@Override
	public boolean willAutoSync(ItemStack itemstack, EntityLivingBase player) {
		return true;
	}

}