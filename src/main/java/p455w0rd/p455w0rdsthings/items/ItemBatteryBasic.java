package p455w0rd.p455w0rdsthings.items;

import java.util.ArrayList;
import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.integration.Baubles;
import p455w0rd.p455w0rdsthings.integration.EnderIo;

@Optional.Interface(iface = "baubles.api.IBauble", modid = "Baubles|API", striprefs = true)
public class ItemBatteryBasic extends ItemRF implements IBauble {

	public ItemBatteryBasic() {
		super(400000, 500, "battery_basic");
	}

	public ItemBatteryBasic(int capacity, int output, String name) {
		super(capacity, output, name);
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tab, List<ItemStack> subItems) {
		subItems.add(new ItemStack(item));
		ItemStack fullStack = setFull(new ItemStack(item));
		subItems.add(fullStack);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean showDurabilityBar(ItemStack stack) {
		return stack.getItemDamage() < 2;
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		double max = getMaxEnergyStored(stack);
		return (max - getEnergyStored(stack)) / max;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add(TextFormatting.DARK_GRAY + "Charges Armor+Inventory Items");
		tooltip.add(TextFormatting.WHITE + (stack.getItemDamage() == 0 ? "Dea" : "A") + "ctivated");
		tooltip.add(ChatFormatting.WHITE + "" + ChatFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
	}

	@Override
	public void initModel() {
		for (int i = 0; i < 3; i++) {
			ModelLoader.setCustomModelResourceLocation(this, i, new ModelResourceLocation(getRegistryName(), "inventory"));
		}
	}

	@Override
	public boolean hasEffect(ItemStack item) {
		return isActivated(item);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand) {
		if (!worldIn.isRemote && playerIn.isSneaking()) {
			if (itemStackIn.getItemDamage() == 0) {
				if (getEnergyStored(itemStackIn) > 0) {
					itemStackIn.setItemDamage(1);
				}
			}
			else {
				itemStackIn.setItemDamage(0);
			}
		}
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStackIn);
	}

	protected boolean isActivated(ItemStack item) {
		return item.getItemDamage() == 1;
	}

	@Override
	public void onUpdate(ItemStack container, World world, Entity entity, int itemSlot, boolean isSelected) {
		if (!(entity instanceof EntityPlayer) || !isActivated(container)) {
			return;
		}
		if (getEnergyStored(container) <= 0) {
			container.setItemDamage(0);
			return;
		}
		EntityPlayer player = (EntityPlayer) entity;
		ArrayList<ItemStack> invItems = new ArrayList<ItemStack>();
		for (ItemStack stack : player.inventory.mainInventory) {
			invItems.add(stack);
		}
		for (ItemStack stack : player.getArmorInventoryList()) {
			invItems.add(stack);
		}
		if (Mods.BAUBLES.isLoaded()) {
			for (ItemStack stack : Baubles.getEquippedBaubles(player)) {
				invItems.add(stack);
			}
		}
		for (ItemStack stack : invItems) {
			int max = Math.min(getEnergyStored(container), getMaxExtract(container));
			if (stack != null) {
				if (stack.getItem() instanceof IEnergyContainerItem) {
					IEnergyContainerItem item = (IEnergyContainerItem) stack.getItem();
					extractEnergy(container, item.receiveEnergy(stack, max, false), false);
				}
				else if (Mods.ENDERIO.isLoaded()) {
					if (EnderIo.isEndeIOEnergyItem(stack)) {
						int currentPower = EnderIo.getPoweredItem(stack).getEnergyStored(stack);
						int maxPower = EnderIo.getPoweredItem(stack).getMaxEnergyStored(stack);
						if (currentPower < maxPower) {
							int maxInput = EnderIo.getPoweredItem(stack).getMaxInput(stack);
							int amountToOutput = Math.min(max, maxInput);
							if (amountToOutput > (maxPower - currentPower)) {
								amountToOutput = maxPower - currentPower;
							}
							EnderIo.getPoweredItem(stack).setEnergyStored(stack, amountToOutput + currentPower);
							extractEnergy(container, amountToOutput, false);
						}
					}
				}
			}
		}

	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public BaubleType getBaubleType(ItemStack itemstack) {
		return BaubleType.TRINKET;
	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public void onWornTick(ItemStack container, EntityLivingBase entity) {
		if (!(entity instanceof EntityPlayer) || !isActivated(container)) {
			return;
		}
		if (getEnergyStored(container) <= 0) {
			container.setItemDamage(0);
			return;
		}
		EntityPlayer player = (EntityPlayer) entity;
		ArrayList<ItemStack> invItems = new ArrayList<ItemStack>();
		for (ItemStack stack : player.inventory.mainInventory) {
			invItems.add(stack);
		}
		for (ItemStack stack : player.getArmorInventoryList()) {
			invItems.add(stack);
		}
		for (ItemStack stack : Baubles.getEquippedBaubles(player)) {
			invItems.add(stack);
		}
		for (ItemStack stack : invItems) {
			int max = Math.min(getEnergyStored(container), getMaxExtract(container));
			if (stack != null) {
				if (stack.getItem() instanceof IEnergyContainerItem) {
					IEnergyContainerItem item = (IEnergyContainerItem) stack.getItem();
					extractEnergy(container, item.receiveEnergy(stack, max, false), false);
				}
				else if (EnderIo.isEndeIOEnergyItem(stack)) {
					int currentPower = EnderIo.getPoweredItem(stack).getEnergyStored(stack);
					int maxPower = EnderIo.getPoweredItem(stack).getMaxEnergyStored(stack);
					if (currentPower < maxPower) {
						int maxInput = EnderIo.getPoweredItem(stack).getMaxInput(stack);
						int amountToOutput = Math.min(max, maxInput);
						if (amountToOutput > (maxPower - currentPower)) {
							amountToOutput = maxPower - currentPower;
						}
						EnderIo.getPoweredItem(stack).setEnergyStored(stack, amountToOutput + currentPower);
						extractEnergy(container, amountToOutput, false);
					}
				}
			}
		}
	}

	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return !ItemStack.areItemsEqual(oldStack, newStack) && slotChanged;
	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public boolean willAutoSync(ItemStack itemstack, EntityLivingBase player) {
		return false;
	}

}
