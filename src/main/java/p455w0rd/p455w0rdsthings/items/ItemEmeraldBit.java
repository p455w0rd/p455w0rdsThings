package p455w0rd.p455w0rdsthings.items;

import net.minecraft.creativetab.CreativeTabs;

/**
 * @author p455w0rd
 *
 */
public class ItemEmeraldBit extends ItemBase {

	public ItemEmeraldBit() {
		super("emerald_bit");
		setCreativeTab(CreativeTabs.MATERIALS);
	}
}
