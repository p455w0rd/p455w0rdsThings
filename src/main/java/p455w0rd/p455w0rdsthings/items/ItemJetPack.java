package p455w0rd.p455w0rdsthings.items;

import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IFlightItem;
import p455w0rd.p455w0rdsthings.client.model.ModelJetPack;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModMaterials;

public class ItemJetPack extends ItemRFArmor implements IFlightItem {

	private static final String name = "carbon_jetpack";

	public ItemJetPack(ItemArmor.ArmorMaterial materialIn) {
		super(materialIn, EntityEquipmentSlot.CHEST, 400000, 0, name);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, EntityEquipmentSlot armorSlot, ModelBiped original) {
		return new ModelJetPack();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getArmorTexture(ItemStack itemStack, Entity entity, EntityEquipmentSlot slot, String layer) {
		if (slot != EntityEquipmentSlot.CHEST) {
			return null;
		}
		return ModGlobals.MODID + ":textures/models/armor/modeljetpack.png";
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot) {
		return new ArmorProperties(0, ModMaterials.JETPACK_MATERIAL.getDamageReductionAmount(getSlot(3 - slot)) / 17.0D, getDamageReduction(3 - slot) * 10);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add("");
		tooltip.add(ChatFormatting.WHITE + "" + ChatFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
		if (playerIn.capabilities.isFlying) {
			if (armorType == EntityEquipmentSlot.CHEST && !playerIn.capabilities.isCreativeMode && !playerIn.isSpectator() && playerIn.getItemStackFromSlot(getEquipmentSlot()) == stack) {
				tooltip.add("");
				tooltip.add(TextFormatting.RED + "" + TextFormatting.ITALIC + "Cannot remove jetpack while flying!");
			}
		}
	}
	/*
		@Override
		public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
			ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
			if (chest != null && chest.getItem().equals(this)) {
				if (chest.getItem() == ModItems.jetPackItem) {
					if (getEnergyStored(chest) > 0) {
						player.capabilities.allowFlying = true;
						if (player.capabilities.isFlying && (!player.capabilities.isCreativeMode && !player.isSpectator())) {
							extractEnergy(chest, 10, false);
						}
					}
					else {
						if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
							player.capabilities.isFlying = false;
							player.capabilities.allowFlying = false;
						}
					}
				}
			}
		}

		@Override
		public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
			EntityPlayer player = (EntityPlayer) entityIn;
			ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
			if ((chest == null || (chest.getItem() != ModItems.emeraldCarbonJetplate && chest.getItem() != ModItems.jetPackItem))) {
				if (Globals.CARBONJETPLATE_ISEQUIPPED || Globals.CARBONJETPACK_ISEQUIPPED) {
					Globals.CARBONJETPLATE_ISEQUIPPED = false;
					Globals.CARBONJETPACK_ISEQUIPPED = false;
					if (!player.capabilities.isCreativeMode && !player.isSpectator()) {
						player.capabilities.isFlying = false;
						player.capabilities.allowFlying = false;
					}
				}
			}
		}
	*/
}
