package p455w0rd.p455w0rdsthings.items;

import net.minecraft.creativetab.CreativeTabs;

public class ItemCarbonIngot extends ItemBase {

	public ItemCarbonIngot() {
		super("carbon_ingot");
		setCreativeTab(CreativeTabs.MATERIALS);
	}
	
}
