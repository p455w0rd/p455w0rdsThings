package p455w0rd.p455w0rdsthings.items;

import java.util.Iterator;
import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import codechicken.lib.model.ModelRegistryHelper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.client.render.MagnetItemRenderer;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.util.FriendermanUtils;
import p455w0rdslib.util.EntityUtils;

@Optional.Interface(iface = "baubles.api.IBauble", modid = "Baubles|API", striprefs = true)
public class ItemMagnet extends ItemRF implements IBauble {

	protected static String name = "magnet";
	public static int ENERGY_OUTPUT = 1000;
	public static int ENERGY_PER_XP = 25;
	public boolean tempDisable = false;
	//int lastDmg = 0;

	public ItemMagnet() {
		super(ConfigOptions.MAGNET_RF_CAPCITY, ENERGY_OUTPUT, name);
		setMaxStackSize(1);
		canRepair = false;
		setMaxDamage(0);
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tab, List<ItemStack> subItems) {
		subItems.add(new ItemStack(item));
		ItemStack fullStack = setFull(new ItemStack(item));
		subItems.add(fullStack);
	}

	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return stack.getItemDamage() < 2;
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		double max = getMaxEnergyStored(stack);
		return (max - getEnergyStored(stack)) / max;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add(ChatFormatting.WHITE + "" + ChatFormatting.ITALIC + "RF: " + getEnergyStored(stack) + "/" + getMaxEnergyStored(stack));
		tooltip.add((stack.getItemDamage() == 0 ? "Dea" : "A") + "ctivated");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void initModel() {
		ModelRegistryHelper.registerItemRenderer(this, MagnetItemRenderer.getInstance());
		/*
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
		ModelLoader.setCustomModelResourceLocation(this, 1, new ModelResourceLocation(getRegistryName(), "inventory"));
		ModelLoader.setCustomModelResourceLocation(this, 2, new ModelResourceLocation(getRegistryName(), "inventory")); //For Guide Icon
		*/
	}

	@Override
	public boolean hasEffect(ItemStack item) {
		return isActivated(item);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand) {
		if (!worldIn.isRemote && playerIn.isSneaking()) {
			if (itemStackIn.getItemDamage() == 0) {
				if (getEnergyStored(itemStackIn) > 0) {
					itemStackIn.setItemDamage(1);
				}
			}
			else {
				itemStackIn.setItemDamage(0);
			}
		}
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStackIn);
	}

	@Override
	public void onUpdate(ItemStack item, World world, Entity entity, int i, boolean f) {
		doMagnet(item, entity);
	}

	@SuppressWarnings("unchecked")
	private void doMagnet(ItemStack item, Entity entity) {
		if (entity == null || item == null || entity.getEntityWorld() == null || !(entity instanceof EntityPlayer)) {
			return;
		}
		World world = entity.getEntityWorld();
		if (entity.isSneaking() || entity.ticksExisted % 5 == 0 || !isActivated(item) || !(entity instanceof EntityPlayer) || world == null || world.isRemote) {
			return;
		}
		double distanceFromPlayer = ConfigOptions.MAGNET_RADIUS;
		if (getEnergyStored(item) <= 0) {
			if (isActivated(item)) {
				item.setItemDamage(0);
			}
			return;
		}
		EntityPlayer player = (EntityPlayer) entity;

		// items
		Iterator<Entity> iterator = (Iterator<Entity>) EntityUtils.getEntitiesInRange(EntityItem.class, world, player.posX, player.posY, player.posZ, distanceFromPlayer).iterator();
		while (iterator.hasNext()) {
			EntityItem itemToGet = (EntityItem) iterator.next();

			if (itemToGet != null) {
				if (FriendermanUtils.getFirstFriendermenWithStorageSpaceInRangeForStack(player, itemToGet.getEntityItem(), 3.0D) != null) {
					itemToGet.setThrower(player.getDisplayNameString());
					tempDisable = true;
					//lastDmg = item.getItemDamage();
					//item.setItemDamage(3);
				}
				else {
					itemToGet.setThrower("");
					//item.setItemDamage(lastDmg);
					tempDisable = false;
				}
			}
			if (itemToGet.getThrower() != null && itemToGet.getThrower().equals(player.getDisplayNameString())) {
				continue;
			}
			if (!tempDisable) {
				//item.setItemDamage(lastDmg);
				EntityPlayer closestPlayer = world.getClosestPlayerToEntity(itemToGet, distanceFromPlayer);
				if (closestPlayer != null && closestPlayer != player) {
					continue;
				}
				//if (DankNullUtils.getDankNullForStack(player, itemToGet.getEntityItem()) != null) {
				//	continue;
				//}
				if (FriendermanUtils.getFirstFriendermenWithStorageSpaceInRangeForStack(player, itemToGet.getEntityItem(), distanceFromPlayer) != null) {
					itemToGet.setThrower(player.getDisplayNameString());
					continue;
				}
				if (!world.isRemote) {
					if (itemToGet.cannotPickup()) {
						itemToGet.setPickupDelay(0);
					}
					itemToGet.motionX = itemToGet.motionY = itemToGet.motionZ = 0;
					itemToGet.setPosition(entity.posX - 0.2 + (world.rand.nextDouble() * 0.4), entity.posY - 0.6, entity.posZ - 0.2 + (world.rand.nextDouble() * 0.4));
					extractEnergy(item, ConfigOptions.MAGNET_RF_PER_ITEM, false);
				}
			}
		}
		// xp - always ty to collect exprience
		iterator = (Iterator<Entity>) EntityUtils.getEntitiesInRange(EntityXPOrb.class, world, player.posX, player.posY, player.posZ, distanceFromPlayer).iterator();
		while (iterator.hasNext()) {
			EntityXPOrb xpToGet = (EntityXPOrb) iterator.next();
			if (xpToGet.isDead || xpToGet.isInvisible()) {
				continue;
			}
			player.xpCooldown = 0;
			xpToGet.delayBeforeCanPickup = 0;
			xpToGet.setPosition(player.posX, player.posY, player.posZ);
			PlayerPickupXpEvent xpEvent = new PlayerPickupXpEvent(player, xpToGet);
			MinecraftForge.EVENT_BUS.post(xpEvent);
			if (xpEvent.getResult() == Result.ALLOW) {
				xpToGet.onCollideWithPlayer(player);
				extractEnergy(item, ENERGY_PER_XP, false);
			}
		}
	}

	protected boolean isActivated(ItemStack item) {
		return item.getItemDamage() > 0;
	}

	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return (!ItemStack.areItemsEqual(oldStack, newStack) && slotChanged) || (oldStack.getItemDamage() != newStack.getItemDamage());
	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public BaubleType getBaubleType(ItemStack itemstack) {
		return BaubleType.TRINKET;
	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public void onWornTick(ItemStack item, EntityLivingBase entity) {
		doMagnet(item, entity);
	}

	@Optional.Method(modid = "Baubles|API")
	@Override
	public boolean willAutoSync(ItemStack itemstack, EntityLivingBase player) {
		return true;
	}

}
