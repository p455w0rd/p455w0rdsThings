package p455w0rd.p455w0rdsthings.items;

/**
 * @author p455w0rd
 *
 */
public class ItemCompressor extends ItemBase {

	private static final String NAME = "compressor_item";

	public ItemCompressor() {
		super(NAME);
	}

}
