package p455w0rd.p455w0rdsthings.items;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rd.p455w0rdsthings.init.ModLogger;

public class ItemBase extends Item implements IModelHolder {

	private String name = "";

	public ItemBase(String name) {
		this.name = name;
		setRegistryName(this.name);
		setUnlocalizedName(this.name);
		GameRegistry.register(this);
		setMaxStackSize(64);
		setMaxDamage(0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModLogger.info("    Registering model for " + this.getUnlocalizedName().substring(5));
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}
}
