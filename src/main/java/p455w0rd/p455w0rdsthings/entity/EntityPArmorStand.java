package p455w0rd.p455w0rdsthings.entity;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.scoreboard.Team;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.datafix.FixTypes;
import net.minecraft.util.datafix.walkers.ItemStackDataLists;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Rotations;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonJetPlate;
import p455w0rd.p455w0rdsthings.items.ItemJetPack;
import p455w0rd.p455w0rdsthings.network.PacketSetArmor;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.PotionUtils;

public class EntityPArmorStand extends EntityLivingBase {
	private static final Rotations DEFAULT_HEAD_ROTATION = new Rotations(0.0F, 0.0F, 0.0F);
	private static final Rotations DEFAULT_BODY_ROTATION = new Rotations(0.0F, 0.0F, 0.0F);
	private static final Rotations DEFAULT_LEFTARM_ROTATION = new Rotations(-1.0F, 0.0F, -10.0F);
	private static final Rotations DEFAULT_RIGHTARM_ROTATION = new Rotations(-1.0F, 0.0F, 10.0F);
	private static final Rotations DEFAULT_LEFTLEG_ROTATION = new Rotations(-1.0F, 0.0F, -1.0F);
	private static final Rotations DEFAULT_RIGHTLEG_ROTATION = new Rotations(1.0F, 0.0F, 1.0F);
	public static final DataParameter<Byte> STATUS = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.BYTE);
	public static final DataParameter<Rotations> HEAD_ROTATION = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.ROTATIONS);
	public static final DataParameter<Rotations> BODY_ROTATION = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.ROTATIONS);
	public static final DataParameter<Rotations> LEFT_ARM_ROTATION = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.ROTATIONS);
	public static final DataParameter<Rotations> RIGHT_ARM_ROTATION = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.ROTATIONS);
	public static final DataParameter<Rotations> LEFT_LEG_ROTATION = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.ROTATIONS);
	public static final DataParameter<Rotations> RIGHT_LEG_ROTATION = EntityDataManager.createKey(EntityPArmorStand.class, DataSerializers.ROTATIONS);
	private static final Predicate<Entity> IS_RIDEABLE_MINECART = (@Nullable Entity p_apply_1_) -> p_apply_1_ instanceof EntityMinecart && ((EntityMinecart) p_apply_1_).getType() == EntityMinecart.Type.RIDEABLE;
	private final ItemStack[] handItems;
	private final ItemStack[] armorItems;
	private boolean canInteract;
	public long punchCooldown;
	private int disabledSlots;
	private boolean wasMarker;
	private Rotations headRotation;
	private Rotations bodyRotation;
	private Rotations leftArmRotation;
	private Rotations rightArmRotation;
	private Rotations leftLegRotation;
	private Rotations rightLegRotation;

	public EntityPArmorStand(World worldIn) {
		super(worldIn);
		handItems = new ItemStack[2];
		armorItems = new ItemStack[4];
		headRotation = DEFAULT_HEAD_ROTATION;
		bodyRotation = DEFAULT_BODY_ROTATION;
		leftArmRotation = DEFAULT_LEFTARM_ROTATION;
		rightArmRotation = DEFAULT_RIGHTARM_ROTATION;
		leftLegRotation = DEFAULT_LEFTLEG_ROTATION;
		rightLegRotation = DEFAULT_RIGHTLEG_ROTATION;
		noClip = hasNoGravity();
		setSize(0.5F, 1.975F);
	}

	public EntityPArmorStand(World worldIn, double posX, double posY, double posZ) {
		this(worldIn);
		setPosition(posX, posY, posZ);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean isInvisibleToPlayer(EntityPlayer player) {
		return false;
	}

	@Override
	public Team getTeam() {
		return null;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public int getBrightnessForRender(float partialTicks) {
		BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos(MathHelper.floor(posX), 0, MathHelper.floor(posZ));

		if (world != null && world.isBlockLoaded(blockpos$mutableblockpos)) {
			blockpos$mutableblockpos.setY(MathHelper.floor(posY + getEyeHeight()));
			return world.getCombinedLight(blockpos$mutableblockpos, 0);
		}
		else {
			return 15;
		}
	}

	@Override
	public float getBrightness(float partialTicks) {
		BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos(MathHelper.floor(posX), 0, MathHelper.floor(posZ));

		if (world != null && world.isBlockLoaded(blockpos$mutableblockpos)) {
			blockpos$mutableblockpos.setY(MathHelper.floor(posY + getEyeHeight()));
			return world.getLightBrightness(blockpos$mutableblockpos);
		}
		else {
			return 1.0F;
		}
	}

	@Override
	public boolean isServerWorld() {
		return (super.isServerWorld()) && (!hasNoGravity());
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataManager.register(STATUS, Byte.valueOf((byte) 0));
		dataManager.register(HEAD_ROTATION, DEFAULT_HEAD_ROTATION);
		dataManager.register(BODY_ROTATION, DEFAULT_BODY_ROTATION);
		dataManager.register(LEFT_ARM_ROTATION, DEFAULT_LEFTARM_ROTATION);
		dataManager.register(RIGHT_ARM_ROTATION, DEFAULT_RIGHTARM_ROTATION);
		dataManager.register(LEFT_LEG_ROTATION, DEFAULT_LEFTLEG_ROTATION);
		dataManager.register(RIGHT_LEG_ROTATION, DEFAULT_RIGHTLEG_ROTATION);
		setShowArms(true);
	}

	@Override
	public Iterable<ItemStack> getHeldEquipment() {
		return Arrays.asList(handItems);
	}

	@Override
	public Iterable<ItemStack> getArmorInventoryList() {
		return Arrays.asList(armorItems);
	}

	@Override
	@Nullable
	public ItemStack getItemStackFromSlot(EntityEquipmentSlot slotIn) {
		ItemStack itemstack = null;
		switch (slotIn.getSlotType()) {
		case HAND:
			itemstack = handItems[slotIn.getIndex()];
			break;
		case ARMOR:
			itemstack = armorItems[slotIn.getIndex()];
		}
		return itemstack;
	}

	@Override
	public void setItemStackToSlot(EntityEquipmentSlot slotIn, @Nullable ItemStack stack) {
		switch (slotIn.getSlotType()) {
		case HAND:
			playEquipSound(stack);
			handItems[slotIn.getIndex()] = stack;
			break;
		case ARMOR:
			playEquipSound(stack);
			armorItems[slotIn.getIndex()] = stack;
		}
	}

	@Override
	public boolean replaceItemInInventory(int inventorySlot, @Nullable ItemStack itemStackIn) {
		EntityEquipmentSlot entityequipmentslot;

		if (inventorySlot == 98) {
			entityequipmentslot = EntityEquipmentSlot.MAINHAND;
		}
		else if (inventorySlot == 99) {
			entityequipmentslot = EntityEquipmentSlot.OFFHAND;
		}
		else if (inventorySlot == 100 + EntityEquipmentSlot.HEAD.getIndex()) {
			entityequipmentslot = EntityEquipmentSlot.HEAD;
		}
		else if (inventorySlot == 100 + EntityEquipmentSlot.CHEST.getIndex()) {
			entityequipmentslot = EntityEquipmentSlot.CHEST;
		}
		else if (inventorySlot == 100 + EntityEquipmentSlot.LEGS.getIndex()) {
			entityequipmentslot = EntityEquipmentSlot.LEGS;
		}
		else {
			if (inventorySlot != 100 + EntityEquipmentSlot.FEET.getIndex()) {
				return false;
			}

			entityequipmentslot = EntityEquipmentSlot.FEET;
		}

		if (itemStackIn != null && !EntityLiving.isItemStackInSlot(entityequipmentslot, itemStackIn) && entityequipmentslot != EntityEquipmentSlot.HEAD) {
			return false;
		}
		else {
			setItemStackToSlot(entityequipmentslot, itemStackIn);
			return true;
		}
	}

	public static void func_189805_a(DataFixer p_189805_0_) {
		p_189805_0_.registerWalker(FixTypes.ENTITY, new ItemStackDataLists("ArmorStand", new String[] {
				"ArmorItems",
				"HandItems"
		}));
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound compound) {
		super.writeEntityToNBT(compound);
		NBTTagList nbttaglist = new NBTTagList();

		for (ItemStack itemstack : armorItems) {
			NBTTagCompound nbttagcompound = new NBTTagCompound();

			if (itemstack != null) {
				itemstack.writeToNBT(nbttagcompound);
			}

			nbttaglist.appendTag(nbttagcompound);
		}

		compound.setTag("ArmorItems", nbttaglist);
		NBTTagList nbttaglist1 = new NBTTagList();

		for (ItemStack itemstack1 : handItems) {
			NBTTagCompound nbttagcompound1 = new NBTTagCompound();

			if (itemstack1 != null) {
				itemstack1.writeToNBT(nbttagcompound1);
			}

			nbttaglist1.appendTag(nbttagcompound1);
		}

		compound.setTag("HandItems", nbttaglist1);

		if (getAlwaysRenderNameTag() && (getCustomNameTag() == null || getCustomNameTag().isEmpty())) {
			compound.setBoolean("CustomNameVisible", getAlwaysRenderNameTag());
		}

		compound.setBoolean("Invisible", isInvisible());
		compound.setBoolean("Small", isSmall());
		compound.setBoolean("ShowArms", getShowArms());
		compound.setInteger("DisabledSlots", disabledSlots);
		compound.setBoolean("NoBasePlate", hasNoBasePlate());

		if (hasMarker()) {
			compound.setBoolean("Marker", hasMarker());
		}

		compound.setTag("Pose", readPoseFromNBT());
	}

	public EntityPArmorStand setNoClip() {
		noClip = true;
		return this;
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound compound) {
		super.readEntityFromNBT(compound);
		if (compound.hasKey("ArmorItems", 9)) {
			NBTTagList nbttaglist = compound.getTagList("ArmorItems", 10);
			for (int i = 0; i < armorItems.length; i++) {
				armorItems[i] = ItemStack.loadItemStackFromNBT(nbttaglist.getCompoundTagAt(i));
			}
		}
		if (compound.hasKey("HandItems", 9)) {
			NBTTagList nbttaglist1 = compound.getTagList("HandItems", 10);
			for (int j = 0; j < handItems.length; j++) {
				handItems[j] = ItemStack.loadItemStackFromNBT(nbttaglist1.getCompoundTagAt(j));
			}
		}
		setInvisible(compound.getBoolean("Invisible"));
		setSmall(compound.getBoolean("Small"));
		setShowArms(compound.getBoolean("ShowArms"));
		disabledSlots = compound.getInteger("DisabledSlots");
		setNoBasePlate(compound.getBoolean("NoBasePlate"));
		setMarker(compound.getBoolean("Marker"));
		wasMarker = (!hasMarker());
		noClip = hasNoGravity();
		NBTTagCompound nbttagcompound = compound.getCompoundTag("Pose");
		writePoseToNBT(nbttagcompound);
	}

	private void writePoseToNBT(NBTTagCompound tagCompound) {
		NBTTagList nbttaglist = tagCompound.getTagList("Head", 5);
		setHeadRotation(nbttaglist.hasNoTags() ? DEFAULT_HEAD_ROTATION : new Rotations(nbttaglist));
		NBTTagList nbttaglist1 = tagCompound.getTagList("Body", 5);
		setBodyRotation(nbttaglist1.hasNoTags() ? DEFAULT_BODY_ROTATION : new Rotations(nbttaglist1));
		NBTTagList nbttaglist2 = tagCompound.getTagList("LeftArm", 5);
		setLeftArmRotation(nbttaglist2.hasNoTags() ? DEFAULT_LEFTARM_ROTATION : new Rotations(nbttaglist2));
		NBTTagList nbttaglist3 = tagCompound.getTagList("RightArm", 5);
		setRightArmRotation(nbttaglist3.hasNoTags() ? DEFAULT_RIGHTARM_ROTATION : new Rotations(nbttaglist3));
		NBTTagList nbttaglist4 = tagCompound.getTagList("LeftLeg", 5);
		setLeftLegRotation(nbttaglist4.hasNoTags() ? DEFAULT_LEFTLEG_ROTATION : new Rotations(nbttaglist4));
		NBTTagList nbttaglist5 = tagCompound.getTagList("RightLeg", 5);
		setRightLegRotation(nbttaglist5.hasNoTags() ? DEFAULT_RIGHTLEG_ROTATION : new Rotations(nbttaglist5));
	}

	private NBTTagCompound readPoseFromNBT() {
		NBTTagCompound nbttagcompound = new NBTTagCompound();
		if (!DEFAULT_HEAD_ROTATION.equals(headRotation)) {
			nbttagcompound.setTag("Head", headRotation.writeToNBT());
		}
		if (!DEFAULT_BODY_ROTATION.equals(bodyRotation)) {
			nbttagcompound.setTag("Body", bodyRotation.writeToNBT());
		}
		if (!DEFAULT_LEFTARM_ROTATION.equals(leftArmRotation)) {
			nbttagcompound.setTag("LeftArm", leftArmRotation.writeToNBT());
		}
		if (!DEFAULT_RIGHTARM_ROTATION.equals(rightArmRotation)) {
			nbttagcompound.setTag("RightArm", rightArmRotation.writeToNBT());
		}
		if (!DEFAULT_LEFTLEG_ROTATION.equals(leftLegRotation)) {
			nbttagcompound.setTag("LeftLeg", leftLegRotation.writeToNBT());
		}
		if (!DEFAULT_RIGHTLEG_ROTATION.equals(rightLegRotation)) {
			nbttagcompound.setTag("RightLeg", rightLegRotation.writeToNBT());
		}
		return nbttagcompound;
	}

	@Override
	public ItemStack getPickedResult(RayTraceResult target) {
		return new ItemStack(ModItems.ARMOR_STAND);
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	protected void collideWithEntity(Entity entityIn) {
	}

	@Override
	protected void collideWithNearbyEntities() {
		List<Entity> list = EasyMappings.world(this).getEntitiesInAABBexcluding(this, getEntityBoundingBox(), IS_RIDEABLE_MINECART);
		for (int i = 0; i < list.size(); i++) {
			Entity entity = list.get(i);
			if (getDistanceSqToEntity(entity) <= 0.2D) {
				entity.applyEntityCollision(this);
			}
		}
	}

	@Override
	public EnumActionResult applyPlayerInteraction(EntityPlayer player, Vec3d vec, @Nullable ItemStack stack, EnumHand hand) {
		if (hasMarker()) {
			return EnumActionResult.PASS;
		}
		// ensure we're the server and player isn't spectating
		if ((!EasyMappings.world(this).isRemote) && (!player.isSpectator())) {
			EntityEquipmentSlot entityequipmentslot = EntityEquipmentSlot.MAINHAND;
			boolean flag = stack != null;
			Item item = flag ? stack.getItem() : null;
			if ((flag) && ((item instanceof ItemArmor))) {
				entityequipmentslot = ((ItemArmor) item).armorType;
			}
			if ((flag) && ((item == Items.SKULL) || (item == Item.getItemFromBlock(Blocks.PUMPKIN)))) {
				entityequipmentslot = EntityEquipmentSlot.HEAD;
			}
			// swap available pieces of armor set
			if (player.isSneaking()) {
				for (int i = 1; i <= 4; i++) {
					EntityEquipmentSlot[] typeArmor = EntityEquipmentSlot.values();
					EntityEquipmentSlot currentSlot = typeArmor[(1 + i)];
					ItemStack playerArmorStack = player.inventory.getStackInSlot(35 + i);
					ItemStack armorStandStack = getItemStackFromSlot(currentSlot);
					if (playerArmorStack != null) {
						setItemStackToSlot(currentSlot, playerArmorStack);

						// if it's upgraded carbon armor and we're not trading
						// for another upgraded carbon armor
						if ((playerArmorStack.getItem() instanceof ItemEmeraldCarbonJetPlate && !(armorStandStack.getItem() instanceof ItemEmeraldCarbonJetPlate)) || ((playerArmorStack.getItem() instanceof ItemJetPack && !(armorStandStack.getItem() instanceof ItemJetPack)))) {
							switch (currentSlot) {
							case CHEST:
								if (!player.capabilities.isCreativeMode) {
									ModGlobals.CARBONCHESTPLATE_ISEQUIPPED = false;
									ModGlobals.CARBONJETPACK_ISEQUIPPED = false;
									player.capabilities.isFlying = false;
									player.capabilities.allowFlying = false;
									ModNetworking.INSTANCE.sendTo(new PacketSetArmor(1), (EntityPlayerMP) player);
								}
								break;
							case FEET:
								ModGlobals.CARBONBOOTS_ISEQUIPPED = false;
								if (player.stepHeight != 0.6F) {
									player.stepHeight = 0.6F;
								}
								ModNetworking.INSTANCE.sendTo(new PacketSetArmor(3), (EntityPlayerMP) player);
								break;
							case HEAD:
								ModGlobals.CARBONHELMET_ISEQUIPPED = false;
								if (PotionUtils.isPotionActive(player, MobEffects.NIGHT_VISION)) {
									PotionUtils.clearPotionEffect(player, MobEffects.NIGHT_VISION, 1);
								}
								ModNetworking.INSTANCE.sendTo(new PacketSetArmor(), (EntityPlayerMP) player);
								break;
							case LEGS:
								ModGlobals.CARBONLEGGINGS_ISEQUIPPED = false;
								if (PotionUtils.isPotionActive(player, MobEffects.SPEED)) {
									PotionUtils.clearPotionEffect(player, MobEffects.SPEED, 0);
								}
								ModNetworking.INSTANCE.sendTo(new PacketSetArmor(2), (EntityPlayerMP) player);
								break;
							case MAINHAND:
							case OFFHAND:
							default:
								break;
							}
						}
						player.setItemStackToSlot(currentSlot, armorStandStack);
					}
					if (armorStandStack != null) {
						player.setItemStackToSlot(currentSlot, armorStandStack);
						setItemStackToSlot(currentSlot, playerArmorStack);
					}
				}
				return EnumActionResult.SUCCESS;
			}
			EntityEquipmentSlot entityequipmentslot1 = EntityEquipmentSlot.MAINHAND;
			if ((getItemStackFromSlot(EntityEquipmentSlot.MAINHAND) != null) && (getItemStackFromSlot(EntityEquipmentSlot.OFFHAND) == null)) {
				entityequipmentslot = EntityEquipmentSlot.OFFHAND;
			}
			if ((getItemStackFromSlot(EntityEquipmentSlot.MAINHAND) == null) && (getItemStackFromSlot(EntityEquipmentSlot.OFFHAND) != null)) {
				entityequipmentslot1 = EntityEquipmentSlot.OFFHAND;
			}
			boolean flag1 = isSmall();
			double d4 = flag1 ? vec.yCoord * 2.0D : vec.yCoord;
			if (d4 >= 0.1D) {
				if ((d4 < 0.1D + (flag1 ? 0.8D : 0.45D)) && (getItemStackFromSlot(EntityEquipmentSlot.FEET) != null)) {
					entityequipmentslot1 = EntityEquipmentSlot.FEET;
				}
			}
			if (d4 >= 0.9D + (flag1 ? 0.3D : 0.0D)) {
				if ((d4 < 0.9D + (flag1 ? 1.0D : 0.7D)) && (getItemStackFromSlot(EntityEquipmentSlot.CHEST) != null)) {
					entityequipmentslot1 = EntityEquipmentSlot.CHEST;
				}
			}
			if (d4 >= 0.4D) {
				if ((d4 < 0.4D + (flag1 ? 1.0D : 0.8D)) && (getItemStackFromSlot(EntityEquipmentSlot.LEGS) != null)) {
					entityequipmentslot1 = EntityEquipmentSlot.LEGS;
				}
			}
			if ((d4 >= 1.6D) && (getItemStackFromSlot(EntityEquipmentSlot.HEAD) != null)) {
				entityequipmentslot1 = EntityEquipmentSlot.HEAD;
			}

			boolean flag2 = getItemStackFromSlot(entityequipmentslot1) != null;
			if ((isDisabled(entityequipmentslot1)) || (isDisabled(entityequipmentslot))) {
				entityequipmentslot1 = entityequipmentslot;
				if (isDisabled(entityequipmentslot)) {
					return EnumActionResult.FAIL;
				}
			}
			if ((flag) && (entityequipmentslot == EntityEquipmentSlot.MAINHAND) && (!getShowArms())) {
				return EnumActionResult.FAIL;
			}
			if (flag) {
				swapItem(player, entityequipmentslot, stack, hand);
			}
			else if (flag2) {
				swapItem(player, entityequipmentslot1, stack, hand);
			}
			return EnumActionResult.SUCCESS;
		}
		return EnumActionResult.SUCCESS;
	}

	private boolean isDisabled(EntityEquipmentSlot slotIn) {
		return false;
	}

	private void swapItem(EntityPlayer player, EntityEquipmentSlot p_184795_2_, @Nullable ItemStack p_184795_3_, EnumHand hand) {
		ItemStack itemstack = getItemStackFromSlot(p_184795_2_);
		if (((itemstack == null) || ((disabledSlots & 1 << p_184795_2_.getSlotIndex() + 8) == 0)) && ((itemstack != null) || ((disabledSlots & 1 << p_184795_2_.getSlotIndex() + 16) == 0))) {
			if ((player.capabilities.isCreativeMode) && ((itemstack == null) || (itemstack.getItem() == Item.getItemFromBlock(Blocks.AIR))) && (p_184795_3_ != null)) {
				ItemStack itemstack2 = p_184795_3_.copy();
				itemstack2.stackSize = 1;
				setItemStackToSlot(p_184795_2_, itemstack2);
			}
			else if ((p_184795_3_ != null) && (p_184795_3_.stackSize > 1)) {
				if (itemstack == null) {
					ItemStack itemstack1 = p_184795_3_.copy();
					itemstack1.stackSize = 1;
					setItemStackToSlot(p_184795_2_, itemstack1);
					p_184795_3_.stackSize -= 1;
				}
			}
			else {
				setItemStackToSlot(p_184795_2_, p_184795_3_);
				player.setHeldItem(hand, itemstack);
			}
		}
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if ((!EasyMappings.world(this).isRemote) && (!isDead)) {
			if (DamageSource.outOfWorld.equals(source)) {
				setDead();
				return false;
			}
			if ((!isEntityInvulnerable(source)) && (!canInteract) && (!hasMarker())) {
				if (source.isExplosion()) {
					dropContents();
					setDead();
					return false;
				}
				if (DamageSource.inFire.equals(source)) {
					if (isBurning()) {
						damageArmorStand(0.15F);
					}
					else {
						setFire(5);
					}
					return false;
				}
				if ((DamageSource.onFire.equals(source)) && (getHealth() > 0.5F)) {
					damageArmorStand(4.0F);
					return false;
				}
				boolean flag = "arrow".equals(source.getDamageType());
				boolean flag1 = "player".equals(source.getDamageType());
				if ((!flag1) && (!flag)) {
					return false;
				}
				if ((source.getSourceOfDamage() instanceof EntityArrow)) {
					source.getSourceOfDamage().setDead();
				}
				if (((source.getEntity() instanceof EntityPlayer)) && (!((EntityPlayer) source.getEntity()).capabilities.allowEdit)) {
					return false;
				}
				if (source.isCreativePlayer()) {
					playParticles();
					setDead();
					return false;
				}
				long i = EasyMappings.world(this).getTotalWorldTime();
				if ((i - punchCooldown > 5L) && (!flag)) {
					EasyMappings.world(this).setEntityState(this, (byte) 32);
					punchCooldown = i;
				}
				else {
					dropBlock();
					playParticles();
					setDead();
				}
				return false;
			}
			return false;
		}
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleStatusUpdate(byte id) {
		if (id == 32) {
			if (EasyMappings.world(this).isRemote) {
				EasyMappings.world(this).playSound(posX, posY, posZ, SoundEvents.ENTITY_ARMORSTAND_HIT, getSoundCategory(), 0.3F, 1.0F, false);
				punchCooldown = EasyMappings.world(this).getTotalWorldTime();
			}
		}
		else {
			super.handleStatusUpdate(id);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRenderDist(double distance) {
		double d0 = getEntityBoundingBox().getAverageEdgeLength() * 4.0D;
		if ((Double.isNaN(d0)) || (d0 == 0.0D)) {
			d0 = 4.0D;
		}
		d0 *= 64.0D;
		return distance < d0 * d0;
	}

	private void playParticles() {
		if ((EasyMappings.world(this) instanceof WorldServer)) {
			((WorldServer) EasyMappings.world(this)).spawnParticle(EnumParticleTypes.FIREWORKS_SPARK, posX, posY + height / 1.5D, posZ, 10, width / 4.0F, height / 4.0F, width / 4.0F, 0.05D, new int[] {
					Block.getStateId(Blocks.PLANKS.getDefaultState())
			});
		}
	}

	private void damageArmorStand(float damage) {
		float f = getHealth();
		f -= damage;
		if (f <= 0.5F) {
			dropContents();
			setDead();
		}
		else {
			setHealth(f);
		}
	}

	private void dropBlock() {
		Block.spawnAsEntity(EasyMappings.world(this), new BlockPos(this), new ItemStack(ModItems.ARMOR_STAND));
		dropContents();
	}

	private void dropContents() {
		EasyMappings.world(this).playSound((EntityPlayer) null, posX, posY, posZ, SoundEvents.ENTITY_ARMORSTAND_BREAK, getSoundCategory(), 1.0F, 1.0F);
		for (int i = 0; i < handItems.length; i++) {
			if ((handItems[i] != null) && (handItems[i].stackSize > 0)) {
				if (handItems[i] != null) {
					Block.spawnAsEntity(EasyMappings.world(this), new BlockPos(this).up(), handItems[i]);
				}
				handItems[i] = null;
			}
		}
		for (int j = 0; j < armorItems.length; j++) {
			if ((armorItems[j] != null) && (armorItems[j].stackSize > 0)) {
				if (armorItems[j] != null) {
					Block.spawnAsEntity(EasyMappings.world(this), new BlockPos(this).up(), armorItems[j]);
				}
				armorItems[j] = null;
			}
		}
	}

	@Override
	protected float updateDistance(float p_110146_1_, float p_110146_2_) {
		prevRenderYawOffset = prevRotationYaw;
		renderYawOffset = rotationYaw;
		return 0.0F;
	}

	@Override
	public float getEyeHeight() {
		return isChild() ? height * 0.5F : height * 0.9F;
	}

	@Override
	public double getYOffset() {
		return hasMarker() ? 0.0D : 0.10000000149011612D;
	}

	@Override
	public void moveEntityWithHeading(float strafe, float forward) {
		if (!hasNoGravity()) {
			super.moveEntityWithHeading(strafe, forward);
		}
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		Rotations rotations = dataManager.get(HEAD_ROTATION);
		if (!headRotation.equals(rotations)) {
			setHeadRotation(rotations);
		}
		Rotations rotations1 = dataManager.get(BODY_ROTATION);
		if (!bodyRotation.equals(rotations1)) {
			setBodyRotation(rotations1);
		}
		Rotations rotations2 = dataManager.get(LEFT_ARM_ROTATION);
		if (!leftArmRotation.equals(rotations2)) {
			setLeftArmRotation(rotations2);
		}
		Rotations rotations3 = dataManager.get(RIGHT_ARM_ROTATION);
		if (!rightArmRotation.equals(rotations3)) {
			setRightArmRotation(rotations3);
		}
		Rotations rotations4 = dataManager.get(LEFT_LEG_ROTATION);
		if (!leftLegRotation.equals(rotations4)) {
			setLeftLegRotation(rotations4);
		}
		Rotations rotations5 = dataManager.get(RIGHT_LEG_ROTATION);
		if (!rightLegRotation.equals(rotations5)) {
			setRightLegRotation(rotations5);
		}
		boolean flag = hasMarker();
		if ((!wasMarker) && (flag)) {
			updateBoundingBox(false);
			preventEntitySpawning = false;
		}
		else {
			if ((!wasMarker) || (flag)) {
				return;
			}
			updateBoundingBox(true);
			preventEntitySpawning = true;
		}
		wasMarker = flag;
	}

	private void updateBoundingBox(boolean p_181550_1_) {
		double d0 = posX;
		double d1 = posY;
		double d2 = posZ;
		if (p_181550_1_) {
			setSize(0.5F, 1.975F);
		}
		else {
			setSize(0.0F, 0.0F);
		}
		setPosition(d0, d1, d2);
	}

	@Override
	protected void updatePotionMetadata() {
		setInvisible(canInteract);
	}

	@Override
	public void setInvisible(boolean invisible) {
		canInteract = invisible;
		super.setInvisible(invisible);
	}

	@Override
	public boolean isChild() {
		return isSmall();
	}

	@Override
	public void onKillCommand() {
		setDead();
	}

	@Override
	public boolean isImmuneToExplosions() {
		return isInvisible();
	}

	private void setSmall(boolean small) {
		dataManager.set(STATUS, Byte.valueOf(setBit(dataManager.get(STATUS).byteValue(), 1, small)));
	}

	public boolean isSmall() {
		return (dataManager.get(STATUS).byteValue() & 0x1) != 0;
	}

	private void setShowArms(boolean showArms) {
		dataManager.set(STATUS, Byte.valueOf(setBit(dataManager.get(STATUS).byteValue(), 4, showArms)));
	}

	public boolean getShowArms() {
		return (dataManager.get(STATUS).byteValue() & 0x4) != 0;
	}

	private void setNoBasePlate(boolean noBasePlate) {
		dataManager.set(STATUS, Byte.valueOf(setBit(dataManager.get(STATUS).byteValue(), 8, noBasePlate)));
	}

	public boolean hasNoBasePlate() {
		return (dataManager.get(STATUS).byteValue() & 0x8) != 0;
	}

	private void setMarker(boolean marker) {
		dataManager.set(STATUS, Byte.valueOf(setBit(dataManager.get(STATUS).byteValue(), 16, marker)));
	}

	public boolean hasMarker() {
		return (dataManager.get(STATUS).byteValue() & 0x10) != 0;
	}

	private byte setBit(byte p_184797_1_, int p_184797_2_, boolean p_184797_3_) {
		if (p_184797_3_) {
			p_184797_1_ = (byte) (p_184797_1_ | p_184797_2_);
		}
		else {
			p_184797_1_ = (byte) (p_184797_1_ & (p_184797_2_ ^ 0xFFFFFFFF));
		}
		return p_184797_1_;
	}

	public void setHeadRotation(Rotations vec) {
		headRotation = vec;
		dataManager.set(HEAD_ROTATION, vec);
	}

	public void setBodyRotation(Rotations vec) {
		bodyRotation = vec;
		dataManager.set(BODY_ROTATION, vec);
	}

	public void setLeftArmRotation(Rotations vec) {
		leftArmRotation = vec;
		dataManager.set(LEFT_ARM_ROTATION, vec);
	}

	public void setRightArmRotation(Rotations vec) {
		rightArmRotation = vec;
		dataManager.set(RIGHT_ARM_ROTATION, vec);
	}

	public void setLeftLegRotation(Rotations vec) {
		leftLegRotation = vec;
		dataManager.set(LEFT_LEG_ROTATION, vec);
	}

	public void setRightLegRotation(Rotations vec) {
		rightLegRotation = vec;
		dataManager.set(RIGHT_LEG_ROTATION, vec);
	}

	public Rotations getHeadRotation() {
		return headRotation;
	}

	public Rotations getBodyRotation() {
		return bodyRotation;
	}

	@SideOnly(Side.CLIENT)
	public Rotations getLeftArmRotation() {
		return leftArmRotation;
	}

	@SideOnly(Side.CLIENT)
	public Rotations getRightArmRotation() {
		return rightArmRotation;
	}

	@SideOnly(Side.CLIENT)
	public Rotations getLeftLegRotation() {
		return leftLegRotation;
	}

	@SideOnly(Side.CLIENT)
	public Rotations getRightLegRotation() {
		return rightLegRotation;
	}

	@Override
	public boolean canBeCollidedWith() {
		return (super.canBeCollidedWith()) && (!hasMarker());
	}

	@Override
	public EnumHandSide getPrimaryHand() {
		return EnumHandSide.RIGHT;
	}

	@Override
	protected SoundEvent getFallSound(int heightIn) {
		return SoundEvents.ENTITY_ARMORSTAND_FALL;
	}

	@Override
	@Nullable
	protected SoundEvent getHurtSound() {
		return SoundEvents.ENTITY_ARMORSTAND_HIT;
	}

	@Override
	@Nullable
	protected SoundEvent getDeathSound() {
		return SoundEvents.ENTITY_ARMORSTAND_BREAK;
	}

	@Override
	public void onStruckByLightning(EntityLightningBolt lightningBolt) {
	}

	@Override
	public boolean canBeHitWithPotion() {
		return false;
	}
}
