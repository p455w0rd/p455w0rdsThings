package p455w0rd.p455w0rdsthings.blocks;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.SoundType;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.IStateMapper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModGlobals.HarvestLevel;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModMaterials;
import p455w0rd.p455w0rdsthings.items.ItemCarbonDoor;

/**
 * @author p455w0rd
 *
 */
public class BlockCarbonDoor extends BlockDoor implements IModelHolder {

	private static final String NAME = "carbon_door";

	public BlockCarbonDoor() {
		super(ModMaterials.CARBON);
		setUnlocalizedName(NAME);
		setRegistryName(NAME);
		setHardness(10.0F);
		setResistance(6000000.0F);
		GameRegistry.register(this);
		GameRegistry.register(new ItemCarbonDoor(this), getRegistryName());
		setSoundType(SoundType.METAL);
		if (Mods.TINKERS.isLoaded()) {
			setHarvestLevel("pickaxe", HarvestLevel.CARBON);
		}
		else {
			setHarvestLevel("pickaxe", HarvestLevel.DIAMOND);
		}
		setDefaultState(blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH).withProperty(OPEN, false).withProperty(HINGE, BlockDoor.EnumHingePosition.LEFT).withProperty(POWERED, false).withProperty(HALF, BlockDoor.EnumDoorHalf.LOWER));
	}

	@Override
	public boolean canEntityDestroy(IBlockState state, IBlockAccess world, BlockPos pos, Entity entity) {
		if (entity instanceof net.minecraft.entity.boss.EntityDragon) {
			return false;
		}
		else if ((entity instanceof net.minecraft.entity.boss.EntityWither) || (entity instanceof net.minecraft.entity.projectile.EntityWitherSkull)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {

		BlockPos blockpos = state.getValue(HALF) == BlockDoor.EnumDoorHalf.LOWER ? pos : pos.down();
		IBlockState iblockstate = pos.equals(blockpos) ? state : worldIn.getBlockState(blockpos);

		if (iblockstate.getBlock() != this) {
			return false;
		}
		else {
			state = iblockstate.cycleProperty(OPEN);
			worldIn.setBlockState(blockpos, state, 10);
			worldIn.markBlockRangeForRenderUpdate(blockpos, pos);
			worldIn.playEvent(playerIn, state.getValue(OPEN).booleanValue() ? 1005 : 1011, pos, 0);
			return true;
		}

	}

	@SideOnly(Side.CLIENT)
	public void registerStateMapper() {
		ModelLoader.setCustomStateMapper(ModBlocks.CARBON_DOOR, new DoorStateMapper());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, new IProperty[] {
				HALF,
				FACING,
				OPEN,
				HINGE,
				POWERED
		});

	}

	@Override
	public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
		return state.getValue(HALF) == EnumDoorHalf.LOWER ? Lists.<ItemStack>newArrayList(new ItemStack(Item.getItemFromBlock(state.getBlock()))) : Lists.<ItemStack>newArrayList();
	}

	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
		return new ItemStack(Item.getItemFromBlock(state.getBlock()));
	}

	public static class DoorStateMapper implements IStateMapper {

		protected final Map<IBlockState, ModelResourceLocation> mapStateModelLocations = Maps.newLinkedHashMap();

		@SuppressWarnings({
				"unchecked",
				"rawtypes"
		})
		public String getPropertyString(Map<IProperty<?>, Comparable<?>> map) {
			StringBuilder stringbuilder = new StringBuilder();

			for (Entry<IProperty<?>, Comparable<?>> entry : map.entrySet()) {
				if (stringbuilder.length() != 0) {
					stringbuilder.append(",");
				}

				IProperty iproperty = entry.getKey();
				Comparable comparable = entry.getValue();
				stringbuilder.append(iproperty.getName());
				stringbuilder.append("=");
				stringbuilder.append(iproperty.getName(comparable));
			}

			if (stringbuilder.length() == 0) {
				stringbuilder.append("normal");
			}

			return stringbuilder.toString();
		}

		@Override
		public Map<IBlockState, ModelResourceLocation> putStateModelLocations(Block block) {
			for (IBlockState state : block.getBlockState().getValidStates()) {
				LinkedHashMap<IProperty<?>, Comparable<?>> linkedhashmap = Maps.newLinkedHashMap(state.getProperties());
				linkedhashmap.remove(BlockDoor.POWERED);
				mapStateModelLocations.put(state, new ModelResourceLocation(block.getRegistryName(), getPropertyString(linkedhashmap)));
			}
			return mapStateModelLocations;
		}

	}

}
