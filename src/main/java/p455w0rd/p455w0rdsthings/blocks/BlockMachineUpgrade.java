package p455w0rd.p455w0rdsthings.blocks;

import java.util.List;

import javax.annotation.Nullable;

import codechicken.lib.model.ModelRegistryHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IMachineUpgradeBlock;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineUpgrade;
import p455w0rd.p455w0rdsthings.client.render.TESRMachineUpgrade;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.util.MachineUtils.InWorldUpgrades;

/**
 * @author p455w0rd
 *
 */
public class BlockMachineUpgrade extends BlockMachineBase implements IMachineUpgradeBlock {

	public static final String NAME = "machineupgrade";
	private ResourceLocation sprite;

	public BlockMachineUpgrade(String name, @Nullable ResourceLocation sprite) {
		super(Material.ROCK, MapColor.BLACK, NAME + "_" + name, 10.0F, 600000.0F);
		this.sprite = sprite;
		GameRegistry.registerTileEntity(TileMachineUpgrade.class, ModGlobals.MODID + ":" + NAME);
		setLightOpacity(255);
		useNeighborBrightness = true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void initModel() {
		ModelRegistryHelper.registerItemRenderer(Item.getItemFromBlock(this), new TESRMachineUpgrade());
		ModelRegistryHelper.setParticleTexture(this, sprite == null ? TESRMachineUpgrade.MACHINEUPGRADE_SPRITE : sprite);
		ClientRegistry.bindTileEntitySpecialRenderer(TileMachineUpgrade.class, new TESRMachineUpgrade());
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return new AxisAlignedBB(0.3D, 0.0D, 0.3D, 0.7D, 1.0D, 0.7D);
	}

	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn) {
		//collidingBoxes.clear();
		//collidingBoxes.add(new AxisAlignedBB(0.35D, 0.0D, 0.35D, 0.65D, 1.0D, 0.65D));
		super.addCollisionBoxToList(pos, entityBox, collidingBoxes, Block.FULL_BLOCK_AABB);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileMachineUpgrade();
	}

	private TileMachineUpgrade getTE(World worldIn, BlockPos pos) {
		return (TileMachineUpgrade) worldIn.getTileEntity(pos);
	}

	@Override
	public InWorldUpgrades getType() {
		return null;
	}

	public static class Speed extends BlockMachineUpgrade {

		public static final String NAME = "speed";

		public Speed() {
			super(NAME, null);
		}

		@Override
		public InWorldUpgrades getType() {
			return InWorldUpgrades.SPEED;
		}

	}

}
