package p455w0rd.p455w0rdsthings.blocks;

import java.util.Random;

import codechicken.lib.model.ModelRegistryHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ChunkCache;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.client.render.CompressorItemRenderer;
import p455w0rd.p455w0rdsthings.client.render.TESRCompressor;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler.GUIType;

/**
 * @author p455w0rd
 *
 */
public class BlockCompressor extends BlockMachineBase {

	public static final PropertyBool COOKING = PropertyBool.create("active");

	private static String NAME = "compressor";
	public boolean isCooking = false;

	public BlockCompressor() {
		super(Material.IRON, MapColor.BLACK, NAME, 10.0F, 6000000.0F);
		GameRegistry.registerTileEntity(TileCompressor.class, ModGlobals.MODID + ":" + NAME);
		setDefaultState(blockState.getBaseState().withProperty(COOKING, Boolean.valueOf(false)));
	}

	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess blockAccessor, BlockPos pos) {
		TileEntity te;
		if (blockAccessor instanceof ChunkCache) {
			te = ((ChunkCache) blockAccessor).getTileEntity(pos, Chunk.EnumCreateEntityType.CHECK);

		}
		else {
			te = blockAccessor.getTileEntity(pos);
		}
		if (!(te instanceof TileCompressor)) {
			return getDefaultState();
		}
		boolean isActive = te == null ? Boolean.valueOf(false) : ((TileCompressor) te).isCompressing();
		return state.withProperty(COOKING, isActive);
	}

	@Override
	public IBlockState getStateFromMeta(int meta) {
		return getDefaultState().withProperty(COOKING, meta == 0 ? Boolean.valueOf(false) : Boolean.valueOf(true));
	}

	@Override
	public int getMetaFromState(IBlockState state) {
		return state.getValue(COOKING) ? 1 : 0;
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, new IProperty[] {
				COOKING
		});
	}

	@Override
	public void neighborChanged(IBlockState state, World world, BlockPos pos, Block neighborBlock) {
		updatePowered(world, pos, state);
	}

	@Override
	public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
		updatePowered(world, pos, state);
	}

	private void updatePowered(World world, BlockPos pos, IBlockState state) {
		if (getTE(world, pos) == null || !(getTE(world, pos) instanceof TileCompressor)) {
			return;
		}
		TileCompressor te = getTE(world, pos);
		boolean running = te.isCompressing();
		if (!running && te.maintainLight()) {
			//running = true;
		}
		if (running != world.getBlockState(pos).getValue(COOKING)) {
			world.setBlockState(pos, state.withProperty(COOKING, Boolean.valueOf(running)), 2);
		}
	}

	@Override
	public int getLightValue(IBlockState state) {
		return state.getValue(COOKING) ? 9 : 0;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void initModel() {
		ModelRegistryHelper.registerItemRenderer(Item.getItemFromBlock(this), CompressorItemRenderer.getInstance());
		ClientRegistry.bindTileEntitySpecialRenderer(TileCompressor.class, TESRCompressor.getInstance());
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileCompressor();
	}

	private TileCompressor getTE(World worldIn, BlockPos pos) {
		if (worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileCompressor) {
			return (TileCompressor) worldIn.getTileEntity(pos);
		}
		return null;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (!worldIn.isRemote) {
			TileCompressor te = getTE(worldIn, pos);
			if (te != null && !playerIn.isSneaking()) {
				ModGuiHandler.launchGui(GUIType.COMPRESSOR, playerIn, worldIn, pos.getX(), pos.getY(), pos.getZ());
			}
		}
		return true;
	}

}
