package p455w0rd.p455w0rdsthings.blocks;

import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModGlobals.HarvestLevel;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModMaterials;

public class BlockCarbonBrick extends BlockBase {

	public BlockCarbonBrick() {
		super(ModMaterials.CARBON, "carbon_bricks_block", 10.0F, 6000000.0F);
		setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		if (Mods.TINKERS.isLoaded()) {
			setHarvestLevel("pickaxe", HarvestLevel.CARBON);
		}
		else {
			setHarvestLevel("pickaxe", HarvestLevel.DIAMOND);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add(ChatFormatting.LIGHT_PURPLE + "Wither Proof");
	}

	@Override
	public boolean canEntityDestroy(IBlockState state, IBlockAccess world, BlockPos pos, Entity entity) {
		if (entity instanceof net.minecraft.entity.boss.EntityDragon) {
			return false;
		}
		else if ((entity instanceof net.minecraft.entity.boss.EntityWither) || (entity instanceof net.minecraft.entity.projectile.EntityWitherSkull)) {
			return false;
		}
		return true;
	}
}
