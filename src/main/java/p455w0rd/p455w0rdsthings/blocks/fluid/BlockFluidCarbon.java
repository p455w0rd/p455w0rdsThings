package p455w0rd.p455w0rdsthings.blocks.fluid;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModFluids;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModMaterials;

/**
 * @author p455w0rd
 *
 */
public class BlockFluidCarbon extends BlockFluidClassic implements IFluidBlock, IModelHolder {

	protected FluidStack stack;
	protected int quantaPerBlock = 8;
	protected float quantaPerBlockFloat = 8F;

	public BlockFluidCarbon(Fluid fluid, Material material) {
		super(fluid, material);
		stack = new FluidStack(fluid, 1000);
		setUnlocalizedName(getFluid().getName());
		setRegistryName(getFluid().getName());
		GameRegistry.register(this);
		GameRegistry.register(new ItemBlock(this), getRegistryName());
		fluid.setBlock(this);
		fluid.setTemperature(500);
		FluidRegistry.enableUniversalBucket();
		if (!FluidRegistry.getBucketFluids().contains(fluid)) {
			FluidRegistry.addBucketForFluid(fluid);
		}
	}

	public FluidStack getStack() {
		return stack;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void initModel() {
		Block block = ModBlocks.SUBLIMATED_CARBON;
		Item item = Item.getItemFromBlock(block);
		ModelBakery.registerItemVariants(item);
		ModelResourceLocation modelResourceLocation = new ModelResourceLocation(ModGlobals.MODID + ":fluid", ((BlockFluidCarbon) block).getFluid().getName());
		ModelLoader.setCustomMeshDefinition(item, stack -> modelResourceLocation);
		ModelLoader.setCustomStateMapper(block, new StateMapperBase() {
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState p_178132_1_) {
				return modelResourceLocation;
			}
		});
	}

	@Override
	public Fluid getFluid() {
		return ModFluids.SUBLIMATED_CARBON;
	}

	@Override
	public int tickRate(World worldIn) {
		return 5;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand) {
		double d0 = pos.getX();
		double d1 = pos.getY();
		double d2 = pos.getZ();

		int i = state.getValue(LEVEL).intValue();

		if (i > 0 && i < 8) {
			if (rand.nextInt(64) == 0) {
				world.playSound(d0 + 0.5D, d1 + 0.5D, d2 + 0.5D, SoundEvents.BLOCK_WATER_AMBIENT, SoundCategory.BLOCKS, rand.nextFloat() * 0.25F + 0.75F, rand.nextFloat() + 0.5F, false);
			}
		}
		else if (rand.nextInt(10) == 0) {
			world.spawnParticle(EnumParticleTypes.SUSPENDED, d0 + rand.nextFloat(), d1 + rand.nextFloat(), d2 + rand.nextFloat(), 0.0D, 0.0D, 0.0D, new int[0]);
		}

		if (rand.nextInt(10) == 0 && world.getBlockState(pos.down()).isSideSolid(world, pos.down(), EnumFacing.UP)) {
			Material material = world.getBlockState(pos.down(2)).getMaterial();

			if (!material.blocksMovement() && !material.isLiquid()) {
				double d3 = d0 + rand.nextFloat();
				double d5 = d1;
				double d7 = d2 + rand.nextFloat();

				if (blockMaterial == ModMaterials.SUBLIMATED_CARBON) {
					world.spawnParticle(EnumParticleTypes.HEART, d3, d5, d7, 0.0D, 0.0D, 0.0D, new int[0]);
				}
			}
		}
	}

	@Override
	public FluidStack drain(World world, BlockPos pos, boolean doDrain) {
		if (!isSourceBlock(world, pos)) {
			return null;
		}

		if (doDrain) {
			world.setBlockToAir(pos);
		}

		return stack.copy();
	}

	@Override
	public boolean isSourceBlock(IBlockAccess world, BlockPos pos) {
		IBlockState state = world.getBlockState(pos);
		return state.getBlock() == this && state.getValue(LEVEL) == 0;
	}

	@Override
	public boolean canDrain(World world, BlockPos pos) {
		return isSourceBlock(world, pos);
	}

	@Override
	public float getFilledPercentage(World world, BlockPos pos) {
		int quantaRemaining = getQuantaValue(world, pos) + 1;
		float remaining = quantaRemaining / quantaPerBlockFloat;
		if (remaining > 1) {
			remaining = 1.0f;
		}
		return remaining * (stack.getFluid().getDensity() > 0 ? 1 : -1);
	}

	@Override
	public int getQuantaValue(IBlockAccess world, BlockPos pos) {
		IBlockState state = world.getBlockState(pos);
		if (state.getBlock() == Blocks.AIR) {
			return 0;
		}

		if (state.getBlock() != this) {
			return -1;
		}

		int quantaRemaining = quantaPerBlock - state.getValue(LEVEL);
		return quantaRemaining;
	}
}