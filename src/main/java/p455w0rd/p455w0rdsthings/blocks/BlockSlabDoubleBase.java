package p455w0rd.p455w0rdsthings.blocks;

import javax.annotation.Nullable;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
public class BlockSlabDoubleBase extends BlockBase {

	public BlockSlabDoubleBase(Material materialIn, String name, float hardness, float resistance, BlockSlabBase slab) {
		super(materialIn, name, hardness, resistance);
		ModRegistries.registerSlab(slab, this);
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (heldItem != null && heldItem.getItem() == Item.getItemFromBlock(getSlab())) {
			if (world.getBlockState(pos.offset(side)).getBlock() == getSlab()) {
				world.setBlockState(pos.offset(side), getDefaultState(), 3);
				if (!player.isCreative()) {
					if (heldItem.stackSize - 1 == 0) {
						player.setHeldItem(EnumHand.MAIN_HAND, null);
					}
					else {
						heldItem.stackSize -= 1;
					}
				}
				return true;
			}
			return false;
		}
		return false;
	}

	public BlockSlabBase getSlab() {
		return ModRegistries.getSlab(this);
	}

}
