package p455w0rd.p455w0rdsthings.blocks;

import java.util.Collection;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyHelper;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IModelHolder;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
public abstract class BlockSlabBase extends BlockSlab implements IModelHolder {

	private static final PropertyDummy DUMMY = new PropertyDummy("dummy");

	public BlockSlabBase(Material material, String name, float hardness, float resistance) {
		super(material);
		setUnlocalizedName(name);
		setRegistryName(name);
		setResistance(resistance);
		setHardness(hardness);
		GameRegistry.register(this);
		GameRegistry.register(new ItemBlock(this), getRegistryName());
		setLightOpacity(255);
		useNeighborBrightness = true;
		setDefaultState(blockState.getBaseState().withProperty(DUMMY, PropertyDummy.Dummy.getInstance()).withProperty(HALF, EnumBlockHalf.BOTTOM));
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (heldItem != null && heldItem.getItem() == Item.getItemFromBlock(this)) {
			if ((state.getValue(HALF) == EnumBlockHalf.BOTTOM && side == EnumFacing.UP) || (state.getValue(HALF) == EnumBlockHalf.TOP && side == EnumFacing.DOWN)) {
				world.setBlockState(pos, getDoubleSlab().getDefaultState(), 3);
				if (!player.isCreative()) {
					if (heldItem.stackSize - 1 == 0) {
						player.setHeldItem(EnumHand.MAIN_HAND, null);
					}
					else {
						heldItem.stackSize -= 1;
					}
				}
				return true;
			}
		}
		return false;
	}

	public BlockSlabDoubleBase getDoubleSlab() {
		return ModRegistries.getDoubleSlab(this);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	public Comparable<?> getTypeForItem(ItemStack stack) {
		return PropertyDummy.Dummy.getInstance();
	}

	@Override
	public boolean isDouble() {
		return false;
	}

	@Override
	public String getUnlocalizedName(int meta) {
		return getUnlocalizedName();
	}

	@Override
	public IProperty<?> getVariantProperty() {
		return DUMMY;
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, DUMMY, HALF);
	}

	@Override
	public int getMetaFromState(IBlockState state) {
		return state.getValue(HALF).ordinal();
	}

	@Override
	@Deprecated
	public IBlockState getStateFromMeta(int meta) {
		return getDefaultState().withProperty(HALF, EnumBlockHalf.values()[meta]);
	}

	public static class PropertyDummy extends PropertyHelper<PropertyDummy.Dummy> {

		public PropertyDummy(String name) {
			super(name, Dummy.class);
		}

		@Override
		public Collection<Dummy> getAllowedValues() {
			return ImmutableList.of(Dummy.INSTANCE);
		}

		@Override
		public Optional<Dummy> parseValue(String value) {
			return Optional.of(Dummy.INSTANCE);
		}

		@Override
		public String getName(Dummy value) {
			return "dummy";
		}

		public static class Dummy implements Comparable<Dummy> {
			private static final Dummy INSTANCE = new Dummy();

			private Dummy() {
			}

			@Override
			public int compareTo(Dummy o) {
				return 0;
			}

			public static Dummy getInstance() {
				return INSTANCE;
			}
		}

	}

}