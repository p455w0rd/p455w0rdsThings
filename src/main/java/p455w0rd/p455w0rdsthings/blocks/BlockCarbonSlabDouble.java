package p455w0rd.p455w0rdsthings.blocks;

import java.util.List;
import java.util.Random;

import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.IFuelHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModGlobals.HarvestLevel;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModMaterials;

/**
 * @author p455w0rd
 *
 */
public class BlockCarbonSlabDouble extends BlockSlabDoubleBase implements IFuelHandler {

	public static final String NAME = "carbon_slab_double";

	public BlockCarbonSlabDouble() {
		super(ModMaterials.CARBON, NAME, 10.0F, 6000000.0F, ModBlocks.CARBON_SLAB);
		if (Mods.TINKERS.isLoaded()) {
			setHarvestLevel("pickaxe", HarvestLevel.CARBON);
		}
		else {
			setHarvestLevel("pickaxe", HarvestLevel.DIAMOND);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add(ChatFormatting.LIGHT_PURPLE + "Wither Proof");
	}

	@Override
	public boolean canEntityDestroy(IBlockState state, IBlockAccess world, BlockPos pos, Entity entity) {
		if (entity instanceof net.minecraft.entity.boss.EntityDragon) {
			return false;
		}
		else if ((entity instanceof net.minecraft.entity.boss.EntityWither) || (entity instanceof net.minecraft.entity.projectile.EntityWitherSkull)) {
			return false;
		}
		return true;
	}

	@Override
	public int quantityDropped(Random random) {
		return 2;
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return Item.getItemFromBlock(getSlab());
	}

	@Override
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
		return new ItemStack(getSlab());
	}

	@Override
	public int getBurnTime(ItemStack fuel) {
		if (fuel.getItem() == ItemBlock.getItemFromBlock(this)) {
			return 16000;
		}
		return 0;
	}

}
