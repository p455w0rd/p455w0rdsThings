package p455w0rd.p455w0rdsthings.blocks;

import codechicken.lib.model.ModelRegistryHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger;
import p455w0rd.p455w0rdsthings.client.render.TESRInterchanger;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler.GUIType;
import p455w0rdslib.util.EasyMappings;

/**
 * @author p455w0rd
 *
 */
public class BlockInterchanger extends BlockMachineBase {

	private static final String NAME = "interchanger";

	public BlockInterchanger() {
		super(Material.IRON, MapColor.BLACK, NAME, 10.0F, 6000000.0F);
		GameRegistry.registerTileEntity(TileInterchanger.class, ModGlobals.MODID + ":tile_interchanger");
	}

	public TileInterchanger getTE(World world, BlockPos pos) {
		return (TileInterchanger) world.getTileEntity(pos);
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return new AxisAlignedBB((0.0625D * 1.5), 0.0D, (0.0625D * 1.5), (0.0625D * 14.5), (0.0625D * 13.75), (0.0625D * 14.5));
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileInterchanger();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelRegistryHelper.registerItemRenderer(Item.getItemFromBlock(this), new TESRInterchanger());
		ModelRegistryHelper.setParticleTexture(this, TESRInterchanger.INTERCHANGER_SPRITE);
		ClientRegistry.bindTileEntitySpecialRenderer(TileInterchanger.class, new TESRInterchanger());
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (hand == EnumHand.MAIN_HAND) {
			if (playerIn.isSneaking()) {
				if (!worldIn.isRemote) {
					getTE(worldIn, pos).cycleEnergyMode();
				}
				else {
					int mode = getTE(worldIn, pos).getEnergyMode().ordinal() + 1;
					if (mode >= TileInterchanger.EnergyMode.values().length) {
						mode = 0;
					}
					EasyMappings.message(playerIn, new TextComponentString("Mode: " + TileInterchanger.EnergyMode.values()[mode].name()));
				}
			}
			else {
				if (getTE(worldIn, pos) != null) {
					ModGuiHandler.launchGui(GUIType.INTERCHANGER, playerIn, worldIn, pos.getX(), pos.getY(), pos.getZ());
				}
			}
		}
		return true;
	}

	@Override
	public boolean canConnectRedstone(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side) {
		return true;
	}

	@Override
	public void neighborChanged(IBlockState state, World world, BlockPos pos, Block blockIn) {
		if (world != null && !world.isRemote && getTE(world, pos) != null) {
			boolean cachedRSSignal = getTE(world, pos).hasRSSignal();
			if (cachedRSSignal != world.isBlockPowered(pos)) {
				getTE(world, pos).setRSSignal(world.isBlockPowered(pos));
				getTE(world, pos).markDirty();
			}
		}
	}

}
