/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.blocks;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileTrashCan;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler;
import p455w0rd.p455w0rdsthings.init.ModGuiHandler.GUIType;

/**
 * @author p455w0rd
 *
 */
public class BlockTrashCan extends BlockBase implements ITileEntityProvider {

	/**
	 * @param materialIn
	 * @param name
	 * @param hardness
	 * @param resistance
	 */
	public BlockTrashCan() {
		super(Material.ROCK, "trash_can", 5.0F, 20.0F);
		GameRegistry.registerTileEntity(TileTrashCan.class, ModGlobals.MODID + ":tile_trash_can");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * net.minecraft.block.ITileEntityProvider#createNewTileEntity(net.minecraft
	 * .world.World, int)
	 */
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileTrashCan();
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
		ModGuiHandler.launchGui(GUIType.TRASH, player, worldIn, pos.getX(), pos.getY(), pos.getZ());
		return true;
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip, boolean advanced) {
		tooltip.add(TextFormatting.ITALIC + "Voids Items, Fluids, RF, and Forge Energy");
	}
}
