package p455w0rd.p455w0rdsthings.blocks.tileentities;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.wrapper.InvWrapper;
import p455w0rd.p455w0rdsthings.api.IMachineInWorldUpgradable;
import p455w0rd.p455w0rdsthings.api.IMachineUpgradeBlock;
import p455w0rd.p455w0rdsthings.recipe.RecipeCompressor;
import p455w0rd.p455w0rdsthings.util.MachineUtils;
import p455w0rd.p455w0rdsthings.util.MachineUtils.InWorldUpgrades;
import p455w0rdslib.util.ItemUtils;
import p455w0rdslib.util.MathUtils;

/**
 * @author p455w0rd
 *
 */
public class TileCompressor extends TileMachineBase implements IMachineInWorldUpgradable {

	private IItemHandlerModifiable INVENTORY;
	private ISidedInventory INVENTORY_NONWRAPPED;

	private int ENERGY_PER_TICK = 50;
	private boolean IS_COOKING = false;
	public int ENERGY_USED;
	//public ItemStack CURRENT_STACK = null;
	public RecipeCompressor currentRecipe = null;
	private boolean maintainLight = false;

	private final String TAG_SLOT = "Slot";
	private final String TAG_NAME = "Compressor";
	private final String TAG_INVENTORY = "Inventory";
	private final String TAG_ENERGY_USED = "EnergyUsed";
	private final String TAG_ISCOOKING = "IsCooking";
	private final String TAG_ENERGY_PER_TICK = "EnergyPerTick";
	private final String TAG_CURRENT_STACK = "CurrentStack";

	public TileCompressor() {
		super();
		INVENTORY_NONWRAPPED = new CompressorInventory(this);
		INVENTORY = new InvWrapper(INVENTORY_NONWRAPPED);
	}

	@Override
	public List<MachineUtils.InWorldUpgrades> getApplicableUpgrades() {
		return Lists.newArrayList(MachineUtils.InWorldUpgrades.SPEED);
	}

	@Override
	public boolean isUpgradeActive() {
		return isCompressing();
	}

	public IItemHandlerModifiable getInventory() {
		return INVENTORY;
	}

	public ISidedInventory getInventoryNonWrapped() {
		return INVENTORY_NONWRAPPED;
	}

	@Override
	public int extractEnergy(EnumFacing from, int output, boolean simulate) {
		return 0;
	}

	public int getSpeed() {
		return ENERGY_PER_TICK + (50 * (5 * getUpgradeCount(InWorldUpgrades.SPEED)));
	}

	@Override
	public int getUpgradeCount(InWorldUpgrades upgradeType) {
		return getUpgradeTiles(upgradeType).size();
	}

	@Override
	public List<TileEntity> getUpgradeTiles(InWorldUpgrades upgradeType) {
		List<TileEntity> tileList = Lists.newArrayList();
		for (EnumFacing facing : EnumFacing.HORIZONTALS) {
			Block block = getWorld().getBlockState(getPos().offset(facing, 2).offset(facing.rotateY(), 2)).getBlock();
			if (block != null && block instanceof IMachineUpgradeBlock) {
				IMachineUpgradeBlock upgradeBlock = (IMachineUpgradeBlock) block;
				TileEntity currentTile = getWorld().getTileEntity(getPos().offset(facing, 2).offset(facing.rotateY(), 2));
				if (upgradeBlock.getType() == upgradeType && currentTile != null && currentTile instanceof TileMachineBase) {
					TileMachineBase machineTile = (TileMachineBase) currentTile;
					if (getWorld().getBlockState(getPos().offset(facing).offset(facing.rotateY())).getBlock() instanceof BlockAir) {
						if (machineTile.getEnergy() > 1000) {
							tileList.add(currentTile);
						}
					}
				}
			}
		}
		return tileList;
	}

	/**
	 * MAX = 20000
	 */
	private void setSpeed(int RFPerTick) {
		int newAmount = RFPerTick;
		if (newAmount <= 0) {
			newAmount = 1;
		}
		if (newAmount > 20000) {
			newAmount = 20000;
		}
		ENERGY_PER_TICK = newAmount;
	}

	public void addSpeed(int additionalRF) {
		setSpeed(getSpeed() + additionalRF);
	}

	public void subtractSpeed(int retractedRF) {
		setSpeed(getSpeed() - retractedRF);
	}

	public boolean maintainLight() {
		return maintainLight;
	}

	public void setLighting(boolean isLit) {
		maintainLight = isLit;
	}

	public void drainEnergy(int amount) {
		ENERGY -= amount;
		ENERGY_USED += amount;
		markDirty();
	}

	public ItemStack getInputStack() {
		return INVENTORY.getStackInSlot(0);
	}

	public void setInputStack(ItemStack stack) {
		INVENTORY.setStackInSlot(0, stack);
	}

	public void decrInputStack(int amount) {
		if (getInputStack() != null && amount > 0) {
			int stackSize = getInputStack().stackSize;
			int newStackSize = stackSize - amount;
			if (newStackSize <= 0) {
				INVENTORY.setStackInSlot(0, null);
				return;
			}
			ItemStack newStack = getInputStack().copy();
			newStack.stackSize -= amount;
			setInputStack(newStack);
			markDirty();
		}
	}

	public ItemStack getOutputStack() {
		return INVENTORY.getStackInSlot(1);
	}

	public void setOutputStack(ItemStack stack) {
		INVENTORY.setStackInSlot(1, stack);
	}

	public void appendOutputStack(int amount) {
		if (getOutputStack() != null && amount > 0) {
			ItemStack newStack = getOutputStack().copy();
			newStack.stackSize += amount;
			setOutputStack(newStack);
		}
	}

	public ItemStack getSecondaryStack() {
		return INVENTORY.getStackInSlot(2);
	}

	public void setSecondaryStack(ItemStack stack) {
		INVENTORY.setStackInSlot(2, stack);
	}

	public void appendSecondaryStack(int amount) {
		if (getSecondaryStack() != null && amount > 0) {
			ItemStack newStack = getSecondaryStack().copy();
			newStack.stackSize += amount;
			setSecondaryStack(newStack);
		}
	}

	public int getEnergyUsed() {
		return ENERGY_USED;
	}

	public boolean isCompressing() {
		return IS_COOKING;
	}

	public void setCompressing(boolean isCompressing) {
		IS_COOKING = isCompressing;
		markDirty();
	}

	public RecipeCompressor getCurrentRecipe() {
		return getInputStack() != null ? (RecipeCompressor.isValidInput(getInputStack()) ? RecipeCompressor.getRecipeFromInput(getInputStack()) : null) : null;
	}

	public int getPercentComplete() {
		if (isCompressing()) {
			return MathUtils.ceil(MathUtils.getPercent(ENERGY_USED, getCurrentRecipe().getEnergyRequired()));
		}
		return 0;
	}

	@Override
	public void update() {
		//if (!getWorld().isRemote) {
		if (!isCompressing()) {
			if (canCompress()) {
				setLighting(true);
				setCompressing(true);
				//setCurrentStack(getInputStack());
				drainEnergy(getSpeed());
			}
			//return;
		}
		else {
			if (getCurrentRecipe() == null) {
				setLighting(false);
				setCompressing(false);
				ENERGY_USED = 0;
				super.update();
				return;
			}
			if (ENERGY_USED >= getCurrentRecipe().getEnergyRequired()) {
				if (getInputStack() == null || (getInputStack().stackSize < getCurrentRecipe().getInputCount() || getEnergy() < getCurrentRecipe().getEnergyRequired())) {
					setLighting(false);
				}
				compressItem();
			}
			else {
				drainEnergy(getSpeed());
			}
			for (InWorldUpgrades upgrade : InWorldUpgrades.values()) {
				for (TileEntity tile : getUpgradeTiles(upgrade)) {
					if (tile instanceof TileMachineBase) {
						TileMachineBase machineTile = (TileMachineBase) tile;
						if (machineTile.getEnergy() > 1000) {
							machineTile.extractEnergy(null, 1000, false);
							machineTile.markDirty();
						}
					}
				}
			}
		}
		super.update();
		//}

	}

	public boolean canCompress() {
		if (getInputStack() == null) {
			return false;
		}
		if (!RecipeCompressor.isValidInput(getInputStack())) {
			return false; //failsafe..this shouldn't be possible
		}
		int inputCount = getCurrentRecipe().getInputCount();
		int inputSlotCount = getInputStack().stackSize;
		if (inputSlotCount < inputCount) {
			return false;
		}
		if (getOutputStack() != null) { //output slot contains items
			if (!getOutputStack().isItemEqual(getCurrentRecipe().getOutput())) { //output of input item isn't same as items already in output slot
				return false;
			}
			int outputCount = getCurrentRecipe().getOutputCount();
			int maxOutputCount = getCurrentRecipe().getOutput().getMaxStackSize();
			int outputSlotCount = getOutputStack().stackSize;
			if (outputSlotCount + outputCount > maxOutputCount) {
				return false;
			}
		}
		if (getSecondaryStack() != null) {
			if (getCurrentRecipe().hasSecondOutput()) {
				if (!getSecondaryStack().isItemEqual(getCurrentRecipe().getSecondOutput())) {
					return false;
				}
				int secondCount = getCurrentRecipe().getSecondOutputCount();
				int maxSecondCount = getCurrentRecipe().getSecondOutput().getMaxStackSize();
				int secondSlotCount = getSecondaryStack().stackSize;
				if (secondCount + secondSlotCount > maxSecondCount) {
					return false;
				}
			}
		}
		if (!hasEnergy() || getEnergy() < getCurrentRecipe().getEnergyRequired()) {
			return false;
		}
		return true;
	}

	public void compressItem() {
		if (getCurrentRecipe() != null) {
			ItemStack outputStack = getCurrentRecipe().getOutput();
			if (getOutputStack() == null) {
				setOutputStack(outputStack.copy());
			}
			else if (getOutputStack() != null && getOutputStack().getItem() == outputStack.getItem()) {
				appendOutputStack(getCurrentRecipe().getOutputCount());
			}
			if (getCurrentRecipe().hasSecondOutput()) {
				double chance = getCurrentRecipe().getSecondOutputChance();
				double r = getWorld().rand.nextDouble();
				if (r <= chance) {
					ItemStack secondaryStack = getCurrentRecipe().getSecondOutput();
					if (getSecondaryStack() == null) {
						setSecondaryStack(secondaryStack.copy());
					}
					else if (getSecondaryStack().getItem() == secondaryStack.getItem()) {
						appendSecondaryStack(getCurrentRecipe().getSecondOutputCount());
					}
				}
			}
			decrInputStack(getCurrentRecipe().getInputCount());
			setCompressing(false);
			//setCurrentStack(null);
			ENERGY_USED = 0;
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		NBTTagCompound nbt = compound.getCompoundTag(TAG_NAME);
		ENERGY_USED = nbt.getInteger(TAG_ENERGY_USED);
		IS_COOKING = nbt.getBoolean(TAG_ISCOOKING);
		ENERGY_PER_TICK = nbt.getInteger(TAG_ENERGY_PER_TICK);
		//CURRENT_STACK = ItemStack.loadItemStackFromNBT(nbt.getCompoundTag(TAG_CURRENT_STACK));
		NBTTagList tagList = nbt.getTagList(TAG_INVENTORY, 10);
		for (int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound slotNBT = tagList.getCompoundTagAt(i);
			if (slotNBT != null) {
				INVENTORY.setStackInSlot(slotNBT.getInteger(TAG_SLOT), ItemStack.loadItemStackFromNBT(slotNBT));
			}
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagList nbtList = new NBTTagList();
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setInteger(TAG_ENERGY_USED, ENERGY_USED);
		nbt.setBoolean(TAG_ISCOOKING, IS_COOKING);
		nbt.setInteger(TAG_ENERGY_PER_TICK, ENERGY_PER_TICK);
		//if (CURRENT_STACK != null) {
		//	nbt.setTag(TAG_CURRENT_STACK, CURRENT_STACK.writeToNBT(new NBTTagCompound()));
		//}
		for (int i = 0; i < INVENTORY.getSlots(); i++) {
			if (INVENTORY.getStackInSlot(i) != null) {
				NBTTagCompound slotNBT = new NBTTagCompound();
				slotNBT.setInteger(TAG_SLOT, i);
				INVENTORY.getStackInSlot(i).writeToNBT(slotNBT);
				nbtList.appendTag(slotNBT);
			}
		}
		nbt.setTag(TAG_INVENTORY, nbtList);
		compound.setTag(TAG_NAME, nbt);
		return super.writeToNBT(compound);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);

	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(INVENTORY);
		}
		return super.getCapability(capability, facing);
	}

	public static class CompressorInventory implements ISidedInventory {

		private ItemStack[] inventory = new ItemStack[3];
		TileCompressor te;

		public CompressorInventory(TileCompressor tileIn) {
			te = tileIn;
		}

		@Override
		public int getSizeInventory() {
			return inventory.length;
		}

		@Override
		public ItemStack getStackInSlot(int index) {
			return inventory[index];
		}

		@Override
		public ItemStack decrStackSize(int index, int count) {
			ItemStack stack = ItemStackHelper.getAndSplit(inventory, index, count);
			markDirty();
			return stack;
		}

		@Override
		public ItemStack removeStackFromSlot(int index) {
			ItemStack stack = ItemStackHelper.getAndRemove(inventory, index);
			markDirty();
			return stack;
		}

		@Override
		public void setInventorySlotContents(int index, ItemStack stack) {
			//boolean flag = stack != null && stack.isItemEqual(inventory[index]) && ItemStack.areItemStackTagsEqual(stack, inventory[index]);

			inventory[index] = stack;

			if (stack != null && stack.stackSize > getInventoryStackLimit()) {
				stack.stackSize = getInventoryStackLimit();
			}

			/*
			if (stack != null && stack.stackSize > getInventoryStackLimit()) {
				stack.stackSize = getInventoryStackLimit();
			}
			inventory[index] = stack;
			*/
			markDirty();
		}

		@Override
		public int getInventoryStackLimit() {
			return 64;
		}

		@Override
		public boolean isUsableByPlayer(EntityPlayer player) {
			return true;
		}

		@Override
		public void openInventory(EntityPlayer player) {
		}

		@Override
		public void closeInventory(EntityPlayer player) {
		}

		/*
				public boolean canAddToInput(ItemStack stack) {
					if (te.getCurrentRecipe() != null) { // there is an item in input slot
						if (te.isCompressing()) { // it is running
							if (te.getOutputStack() == null) { // output is empty
								if (te.getSecondaryStack() == null) { // no luck for secondary so far (secondary slot empty)

								}
							}
						}
						else { // there is an item in input slot, but not enough to run

						}
					}
					else { // there is an item in input slot, but not enough to run
						if (te.getOutputStack() == null) { // output is empty

						}
					}
				}
		*/
		@Override
		public boolean isItemValidForSlot(int index, ItemStack stack) {
			if (index == 0) {
				if (te.getInputStack() == null && te.getOutputStack() == null && te.getSecondaryStack() == null && te.getCurrentRecipe() == null) { //completely empty and not processing
					return RecipeCompressor.isValidInput(stack);
				}
				else if (te.getCurrentRecipe() != null) { //processing
					if (te.getInputStack() == null && te.getOutputStack() == null && te.getSecondaryStack() == null) { //all slots empty
						return RecipeCompressor.isValidInput(stack);
					}
					else if (te.getInputStack() != null && te.getOutputStack() == null && te.getSecondaryStack() == null) { //only input has items
						return RecipeCompressor.isValidInput(stack) && ItemUtils.compareStacks(stack, te.getInputStack());
					}
					else if (te.getInputStack() == null && te.getOutputStack() == null && te.getSecondaryStack() != null) { //only secondary has items
						if (RecipeCompressor.isValidInput(stack) && !te.getCurrentRecipe().hasSecondOutput()) { //if this input has no 2nd output, then let it process
							return true;
						}
						else {
							return RecipeCompressor.isValidInput(stack) && ItemUtils.compareStacks(te.getCurrentRecipe().getSecondOutput(), te.getSecondaryStack());
						}
					}
					else if (te.getInputStack() != null && te.getOutputStack() != null && te.getSecondaryStack() == null) {
						return ItemUtils.compareStacks(stack, te.getInputStack());
					}
					else if (te.getInputStack() != null && te.getOutputStack() != null && te.getSecondaryStack() != null) {
						return ItemUtils.compareStacks(stack, te.getInputStack());
					}
					else if (te.getInputStack() == null && te.getOutputStack() != null && te.getSecondaryStack() == null) { //only output has items
						return ItemUtils.compareStacks(stack, te.getCurrentRecipe().getInput());
					}
				}
				else { //not processing
					if (te.getInputStack() != null && te.getOutputStack() == null && te.getSecondaryStack() == null) { //only input has items
						return RecipeCompressor.isValidInput(stack) && ItemUtils.compareStacks(stack, te.getInputStack());
					}
					else if (te.getInputStack() == null && te.getOutputStack() != null && te.getSecondaryStack() == null) { //only output has items
						return ItemUtils.compareStacks(RecipeCompressor.getRecipeFromInput(stack).getOutput(), te.getOutputStack());
					}
					else if (te.getInputStack() == null && te.getOutputStack() == null && te.getSecondaryStack() != null) { //only secondary has items
						if (RecipeCompressor.isValidInput(stack) && !RecipeCompressor.getRecipeFromInput(stack).hasSecondOutput()) { //if this input has no 2nd output, then let it process
							return true;
						}
						else {
							return RecipeCompressor.isValidInput(stack) && ItemUtils.compareStacks(RecipeCompressor.getRecipeFromInput(stack).getSecondOutput(), te.getSecondaryStack());
						}
					}
					else if (te.getInputStack() != null && te.getOutputStack() == null && te.getSecondaryStack() != null) {
						return RecipeCompressor.getRecipeFromInput(te.getInputStack()).getOutput() == null || ItemUtils.compareStacks(RecipeCompressor.getRecipeFromInput(stack).getSecondOutput(), te.getSecondaryStack());
					}
					else if (te.getInputStack() == null && te.getOutputStack() != null && te.getSecondaryStack() != null) {
						return RecipeCompressor.isValidInput(stack) && ItemUtils.compareStacks(RecipeCompressor.getRecipeFromInput(stack).getOutput(), te.getOutputStack());
					}
					else if (te.getInputStack() != null && te.getOutputStack() != null && te.getSecondaryStack() == null) {
						return ItemUtils.compareStacks(stack, te.getInputStack());
					}
					else if (te.getInputStack() != null && te.getOutputStack() != null && te.getSecondaryStack() != null) {
						return ItemUtils.compareStacks(stack, te.getInputStack());
					}
				}
			}

			if (index == 1) {
				return false;//RecipeCompressor.isValidOutput(stack);
			}
			if (index == 2) {
				return false;//RecipeCompressor.isValidSecondaryOutput(stack);
			}
			return false;
		}

		@Override
		public int getField(int id) {
			return 0;
		}

		@Override
		public void setField(int id, int value) {
		}

		@Override
		public int getFieldCount() {
			return 0;
		}

		@Override
		public void clear() {
			inventory = new ItemStack[3];
			markDirty();
		}

		@Override
		public String getName() {
			return "compressor";
		}

		@Override
		public boolean hasCustomName() {
			return false;
		}

		@Override
		public ITextComponent getDisplayName() {
			return new TextComponentString(I18n.format(getName()));
		}

		@Override
		public int[] getSlotsForFace(EnumFacing side) {
			return new int[] {
					0,
					1,
					2
			};
		}

		@Override
		public boolean canInsertItem(int index, ItemStack stack, EnumFacing direction) {
			return index == 0 && RecipeCompressor.isValidInput(stack);
		}

		@Override
		public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
			return index > 0;
		}

		@Override
		public void markDirty() {
			if (te != null) {
				te.markDirty();
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public double getMaxRenderDistanceSquared() {
		return 65536.0D;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return INFINITE_EXTENT_AABB;
	}

}
