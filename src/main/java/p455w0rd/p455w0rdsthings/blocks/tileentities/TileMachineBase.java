package p455w0rd.p455w0rdsthings.blocks.tileentities;

import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import cofh.api.energy.IEnergyProvider;
import cofh.api.energy.IEnergyReceiver;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import p455w0rd.p455w0rdsthings.api.IAccessible;
import p455w0rd.p455w0rdsthings.api.IMachineUpgrade;
import p455w0rd.p455w0rdsthings.api.IRedstoneControllable;
import p455w0rd.p455w0rdsthings.util.MachineUtils.Upgrades;
import p455w0rdslib.util.EnergyUtils.EnergyWrapperRFtoFE;

/**
 * @author p455w0rd
 *
 */
/*
@InterfaceList({
		@Interface(iface = "crazypants.enderio.machine.IRedstoneModeControlable", modid = EnderIo.MODID, striprefs = true),
		@Interface(iface = "crazypants.enderio.api.redstone.IRedstoneConnectable", modid = EnderIo.MODID, striprefs = true)
})*/
public class TileMachineBase extends TileEntity implements IEnergyReceiver, IEnergyProvider, ITickable, IRedstoneControllable, IAccessible {

	protected int CAPACITY = 16000000;
	protected int ENERGY = 0;
	private final String TAG_ENERGY = "Energy";
	private final String TAG_CAPACITY = "Capacity";
	private final String TAG_NAME = "PWMachine";
	private final String TAG_UPGRADEINVENTORY = "UpgradeInventory";
	//private final String TAG_UPGRADESLOT = "UpgradeSlot";
	private final String TAG_REDSTONEMODE = "RedstoneMode";
	private final String TAG_ACCESSMODE = "AccessMode";
	private final String TAG_ACCESSLIST = "AccessList";
	private final String TAG_PLAYERID = "PlayerID";
	private final String TAG_OWNERID = "OwnerID";
	private final String TAG_HAS_REDSTONE_SIGNAL = "HasRSSignal";

	private RedstoneMode redstoneMode = RedstoneMode.REQUIRED;
	private AccessMode accessMode = AccessMode.PUBLIC;
	private List<UUID> accessList = Lists.newArrayList();
	//private IItemHandlerModifiable UPGRADEINVENTORY;
	//private IInventory UPGRADEINVENTORY_NONWRAPPED;

	private boolean hasRedstoneSignal = false;

	private UUID ownerID = null;

	public TileMachineBase() {
		//UPGRADEINVENTORY_NONWRAPPED = new UpgradeInventory(this);
		//UPGRADEINVENTORY = new InvWrapper(UPGRADEINVENTORY_NONWRAPPED);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityEnergy.ENERGY || super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		if (capability == CapabilityEnergy.ENERGY) {
			return CapabilityEnergy.ENERGY.cast(new EnergyWrapperRFtoFE(this, facing));
		}
		return super.getCapability(capability, facing);
	}

	public void init(EntityLivingBase owner) {
		setOwner(owner.getUniqueID());
	}

	/*
		public IItemHandlerModifiable getUpgradeInventory() {
			return UPGRADEINVENTORY;
		}
	
		public IInventory getUpgradeInventoryNonWrapped() {
			return UPGRADEINVENTORY_NONWRAPPED;
		}
	*/
	@Override
	public int getEnergyStored(EnumFacing paramEnumFacing) {
		return ENERGY;
	}

	public int getEnergy() {
		return getEnergyStored(null);
	}

	public void setEnergy(int amount) {
		int result = 0;
		if (amount > getCapacity()) {
			result = getCapacity();
		}
		if (amount > 0) {
			result = amount;
		}
		ENERGY = result;
	}

	@Override
	public int getMaxEnergyStored(EnumFacing paramEnumFacing) {
		return CAPACITY;
	}

	public int getCapacity() {
		return getMaxEnergyStored(null);
	}

	public void setCapacity(int amount) {
		int result = 0;
		if (amount > 0) {
			result = amount;
		}
		CAPACITY = result;
	}

	@Override
	public boolean canConnectEnergy(EnumFacing from) {
		return true;
	}

	public boolean canConnectEnergy() {
		return canConnectEnergy(null);
	}

	@Override
	public int receiveEnergy(EnumFacing from, int input, boolean simulate) {
		int energyReceived = Math.min(CAPACITY - ENERGY, Math.min(CAPACITY, input));
		if (!simulate && ENERGY < CAPACITY) {
			ENERGY += energyReceived;
		}
		markDirty();
		return energyReceived;
	}

	@Override
	public int extractEnergy(EnumFacing from, int output, boolean simulate) {
		int energyExtracted = Math.min(ENERGY, Math.min(CAPACITY, output));
		if (!simulate && ENERGY > 0) {
			ENERGY -= energyExtracted;
		}
		markDirty();
		return energyExtracted;
	}

	public boolean hasEnergy() {
		return ENERGY > 0;
	}

	@Override
	public UUID getOwner() {
		return ownerID;
	}

	@Override
	public void setOwner(UUID uuid) {
		ownerID = uuid;
		markDirty();
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		NBTTagCompound nbt = compound.getCompoundTag(TAG_NAME);
		CAPACITY = nbt.getInteger(TAG_CAPACITY);
		ENERGY = nbt.getInteger(TAG_ENERGY);
		setAccessMode(AccessMode.values()[nbt.getInteger(TAG_ACCESSMODE)]);
		NBTTagList accessList = nbt.getTagList(TAG_ACCESSLIST, 10);
		for (int i = 0; i < accessList.tagCount(); i++) {
			addPlayerToAccessList(UUID.fromString(accessList.getCompoundTagAt(i).getString(TAG_PLAYERID)));
		}
		if (nbt != null && nbt.hasKey(TAG_OWNERID) && nbt.getString(TAG_OWNERID) != null && !nbt.getString(TAG_OWNERID).isEmpty()) {
			setOwner(UUID.fromString(nbt.getString(TAG_OWNERID)));
		}
		setRedstoneMode(RedstoneMode.values()[nbt.getInteger(TAG_REDSTONEMODE)]);
		setRSSignal(nbt.getBoolean(TAG_HAS_REDSTONE_SIGNAL));
		/*
		NBTTagList tagList = nbt.getTagList(TAG_UPGRADEINVENTORY, 10);
		for (int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound slotNBT = tagList.getCompoundTagAt(i);
			if (slotNBT != null) {
				UPGRADEINVENTORY.setStackInSlot(slotNBT.getInteger(TAG_UPGRADESLOT), ItemStack.loadItemStackFromNBT(slotNBT));
			}
		}
		*/
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagList nbtList = new NBTTagList();
		NBTTagList accessList = new NBTTagList();
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setInteger(TAG_CAPACITY, CAPACITY);
		nbt.setInteger(TAG_ENERGY, ENERGY);
		nbt.setInteger(TAG_REDSTONEMODE, redstoneMode.ordinal());
		nbt.setBoolean(TAG_HAS_REDSTONE_SIGNAL, hasRSSignal());
		nbt.setString(TAG_OWNERID, getOwner() + "");
		compound.setTag(TAG_NAME, nbt);
		/*
		for (int i = 0; i < UPGRADEINVENTORY.getSlots(); i++) {
			if (UPGRADEINVENTORY.getStackInSlot(i) != null) {
				NBTTagCompound slotNBT = new NBTTagCompound();
				slotNBT.setInteger(TAG_UPGRADESLOT, i);
				UPGRADEINVENTORY.getStackInSlot(i).writeToNBT(slotNBT);
				nbtList.appendTag(slotNBT);
			}
		}
		*/
		for (UUID uuid : getAccessList()) {
			NBTTagCompound playerIDNBT = new NBTTagCompound();
			playerIDNBT.setString(TAG_PLAYERID, uuid + "");
			accessList.appendTag(playerIDNBT);
		}
		nbt.setTag(TAG_UPGRADEINVENTORY, nbtList);
		nbt.setInteger(TAG_ACCESSMODE, getAccessMode().ordinal());
		nbt.setTag(TAG_ACCESSLIST, accessList);
		return super.writeToNBT(compound);
	}

	@Override
	public void update() {
		markDirty();
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
		return oldState.getBlock() != newSate.getBlock();
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		return writeToNBT(new NBTTagCompound());
	}

	@Override
	@Nullable
	public SPacketUpdateTileEntity getUpdatePacket() {
		return new SPacketUpdateTileEntity(getPos(), 255, getUpdateTag());
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
		readFromNBT(pkt.getNbtCompound());
	}

	@Override
	public void markDirty() {
		if (getWorld() != null && !getWorld().isRemote) {
			IBlockState state = getWorld().getBlockState(pos);
			if (state != null) {
				state.getBlock().updateTick(getWorld(), getPos(), state, getWorld().rand);
				//getWorld().notifyBlockUpdate(pos, state, state, 11);

				WorldServer worldS = (WorldServer) getWorld();
				List<EntityPlayer> players = worldS.playerEntities;
				for (EntityPlayer player : players) {
					if (player instanceof EntityPlayerMP && player.getEntityWorld().provider.getDimension() == worldS.provider.getDimension()) {
						//((EntityPlayerMP) player).connection.sendPacket(new SPacketBlockChange(worldS, getPos()));
						((EntityPlayerMP) player).connection.sendPacket(getUpdatePacket());
					}
				}

				//getWorld().updateComparatorOutputLevel(getPos(), state.getBlock());
			}
		}
	}

	// Inventory for item upgrades (will probably be ditched completely in favor of in-world upgrades)
	public static class UpgradeInventory implements IInventory {

		private ItemStack[] inventory = new ItemStack[1];
		TileMachineBase te;

		public UpgradeInventory(TileMachineBase tileIn) {
			te = tileIn;
		}

		@Override
		public int getSizeInventory() {
			return inventory.length;
		}

		@Override
		public ItemStack getStackInSlot(int index) {
			return inventory[index];
		}

		@Override
		public ItemStack decrStackSize(int index, int count) {
			ItemStack stack = ItemStackHelper.getAndSplit(inventory, index, count);
			markDirty();
			return stack;
		}

		@Override
		public ItemStack removeStackFromSlot(int index) {
			ItemStack stack = ItemStackHelper.getAndRemove(inventory, index);
			markDirty();
			return stack;
		}

		@Override
		public void setInventorySlotContents(int index, ItemStack stack) {
			inventory[index] = stack;

			if (stack != null && stack.stackSize > getInventoryStackLimit()) {
				stack.stackSize = getInventoryStackLimit();
			}
			markDirty();
		}

		@Override
		public int getInventoryStackLimit() {
			return 64;
		}

		@Override
		public boolean isUsableByPlayer(EntityPlayer player) {
			return true;
		}

		@Override
		public void openInventory(EntityPlayer player) {
		}

		@Override
		public void closeInventory(EntityPlayer player) {
		}

		@Override
		public boolean isItemValidForSlot(int index, ItemStack stack) {
			return index == 0 && stack != null && stack.getItem() instanceof IMachineUpgrade && ((IMachineUpgrade) stack.getItem()).getUpgradeType() == Upgrades.SPEED;
		}

		@Override
		public int getField(int id) {
			return 0;
		}

		@Override
		public void setField(int id, int value) {
		}

		@Override
		public int getFieldCount() {
			return 0;
		}

		@Override
		public void clear() {
			inventory = new ItemStack[1];
			markDirty();
		}

		@Override
		public String getName() {
			return "machine";
		}

		@Override
		public boolean hasCustomName() {
			return false;
		}

		@Override
		public ITextComponent getDisplayName() {
			return new TextComponentString(I18n.format(getName()));
		}

		@Override
		public void markDirty() {
			if (te != null) {
				te.markDirty();
			}
		}
	}

	@Override
	public RedstoneMode getRedstoneMode() {
		return redstoneMode;
	}

	@Override
	public void setRedstoneMode(RedstoneMode mode) {
		redstoneMode = mode;
		markDirty();
	}

	@Override
	public boolean isRedstoneRequirementMet() {
		switch (getRedstoneMode()) {
		default:
		case IGNORED:
			return true;
		case REQUIRED:
			return hasRSSignal();
		case REQUIRE_NONE:
			return !hasRSSignal();
		}
	}

	@Override
	public boolean hasRSSignal() {
		return hasRedstoneSignal;
	}

	@Override
	public void setRSSignal(boolean isPowered) {
		hasRedstoneSignal = isPowered;
	}

	public void cycleRedstoneMode() {
		int redstoneModeIndex = redstoneMode.ordinal();
		redstoneModeIndex++;
		if (redstoneModeIndex >= RedstoneMode.values().length) {
			redstoneModeIndex = 0;
		}
		setRedstoneMode(RedstoneMode.values()[redstoneModeIndex]);
	}

	@Override
	public List<UUID> getAccessList() {
		return accessList;
	}

	@Override
	public void addPlayerToAccessList(UUID playerID) {
		for (UUID uuid : getAccessList()) {
			if (uuid.equals(playerID)) {
				return;
			}
		}
		accessList.add(playerID);
	}

	@Override
	public void removePlayerFromAccessList(UUID playerID) {
		for (UUID uuid : getAccessList()) {
			if (uuid.equals(playerID)) {
				accessList.remove(uuid);
				return;
			}
		}
	}

	@Override
	public AccessMode getAccessMode() {
		return accessMode;
	}

	@Override
	public void setAccessMode(AccessMode mode) {
		accessMode = mode;
		markDirty();
	}

	public enum RedstoneMode {
			REQUIRED, REQUIRE_NONE, IGNORED
	}

	public enum ModeType {
			RF, ITEM, FLUID
	}

	public enum AccessMode {
			PUBLIC, PRIVATE
	}

}
