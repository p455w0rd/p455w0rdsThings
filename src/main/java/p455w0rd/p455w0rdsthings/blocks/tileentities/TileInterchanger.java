package p455w0rd.p455w0rdsthings.blocks.tileentities;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import cofh.api.energy.IEnergyReceiver;
import mcjty.theoneprobe.Tools;
import mcjty.theoneprobe.api.IProbeHitData;
import mcjty.theoneprobe.api.IProbeInfo;
import mcjty.theoneprobe.api.ProbeMode;
import mcjty.theoneprobe.api.TextStyleClass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;
import p455w0rd.p455w0rdsthings.P455w0rdsThings;
import p455w0rd.p455w0rdsthings.api.IAccessible;
import p455w0rd.p455w0rdsthings.api.IInterchanger;
import p455w0rd.p455w0rdsthings.api.ITOPBlockDisplayOverride;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModConfig.ConfigOptions;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.init.ModRegistries;
import p455w0rd.p455w0rdsthings.network.PacketFluidFill;
import p455w0rdslib.api.IChunkLoadable;
import p455w0rdslib.util.FluidUtils;
import p455w0rdslib.util.InventoryUtils;
import p455w0rdslib.util.ItemUtils;
import p455w0rdslib.util.TileEntityUtils;

/**
 * @author p455w0rd
 *
 */
public class TileInterchanger extends TileMachineBase implements IChunkLoadable, IInterchanger, ISidedInventory, ITOPBlockDisplayOverride {

	public static final String TAG_NAME = "Interchanger";
	private final String TAG_ENERGYMODE = "EnergyMode";
	private final String TAG_ITEMMODE = "ItemMode";
	private final String TAG_FLUIDMODE = "FluidMode";
	public static final String TAG_CHANNELID = "ChannelID";
	private final String TAG_INTERCHANGER_ENERGYINDEX = "EnergyIndex";
	private final String TAG_INTERCHANGER_ITEMINDEX = "ItemIndex";
	private final String TAG_INTERCHANGER_FLUIDINDEX = "FluidIndex";

	private final ChannelInventory CHANNELINVENTORY = new ChannelInventory();

	private int energyIndex = -1;
	private int itemIndex = -1;
	private int fluidIndex = -1;

	private EnergyMode energyMode = EnergyMode.DISABLED;
	private ItemMode itemMode = ItemMode.DISABLED;
	private FluidMode fluidMode = FluidMode.DISABLED;

	private List<ItemStack> channelID = Lists.newArrayList();

	private ItemStack[] itemInventory = new ItemStack[1];

	private boolean sendingEnergy = false;
	private boolean sendingItems = false;
	private boolean sendingFluids = false;

	public TileInterchanger() {
		ModRegistries.registerInterchanger(this);
	}

	@Override
	public boolean overrideStandardInfo(ProbeMode mode, IProbeInfo probeInfo, EntityPlayer player, World world, IBlockState state, IProbeHitData data) {
		if (state.getBlock() == ModBlocks.INTERCHANGER) {
			ItemStack stack = new ItemStack(ModBlocks.INTERCHANGER);
			TileEntity tile = world.getTileEntity(data.getPos());
			if (tile != null && tile instanceof TileInterchanger) {
				TileInterchanger te = (TileInterchanger) tile;
				NBTTagCompound nbttagcompound = new NBTTagCompound();
				te.writeToNBT(nbttagcompound);
				stack.setTagInfo("BlockEntityTag", nbttagcompound);
				probeInfo.horizontal().item(stack).vertical().itemLabel(stack).text(TextStyleClass.MODNAME.toString() + Tools.getModName(state.getBlock()));
				return true;
			}
		}
		return false;
	}

	@Override
	public void init(EntityLivingBase owner) {
		if (world() != null && !world().isRemote) {
			if (getChannelID().size() <= 0) {
				setChannelID(getDefaultChannelID());
			}
		}
		super.init(owner);
	}

	public ChannelInventory getChannelInventory() {
		return CHANNELINVENTORY;
	}

	@Override
	public void update() {
		//markDirty();
	}

	public int getEnergyInterchangerIndex() {
		return energyIndex;
	}

	public void setEnergyInterchangerIndex(int index) {
		energyIndex = index;
	}

	public void cycleEnergyInterchangerIndex() {
		int currentIndex = getEnergyInterchangerIndex();
		currentIndex++;
		List<IInterchanger> list = getLinkedReceivingInterchangers(ModeType.RF);
		if (list.isEmpty()) {
			currentIndex = -1;
		}
		else {
			if (currentIndex >= list.size()) {
				currentIndex = 0;
			}
		}
		setEnergyInterchangerIndex(currentIndex);
	}

	public int getItemInterchangerIndex() {
		return itemIndex;
	}

	public void setItemInterchangerIndex(int index) {
		itemIndex = index;
	}

	public void cycleItemInterchangerIndex() {
		int currentIndex = getItemInterchangerIndex();
		currentIndex++;
		List<IInterchanger> list = getLinkedReceivingInterchangers(ModeType.ITEM);
		if (list.isEmpty()) {
			currentIndex = -1;
		}
		else {
			if (currentIndex >= list.size()) {
				currentIndex = 0;
			}
		}
		setItemInterchangerIndex(currentIndex);
	}

	public int getFluidInterchangerIndex() {
		return fluidIndex;
	}

	public void setFluidInterchangerIndex(int index) {
		fluidIndex = index;
	}

	public void cycleFluidInterchangerIndex() {
		int currentIndex = getFluidInterchangerIndex();
		currentIndex++;
		List<IInterchanger> list = getLinkedReceivingInterchangers(ModeType.FLUID);
		if (list.isEmpty()) {
			currentIndex = -1;
		}
		else {
			if (currentIndex >= list.size()) {
				currentIndex = 0;
			}
		}
		setFluidInterchangerIndex(currentIndex);
	}

	/*
		public List<IInterchanger> getLinkedSendingInterchangers(ModeType mode) {
			List<IInterchanger> tileList = Lists.newArrayList();
			for (IAccessible accessibleTile : (getAccessMode() == AccessMode.PUBLIC ? ModRegistries.getPublicSendingInterchangers(mode) : ModRegistries.getPrivateSendingInterchangers(mode))) {
				if (accessibleTile instanceof IInterchanger) {
					IInterchanger tile = (IInterchanger) accessibleTile;
					if (tile != this && areChannelsEqual(tile.getChannelID(), getChannelID())) {
						tileList.add(tile);
					}
				}
			}
			return tileList;
		}
	*/
	public List<IInterchanger> getLinkedReceivingInterchangers(ModeType mode) {
		List<IInterchanger> tileList = Lists.newArrayList();
		for (IInterchanger tile : (getAccessMode() == AccessMode.PUBLIC ? ModRegistries.getPublicReceivingInterchangers(mode) : ModRegistries.getPrivateReceivingInterchangers(mode, getOwner()))) {
			if (tile != this && areChannelsEqual(tile.getChannelID(), getChannelID())) {
				if (getAccessMode() == AccessMode.PRIVATE && !((IAccessible) tile).getOwner().equals(getOwner())) {
					continue;
				}
				tileList.add(tile);
			}
		}
		return tileList;
	}

	public boolean areChannelsEqual(List<ItemStack> ID1, List<ItemStack> ID2) {
		int matches = 0;
		for (int i = 0; i < ID1.size(); i++) {
			if (ItemUtils.areItemsEqual(ID1.get(i), ID2.get(i))) {
				++matches;
			}
		}
		return matches == 6;
	}

	public static List<ItemStack> getDefaultChannelID() {
		List<ItemStack> itemList = Lists.newArrayList();
		for (int i = 0; i < 6; i++) {
			itemList.add(new ItemStack(Blocks.WOOL));
		}
		return itemList;
	}

	@Override
	public void invalidate() {
		onChunkUnload();
		super.invalidate();
	}

	@Override
	public List<ItemStack> getChannelID() {
		return channelID;
	}

	@Override
	public void setChannelID(@Nonnull List<ItemStack> code) {
		List<ItemStack> newCode = Lists.newArrayList();
		if (code == null) {
			channelID = getDefaultChannelID();
			return;
		}
		for (int i = 0; i < 6; i++) {
			if (code.get(i) == null || !code.get(i).getItem().equals(Item.getItemFromBlock(Blocks.WOOL))) {
				ItemStack defaultStack = new ItemStack(Blocks.WOOL);
				newCode.add(defaultStack);
				CHANNELINVENTORY.setInventorySlotContents(i, defaultStack);
				continue;
			}
			newCode.add(code.get(i));
			CHANNELINVENTORY.setInventorySlotContents(i, code.get(i));
		}
		channelID = newCode;
		markDirty();
	}

	public void setChannelID(int index, @Nullable ItemStack stack) {
		if (index < 0 || index >= 6) {
			return;
		}
		ItemStack defaultStack = new ItemStack(Blocks.WOOL);
		if (stack == null || !stack.getItem().equals(Item.getItemFromBlock(Blocks.WOOL))) {
			channelID.set(index, defaultStack);
			CHANNELINVENTORY.setInventorySlotContents(index, defaultStack);
		}
		channelID.set(index, stack);
		CHANNELINVENTORY.setInventorySlotContents(index, stack);
		markDirty();
	}

	public void cycleChannelItem(int index) {
		cycleChannelItem(index, true);
	}

	public void cycleChannelItem(int index, boolean forward) {
		ItemStack stack = CHANNELINVENTORY.getStackInSlot(index);
		if (stack == null) {
			return;
		}
		int damage = stack.getItemDamage();
		if (forward) {
			damage++;
		}
		else {
			damage--;
		}
		if (damage > 15) {
			damage = 0;
		}
		if (damage < 0) {
			damage = 15;
		}
		stack.setItemDamage(damage);
	}

	@Override
	public void onChunkUnload() {
		ModRegistries.unregisterInterchanger(this);
	}

	@Override
	public BlockPos pos() {
		return getPos();
	}

	@Override
	public World world() {
		return getWorld();
	}

	@Override
	public int getDimension() {
		return world().provider.getDimension();
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		NBTTagCompound nbt = compound.getCompoundTag(TAG_NAME);
		setEnergyMode(EnergyMode.values()[nbt.getInteger(TAG_ENERGYMODE)]);
		setItemMode(ItemMode.values()[nbt.getInteger(TAG_ITEMMODE)]);
		setFluidMode(FluidMode.values()[nbt.getInteger(TAG_FLUIDMODE)]);
		List<ItemStack> channelIDItemListX = Lists.newArrayList();
		NBTTagList channelIDItemList = nbt.getTagList(TAG_CHANNELID, 10);
		for (int i = 0; i < channelIDItemList.tagCount(); i++) {
			channelIDItemListX.add(ItemStack.loadItemStackFromNBT(channelIDItemList.getCompoundTagAt(i)));
		}
		setChannelID(channelIDItemListX.isEmpty() ? null : channelIDItemListX);
		setEnergyInterchangerIndex(nbt.getInteger(TAG_INTERCHANGER_ENERGYINDEX));
		setItemInterchangerIndex(nbt.getInteger(TAG_INTERCHANGER_ITEMINDEX));
		setFluidInterchangerIndex(nbt.getInteger(TAG_INTERCHANGER_FLUIDINDEX));
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagCompound nbt = new NBTTagCompound();
		NBTTagList channelIDItemList = new NBTTagList();
		nbt.setInteger(TAG_ENERGYMODE, getEnergyMode().ordinal());
		nbt.setInteger(TAG_ITEMMODE, getItemMode().ordinal());
		nbt.setInteger(TAG_FLUIDMODE, getFluidMode().ordinal());
		nbt.setInteger(TAG_INTERCHANGER_ENERGYINDEX, getEnergyInterchangerIndex());
		nbt.setInteger(TAG_INTERCHANGER_ITEMINDEX, getItemInterchangerIndex());
		nbt.setInteger(TAG_INTERCHANGER_FLUIDINDEX, getFluidInterchangerIndex());
		for (ItemStack channelIDItem : getChannelID()) {
			NBTTagCompound channelIDItemNBT = new NBTTagCompound();
			channelIDItem.writeToNBT(channelIDItemNBT);
			channelIDItemList.appendTag(channelIDItemNBT);
		}
		nbt.setTag(TAG_CHANNELID, channelIDItemList);
		compound.setTag(TAG_NAME, nbt);
		return super.writeToNBT(compound);
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		return writeToNBT(new NBTTagCompound());
	}

	@Override
	@Nullable
	public SPacketUpdateTileEntity getUpdatePacket() {
		return new SPacketUpdateTileEntity(getPos(), 255, getUpdateTag());
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
		readFromNBT(pkt.getNbtCompound());
	}

	/* ENERGY */

	@Override
	public int sendEnergy(int amount, boolean simulate) {
		List<IInterchanger> connectedInterchangers = getLinkedReceivingInterchangers(ModeType.RF);
		cycleEnergyInterchangerIndex();
		if (connectedInterchangers.size() <= 0 || amount <= 0) {
			return 0;
		}
		sendingEnergy = true;
		int returnAmount = amount;
		IInterchanger tile = connectedInterchangers.get(getEnergyInterchangerIndex());
		if (tile.canReceiveEnergy()) {
			amount = tile.receiveEnergy(amount, simulate);
		}
		sendingEnergy = false;
		return returnAmount - amount;
	}

	@Override
	public int receiveEnergy(int amount, boolean simulate) {
		for (IEnergyReceiver receiver : TileEntityUtils.getAdjacentRFReceivers(this, false)) {
			if (receiver instanceof TileInterchanger) {
				continue;
			}
			for (int i = 0; i < EnumFacing.values().length; i++) {
				amount -= receiver.receiveEnergy(EnumFacing.VALUES[i], amount, simulate);
			}
		}
		return amount;
	}

	@Override
	public int receiveEnergy(EnumFacing from, int maxReceive, boolean simulate) {
		if (!canSendEnergy() || world().isRemote) {
			return 0;
		}
		return sendEnergy(maxReceive, simulate);
	}

	@Override
	public int extractEnergy(EnumFacing from, int maxExtract, boolean simulate) {
		return 0;
	}

	@Override
	public int getEnergyStored(EnumFacing from) {
		return 0;
	}

	@Override
	public int getMaxEnergyStored(EnumFacing from) {
		return 0;
	}

	@Override
	public EnergyMode getEnergyMode() {
		return energyMode;
	}

	@Override
	public boolean canSendEnergy() {
		return isRedstoneRequirementMet() && (getEnergyMode() == EnergyMode.SENDING || getEnergyMode() == EnergyMode.BOTH);
	}

	@Override
	public boolean canReceiveEnergy() {
		return !sendingEnergy && isRedstoneRequirementMet() && (getEnergyMode() == EnergyMode.RECEIVING || getEnergyMode() == EnergyMode.BOTH);
	}

	@Override
	public void setEnergyMode(EnergyMode mode) {
		energyMode = mode;
	}

	public void cycleEnergyMode() {
		int energyModeIndex = energyMode.ordinal();
		energyModeIndex++;
		if (energyModeIndex >= EnergyMode.values().length) {
			energyModeIndex = 0;
		}
		setEnergyMode(EnergyMode.values()[energyModeIndex]);
		markDirty();
	}

	/* ITEMS */

	@Override
	public ItemMode getItemMode() {
		return itemMode;
	}

	@Override
	public void setItemMode(ItemMode mode) {
		itemMode = mode;
		markDirty();
	}

	public void cycleItemMode() {
		int itemModeIndex = itemMode.ordinal();
		itemModeIndex++;
		if (itemModeIndex >= ItemMode.values().length) {
			itemModeIndex = 0;
		}
		setItemMode(ItemMode.values()[itemModeIndex]);
	}

	@Override
	public boolean canSendItems() {
		return isRedstoneRequirementMet() && !sendingItems && (getItemMode() == ItemMode.SENDING || getItemMode() == ItemMode.BOTH);
	}

	@Override
	public boolean canReceiveItems() {
		return isRedstoneRequirementMet() && (getItemMode() == ItemMode.RECEIVING || getItemMode() == ItemMode.BOTH);
	}

	@Override
	public boolean sendItem(ItemStack stack, boolean simulate) {
		if (world().isRemote) {
			//return false;
		}
		boolean destInvFound = false;
		ItemStack tempStack = stack.copy();
		List<IInterchanger> connectedInterchangers = getLinkedReceivingInterchangers(ModeType.ITEM);
		if (connectedInterchangers.size() <= 0 || stack == null) {
			return false;
		}
		sendingItems = true;
		for (IInterchanger tile : connectedInterchangers) {
			if (tile.world().isRemote) {
				((TileEntity) tile).markDirty();
				continue;
			}
			if (TileEntityUtils.hasAdjacentItemStorageTileWithSpace((TileEntity) tile, stack) && tile.canReceiveItems() && tile != this) {

				TileEntity availableTile = TileEntityUtils.getAdjacentItemStorageTilesWithSpace((TileEntity) tile, tempStack).get(0);
				if (availableTile instanceof TileInterchanger) {
					continue;
				}
				if (!simulate && availableTile != null) {
					stack = InventoryUtils.insertStack(availableTile, stack);
					availableTile.markDirty();

				}
				if (availableTile != null && InventoryUtils.hasRoomForStack(availableTile, tempStack)) {
					if (!ItemStack.areItemStacksEqual(stack, tempStack)) {
						break;
					}
					destInvFound = true;
				}
			}
		}
		sendingItems = false;
		return destInvFound;
	}
	/*
		@Override
		public ItemStack receiveItem(ItemStack stack) {
			ItemStack tempStack = null;
			if (stack != null) {
				if (TileEntityUtils.hasAdjacentItemStorageTileWithSpace(this, stack)) {
					List<TileEntity> invList = TileEntityUtils.getAdjacentItemStorageTilesWithSpace(this, stack);
					for (TileEntity invTile : invList) {
						//if (invTile.getWorld() instanceof WorldClient) {
						//	continue;
						//}
						tempStack = InventoryUtils.insertStack(invTile, stack.copy());
						//stack.stackSize -= tempStack == null ? stack.stackSize : tempStack.stackSize;
						if (tempStack == null || !ItemUtils.areStacksSameSize(stack, tempStack)) {
							break;
						}
					}
				}
			}
			if (tempStack != null && tempStack.stackSize <= 0) {
				tempStack = null;
			}
			return tempStack;
		}
	*/
	/* FLUIDS */

	@Override
	public int sendFluid(FluidStack fluid, boolean doFill) {
		List<IInterchanger> connectedInterchangers = getLinkedReceivingInterchangers(ModeType.FLUID);
		cycleFluidInterchangerIndex();
		if (fluid == null || connectedInterchangers.size() <= 0 || fluid.amount <= 0) {
			return 0;
		}
		int fluidVolume = fluid.amount;
		sendingFluids = true;
		//IInterchanger tile = connectedInterchangers.get(getFluidInterchangerIndex());
		for (IInterchanger tile : connectedInterchangers) {
			if (tile.canReceiveFluids()) { //receiveFluid stuff
				fluid = tile.receiveFluid(fluid, doFill);
			}
		}
		sendingFluids = false;
		return fluidVolume - fluid.amount;
	}

	@Override
	public FluidStack receiveFluid(FluidStack fluid, boolean doFill) {
		List<TileEntity> applicableTiles = TileEntityUtils.getAdjacentFluidTilesWidthSpace(this, fluid);
		if (applicableTiles != null && applicableTiles.size() > 0) {
			for (TileEntity applicableTile : applicableTiles) {
				for (EnumFacing facing : EnumFacing.values()) {
					if (applicableTile != null && fluid != null && !(applicableTile instanceof TileInterchanger)) {
						fluid.amount -= doFill ? FluidUtils.fill(applicableTile, fluid, fluid.amount, facing) : FluidUtils.testFill(applicableTile, fluid, fluid.amount, facing);
						ModNetworking.INSTANCE.sendToServer(new PacketFluidFill(applicableTile, fluid, applicableTile.getWorld().provider.getDimension(), facing));
					}
				}
			}
		}
		return fluid;
	}

	@Override
	public FluidMode getFluidMode() {
		return fluidMode;
	}

	@Override
	public void setFluidMode(FluidMode mode) {
		fluidMode = mode;
		markDirty();
	}

	public void cycleFluidMode() {
		int fluidModeIndex = fluidMode.ordinal();
		fluidModeIndex++;
		if (fluidModeIndex >= FluidMode.values().length) {
			fluidModeIndex = 0;
		}
		setFluidMode(FluidMode.values()[fluidModeIndex]);
	}

	@Override
	public boolean canSendFluids() {
		return isRedstoneRequirementMet() && (getFluidMode() == FluidMode.SENDING || getFluidMode() == FluidMode.BOTH);
	}

	@Override
	public boolean canReceiveFluids() {
		return isRedstoneRequirementMet() && !sendingFluids && (getFluidMode() == FluidMode.RECEIVING || getFluidMode() == FluidMode.BOTH);
	}

	public boolean canFill(FluidStack fluid, IFluidTankProperties properties) {
		if (fluid != null) {
			if (properties.getContents() != null) {
				if (properties.getContents().isFluidEqual(fluid)) {
					return properties.getContents().amount < properties.getCapacity();
				}
			}
			else {
				return properties.canFillFluidType(fluid) && properties.getCapacity() > 0;
			}
		}
		return false;
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return super.hasCapability(capability, facing) || capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {

		if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) {
			return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
				@Override
				public IFluidTankProperties[] getTankProperties() {
					return new IFluidTankProperties[] {
							new FluidTankProperties(null, Integer.MAX_VALUE, true, false)
					};
				}

				@Override
				public int fill(FluidStack fluid, boolean doFill) {
					if (!canSendFluids() || getWorld().isRemote || fluid == null || !canFill(fluid, getTankProperties()[0])) {
						return 0;
					}
					return sendFluid(fluid, doFill);
				}

				@Nullable
				@Override
				public FluidStack drain(FluidStack fluid, boolean doDrain) {
					return null;
				}

				@Nullable
				@Override
				public FluidStack drain(int maxDrain, boolean doDrain) {
					return null;
				}
			});
		}

		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(new SidedInvWrapper(this, facing));
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public String getModID() {
		return ModGlobals.MODID;
	}

	@Override
	public Object getModInstance() {
		return P455w0rdsThings.INSTANCE;
	}

	@Override
	public boolean shouldChunkLoad() {
		return ConfigOptions.ENABLE_INTERCHANGER_AUTOCHUNKLOAD;
	}

	public enum EnergyMode {
			SENDING, RECEIVING, BOTH, DISABLED
	}

	public enum ItemMode {
			SENDING, RECEIVING, BOTH, DISABLED
	}

	public enum FluidMode {
			SENDING, RECEIVING, BOTH, DISABLED
	}

	@Override
	public int getSizeInventory() {
		return 1;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		return null;
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if (sendItem(stack, true)) {
			sendItem(stack, false);
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return true;
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		if (!canSendItems() || itemInventory[0] != null) {
			return new int[] {};
		}
		return new int[] {
				0
		};
	}

	@Override
	public boolean canInsertItem(int index, ItemStack stack, EnumFacing direction) {
		return canSendItems() && itemInventory[0] == null && sendItem(stack, true);
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		return false;
	}

	public class ChannelInventory implements IInventory {

		ItemStack[] inventory = new ItemStack[6];

		@Override
		public String getName() {
			return "ChannelInventory";
		}

		@Override
		public boolean hasCustomName() {
			return false;
		}

		@Override
		public ITextComponent getDisplayName() {
			return new TextComponentString(getName());
		}

		@Override
		public int getSizeInventory() {
			return inventory.length;
		}

		@Override
		public ItemStack getStackInSlot(int index) {
			return inventory[index];
		}

		@Override
		public ItemStack decrStackSize(int index, int count) {
			return null;
		}

		@Override
		public ItemStack removeStackFromSlot(int index) {
			return null;
		}

		@Override
		public void setInventorySlotContents(int index, ItemStack stack) {
			inventory[index] = stack;
		}

		@Override
		public int getInventoryStackLimit() {
			return 1;
		}

		@Override
		public void markDirty() {
		}

		@Override
		public boolean isUsableByPlayer(EntityPlayer player) {
			return true;
		}

		@Override
		public void openInventory(EntityPlayer player) {
		}

		@Override
		public void closeInventory(EntityPlayer player) {
		}

		@Override
		public boolean isItemValidForSlot(int index, ItemStack stack) {
			return false;
		}

		@Override
		public int getField(int id) {
			return 0;
		}

		@Override
		public void setField(int id, int value) {
		}

		@Override
		public int getFieldCount() {
			return 0;
		}

		@Override
		public void clear() {

		}

	}

}
