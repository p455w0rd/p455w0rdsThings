/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.blocks.tileentities;

import javax.annotation.Nullable;

import cofh.api.energy.IEnergyReceiver;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

/**
 * @author p455w0rd
 *
 */
public class TileTrashCan extends TileEntity implements IEnergyReceiver {

	public IFluidHandler invFluid = new IFluidHandler() {

		@Override
		public IFluidTankProperties[] getTankProperties() {
			return new IFluidTankProperties[] {
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false),
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false),
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false),
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false),
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false),
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false),
					new FluidTankProperties(null, Integer.MAX_VALUE, true, false)
			};
		}

		@Override
		public int fill(FluidStack fluid, boolean doFill) {
			return fluid != null ? fluid.amount : 0;
		}

		@Override
		@Nullable
		public FluidStack drain(FluidStack fluid, boolean doDrain) {
			return null;
		}

		@Override
		@Nullable
		public FluidStack drain(int maxDrain, boolean doDrain) {
			return null;
		}
	};

	public ItemStackHandler invItem = new ItemStackHandler(27) {

		@Override
		public void setStackInSlot(int slot, ItemStack stack) {
		}

		@Override
		public ItemStack getStackInSlot(int slot) {
			return null;
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
			return null;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate) {
			return null;
		}
	};

	public EnergyStorage storageFE = new EnergyStorage(Integer.MAX_VALUE) {

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate) {
			return maxReceive;
		}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate) {
			return 0;
		}

		@Override
		public int getEnergyStored() {
			return 0;
		}

		@Override
		public int getMaxEnergyStored() {
			return capacity;
		}

		@Override
		public boolean canExtract() {
			return false;
		}

		@Override
		public boolean canReceive() {
			return true;
		}
	};

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityEnergy.ENERGY || super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(invItem);
		}
		else if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) {
			return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(invFluid);
		}
		else if (capability == CapabilityEnergy.ENERGY) {
			return CapabilityEnergy.ENERGY.cast(storageFE);
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public int getEnergyStored(EnumFacing paramEnumFacing) {
		return 0;
	}

	@Override
	public int getMaxEnergyStored(EnumFacing paramEnumFacing) {
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean canConnectEnergy(EnumFacing paramEnumFacing) {
		return true;
	}

	@Override
	public int receiveEnergy(EnumFacing paramEnumFacing, int paramInt, boolean paramBoolean) {
		return paramInt;
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		return writeToNBT(new NBTTagCompound());
	}

	@Override
	@Nullable
	public SPacketUpdateTileEntity getUpdatePacket() {
		return new SPacketUpdateTileEntity(getPos(), 0, getUpdateTag());
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
		readFromNBT(pkt.getNbtCompound());
	}

}
