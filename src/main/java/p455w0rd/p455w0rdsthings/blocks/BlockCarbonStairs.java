package p455w0rd.p455w0rdsthings.blocks;

import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModGlobals.HarvestLevel;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;

public class BlockCarbonStairs extends BlockStairsBase {

	public BlockCarbonStairs() {
		super(Blocks.DIAMOND_BLOCK.getDefaultState(), "carbon_stairs_block", 6000000.0F, 10.0F);
	}

	@Override
	public String getHarvestTool(IBlockState state) {
		return "pickaxe";
	}

	@Override
	public float getExplosionResistance(Entity exploder) {
		return blockResistance;
	}

	@Override
	public int getHarvestLevel(IBlockState state) {
		return Mods.TINKERS.isLoaded() ? HarvestLevel.CARBON : HarvestLevel.DIAMOND;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add(ChatFormatting.LIGHT_PURPLE + "Wither Proof");
	}

	@Override
	public boolean canEntityDestroy(IBlockState state, IBlockAccess world, BlockPos pos, Entity entity) {
		if (entity instanceof net.minecraft.entity.boss.EntityDragon) {
			return false;
		}
		else if ((entity instanceof net.minecraft.entity.boss.EntityWither) || (entity instanceof net.minecraft.entity.projectile.EntityWitherSkull)) {
			return false;
		}
		return true;
	}

}
