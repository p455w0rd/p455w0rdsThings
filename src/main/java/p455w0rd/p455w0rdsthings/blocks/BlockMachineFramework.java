package p455w0rd.p455w0rdsthings.blocks;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModGlobals.HarvestLevel;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;

/**
 * @author p455w0rd
 *
 */
public class BlockMachineFramework extends BlockBase {

	public BlockMachineFramework() {
		super(Material.ROCK, MapColor.BLACK, "machine_framework", 10.0F, 6000000.0F);
		setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		if (Mods.TINKERS.isLoaded()) {
			setHarvestLevel("pickaxe", HarvestLevel.CARBON);
		}
		else {
			setHarvestLevel("pickaxe", HarvestLevel.DIAMOND);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public BlockRenderLayer getBlockLayer() {
		return BlockRenderLayer.CUTOUT;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean isBlockSolid(IBlockAccess worldIn, BlockPos pos, EnumFacing side) {
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean isBlockNormalCube(IBlockState state) {
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean isOpaqueCube(IBlockState blockState) {
		return false;
	}

}
