package p455w0rd.p455w0rdsthings.api;

import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.RedstoneMode;

/**
 * @author p455w0rd
 *
 */
public interface IRedstoneControllable {

	void setRedstoneMode(RedstoneMode mode);

	RedstoneMode getRedstoneMode();

	boolean isRedstoneRequirementMet();

	boolean hasRSSignal();

	void setRSSignal(boolean isPowered);

}
