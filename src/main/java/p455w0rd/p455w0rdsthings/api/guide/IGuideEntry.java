package p455w0rd.p455w0rdsthings.api.guide;

import java.util.List;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public interface IGuideEntry {

	List<IGuideChapter> getAllChapters();

	String getIdentifier();

	String getLocalizedName();

	String getLocalizedNameWithFormatting();

	void addChapter(IGuideChapter chapter);

	int getSortingPriority();

	@SideOnly(Side.CLIENT)
	List<IGuideChapter> getChaptersForDisplay(String searchBarText);

}
