package p455w0rd.p455w0rdsthings.api.guide;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.item.ItemStack;

/**
 * @author Ellpeck - used with axplicit permission
 *
 */
public interface IGuideChapter {

	IGuidePage[] getAllPages();

	String getLocalizedName();

	String getLocalizedNameWithFormatting();

	IGuideEntry getEntry();

	@Nullable
	List<ItemStack> getDisplayItemStacks();

	String getIdentifier();

	int getPageIndex(IGuidePage page);

	int getSortingPriority();

}
