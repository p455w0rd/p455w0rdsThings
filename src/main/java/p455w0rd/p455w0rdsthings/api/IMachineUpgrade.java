package p455w0rd.p455w0rdsthings.api;

import p455w0rd.p455w0rdsthings.util.MachineUtils;

/**
 * @author p455w0rd
 *
 */
public interface IMachineUpgrade {

	MachineUtils.Upgrades getUpgradeType();

}
