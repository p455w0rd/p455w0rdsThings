package p455w0rd.p455w0rdsthings.api;

import java.util.List;
import java.util.UUID;

import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.AccessMode;

/**
 * @author p455w0rd
 *
 */
public interface IAccessible {

	AccessMode getAccessMode();

	void setAccessMode(AccessMode mode);

	void removePlayerFromAccessList(UUID playerID);

	void addPlayerToAccessList(UUID playerID);

	List<UUID> getAccessList();

	UUID getOwner();

	void setOwner(UUID ownerID);

}
