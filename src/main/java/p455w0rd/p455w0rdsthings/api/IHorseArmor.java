package p455w0rd.p455w0rdsthings.api;

import net.minecraft.entity.passive.HorseArmorType;
import net.minecraft.item.ItemStack;

public abstract interface IHorseArmor {
	public abstract HorseArmorType getArmorType(ItemStack paramItemStack);
}
