package p455w0rd.p455w0rdsthings.api;

import java.util.List;

import net.minecraft.tileentity.TileEntity;
import p455w0rd.p455w0rdsthings.util.MachineUtils;

/**
 * @author p455w0rd
 *
 */
public interface IMachineInWorldUpgradable {

	List<MachineUtils.InWorldUpgrades> getApplicableUpgrades();

	boolean isUpgradeActive();

	int getUpgradeCount(MachineUtils.InWorldUpgrades upgradeType);

	List<TileEntity> getUpgradeTiles(MachineUtils.InWorldUpgrades upgradeType);

}
