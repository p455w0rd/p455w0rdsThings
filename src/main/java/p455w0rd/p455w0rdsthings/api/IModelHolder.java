package p455w0rd.p455w0rdsthings.api;

/**
 * @author p455w0rd
 *
 */
public interface IModelHolder {

	void initModel();

}
