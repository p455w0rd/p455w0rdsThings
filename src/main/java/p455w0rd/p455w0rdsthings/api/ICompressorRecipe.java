package p455w0rd.p455w0rdsthings.api;

import net.minecraft.item.ItemStack;

/**
 * @author p455w0rd
 *
 */
public interface ICompressorRecipe {

	boolean hasSecondOutput();

	int getEnergyRequired();

	double getSecondOutputChance();

	ItemStack getInput();

	ItemStack getOutput();

	ItemStack getSecondOutput();

	boolean hasEnergyRegistered();

	int getInputCount();

	int getOutputCount();

	int getSecondOutputCount();

}
