package p455w0rd.p455w0rdsthings.api;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

/**
 * @author p455w0rd
 *
 */
public interface IEmeraldCarbonArmor {
	EnumRarity getRarity(ItemStack stack);

	EntityEquipmentSlot getArmorType();
}
