package p455w0rd.p455w0rdsthings.api;

import java.util.List;

import javax.annotation.Nonnull;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.EnergyMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.FluidMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.ItemMode;

/**
 * @author p455w0rd
 *
 */
public interface IInterchanger {

	int sendEnergy(int amount, boolean simulated);

	int receiveEnergy(int amount, boolean simlated);

	BlockPos pos();

	World world();

	int getDimension();

	boolean canSendEnergy();

	boolean canReceiveEnergy();

	EnergyMode getEnergyMode();

	void setEnergyMode(EnergyMode mode);

	ItemMode getItemMode();

	void setItemMode(ItemMode mode);

	boolean canSendItems();

	boolean canReceiveItems();

	boolean sendItem(ItemStack stack, boolean simulate);

	//ItemStack receiveItem(ItemStack stack);

	FluidMode getFluidMode();

	void setFluidMode(FluidMode mode);

	boolean canSendFluids();

	boolean canReceiveFluids();

	int sendFluid(FluidStack fluid, boolean doFill);

	FluidStack receiveFluid(FluidStack fluid, boolean doFill);

	List<ItemStack> getChannelID();

	void setChannelID(@Nonnull List<ItemStack> code);

}
