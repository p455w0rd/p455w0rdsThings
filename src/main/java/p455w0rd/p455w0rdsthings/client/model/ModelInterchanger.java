package p455w0rd.p455w0rdsthings.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * @author p455w0rd
 *
 */
public class ModelInterchanger extends ModelBase {

	ModelRenderer bottombackbar;
	ModelRenderer bottomfrontbar;
	ModelRenderer toprightbar;
	ModelRenderer bottomrightbar;
	ModelRenderer backpostleft;
	ModelRenderer backpostright;
	ModelRenderer frontpostright;
	ModelRenderer frontpostleft;
	ModelRenderer topfrontbar;
	ModelRenderer topbackbar;
	ModelRenderer bottomleftbar;
	ModelRenderer topleftbar;

	public ModelInterchanger() {
		textureWidth = 64;
		textureHeight = 64;

		bottombackbar = new ModelRenderer(this, 0, 0);
		bottombackbar.addBox(0F, 0F, 0F, 16, 2, 2);
		bottombackbar.setRotationPoint(-8F, 22F, -8F);
		bottombackbar.setTextureSize(64, 32);
		bottombackbar.mirror = true;
		setRotation(bottombackbar, 0F, 0F, 0F);
		bottomfrontbar = new ModelRenderer(this, 0, 0);
		bottomfrontbar.addBox(0F, 0F, 0F, 16, 2, 2);
		bottomfrontbar.setRotationPoint(-8F, 22F, 6F);
		bottomfrontbar.setTextureSize(64, 32);
		bottomfrontbar.mirror = true;
		setRotation(bottomfrontbar, 0F, 0F, 0F);
		toprightbar = new ModelRenderer(this, 0, 41);
		toprightbar.addBox(0F, 0F, 0F, 2, 2, 12);
		toprightbar.setRotationPoint(6.5F, 8.5F, -6F);
		toprightbar.setTextureSize(64, 32);
		toprightbar.mirror = true;
		setRotation(toprightbar, 0F, 0F, 0.7853982F);
		bottomrightbar = new ModelRenderer(this, 0, 5);
		bottomrightbar.addBox(0F, 0F, 0F, 2, 2, 12);
		bottomrightbar.setRotationPoint(6F, 22F, -6F);
		bottomrightbar.setTextureSize(64, 32);
		bottomrightbar.mirror = true;
		setRotation(bottomrightbar, 0F, 0F, 0F);
		backpostleft = new ModelRenderer(this, 0, 22);
		backpostleft.addBox(0F, 0F, 0F, 1, 12, 1);
		backpostleft.setRotationPoint(-7F, 10F, 6F);
		backpostleft.setTextureSize(64, 32);
		backpostleft.mirror = true;
		setRotation(backpostleft, 0F, 0F, 0F);
		backpostright = new ModelRenderer(this, 0, 22);
		backpostright.addBox(0F, 0F, 0F, 1, 12, 1);
		backpostright.setRotationPoint(6F, 10F, 6F);
		backpostright.setTextureSize(64, 32);
		backpostright.mirror = true;
		setRotation(backpostright, 0F, 0F, 0F);
		frontpostright = new ModelRenderer(this, 0, 22);
		frontpostright.addBox(0F, 0F, 0F, 1, 12, 1);
		frontpostright.setRotationPoint(6F, 10F, -7F);
		frontpostright.setTextureSize(64, 32);
		frontpostright.mirror = true;
		setRotation(frontpostright, 0F, 0F, 0F);
		frontpostleft = new ModelRenderer(this, 0, 22);
		frontpostleft.addBox(0F, 0F, 0F, 1, 12, 1);
		frontpostleft.setRotationPoint(-7F, 10F, -7F);
		frontpostleft.setTextureSize(64, 32);
		frontpostleft.mirror = true;
		setRotation(frontpostleft, 0F, 0F, 0F);
		topfrontbar = new ModelRenderer(this, 0, 36);
		topfrontbar.addBox(0F, 0F, 0F, 12, 2, 2);
		topfrontbar.setRotationPoint(-6F, 10F, 5F);
		topfrontbar.setTextureSize(64, 32);
		topfrontbar.mirror = true;
		setRotation(topfrontbar, 0.7853982F, 0F, 0F);
		topbackbar = new ModelRenderer(this, 0, 36);
		topbackbar.addBox(0F, 0F, 0F, 12, 2, 2);
		topbackbar.setRotationPoint(-6F, 10F, -8F);
		topbackbar.setTextureSize(64, 32);
		topbackbar.mirror = true;
		setRotation(topbackbar, 0.7853982F, 0F, 0F);
		bottomleftbar = new ModelRenderer(this, 0, 5);
		bottomleftbar.addBox(0F, 0F, 0F, 2, 2, 12);
		bottomleftbar.setRotationPoint(-8F, 22F, -6F);
		bottomleftbar.setTextureSize(64, 32);
		bottomleftbar.mirror = true;
		setRotation(bottomleftbar, 0F, 0F, 0F);
		topleftbar = new ModelRenderer(this, 0, 41);
		topleftbar.addBox(0F, 0F, 0F, 2, 2, 12);
		topleftbar.setRotationPoint(-6.5F, 8.5F, -6F);
		topleftbar.setTextureSize(64, 32);
		topleftbar.mirror = true;
		setRotation(topleftbar, 0F, 0F, 0.7853982F);
	}

	public void render(float scale) {
		render((Entity) null, 0f, 0f, 0f, 0f, 0f, scale);
	}

	@Override
	public void render(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		//super.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		//setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		bottombackbar.render(scale);
		bottomfrontbar.render(scale);
		toprightbar.render(scale);
		bottomrightbar.render(scale);
		backpostleft.render(scale);
		backpostright.render(scale);
		frontpostright.render(scale);
		frontpostleft.render(scale);
		topfrontbar.render(scale);
		topbackbar.render(scale);
		bottomleftbar.render(scale);
		topleftbar.render(scale);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, (Entity) null);
	}

}