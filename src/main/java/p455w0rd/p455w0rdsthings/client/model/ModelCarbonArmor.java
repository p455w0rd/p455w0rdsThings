package p455w0rd.p455w0rdsthings.client.model;

import javax.annotation.Nonnull;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.FMLClientHandler;
import p455w0rd.p455w0rdsthings.entity.EntityPArmorStand;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rd.p455w0rdsthings.util.EnumParticles;
import p455w0rd.p455w0rdsthings.util.ParticleUtil;
import p455w0rdslib.LibGlobals;
import p455w0rdslib.LibGlobals.ConfigOptions;
import p455w0rdslib.math.Pos3D;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.InventoryUtils;
import p455w0rdslib.util.ItemUtils;
import p455w0rdslib.util.MathUtils;

public class ModelCarbonArmor extends ModelBiped {
	ModelRenderer rightshoulder;
	ModelRenderer rightshoulder2;
	ModelRenderer rightArmBand1;
	ModelRenderer rightArmBand2;
	ModelRenderer leftshoulder;
	ModelRenderer leftshoulder2;
	ModelRenderer leftArmBand1;
	ModelRenderer leftArmBand2;
	ModelRenderer leftboot;
	ModelRenderer leftboot2;
	ModelRenderer leftboot3;
	ModelRenderer rightboot;
	ModelRenderer rightboot2;
	ModelRenderer rightboot3;
	ModelRenderer helmet;
	ModelRenderer head;
	ModelRenderer body;
	ModelRenderer body2;
	ModelRenderer rightarm;
	ModelRenderer leftarm;
	ModelRenderer rightleg;
	ModelRenderer leftleg;
	ModelRenderer leftShoulderSpike;
	ModelRenderer rightShoulderSpike;
	ModelRenderer leftArmSpike;
	ModelRenderer leftArmSpike2;
	ModelRenderer leftArmSpike3;
	ModelRenderer leftLegSpike;
	ModelRenderer rightArmSpike;
	ModelRenderer rightArmSpike2;
	ModelRenderer rightArmSpike3;
	ModelRenderer rightLegSpike;
	ModelRenderer headSpike;

	// JetPack
	ModelRenderer rightTankOuter;
	ModelRenderer leftShoulderStrapBack;
	ModelRenderer frontStrap;
	ModelRenderer rightTankMid;
	ModelRenderer leftTankOuter;
	ModelRenderer leftTankFoot;
	ModelRenderer leftTankMid;
	ModelRenderer leftTankTip;
	ModelRenderer rightTankTip;
	ModelRenderer tankBridge;
	ModelRenderer leftBodyStrap;
	ModelRenderer rightBodyStrap;
	ModelRenderer rightTankFoot;
	ModelRenderer rightShoulderStrapFront;
	ModelRenderer rightShoulderStrapBack;
	ModelRenderer rightShoulderStrapTop;
	ModelRenderer leftShoulderStrapTop;
	ModelRenderer leftShoulderStrapFront;

	private final EntityEquipmentSlot slot;

	public ModelCarbonArmor(EntityEquipmentSlot s) {
		MinecraftForge.EVENT_BUS.register(this);
		slot = s;
		textureWidth = 64;
		textureHeight = 64;

		helmet = new ModelRenderer(this, 0, 41);
		helmet.addBox(-4.5F, -7.4F, -4.5F, 9, 1, 9, 0.3F);
		helmet.setRotationPoint(0.0F, 0.0F, 0.0F);
		helmet.setTextureSize(64, 64);
		helmet.mirror = true;
		setRotation(helmet, 0.0F, 0.0F, 0.0F);

		rightshoulder = new ModelRenderer(this, 0, 34);
		rightshoulder.addBox(-4.0F, -3.0F, -2.5F, 5, 1, 5, 0.1F);
		rightshoulder.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightshoulder.setTextureSize(64, 64);
		rightshoulder.mirror = true;
		setRotation(rightshoulder, 0.0F, 0.0F, 0.0F);

		rightshoulder2 = new ModelRenderer(this, 0, 34);
		rightshoulder2.addBox(-4.0F, -1.0F, -2.5F, 5, 1, 5, 0.1F);
		rightshoulder2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightshoulder2.setTextureSize(64, 64);
		rightshoulder2.mirror = true;
		setRotation(rightshoulder2, 0.0F, 0.0F, 0.0F);

		rightArmBand1 = new ModelRenderer(this, 0, 34);
		rightArmBand1.addBox(-4.0F, 6.0F, -2.5F, 5, 1, 5, 0.1F);
		rightArmBand1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightArmBand1.setTextureSize(64, 64);
		rightArmBand1.mirror = true;
		setRotation(rightArmBand1, 0.0F, 0.0F, 0.0F);

		rightArmBand2 = new ModelRenderer(this, 0, 34);
		rightArmBand2.addBox(-4.0F, 4.0F, -2.5F, 5, 1, 5, 0.1F);
		rightArmBand2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightArmBand2.setTextureSize(64, 64);
		rightArmBand2.mirror = true;
		setRotation(rightArmBand2, 0.0F, 0.0F, 0.0F);

		leftshoulder = new ModelRenderer(this, 0, 34);
		leftshoulder.addBox(-1.0F, -3.0F, -2.5F, 5, 1, 5, 0.1F);
		leftshoulder.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftshoulder.setTextureSize(64, 64);
		leftshoulder.mirror = true;
		setRotation(leftshoulder, 0.0F, 0.0F, 0.0F);

		leftshoulder2 = new ModelRenderer(this, 0, 34);
		leftshoulder2.addBox(-1.0F, -1.0F, -2.5F, 5, 1, 5, 0.1F);
		leftshoulder2.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftshoulder2.setTextureSize(64, 64);
		leftshoulder2.mirror = true;
		setRotation(leftshoulder2, 0.0F, 0.0F, 0.0F);

		leftArmBand1 = new ModelRenderer(this, 0, 34);
		leftArmBand1.addBox(-1.5F, 6.0F, -2.5F, 5, 1, 5, 0.1F);
		leftArmBand1.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftArmBand1.setTextureSize(64, 64);
		leftArmBand1.mirror = true;
		setRotation(leftArmBand1, 0.0F, 0.0F, 0.0F);

		leftArmBand2 = new ModelRenderer(this, 0, 34);
		leftArmBand2.addBox(-1.5F, 4.0F, -2.5F, 5, 1, 5, 0.1F);
		leftArmBand2.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftArmBand2.setTextureSize(64, 64);
		leftArmBand2.mirror = true;
		setRotation(leftArmBand2, 0.0F, 0.0F, 0.0F);

		leftboot = new ModelRenderer(this, 33, 0);
		leftboot.addBox(0.0F, 8.0F, -2.5F, 5, 4, 5, 0.3F);
		leftboot.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftboot.setTextureSize(64, 64);
		leftboot.mirror = true;
		setRotation(leftboot, 0.0F, 0.0F, 0.0F);

		leftboot2 = new ModelRenderer(this, 33, 10);
		leftboot2.addBox(0.0F, 6.0F, -2.5F, 5, 1, 5, 0.3F);
		leftboot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftboot2.setTextureSize(64, 64);
		leftboot2.mirror = true;
		setRotation(leftboot2, 0.0F, 0.0F, 0.0F);

		leftboot3 = new ModelRenderer(this, 42, 17);
		leftboot3.addBox(0.5F, 7.0F, -2.0F, 4, 1, 4, 0.35F);
		leftboot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftboot3.setTextureSize(64, 64);
		leftboot3.mirror = true;
		setRotation(leftboot3, 0.0F, 0.0F, 0.0F);

		rightboot = new ModelRenderer(this, 33, 0);
		rightboot.addBox(-5.0F, 8.0F, -2.5F, 5, 4, 5, 0.3F);
		rightboot.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightboot.setTextureSize(64, 64);
		rightboot.mirror = true;
		setRotation(rightboot, 0.0F, 0.0F, 0.0F);

		rightboot2 = new ModelRenderer(this, 33, 10);
		rightboot2.addBox(-5.0F, 6.0F, -2.5F, 5, 1, 5, 0.3F);
		rightboot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightboot2.setTextureSize(64, 64);
		rightboot2.mirror = true;
		setRotation(rightboot2, 0.0F, 0.0F, 0.0F);

		rightboot3 = new ModelRenderer(this, 42, 17);
		rightboot3.addBox(-4.5F, 7.0F, -2.0F, 4, 1, 4, 0.35F);
		rightboot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightboot3.setTextureSize(64, 64);
		rightboot3.mirror = false;
		setRotation(rightboot3, 0.0F, 0.0F, 0.0F);

		head = new ModelRenderer(this, 0, 0);
		head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.5F);
		head.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.setTextureSize(64, 64);
		head.mirror = true;
		setRotation(head, 0.0F, 0.0F, 0.0F);

		body = new ModelRenderer(this, 17, 17);
		body.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, 0.7F);
		body.setRotationPoint(0.0F, 0.0F, 0.0F);
		body.setTextureSize(64, 64);
		body.mirror = true;
		setRotation(body, 0.0F, 0.0F, 0.0F);

		rightarm = new ModelRenderer(this, 42, 17);
		rightarm.addBox(-3.0F, -2.0F, -2.0F, 4, 12, 4, 0.1F);
		rightarm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		rightarm.setTextureSize(64, 64);
		rightarm.mirror = true;
		setRotation(rightarm, 0.0F, 0.0F, 0.0F);

		leftarm = new ModelRenderer(this, 42, 17);
		leftarm.addBox(-1.0F, -2.0F, -2.0F, 4, 12, 4, 0.1F);
		leftarm.setRotationPoint(5.0F, 2.0F, 0.0F);
		leftarm.setTextureSize(64, 64);
		leftarm.mirror = true;
		setRotation(leftarm, 0.0F, 0.0F, 0.0F);

		rightleg = new ModelRenderer(this, 0, 17);
		rightleg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, 0.345F);
		rightleg.setRotationPoint(-2.0F, 12.0F, 0.0F);
		rightleg.setTextureSize(64, 64);
		rightleg.mirror = true;
		setRotation(rightleg, 0.0F, 0.0F, 0.0F);

		leftleg = new ModelRenderer(this, 0, 17);
		leftleg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, 0.345F);
		leftleg.setRotationPoint(2.0F, 12.0F, 0.0F);
		leftleg.setTextureSize(64, 64);
		leftleg.mirror = true;
		setRotation(leftleg, 0.0F, 0.0F, 0.0F);

		leftShoulderSpike = new ModelRenderer(this, 21, 34);
		leftShoulderSpike.addBox(0F, 0F, 0F, 1, 1, 1);
		leftShoulderSpike.setRotationPoint(1F, -3.5F, 0F);
		leftShoulderSpike.setTextureSize(64, 64);
		leftShoulderSpike.mirror = true;
		setRotation(leftShoulderSpike, 0F, 0.7F, 0F);

		rightShoulderSpike = new ModelRenderer(this, 21, 34);
		rightShoulderSpike.addBox(0F, 0F, 0F, 1, 1, 1);
		rightShoulderSpike.setRotationPoint(-2F, -3.5F, 0F);
		rightShoulderSpike.setTextureSize(64, 64);
		rightShoulderSpike.mirror = true;
		setRotation(rightShoulderSpike, 0F, 0.7F, 0F);

		leftArmSpike = new ModelRenderer(this, 21, 34);
		leftArmSpike.addBox(0F, 0F, 0F, 1, 1, 1);
		leftArmSpike.setRotationPoint(2.5F, 2F, -0.75F);
		leftArmSpike.setTextureSize(64, 64);
		leftArmSpike.mirror = true;
		setRotation(leftArmSpike, 0.7F, 0F, 0F);

		leftArmSpike2 = new ModelRenderer(this, 21, 34);
		leftArmSpike2.addBox(0F, 0F, 0F, 1, 1, 1);
		leftArmSpike2.setRotationPoint(1F, 1.5F, 1.5F);
		leftArmSpike2.setTextureSize(64, 64);
		leftArmSpike2.mirror = true;
		setRotation(leftArmSpike2, 0F, 0F, 0.7F);

		leftArmSpike3 = new ModelRenderer(this, 21, 34);
		leftArmSpike3.addBox(0F, 0F, 0F, 1, 1, 1);
		leftArmSpike3.setRotationPoint(1F, 1.5F, -2.5F);
		leftArmSpike3.setTextureSize(64, 64);
		leftArmSpike3.mirror = true;
		setRotation(leftArmSpike3, 0F, 0F, 0.7F);

		leftLegSpike = new ModelRenderer(this, 21, 34);
		leftLegSpike.addBox(0F, 0F, 0F, 1, 1, 1);
		leftLegSpike.setRotationPoint(0F, 3F, -2.5F);
		leftLegSpike.setTextureSize(64, 64);
		leftLegSpike.mirror = true;
		setRotation(leftLegSpike, 0F, 0F, 0.7F);

		rightArmSpike = new ModelRenderer(this, 21, 34);
		rightArmSpike.addBox(0F, 0F, 0F, 1, 1, 1);
		rightArmSpike.setRotationPoint(-3.5F, 2F, -0.75F);
		rightArmSpike.setTextureSize(64, 64);
		rightArmSpike.mirror = true;
		setRotation(rightArmSpike, 0.7F, 0F, 0F);

		rightArmSpike2 = new ModelRenderer(this, 21, 34);
		rightArmSpike2.addBox(0F, 0F, 0F, 1, 1, 1);
		rightArmSpike2.setRotationPoint(-1F, 1.5F, -2.5F);
		rightArmSpike2.setTextureSize(64, 64);
		rightArmSpike2.mirror = true;
		setRotation(rightArmSpike2, 0F, 0F, 0.7F);

		rightArmSpike3 = new ModelRenderer(this, 21, 34);
		rightArmSpike3.addBox(0F, 0F, 0F, 1, 1, 1);
		rightArmSpike3.setRotationPoint(-1F, 1.5F, 1.5F);
		rightArmSpike3.setTextureSize(64, 64);
		rightArmSpike3.mirror = true;
		setRotation(rightArmSpike3, 0F, 0F, 0.7F);

		rightLegSpike = new ModelRenderer(this, 21, 34);
		rightLegSpike.addBox(0F, 0F, 0F, 1, 1, 1);
		rightLegSpike.setRotationPoint(0F, 3F, -2.5F);
		rightLegSpike.setTextureSize(64, 64);
		rightLegSpike.mirror = true;
		setRotation(rightLegSpike, 0F, 0F, 0.7F);

		headSpike = new ModelRenderer(this, 26, 34);
		headSpike.addBox(0F, 0F, 0F, 2, 2, 2, 0.5f);
		headSpike.setRotationPoint(-0F, -9F, -5F);
		headSpike.setTextureSize(64, 64);
		headSpike.mirror = true;
		setRotation(headSpike, 0.0F, 0.0F, 0.75F);

		// JetPack
		body2 = new ModelRenderer(this, 17, 17);
		body2.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, 0.7F);
		body2.setRotationPoint(0.0F, 0.0F, 0.0F);
		body2.setTextureSize(64, 64);
		body2.mirror = true;
		setRotation(body2, 0.0F, 0.0F, 0.0F);

		rightTankOuter = new ModelRenderer(this, 0, 54);
		rightTankOuter.addBox(0F, 0F, 0F, 3, 7, 3);
		rightTankOuter.setRotationPoint(-3.5F, 1F, 3F);
		rightTankOuter.setTextureSize(64, 64);
		rightTankOuter.mirror = true;
		setRotation(rightTankOuter, 0F, 0F, 0F);

		leftShoulderStrapBack = new ModelRenderer(this, 50, 36);
		leftShoulderStrapBack.addBox(0F, 0F, 0F, 2, 3, 1);
		leftShoulderStrapBack.setRotationPoint(1F, -0.5F, 2F);
		leftShoulderStrapBack.setTextureSize(64, 64);
		leftShoulderStrapBack.mirror = true;
		setRotation(leftShoulderStrapBack, 0F, 0F, 0F);

		frontStrap = new ModelRenderer(this, 39, 61);
		frontStrap.addBox(0F, 0F, 0F, 7, 2, 1);
		frontStrap.setRotationPoint(-3.5F, 3F, -2.5F);
		frontStrap.setTextureSize(64, 64);
		frontStrap.mirror = true;
		setRotation(frontStrap, 0F, 0F, 0F);

		rightTankMid = new ModelRenderer(this, 56, 53);
		rightTankMid.addBox(0F, 0F, 0F, 2, 9, 2);
		rightTankMid.setRotationPoint(-3F, 0F, 3.5F);
		rightTankMid.setTextureSize(64, 64);
		rightTankMid.mirror = true;
		setRotation(rightTankMid, 0F, 0F, 0F);

		leftTankOuter = new ModelRenderer(this, 0, 54);
		leftTankOuter.addBox(0F, 0F, 0F, 3, 7, 3);
		leftTankOuter.setRotationPoint(0.5F, 1F, 3F);
		leftTankOuter.setTextureSize(64, 64);
		leftTankOuter.mirror = true;
		setRotation(leftTankOuter, 0F, 0F, 0F);

		leftTankFoot = new ModelRenderer(this, 37, 36);
		leftTankFoot.addBox(0F, 0F, 0F, 3, 1, 3);
		leftTankFoot.setRotationPoint(0.5F, 9F, 3F);
		leftTankFoot.setTextureSize(64, 64);
		leftTankFoot.mirror = true;
		setRotation(leftTankFoot, 0F, 0F, 0F);

		leftTankMid = new ModelRenderer(this, 56, 53);
		leftTankMid.addBox(0F, 0F, 0F, 2, 9, 2);
		leftTankMid.setRotationPoint(1F, 0F, 3.5F);
		leftTankMid.setTextureSize(64, 64);
		leftTankMid.mirror = true;
		setRotation(leftTankMid, 0F, 0F, 0F);

		leftTankTip = new ModelRenderer(this, 32, 57);
		leftTankTip.addBox(0F, 0F, 0F, 1, 2, 1);
		leftTankTip.setRotationPoint(1.5F, -1F, 4F);
		leftTankTip.setTextureSize(64, 64);
		leftTankTip.mirror = true;
		setRotation(leftTankTip, 0F, 0F, 0F);

		rightTankTip = new ModelRenderer(this, 32, 57);
		rightTankTip.addBox(0F, 0F, 0F, 1, 2, 1);
		rightTankTip.setRotationPoint(-2.5F, -1F, 4F);
		rightTankTip.setTextureSize(64, 64);
		rightTankTip.mirror = true;
		setRotation(rightTankTip, 0F, 0F, 0F);

		tankBridge = new ModelRenderer(this, 32, 61);
		tankBridge.addBox(0F, 0F, 0F, 1, 2, 1);
		tankBridge.setRotationPoint(-0.5F, 2F, 4F);
		tankBridge.setTextureSize(64, 64);
		tankBridge.mirror = true;
		setRotation(tankBridge, 0F, 0F, 0F);

		leftBodyStrap = new ModelRenderer(this, 17, 56);
		leftBodyStrap.addBox(0F, 0F, 0F, 1, 2, 6);
		leftBodyStrap.setRotationPoint(3.5F, 3F, -2.5F);
		leftBodyStrap.setTextureSize(64, 64);
		leftBodyStrap.mirror = true;
		setRotation(leftBodyStrap, 0F, 0F, 0F);

		rightBodyStrap = new ModelRenderer(this, 17, 56);
		rightBodyStrap.addBox(0F, 0F, 0F, 1, 2, 6);
		rightBodyStrap.setRotationPoint(-4.5F, 3F, -2.5F);
		rightBodyStrap.setTextureSize(64, 64);
		rightBodyStrap.mirror = true;
		setRotation(rightBodyStrap, 0F, 0F, 0F);

		rightTankFoot = new ModelRenderer(this, 37, 36);
		rightTankFoot.addBox(0F, 0F, 0F, 3, 1, 3);
		rightTankFoot.setRotationPoint(-3.5F, 9F, 3F);
		rightTankFoot.setTextureSize(64, 64);
		rightTankFoot.mirror = true;
		setRotation(rightTankFoot, 0F, 0F, 0F);

		rightShoulderStrapFront = new ModelRenderer(this, 58, 34);
		rightShoulderStrapFront.addBox(0F, 0F, 0F, 2, 5, 1);
		rightShoulderStrapFront.setRotationPoint(-3F, -0.5F, -2.6F);
		rightShoulderStrapFront.setTextureSize(64, 64);
		rightShoulderStrapFront.mirror = true;
		setRotation(rightShoulderStrapFront, 0F, 0F, 0F);

		rightShoulderStrapBack = new ModelRenderer(this, 50, 36);
		rightShoulderStrapBack.addBox(0F, 0F, 0F, 2, 3, 1);
		rightShoulderStrapBack.setRotationPoint(-3F, -0.5F, 2F);
		rightShoulderStrapBack.setTextureSize(64, 64);
		rightShoulderStrapBack.mirror = true;
		setRotation(rightShoulderStrapBack, 0F, 0F, 0F);

		rightShoulderStrapTop = new ModelRenderer(this, 40, 45);
		rightShoulderStrapTop.addBox(0F, 0F, 0F, 2, 1, 5);
		rightShoulderStrapTop.setRotationPoint(-3F, -0.5F, -2.5F);
		rightShoulderStrapTop.setTextureSize(64, 64);
		rightShoulderStrapTop.mirror = true;
		setRotation(rightShoulderStrapTop, 0F, 0F, 0F);

		leftShoulderStrapTop = new ModelRenderer(this, 40, 45);
		leftShoulderStrapTop.addBox(0F, 0F, 0F, 2, 1, 5);
		leftShoulderStrapTop.setRotationPoint(1F, -0.5F, -2.5F);
		leftShoulderStrapTop.setTextureSize(64, 64);
		leftShoulderStrapTop.mirror = true;
		setRotation(leftShoulderStrapTop, 0F, 0F, 0F);

		leftShoulderStrapFront = new ModelRenderer(this, 58, 34);
		leftShoulderStrapFront.addBox(0F, 0F, 0F, 2, 5, 1);
		leftShoulderStrapFront.setRotationPoint(1F, -0.5F, -2.6F);
		leftShoulderStrapFront.setTextureSize(64, 64);
		leftShoulderStrapFront.mirror = true;
		setRotation(leftShoulderStrapFront, 0F, 0F, 0F);

		// make babies :P
		head.addChild(helmet);
		leftarm.addChild(leftshoulder);
		leftarm.addChild(leftshoulder2);
		leftarm.addChild(leftArmBand1);
		leftarm.addChild(leftArmBand2);
		rightarm.addChild(rightshoulder);
		rightarm.addChild(rightshoulder2);
		rightarm.addChild(rightArmBand1);
		rightarm.addChild(rightArmBand2);
		leftboot.addChild(leftboot2);
		leftboot.addChild(leftboot3);
		rightboot.addChild(rightboot2);
		rightboot.addChild(rightboot3);
		head.addChild(headSpike);
		rightleg.addChild(rightLegSpike);
		rightarm.addChild(rightArmSpike);
		rightarm.addChild(rightArmSpike2);
		rightarm.addChild(rightArmSpike3);
		rightarm.addChild(rightShoulderSpike);

		leftleg.addChild(leftLegSpike);
		leftarm.addChild(leftArmSpike);
		leftarm.addChild(leftArmSpike2);
		leftarm.addChild(leftArmSpike3);
		leftarm.addChild(leftShoulderSpike);

		// JetPack
		body2.addChild(tankBridge);

		body2.addChild(rightTankOuter);
		body2.addChild(rightTankMid);
		body2.addChild(rightTankFoot);
		body2.addChild(rightTankTip);

		body2.addChild(leftTankOuter);
		body2.addChild(leftTankMid);
		body2.addChild(leftTankFoot);
		body2.addChild(leftTankTip);

		body2.addChild(rightShoulderStrapBack);
		body2.addChild(rightShoulderStrapFront);
		body2.addChild(rightShoulderStrapTop);

		body2.addChild(leftShoulderStrapBack);
		body2.addChild(leftShoulderStrapFront);
		body2.addChild(leftShoulderStrapTop);

		body2.addChild(frontStrap);

		body2.addChild(leftBodyStrap);
		body2.addChild(rightBodyStrap);
		bipedHeadwear.cubeList.clear();
	}

	@Override
	public void render(@Nonnull Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		World world = FMLClientHandler.instance().getWorldClient();
		bipedBody.showModel = false;
		body.showModel = false;
		body2.showModel = false;
		if (slot == EntityEquipmentSlot.CHEST) {
			if ((entity instanceof EntityArmorStand)) {
				netHeadYaw = 0.0F;
				EntityArmorStand armorStand = (EntityArmorStand) entity;
				ItemStack chestplate = armorStand.getItemStackFromSlot(slot);
				body.showModel = !ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.FLIGHT);
				body2.showModel = ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.FLIGHT);
				bipedBody = body2.showModel ? body2 : body;
			}
			else {
				if ((entity instanceof EntityPArmorStand)) {
					netHeadYaw = 0.0F;
					EntityPArmorStand armorStand = (EntityPArmorStand) entity;
					ItemStack chestplate = armorStand.getItemStackFromSlot(slot);
					body.showModel = !ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.FLIGHT);
					body2.showModel = ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.FLIGHT);
					bipedBody = body2.showModel ? body2 : body;
				}
				else {
					if (entity instanceof EntityPlayer) {
						ItemStack chestplate = InventoryUtils.getArmorPiece(EntityEquipmentSlot.CHEST);
						body.showModel = !ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.FLIGHT);
						body2.showModel = ArmorUtils.isUpgradeEnabled(chestplate, Upgrades.FLIGHT);

						if (LibGlobals.IS_CONTRIBUTOR && ConfigOptions.ENABLE_CONTRIB_CAPE) {
							if (entity instanceof EntityPlayer && EasyMappings.player().getPersistentID().equals(((EntityPlayer) entity).getPersistentID())) {
								bipedBody = body;
							}
						}
						else {
							bipedBody = body2.showModel ? body2 : body;
						}
					}
				}
			}
			bipedBody.showModel = true;
		}

		bipedHead.showModel = (slot == EntityEquipmentSlot.HEAD);
		leftarm.showModel = (slot == EntityEquipmentSlot.CHEST);
		rightarm.showModel = (slot == EntityEquipmentSlot.CHEST);
		bipedLeftLeg.showModel = (slot == EntityEquipmentSlot.LEGS);
		bipedRightLeg.showModel = (slot == EntityEquipmentSlot.LEGS);
		leftboot.showModel = (slot == EntityEquipmentSlot.FEET);
		rightboot.showModel = (slot == EntityEquipmentSlot.FEET);
		bipedHead = head;
		bipedRightArm = rightarm;
		bipedLeftArm = leftarm;
		if (slot == EntityEquipmentSlot.LEGS) {
			bipedRightLeg = rightleg;
			bipedLeftLeg = leftleg;
		}
		else {
			bipedRightLeg = rightboot;
			bipedLeftLeg = leftboot;
		}
		if (slot == EntityEquipmentSlot.CHEST && entity instanceof EntityPlayer && ArmorUtils.isFlightItem(ItemUtils.getChestplate((EntityPlayer) entity))) {
			if ((((EntityPlayer) entity).capabilities.isFlying || !((EntityPlayer) entity).onGround) && (!LibGlobals.IS_CONTRIBUTOR || (LibGlobals.IS_CONTRIBUTOR && !LibGlobals.ConfigOptions.ENABLE_CONTRIB_CAPE))) {
				showJetpackParticles(world, (EntityLivingBase) entity);
			}
		}
		bipedHeadwear.isHidden = true;
		super.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
	}

	public void showJetpackParticles(World world, EntityLivingBase wearer) {

		Minecraft mc = Minecraft.getMinecraft();
		if (mc.gameSettings.particleSetting == 0 || mc.gameSettings.particleSetting == 1 && EasyMappings.world().getTotalWorldTime() % 2L == 0) {
			Pos3D userPos = new Pos3D(wearer).translate(0, 1.7, 0);
			Pos3D vLeft;
			Pos3D vRight;
			if (!wearer.isSneaking()) {
				vLeft = new Pos3D(-0.13, -0.9, -0.285).rotatePitch(0).rotateYaw(wearer.renderYawOffset);
				vRight = new Pos3D(0.11, -0.9, -0.285).rotatePitch(0).rotateYaw(wearer.renderYawOffset);
			}
			else {
				vLeft = new Pos3D(-0.13, -0.9, -0.50).rotatePitch(0).rotateYaw(wearer.renderYawOffset);
				vRight = new Pos3D(0.11, -0.9, -0.50).rotatePitch(0).rotateYaw(wearer.renderYawOffset);
			}
			Pos3D v = userPos.translate(vLeft).translate(new Pos3D(wearer.motionX, wearer.motionY, wearer.motionZ).scale(0.5));
			ParticleUtil.spawn(EnumParticles.GLOW_FLAME, world, v.xCoord, v.yCoord, v.zCoord, 0.0D, 0.0D, 0.0D);

			v = userPos.translate(vRight).translate(new Pos3D(wearer.motionX, wearer.motionY, wearer.motionZ).scale(0.5));
			ParticleUtil.spawn(EnumParticles.GLOW_FLAME, world, v.xCoord, v.yCoord, v.zCoord, 0.0D, 0.0D, 0.0D);

		}
	}

	@Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn) {
		boolean flag = ((entityIn instanceof EntityLivingBase)) && (((EntityLivingBase) entityIn).getTicksElytraFlying() > 4);
		bipedHead.rotateAngleY = (netHeadYaw * 0.017453292F);
		if (flag) {
			bipedHead.rotateAngleX = -0.7853982F;
		}
		else {
			bipedHead.rotateAngleX = (headPitch * 0.017453292F);
		}
		bipedBody.rotateAngleY = 0.0F;
		bipedRightArm.rotationPointZ = 0.0F;
		bipedRightArm.rotationPointX = -5.0F;
		bipedLeftArm.rotationPointZ = 0.0F;
		bipedLeftArm.rotationPointX = 5.0F;
		float f = 1.0F;
		if (flag) {
			f = (float) (entityIn.motionX * entityIn.motionX + entityIn.motionY * entityIn.motionY + entityIn.motionZ * entityIn.motionZ);
			f /= 0.2F;
			f = f * f * f;
		}
		if (f < 1.0F) {
			f = 1.0F;
		}
		bipedRightArm.rotateAngleX = (MathUtils.cos(limbSwing * 0.6662F + 3.1415927F) * 2.0F * limbSwingAmount * 0.5F / f);
		bipedLeftArm.rotateAngleX = (MathUtils.cos(limbSwing * 0.6662F) * 2.0F * limbSwingAmount * 0.5F / f);
		bipedRightArm.rotateAngleZ = 0.0F;
		bipedLeftArm.rotateAngleZ = 0.0F;
		bipedRightLeg.rotateAngleX = (MathUtils.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount / f);
		bipedLeftLeg.rotateAngleX = (MathUtils.cos(limbSwing * 0.6662F + 3.1415927F) * 1.4F * limbSwingAmount / f);
		bipedRightLeg.rotateAngleY = 0.0F;
		bipedLeftLeg.rotateAngleY = 0.0F;
		bipedRightLeg.rotateAngleZ = 0.0F;
		bipedLeftLeg.rotateAngleZ = 0.0F;
		if (isRiding) {
			bipedRightArm.rotateAngleX += -0.62831855F;
			bipedLeftArm.rotateAngleX += -0.62831855F;
			bipedRightLeg.rotateAngleX = -1.4137167F;
			bipedRightLeg.rotateAngleY = 0.31415927F;
			bipedRightLeg.rotateAngleZ = 0.07853982F;
			bipedLeftLeg.rotateAngleX = -1.4137167F;
			bipedLeftLeg.rotateAngleY = -0.31415927F;
			bipedLeftLeg.rotateAngleZ = -0.07853982F;
		}
		bipedRightArm.rotateAngleY = 0.0F;
		bipedRightArm.rotateAngleZ = 0.0F;
		switch (leftArmPose) {
		case EMPTY:
		default:
			bipedLeftArm.rotateAngleY = 0.0F;
			break;
		case BLOCK:
			bipedLeftArm.rotateAngleX = (bipedLeftArm.rotateAngleX * 0.5F - 0.9424779F);
			bipedLeftArm.rotateAngleY = 0.5235988F;
			break;
		case ITEM:
			bipedLeftArm.rotateAngleX = (bipedLeftArm.rotateAngleX * 0.5F - 0.31415927F);
			bipedLeftArm.rotateAngleY = 0.0F;
		}
		switch (rightArmPose) {
		case EMPTY:
		default:
			bipedRightArm.rotateAngleY = 0.0F;
			break;
		case BLOCK:
			bipedRightArm.rotateAngleX = (bipedRightArm.rotateAngleX * 0.5F - 0.9424779F);
			bipedRightArm.rotateAngleY = -0.5235988F;
			break;
		case ITEM:
			bipedRightArm.rotateAngleX = (bipedRightArm.rotateAngleX * 0.5F - 0.31415927F);
			bipedRightArm.rotateAngleY = 0.0F;
		}
		if (swingProgress > 0.0F) {
			EnumHandSide enumhandside = getMainHand(entityIn);
			ModelRenderer modelrenderer = getArmForSide(enumhandside);
			float f1 = swingProgress;
			bipedBody.rotateAngleY = (MathUtils.sin(MathUtils.sqrt(f1) * 6.2831855F) * 0.2F);
			if (enumhandside == EnumHandSide.LEFT) {
				bipedBody.rotateAngleY *= -1.0F;
			}
			bipedRightArm.rotationPointZ = (MathUtils.sin(bipedBody.rotateAngleY) * 5.0F);
			bipedRightArm.rotationPointX = (-MathUtils.cos(bipedBody.rotateAngleY) * 5.0F);
			bipedLeftArm.rotationPointZ = (-MathUtils.sin(bipedBody.rotateAngleY) * 5.0F);
			bipedLeftArm.rotationPointX = (MathUtils.cos(bipedBody.rotateAngleY) * 5.0F);
			bipedRightArm.rotateAngleY += bipedBody.rotateAngleY;
			bipedLeftArm.rotateAngleY += bipedBody.rotateAngleY;
			bipedLeftArm.rotateAngleX += bipedBody.rotateAngleY;
			f1 = 1.0F - swingProgress;
			f1 *= f1;
			f1 *= f1;
			f1 = 1.0F - f1;
			float f2 = MathUtils.sin(f1 * 3.1415927F);
			float f3 = MathUtils.sin(swingProgress * 3.1415927F) * -(bipedHead.rotateAngleX - 0.7F) * 0.75F;
			modelrenderer.rotateAngleX = ((float) (modelrenderer.rotateAngleX - (f2 * 1.2D + f3)));
			modelrenderer.rotateAngleY += bipedBody.rotateAngleY * 2.0F;
			modelrenderer.rotateAngleZ += MathUtils.sin(swingProgress * 3.1415927F) * -0.4F;
		}
		if (isSneak) {
			bipedBody.rotateAngleX = 0.5F;
			bipedRightArm.rotateAngleX += 0.4F;
			bipedLeftArm.rotateAngleX += 0.4F;
			bipedRightLeg.rotationPointZ = 4.0F;
			bipedLeftLeg.rotationPointZ = 4.0F;
			bipedRightLeg.rotationPointY = 9.0F;
			bipedLeftLeg.rotationPointY = 9.0F;
			bipedHead.rotationPointY = 1.0F;
		}
		else {
			bipedBody.rotateAngleX = 0.0F;
			bipedRightLeg.rotationPointZ = 0.1F;
			bipedLeftLeg.rotationPointZ = 0.1F;
			bipedRightLeg.rotationPointY = 12.0F;
			bipedLeftLeg.rotationPointY = 12.0F;
			bipedHead.rotationPointY = 0.0F;
		}
		if (rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW) {
			bipedRightArm.rotateAngleY = (-0.1F + bipedHead.rotateAngleY);
			bipedLeftArm.rotateAngleY = (0.1F + bipedHead.rotateAngleY + 0.4F);
			bipedRightArm.rotateAngleX = (-1.5707964F + bipedHead.rotateAngleX);
			bipedLeftArm.rotateAngleX = (-1.5707964F + bipedHead.rotateAngleX);
		}
		else if (leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW) {
			bipedRightArm.rotateAngleY = (-0.1F + bipedHead.rotateAngleY - 0.4F);
			bipedLeftArm.rotateAngleY = (0.1F + bipedHead.rotateAngleY);
			bipedRightArm.rotateAngleX = (-1.5707964F + bipedHead.rotateAngleX);
			bipedLeftArm.rotateAngleX = (-1.5707964F + bipedHead.rotateAngleX);
		}
		if ((entityIn instanceof EntityPArmorStand)) {
			EntityPArmorStand entityarmorstand = (EntityPArmorStand) entityIn;
			bipedHead.rotateAngleX = 0.017453292F;
			bipedHead.rotateAngleY = 0.017453292F;
			bipedHead.rotateAngleZ = 0.017453292F;
			bipedHead.setRotationPoint(0.0F, 1.0F, 0.0F);
			bipedLeftArm.rotateAngleX = (0.017453292F * entityarmorstand.getLeftArmRotation().getX());
			bipedLeftArm.rotateAngleY = (0.017453292F * entityarmorstand.getLeftArmRotation().getY());
			bipedLeftArm.rotateAngleZ = (0.017453292F * entityarmorstand.getLeftArmRotation().getZ());
			bipedRightArm.rotateAngleX = (0.017453292F * entityarmorstand.getRightArmRotation().getX());
			bipedRightArm.rotateAngleY = (0.017453292F * entityarmorstand.getRightArmRotation().getY());
			bipedRightArm.rotateAngleZ = (0.017453292F * entityarmorstand.getRightArmRotation().getZ());
		}
		else if ((entityIn instanceof EntityArmorStand)) {
			EntityArmorStand entityarmorstand = (EntityArmorStand) entityIn;
			bipedHead.rotateAngleX = 0.017453292F;
			bipedHead.rotateAngleY = 0.017453292F;
			bipedHead.rotateAngleZ = 0.017453292F;
			bipedHead.setRotationPoint(0.0F, 1.0F, 0.0F);
			bipedLeftArm.rotateAngleX = (0.017453292F * entityarmorstand.getLeftArmRotation().getX());
			bipedLeftArm.rotateAngleY = (0.017453292F * entityarmorstand.getLeftArmRotation().getY());
			bipedLeftArm.rotateAngleZ = (0.017453292F * entityarmorstand.getLeftArmRotation().getZ());
			bipedRightArm.rotateAngleX = (0.017453292F * entityarmorstand.getRightArmRotation().getX());
			bipedRightArm.rotateAngleY = (0.017453292F * entityarmorstand.getRightArmRotation().getY());
			bipedRightArm.rotateAngleZ = (0.017453292F * entityarmorstand.getRightArmRotation().getZ());
		}
		else {
			bipedRightArm.rotateAngleZ += MathUtils.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
			bipedLeftArm.rotateAngleZ -= MathUtils.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
			bipedRightArm.rotateAngleX += MathUtils.sin(ageInTicks * 0.067F) * 0.05F;
			bipedLeftArm.rotateAngleX -= MathUtils.sin(ageInTicks * 0.067F) * 0.05F;
		}
		copyModelAngles(bipedHead, bipedHeadwear);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}
}