/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.client.model;

import net.minecraft.client.model.*;
import net.minecraft.entity.Entity;

/**
 * @author p455w0rd
 *
 */
public class ModelNightVisionGoggles extends ModelBiped {
	//fields
	ModelRenderer leftEye1;
	ModelRenderer frontMiddleBar;
	ModelRenderer rightEye1;
	ModelRenderer backMiddleBar;
	ModelRenderer rightMiddleBar;
	ModelRenderer leftMiddleBar;
	ModelRenderer leftEye2;
	ModelRenderer rightEye2;
	ModelRenderer head;

	public ModelNightVisionGoggles() {
		textureWidth = 32;
		textureHeight = 32;

		head = new ModelRenderer(this, 0, 16);
		head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.1F);
		head.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.setTextureSize(32, 32);
		head.mirror = true;
		setRotation(head, 0.0F, 0.0F, 0.0F);

		leftEye1 = new ModelRenderer(this, 0, 0);
		leftEye1.addBox(0F, 0F, 0F, 2, 2, 2);
		leftEye1.setRotationPoint(2F, -5.5F, -5.5F);
		leftEye1.setTextureSize(32, 32);
		leftEye1.mirror = true;
		setRotation(leftEye1, 0F, 0F, 0.7853982F);

		frontMiddleBar = new ModelRenderer(this, 9, 0);
		frontMiddleBar.addBox(0F, 0F, 0F, 9, 1, 1);
		frontMiddleBar.setRotationPoint(-4.5F, -4.5F, -4.5F);
		frontMiddleBar.setTextureSize(32, 32);
		frontMiddleBar.mirror = true;
		setRotation(frontMiddleBar, 0F, 0F, 0F);

		leftEye2 = new ModelRenderer(this, 19, 6);
		leftEye2.addBox(0F, 0F, 0F, 1, 1, 1);
		leftEye2.setRotationPoint(2F, -4.75F, -6.25F);
		leftEye2.setTextureSize(32, 32);
		leftEye2.mirror = true;
		setRotation(leftEye2, 0F, 0F, 0.7853982F);

		backMiddleBar = new ModelRenderer(this, 9, 3);
		backMiddleBar.addBox(0F, 0F, 0F, 7, 1, 1);
		backMiddleBar.setRotationPoint(-3.5F, -4.5F, 3.5F);
		backMiddleBar.setTextureSize(32, 32);
		backMiddleBar.mirror = true;
		setRotation(backMiddleBar, 0F, 0F, 0F);

		rightMiddleBar = new ModelRenderer(this, 0, 6);
		rightMiddleBar.addBox(0F, 0F, 0F, 1, 1, 8);
		rightMiddleBar.setRotationPoint(-4.5F, -4.5F, -3.5F);
		rightMiddleBar.setTextureSize(32, 32);
		rightMiddleBar.mirror = true;
		setRotation(rightMiddleBar, 0F, 0F, 0F);

		leftMiddleBar = new ModelRenderer(this, 0, 6);
		leftMiddleBar.addBox(0F, 0F, 0F, 1, 1, 8);
		leftMiddleBar.setRotationPoint(3.5F, -4.5F, -3.5F);
		leftMiddleBar.setTextureSize(32, 32);
		leftMiddleBar.mirror = true;
		setRotation(leftMiddleBar, 0F, 0F, 0F);

		rightEye1 = new ModelRenderer(this, 0, 0);
		rightEye1.addBox(0F, 0F, 0F, 2, 2, 2);
		rightEye1.setRotationPoint(-2F, -5.5F, -5.5F);
		rightEye1.setTextureSize(32, 32);
		rightEye1.mirror = true;
		setRotation(rightEye1, 0F, 0F, 0.7853982F);

		rightEye2 = new ModelRenderer(this, 19, 6);
		rightEye2.addBox(0F, 0F, 0F, 1, 1, 1);
		rightEye2.setRotationPoint(-2F, -4.75F, -6.25F);
		rightEye2.setTextureSize(32, 32);
		rightEye2.mirror = true;
		setRotation(rightEye2, 0F, 0F, 0.7853982F);

		head.addChild(leftEye1);
		head.addChild(rightEye1);
		head.addChild(leftEye2);
		head.addChild(rightEye2);
		head.addChild(frontMiddleBar);
		head.addChild(backMiddleBar);
		head.addChild(leftMiddleBar);
		head.addChild(rightMiddleBar);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		setRotation(head, 0.0F, 0.0F, 0.0F);
		bipedHead = head;
		head.showModel = true;

		bipedLeftArm.showModel = bipedRightArm.showModel = bipedLeftLeg.showModel = bipedHeadwear.showModel = bipedLeftLeg.showModel = bipedRightLeg.showModel = false;
		super.render(entity, f, f1, f2, f3, f4, f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

}