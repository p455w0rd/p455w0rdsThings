package p455w0rd.p455w0rdsthings.client.model;

import javax.annotation.Nonnull;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import p455w0rdslib.LibGlobals;
import p455w0rdslib.LibGlobals.ConfigOptions;
import p455w0rdslib.util.EasyMappings;

public class ModelJetPack extends ModelBiped {

	ModelRenderer rightTankOuter;
	ModelRenderer leftShoulderStrapBack;
	ModelRenderer frontStrap;
	ModelRenderer rightTankMid;
	ModelRenderer leftTankOuter;
	ModelRenderer leftTankFoot;
	ModelRenderer leftTankMid;
	ModelRenderer leftTankTip;
	ModelRenderer rightTankTip;
	ModelRenderer tankBridge;
	ModelRenderer leftBodyStrap;
	ModelRenderer rightBodyStrap;
	ModelRenderer rightTankFoot;
	ModelRenderer rightShoulderStrapFront;
	ModelRenderer rightShoulderStrapBack;
	ModelRenderer rightShoulderStrapTop;
	ModelRenderer leftShoulderStrapTop;
	ModelRenderer leftShoulderStrapFront;
	ModelRenderer body;

	public ModelJetPack() {
		textureWidth = 64;
		textureHeight = 64;

		rightTankOuter = new ModelRenderer(this, 0, 54);
		rightTankOuter.addBox(0F, 0F, 0F, 3, 7, 3);
		rightTankOuter.setRotationPoint(-3.5F, 1F, 3F);
		rightTankOuter.setTextureSize(64, 64);
		rightTankOuter.mirror = true;
		setRotation(rightTankOuter, 0F, 0F, 0F);

		leftShoulderStrapBack = new ModelRenderer(this, 50, 36);
		leftShoulderStrapBack.addBox(0F, 0F, 0F, 2, 3, 1);
		leftShoulderStrapBack.setRotationPoint(1F, -0.5F, 2F);
		leftShoulderStrapBack.setTextureSize(64, 64);
		leftShoulderStrapBack.mirror = true;
		setRotation(leftShoulderStrapBack, 0F, 0F, 0F);

		frontStrap = new ModelRenderer(this, 39, 61);
		frontStrap.addBox(0F, 0F, 0F, 7, 2, 1);
		frontStrap.setRotationPoint(-3.5F, 3F, -2.5F);
		frontStrap.setTextureSize(64, 64);
		frontStrap.mirror = true;
		setRotation(frontStrap, 0F, 0F, 0F);

		rightTankMid = new ModelRenderer(this, 56, 53);
		rightTankMid.addBox(0F, 0F, 0F, 2, 9, 2);
		rightTankMid.setRotationPoint(-3F, 0F, 3.5F);
		rightTankMid.setTextureSize(64, 64);
		rightTankMid.mirror = true;
		setRotation(rightTankMid, 0F, 0F, 0F);

		leftTankOuter = new ModelRenderer(this, 0, 54);
		leftTankOuter.addBox(0F, 0F, 0F, 3, 7, 3);
		leftTankOuter.setRotationPoint(0.5F, 1F, 3F);
		leftTankOuter.setTextureSize(64, 64);
		leftTankOuter.mirror = true;
		setRotation(leftTankOuter, 0F, 0F, 0F);

		leftTankFoot = new ModelRenderer(this, 37, 36);
		leftTankFoot.addBox(0F, 0F, 0F, 3, 1, 3);
		leftTankFoot.setRotationPoint(0.5F, 9F, 3F);
		leftTankFoot.setTextureSize(64, 64);
		leftTankFoot.mirror = true;
		setRotation(leftTankFoot, 0F, 0F, 0F);

		leftTankMid = new ModelRenderer(this, 56, 53);
		leftTankMid.addBox(0F, 0F, 0F, 2, 9, 2);
		leftTankMid.setRotationPoint(1F, 0F, 3.5F);
		leftTankMid.setTextureSize(64, 64);
		leftTankMid.mirror = true;
		setRotation(leftTankMid, 0F, 0F, 0F);

		leftTankTip = new ModelRenderer(this, 32, 57);
		leftTankTip.addBox(0F, 0F, 0F, 1, 2, 1);
		leftTankTip.setRotationPoint(1.5F, -1F, 4F);
		leftTankTip.setTextureSize(64, 64);
		leftTankTip.mirror = true;
		setRotation(leftTankTip, 0F, 0F, 0F);

		rightTankTip = new ModelRenderer(this, 32, 57);
		rightTankTip.addBox(0F, 0F, 0F, 1, 2, 1);
		rightTankTip.setRotationPoint(-2.5F, -1F, 4F);
		rightTankTip.setTextureSize(64, 64);
		rightTankTip.mirror = true;
		setRotation(rightTankTip, 0F, 0F, 0F);

		tankBridge = new ModelRenderer(this, 32, 61);
		tankBridge.addBox(0F, 0F, 0F, 1, 2, 1);
		tankBridge.setRotationPoint(-0.5F, 2F, 4F);
		tankBridge.setTextureSize(64, 64);
		tankBridge.mirror = true;
		setRotation(tankBridge, 0F, 0F, 0F);

		leftBodyStrap = new ModelRenderer(this, 17, 56);
		leftBodyStrap.addBox(0F, 0F, 0F, 1, 2, 6);
		leftBodyStrap.setRotationPoint(3.5F, 3F, -2.5F);
		leftBodyStrap.setTextureSize(64, 64);
		leftBodyStrap.mirror = true;
		setRotation(leftBodyStrap, 0F, 0F, 0F);

		rightBodyStrap = new ModelRenderer(this, 17, 56);
		rightBodyStrap.addBox(0F, 0F, 0F, 1, 2, 6);
		rightBodyStrap.setRotationPoint(-4.5F, 3F, -2.5F);
		rightBodyStrap.setTextureSize(64, 64);
		rightBodyStrap.mirror = true;
		setRotation(rightBodyStrap, 0F, 0F, 0F);

		rightTankFoot = new ModelRenderer(this, 37, 36);
		rightTankFoot.addBox(0F, 0F, 0F, 3, 1, 3);
		rightTankFoot.setRotationPoint(-3.5F, 9F, 3F);
		rightTankFoot.setTextureSize(64, 64);
		rightTankFoot.mirror = true;
		setRotation(rightTankFoot, 0F, 0F, 0F);

		rightShoulderStrapFront = new ModelRenderer(this, 58, 34);
		rightShoulderStrapFront.addBox(0F, 0F, 0F, 2, 5, 1);
		rightShoulderStrapFront.setRotationPoint(-3F, -0.5F, -2.6F);
		rightShoulderStrapFront.setTextureSize(64, 64);
		rightShoulderStrapFront.mirror = true;
		setRotation(rightShoulderStrapFront, 0F, 0F, 0F);

		rightShoulderStrapBack = new ModelRenderer(this, 50, 36);
		rightShoulderStrapBack.addBox(0F, 0F, 0F, 2, 3, 1);
		rightShoulderStrapBack.setRotationPoint(-3F, -0.5F, 2F);
		rightShoulderStrapBack.setTextureSize(64, 64);
		rightShoulderStrapBack.mirror = true;
		setRotation(rightShoulderStrapBack, 0F, 0F, 0F);

		rightShoulderStrapTop = new ModelRenderer(this, 40, 45);
		rightShoulderStrapTop.addBox(0F, 0F, 0F, 2, 1, 5);
		rightShoulderStrapTop.setRotationPoint(-3F, -0.5F, -2.5F);
		rightShoulderStrapTop.setTextureSize(64, 64);
		rightShoulderStrapTop.mirror = true;
		setRotation(rightShoulderStrapTop, 0F, 0F, 0F);

		leftShoulderStrapTop = new ModelRenderer(this, 40, 45);
		leftShoulderStrapTop.addBox(0F, 0F, 0F, 2, 1, 5);
		leftShoulderStrapTop.setRotationPoint(1F, -0.5F, -2.5F);
		leftShoulderStrapTop.setTextureSize(64, 64);
		leftShoulderStrapTop.mirror = true;
		setRotation(leftShoulderStrapTop, 0F, 0F, 0F);

		leftShoulderStrapFront = new ModelRenderer(this, 58, 34);
		leftShoulderStrapFront.addBox(0F, 0F, 0F, 2, 5, 1);
		leftShoulderStrapFront.setRotationPoint(1F, -0.5F, -2.6F);
		leftShoulderStrapFront.setTextureSize(64, 64);
		leftShoulderStrapFront.mirror = true;
		setRotation(leftShoulderStrapFront, 0F, 0F, 0F);

		body = new ModelRenderer(this, 17, 17);
		body.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, 0.1F);
		body.setRotationPoint(0.0F, 0.0F, 0.0F);
		body.setTextureSize(64, 64);
		body.mirror = true;
		setRotation(body, 0.0F, 0.0F, 0.0F);

		bipedBody = body;
		bipedBody.addChild(tankBridge);

		bipedBody.addChild(rightTankOuter);
		bipedBody.addChild(rightTankMid);
		bipedBody.addChild(rightTankFoot);
		bipedBody.addChild(rightTankTip);

		bipedBody.addChild(leftTankOuter);
		bipedBody.addChild(leftTankMid);
		bipedBody.addChild(leftTankFoot);
		bipedBody.addChild(leftTankTip);

		bipedBody.addChild(rightShoulderStrapBack);
		bipedBody.addChild(rightShoulderStrapFront);
		bipedBody.addChild(rightShoulderStrapTop);

		bipedBody.addChild(leftShoulderStrapBack);
		bipedBody.addChild(leftShoulderStrapFront);
		bipedBody.addChild(leftShoulderStrapTop);

		bipedBody.addChild(frontStrap);

		bipedBody.addChild(leftBodyStrap);
		bipedBody.addChild(rightBodyStrap);

	}

	@Override
	public void render(@Nonnull Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (LibGlobals.IS_CONTRIBUTOR && ConfigOptions.ENABLE_CONTRIB_CAPE) {
			if (entity instanceof EntityPlayer && EasyMappings.player().getPersistentID().equals(((EntityPlayer) entity).getPersistentID())) {
				bipedBody.showModel = false;
			}
		}
		else {
			bipedBody.showModel = true;
		}
		bipedLeftArm.showModel = bipedRightArm.showModel = bipedLeftLeg.showModel = bipedHead.showModel = bipedHeadwear.showModel = bipedLeftLeg.showModel = bipedRightLeg.showModel = false;
		super.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

}
