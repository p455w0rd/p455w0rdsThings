package p455w0rd.p455w0rdsthings.client.render;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.items.ItemDankNull;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.MathUtils;

/**
 * @author p455w0rd
 *
 */
public class TESRCompressor extends TileEntitySpecialRenderer<TileCompressor> {

	private float rotation;
	public static final ResourceLocation TEXTURE_BEACON_BEAM = new ResourceLocation("textures/entity/beacon_beam.png");
	private static final TESRCompressor INSTANCE = new TESRCompressor();

	public static TESRCompressor getInstance() {
		return INSTANCE;
	}

	@Override
	public void renderTileEntityAt(TileCompressor te, double x, double y, double z, float partialTicks, int destroyStage) {
		//RenderUtils.renderBeam(te, partialTicks, 20, ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, 0.05, 0.057, EnumFacing.EAST, EnumFacing.SOUTH, EnumFacing.UP, EnumFacing.DOWN, EnumFacing.WEST, EnumFacing.NORTH);

		GlStateManager.pushAttrib();
		GlStateManager.pushMatrix();

		// Translate to the location of our tile entity
		GlStateManager.translate(x, y, z);
		GlStateManager.disableRescaleNormal();
		renderInputItem(te);
		renderOutputItem(te);
		GlStateManager.popMatrix();
		GlStateManager.popAttrib();
		if (te != null && te.isCompressing()) {
			renderLightRays(partialTicks, te, x, y, z);
			renderBeam(x, y - 1, z, partialTicks, 0.5, te.getWorld().getTotalWorldTime(), 1, 1, new float[] {
					0.0F,
					1.0F,
					0.0F
			}, 0.009D, 0.35D);
		}
	}

	@Override
	public boolean isGlobalRenderer(TileCompressor te) {
		return false;
	}

	public void renderBeam(double x, double y, double z, double partialTicks, double textureScale, double totalWorldTime, int yOffset, int height, float[] colors, double beamRadius, double glowRadius) {
		double i = yOffset + height;
		GlStateManager.disableFog();
		GlStateManager.alphaFunc(516, 0.1F);
		bindTexture(TEXTURE_BEACON_BEAM);
		GlStateManager.glTexParameteri(3553, 10242, 10497);
		GlStateManager.glTexParameteri(3553, 10243, 10497);
		GlStateManager.disableLighting();
		GlStateManager.disableCull();
		GlStateManager.disableBlend();
		GlStateManager.depthMask(true);
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		double d0 = totalWorldTime + partialTicks;
		double d1 = height < 0 ? d0 : -d0;
		double d2 = MathHelper.frac(d1 * 0.2D - MathUtils.floor(d1 * 0.1D));
		float f = colors[0];
		float f1 = colors[1];
		float f2 = colors[2];
		double d3 = d0 * 0.025D * -1.5D;
		double d4 = 0.5D + Math.cos(d3 + 2.356194490192345D) * beamRadius;
		double d5 = 0.5D + Math.sin(d3 + 2.356194490192345D) * beamRadius;
		double d6 = 0.5D + Math.cos(d3 + (Math.PI / 4D)) * beamRadius;
		double d7 = 0.5D + Math.sin(d3 + (Math.PI / 4D)) * beamRadius;
		double d8 = 0.5D + Math.cos(d3 + 3.9269908169872414D) * beamRadius;
		double d9 = 0.5D + Math.sin(d3 + 3.9269908169872414D) * beamRadius;
		double d10 = 0.5D + Math.cos(d3 + 5.497787143782138D) * beamRadius;
		double d11 = 0.5D + Math.sin(d3 + 5.497787143782138D) * beamRadius;
		double d12 = 0.0D;
		double d13 = 1.0D;
		double d14 = -1.0D + d2;
		double d15 = height * textureScale * (0.5D / beamRadius) + d14;
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		vertexbuffer.pos(x + d4 - 0.4, y + i, z + d5 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 + 0.4, y + yOffset, z + d5 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + yOffset, z + d7 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + i, z + d7 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + i, z + d11 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + yOffset, z + d11 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + yOffset, z + d9 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + i, z + d9 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + i, z + d7 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + yOffset, z + d7 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + yOffset, z + d11 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + i, z + d11 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + i, z + d9 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + yOffset, z + d9 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 + 0.4, y + yOffset, z + d5 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 - 0.4, y + i, z + d5 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		tessellator.draw();

		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		vertexbuffer.pos(x + d4 + 0.4, y + i, z + d5 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 - 0.4, y + yOffset, z + d5 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + yOffset, z + d7 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + i, z + d7 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + i, z + d11 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + yOffset, z + d11 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + yOffset, z + d9 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + i, z + d9 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + i, z + d7 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + yOffset, z + d7 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + yOffset, z + d11 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + i, z + d11 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + i, z + d9 - 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + yOffset, z + d9 + 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 - 0.4, y + yOffset, z + d5 + 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 + 0.4, y + i, z + d5 - 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		tessellator.draw();

		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		vertexbuffer.pos(x + d4 + 0.4, y + i, z + d5 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 - 0.4, y + yOffset, z + d5 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + yOffset, z + d7 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + i, z + d7 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + i, z + d11 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + yOffset, z + d11 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + yOffset, z + d9 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + i, z + d9 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + i, z + d7 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + yOffset, z + d7 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + yOffset, z + d11 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + i, z + d11 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + i, z + d9 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + yOffset, z + d9 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 - 0.4, y + yOffset, z + d5 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 + 0.4, y + i, z + d5 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		tessellator.draw();

		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		vertexbuffer.pos(x + d4 - 0.4, y + i, z + d5 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 + 0.4, y + yOffset, z + d5 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + yOffset, z + d7 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + i, z + d7 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + i, z + d11 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + yOffset, z + d11 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + yOffset, z + d9 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + i, z + d9 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 - 0.4, y + i, z + d7 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d6 + 0.4, y + yOffset, z + d7 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 + 0.4, y + yOffset, z + d11 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d10 - 0.4, y + i, z + d11 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 - 0.4, y + i, z + d9 + 0.4).tex(1.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d8 + 0.4, y + yOffset, z + d9 - 0.4).tex(1.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 + 0.4, y + yOffset, z + d5 - 0.4).tex(0.0D, d14).color(f, f1, f2, 1.0F).endVertex();
		vertexbuffer.pos(x + d4 - 0.4, y + i, z + d5 + 0.4).tex(0.0D, d15).color(f, f1, f2, 1.0F).endVertex();
		tessellator.draw();
		GlStateManager.enableBlend();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		GlStateManager.depthMask(false);
		d3 = 0.5D - glowRadius;
		d4 = 0.5D - glowRadius;
		d5 = 0.5D + glowRadius;
		d6 = 0.5D - glowRadius;
		d7 = 0.5D - glowRadius;
		d8 = 0.5D + glowRadius;
		d9 = 0.5D + glowRadius;
		d10 = 0.5D + glowRadius;
		d11 = 0.0D;
		d12 = 1.0D;
		d13 = -1.0D + d2;
		d14 = height * textureScale + d13;
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
		vertexbuffer.pos(x + d3, y + i, z + d4).tex(1.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d3, y + yOffset, z + d4).tex(1.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d5, y + yOffset, z + d6).tex(0.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d5, y + i, z + d6).tex(0.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d9, y + i, z + d10).tex(1.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d9, y + yOffset, z + d10).tex(1.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d7, y + yOffset, z + d8).tex(0.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d7, y + i, z + d8).tex(0.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d5, y + i, z + d6).tex(1.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d5, y + yOffset, z + d6).tex(1.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d9, y + yOffset, z + d10).tex(0.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d9, y + i, z + d10).tex(0.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d7, y + i, z + d8).tex(1.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d7, y + yOffset, z + d8).tex(1.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d3, y + yOffset, z + d4).tex(0.0D, d13).color(f, f1, f2, 0.125F).endVertex();
		vertexbuffer.pos(x + d3, y + i, z + d4).tex(0.0D, d14).color(f, f1, f2, 0.125F).endVertex();
		tessellator.draw();
		GlStateManager.enableLighting();
		GlStateManager.enableTexture2D();
		GlStateManager.depthMask(true);
		GlStateManager.enableFog();
	}

	private void renderLightRays(float partialTicks, TileCompressor te, double x, double y, double z) {
		if (te == null) {
			return;
		}
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		RenderHelper.disableStandardItemLighting();
		float f = te.getEnergyUsed() / 100000.0F;//(te.getEnergyUsed() + partialTicks) / 10000.0F;
		float f1 = 0.0F;

		if (f > 0.8F) {
			f1 = (f - 0.8F) / 0.2F;
		}
		float f4 = (f + f * f) + 6 / 2.0F * 12.0F;
		if (f4 > 36.5F) {
			//f4 = 8.5F;
		}
		//System.out.println("" + f4);
		Random random = new Random(432L);
		GlStateManager.disableTexture2D();
		GlStateManager.shadeModel(7425);
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE);
		GlStateManager.disableAlpha();
		GlStateManager.enableCull();
		GlStateManager.depthMask(false);
		GlStateManager.pushMatrix();
		//GlStateManager.translate(0.0F, -1.0F, -2.0F);
		GlStateManager.translate(x + 0.5, y + 0.5, z + 0.5);

		for (int i = 6; i < f4; ++i) {
			GlStateManager.rotate(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F + f * 90.0F, 0.0F, 0.0F, 1.0F);

			float f2 = random.nextFloat() * 10.0F + 5.0F + (f1 / 2) * 10.0F;
			float f3 = random.nextFloat() * 2.0F + 1.0F + (f1 / 2) * 2.0F;
			f2 /= 20;
			f3 /= 20;
			if (f2 > 0.8F) {
				f2 = 0.8F;
			}
			if (f3 > 0.2F) {
				f3 = 0.2F;
			}

			/*
			float f2 = random.nextFloat() * 20.0F + 5.0F + f1 * 10.0F;
			float f3 = random.nextFloat() * 2.0F + 1.0F + f1 * 2.0F;
			*/
			vertexbuffer.begin(6, DefaultVertexFormats.POSITION_COLOR);

			vertexbuffer.pos(0.0D, 0.0D, 0.0D).color(0, 255, 0, (int) (255.0F)).endVertex();

			//vertexbuffer.pos(0.0D, 0.0D, 0.0D).color(0, 255, 0, (int)(255.0F * (1.0F - f1))).endVertex();

			vertexbuffer.pos(-0.866D * f3, f2, -0.5F * f3).color(0, 0, 0, 0).endVertex();
			vertexbuffer.pos(0.866D * f3, f2, -0.5F * f3).color(0, 0, 0, 0).endVertex();
			vertexbuffer.pos(0.0D, f2, 1.0F * f3).color(0, 0, 0, 0).endVertex();
			vertexbuffer.pos(-0.866D * f3, f2, -0.5F * f3).color(0, 0, 0, 0).endVertex();
			/*
			vertexbuffer.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(255, 0, 255, 0).endVertex();
			vertexbuffer.pos(0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(255, 0, 255, 0).endVertex();
			vertexbuffer.pos(0.0D, (double)f2, (double)(1.0F * f3)).color(255, 0, 255, 0).endVertex();
			vertexbuffer.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(255, 0, 255, 0).endVertex();
			*/
			tessellator.draw();
		}

		GlStateManager.popMatrix();
		GlStateManager.depthMask(true);
		GlStateManager.disableCull();
		GlStateManager.disableBlend();
		GlStateManager.shadeModel(7424);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.enableTexture2D();
		GlStateManager.enableAlpha();
		RenderHelper.enableStandardItemLighting();
	}

	private void renderInputItem(TileCompressor te) {
		if (te == null) {
			return;
		}
		if (te.getCurrentRecipe() != null && te.getCurrentRecipe().getInput() != null) {
			ItemStack stack = te.getCurrentRecipe().getInput();
			IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(stack);
			RenderHelper.enableStandardItemLighting();
			GlStateManager.enableLighting();

			GlStateManager.pushMatrix();

			GlStateManager.enableAlpha();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			GlStateManager.enableBlend();
			GlStateManager.enableColorMaterial();
			GlStateManager.color(1.0f, 1.0f, 1.0f, 0.0f);
			GlStateManager.translate(.5, .5, .5);
			GlStateManager.scale(.35f, .35f, .35f);
			GlStateManager.scale(1.0 - ((float) te.getPercentComplete() / 100), 1.0 - ((float) te.getPercentComplete() / 100), 1.0 - ((float) te.getPercentComplete() / 100));
			if (rotation >= 360) {
				rotation = 0;
			}

			if (getWorld().canBlockSeeSky(te.getPos()) && !Minecraft.getMinecraft().isGamePaused()) {
				//if (!getWorld().isBlockPowered(te.getPos())) {
				int x = te.getPos().getX();
				int y = te.getPos().getY();
				int z = te.getPos().getZ();
				rotation += 0.5;
				if (rotation % 36 == 0) {
					//getWorld().spawnParticle(EnumParticleTypes.PORTAL, x + 0.5, y + 0.75, z + 0.5, 0, 0, 0, new int[0]);
				}
				//}
			}

			GlStateManager.rotate(rotation, 0, rotation, 0);

			Minecraft.getMinecraft().getRenderItem().renderItem(stack, ItemCameraTransforms.TransformType.NONE);
			GlStateManager.disableAlpha();
			GlStateManager.disableBlend();
			GlStateManager.translate(-0.5, -0.5, -0.5);
			if (te.isCompressing()) {
				GlStateManager.enableBlend();
				renderEffect(model);
				GlStateManager.disableBlend();
			}
			GlStateManager.popMatrix();

		}
	}

	private void renderOutputItem(TileCompressor te) {
		if (te == null) {
			return;
		}
		if (te.getCurrentRecipe() != null && te.getCurrentRecipe().getInput() != null) {
			ItemStack stack = te.getCurrentRecipe().getOutput();
			IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(stack);
			RenderHelper.enableStandardItemLighting();
			GlStateManager.enableLighting();

			GlStateManager.pushMatrix();

			GlStateManager.enableAlpha();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			GlStateManager.enableBlend();
			GlStateManager.enableColorMaterial();
			GlStateManager.color(1.0f, 1.0f, 1.0f, 0.0f);
			GlStateManager.translate(.5, .5, .5);
			GlStateManager.scale(.35f, .35f, .35f);
			GlStateManager.scale(((float) te.getPercentComplete() / 100), ((float) te.getPercentComplete() / 100), ((float) te.getPercentComplete() / 100));
			if (rotation >= 360) {
				rotation = 0;
			}

			if (getWorld().canBlockSeeSky(te.getPos()) && !Minecraft.getMinecraft().isGamePaused()) {
				//if (!getWorld().isBlockPowered(te.getPos())) {
				int x = te.getPos().getX();
				int y = te.getPos().getY();
				int z = te.getPos().getZ();
				rotation += 0.5;
				if (rotation % 36 == 0) {
					//getWorld().spawnParticle(EnumParticleTypes.PORTAL, x + 0.5, y + 0.75, z + 0.5, 0, 0, 0, new int[0]);
				}
				//}
			}

			GlStateManager.rotate(rotation, 0, rotation, 0);

			Minecraft.getMinecraft().getRenderItem().renderItem(stack, ItemCameraTransforms.TransformType.NONE);
			GlStateManager.disableAlpha();
			GlStateManager.disableBlend();
			GlStateManager.translate(-0.5, -0.5, -0.5);
			if (te.isCompressing()) {
				GlStateManager.enableBlend();
				renderEffect(model);
				GlStateManager.disableBlend();
			}
			GlStateManager.popMatrix();

		}
	}

	public IBakedModel getItemModelWithOverrides(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entitylivingbaseIn) {
		IBakedModel ibakedmodel = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(stack);
		return ibakedmodel.getOverrides().handleItemState(ibakedmodel, stack, worldIn, entitylivingbaseIn);
	}

	protected void renderItemModel(ItemStack stack, IBakedModel bakedmodel, ItemCameraTransforms.TransformType transform, boolean leftHanded) {
		if (stack.getItem() != null) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
			Minecraft.getMinecraft().getTextureManager().getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).setBlurMipmap(false, false);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			GlStateManager.enableRescaleNormal();
			GlStateManager.alphaFunc(516, 0.1F);
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.pushMatrix();
			bakedmodel = net.minecraftforge.client.ForgeHooksClient.handleCameraTransforms(bakedmodel, transform, leftHanded);

			renderItem(stack, bakedmodel);
			GlStateManager.cullFace(GlStateManager.CullFace.BACK);
			GlStateManager.popMatrix();
			GlStateManager.disableRescaleNormal();
			GlStateManager.disableBlend();
			Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
			Minecraft.getMinecraft().getTextureManager().getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).restoreLastBlurMipmap();
		}
	}

	private void renderModel(IBakedModel model, ItemStack stack) {
		this.renderModel(model, -1, stack);
	}

	private void renderModel(IBakedModel model, int color) {
		this.renderModel(model, color, (ItemStack) null);
	}

	private void renderModel(IBakedModel model, int color, ItemStack stack) {
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.ITEM);

		for (EnumFacing enumfacing : EnumFacing.values()) {
			renderQuads(vertexbuffer, model.getQuads((IBlockState) null, enumfacing, 0L), color, stack);
		}

		renderQuads(vertexbuffer, model.getQuads((IBlockState) null, (EnumFacing) null, 0L), color, stack);
		tessellator.draw();
	}

	public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
		return new ArrayList<BakedQuad>();
	}

	private void renderQuads(VertexBuffer renderer, List<BakedQuad> quads, int color, ItemStack stack) {
		boolean flag = color == -1 && stack != null;
		int i = 0;

		for (int j = quads.size(); i < j; ++i) {
			BakedQuad bakedquad = quads.get(i);
			int k = color;

			if (flag && bakedquad.hasTintIndex()) {
				ItemColors itemColors = Minecraft.getMinecraft().getItemColors();
				k = itemColors.getColorFromItemstack(stack, bakedquad.getTintIndex());
				//k=0x00FF00;

				if (EntityRenderer.anaglyphEnable) {
					k = TextureUtil.anaglyphColor(k);
				}
				k = k | -16777216;
				//k = k | 0x00FF00;
			}
			net.minecraftforge.client.model.pipeline.LightUtil.renderQuadColor(renderer, bakedquad, k);
		}
	}

	public void renderItem(ItemStack stack, IBakedModel model) {
		if (stack != null) {
			GlStateManager.pushMatrix();
			GlStateManager.translate(-0.5F, -0.5F, -0.5F);

			if (model.isBuiltInRenderer()) {
				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				GlStateManager.enableRescaleNormal();
				//TileEntityItemStackRenderer.instance.renderByItem(stack);
				Minecraft.getMinecraft().getItemRenderer().renderItem(EasyMappings.player(), stack, TransformType.NONE);
			}
			else {

				this.renderModel(model, stack);

				if (stack.hasEffect()) {
					if (stack.getItem() instanceof ItemDankNull) {
						renderEffect2(model);
					}
					else {
						renderEffect(model);
					}
				}
			}

			GlStateManager.popMatrix();
		}
	}

	private void renderEffect(IBakedModel model) {
		GlStateManager.depthMask(false);
		GlStateManager.depthFunc(514);
		GlStateManager.disableLighting();
		//GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_COLOR, GlStateManager.DestFactor.ONE);
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("minecraft", "textures/misc/enchanted_item_glint.png"));
		GlStateManager.matrixMode(5890);
		GlStateManager.pushMatrix();
		GlStateManager.scale(4.0F, 4.0F, 4.0F);
		float f = Minecraft.getSystemTime() % 3000L / 3000.0F / 8.0F;
		GlStateManager.translate(f, 0.0F, 0.0F);
		GlStateManager.rotate(-50.0F, 0.0F, 0.0F, 1.0F);
		this.renderModel(model, 0x2200FF00);
		GlStateManager.popMatrix();
		GlStateManager.pushMatrix();
		GlStateManager.scale(12.0F, 12.0F, 12.0F);
		float f1 = Minecraft.getSystemTime() % 4873L / 4873.0F / 8.0F;
		GlStateManager.translate(-f1, 0.0F, 0.0F);
		GlStateManager.rotate(10.0F, 0.0F, 0.0F, 1.0F);
		this.renderModel(model, 0x33FFFFFF);
		GlStateManager.popMatrix();
		GlStateManager.matrixMode(5888);
		//GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.enableLighting();
		GlStateManager.depthFunc(515);
		GlStateManager.depthMask(true);
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
	}

	private void renderEffect2(IBakedModel model) {
		GlStateManager.depthMask(false);
		GlStateManager.depthFunc(514);
		GlStateManager.disableLighting();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_COLOR, GlStateManager.DestFactor.ONE);
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("minecraft", "textures/misc/enchanted_item_glint.png"));
		GlStateManager.matrixMode(5890);
		GlStateManager.pushMatrix();
		GlStateManager.scale(8.0F, 8.0F, 8.0F);
		float f = Minecraft.getSystemTime() % 3000L / 3000.0F / 8.0F;
		GlStateManager.translate(f, 0.0F, 0.0F);
		GlStateManager.rotate(-50.0F, 0.0F, 0.0F, 1.0F);
		//this.renderModel(model, -8372020);
		this.renderModel(model, 0xFFFFFF00);
		GlStateManager.popMatrix();
		/*
		GlStateManager.pushMatrix();
		GlStateManager.scale(8.0F, 8.0F, 8.0F);
		float f1 = (float) (Minecraft.getSystemTime() % 4873L) / 4873.0F / 8.0F;
		GlStateManager.translate(-f1, 0.0F, 0.0F);
		GlStateManager.rotate(10.0F, 0.0F, 0.0F, 1.0F);
		//this.renderModel(model, -8372020);
		this.renderModel(model, 0xFFFFFF00);
		GlStateManager.popMatrix();
		*/
		GlStateManager.matrixMode(5888);
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.enableLighting();
		GlStateManager.depthFunc(515);
		GlStateManager.depthMask(true);
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
	}

}