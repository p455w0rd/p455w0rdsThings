package p455w0rd.p455w0rdsthings.client.render;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.vecmath.Matrix4f;

import org.apache.commons.lang3.tuple.Pair;

import codechicken.lib.render.item.IItemRenderer;
import codechicken.lib.render.state.GlStateManagerHelper;
import codechicken.lib.util.TransformUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IPerspectiveAwareModel;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModItems;

/**
 * @author p455w0rd
 *
 */
@SideOnly(Side.CLIENT)
public class CompressorItemRenderer implements IItemRenderer, IPerspectiveAwareModel {

	private static final CompressorItemRenderer INSTANCE = new CompressorItemRenderer();
	public static final ResourceLocation TEXTURE_BEACON_BEAM = new ResourceLocation("textures/entity/beacon_beam.png");

	public static CompressorItemRenderer getInstance() {
		return INSTANCE;
	}

	@Override
	public void renderItem(ItemStack item) {
		GlStateManagerHelper.pushState();
		ItemStack newItem = new ItemStack(ModItems.COMPRESSOR);
		IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(newItem);
		RenderModel.render(model, newItem);

		renderLightRays();
		GlStateManager.disableDepth();
		GlStateManager.enableBlend();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		GlStateManagerHelper.popState();
	}

	private void bindTexture(ResourceLocation texture) {
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
	}

	private void renderLightRays() {
		float partialTicks = Minecraft.getMinecraft().getRenderPartialTicks();
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		//RenderHelper.disableStandardItemLighting();
		float f = (ModGlobals.TIME + (float) Math.PI * partialTicks) / 1000.0F;
		float f1 = 0.0F;

		if (f > 0.8F) {
			f1 = (f - 0.8F) / 0.2F;
		}
		float f4 = (f + f * f) + 6 / 2.0F * 12.0F;
		if (f4 > 36.5F) {
			//f4 = 8.5F;
		}
		//System.out.println("" + f4);
		Random random = new Random(432L);
		GlStateManager.disableTexture2D();
		GlStateManager.shadeModel(7425);
		GlStateManager.enableBlend();
		//GlStateManager.alphaFunc(516, 0.1F);
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE);
		//GlStateManager.disableAlpha();
		//GlStateManager.enableCull();

		GlStateManager.depthMask(false);
		GlStateManager.pushAttrib();
		GlStateManager.pushMatrix();

		//GlStateManager.disableDepth();
		//GlStateManager.translate(0.0F, -1.0F, -2.0F);

		GlStateManager.translate(0.5, 0.5, 0.5);

		for (int i = 6; i < f4; ++i) {
			GlStateManager.rotate(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
			GlStateManager.rotate(random.nextFloat() * 360.0F + f * 90.0F, 0.0F, 0.0F, 1.0F);

			float f2 = random.nextFloat() * 10.0F + 5.0F + (f1 / 2) * 10.0F;
			float f3 = random.nextFloat() * 2.0F + 1.0F + (f1 / 2) * 2.0F;
			f2 /= 20;
			f3 /= 20;
			if (f2 > 0.8F) {
				f2 = 0.8F;
			}
			if (f3 > 0.2F) {
				f3 = 0.2F;
			}

			/*
			float f2 = random.nextFloat() * 20.0F + 5.0F + f1 * 10.0F;
			float f3 = random.nextFloat() * 2.0F + 1.0F + f1 * 2.0F;
			*/
			vertexbuffer.begin(6, DefaultVertexFormats.POSITION_COLOR);

			vertexbuffer.pos(0.0D, 0.0D, 0.0D).color(0, 255, 0, (int) (255.0F)).endVertex();

			//vertexbuffer.pos(0.0D, 0.0D, 0.0D).color(0, 255, 0, (int)(255.0F * (1.0F - f1))).endVertex();

			vertexbuffer.pos(-0.866D * f3, f2, -0.5F * f3).color(0, 0, 0, 0).endVertex();
			vertexbuffer.pos(0.866D * f3, f2, -0.5F * f3).color(0, 0, 0, 0).endVertex();
			vertexbuffer.pos(0.0D, f2, 1.0F * f3).color(0, 0, 0, 0).endVertex();
			vertexbuffer.pos(-0.866D * f3, f2, -0.5F * f3).color(0, 0, 0, 0).endVertex();
			/*
			vertexbuffer.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(255, 0, 255, 0).endVertex();
			vertexbuffer.pos(0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(255, 0, 255, 0).endVertex();
			vertexbuffer.pos(0.0D, (double)f2, (double)(1.0F * f3)).color(255, 0, 255, 0).endVertex();
			vertexbuffer.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(255, 0, 255, 0).endVertex();
			*/
			tessellator.draw();
		}
		//GlStateManager.enableDepth();
		GlStateManager.popMatrix();
		GlStateManager.popAttrib();

		GlStateManager.depthMask(true);
		//GlStateManager.disableCull();
		GlStateManager.disableBlend();
		GlStateManager.shadeModel(7424);
		//GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.enableTexture2D();
		//RenderHelper.enableStandardItemLighting();
	}

	@Override
	public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
		return MapWrapper.handlePerspective(this, TransformUtils.DEFAULT_BLOCK.getTransforms(), cameraTransformType);
	}

	@Override
	public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
		return new ArrayList<BakedQuad>();
	}

	@Override
	public boolean isAmbientOcclusion() {
		return false;
	}

	@Override
	public boolean isGui3d() {
		return false;
	}

	@Override
	public boolean isBuiltInRenderer() {
		return true;
	}

	@Override
	public TextureAtlasSprite getParticleTexture() {
		return null;
	}

	@Override
	public ItemCameraTransforms getItemCameraTransforms() {
		return ItemCameraTransforms.DEFAULT;
	}

	@Override
	public ItemOverrideList getOverrides() {
		return ItemOverrideList.NONE;
	}

}
