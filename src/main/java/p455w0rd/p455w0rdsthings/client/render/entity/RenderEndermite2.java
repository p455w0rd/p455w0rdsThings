/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.client.render.entity;

import net.minecraft.client.model.ModelEnderMite;
import net.minecraft.client.renderer.entity.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.*;
import p455w0rd.p455w0rdsthings.entity.EntityEndermite2;
import p455w0rd.p455w0rdsthings.init.ModGlobals;

/**
 * @author p455w0rd
 *
 */
@SideOnly(Side.CLIENT)
public class RenderEndermite2 extends RenderLiving<EntityEndermite2> {
	private static final ResourceLocation ENDERMITE_TEXTURES = new ResourceLocation(ModGlobals.MODID, "textures/entity/endermite2.png");

	public RenderEndermite2(RenderManager renderManagerIn) {
		super(renderManagerIn, new ModelEnderMite(), 0.3F);
	}

	@Override
	protected float getDeathMaxRotation(EntityEndermite2 entityLivingBaseIn) {
		return 180.0F;
	}

	/**
	 * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
	 */
	@Override
	protected ResourceLocation getEntityTexture(EntityEndermite2 entity) {
		return ENDERMITE_TEXTURES;
	}
}