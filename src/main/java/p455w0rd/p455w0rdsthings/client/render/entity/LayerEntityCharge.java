package p455w0rd.p455w0rdsthings.client.render.entity;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.util.ResourceLocation;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModRegistries;

/**
 * @author p455w0rd
 *
 */
public class LayerEntityCharge implements LayerRenderer<EntityLivingBase> {

	private static final ResourceLocation LIGHTNING_TEXTURE = new ResourceLocation(ModGlobals.MODID, "textures/entity/charge_nocolor.png");
	private final RenderLivingBase<EntityLivingBase> entityRenderer;
	private final ModelBase entityModel;

	public LayerEntityCharge(RenderLivingBase<EntityLivingBase> rendererIn, ModelBase modelIn) {
		entityRenderer = rendererIn;
		entityModel = modelIn;
	}

	@Override
	public void doRenderLayer(EntityLivingBase entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		ModRegistries.updateChargedList();
		if (ModRegistries.isEntityCharged(entitylivingbaseIn)) {
			boolean flag = !entitylivingbaseIn.isInvisible();
			GlStateManager.depthMask(!flag);
			entityRenderer.bindTexture(LIGHTNING_TEXTURE);
			GlStateManager.matrixMode(5890);
			GlStateManager.loadIdentity();
			float f = entitylivingbaseIn.ticksExisted + partialTicks;
			//GlStateManager.translate(0.5F, 0.5F, 0.5F);
			GlStateManager.translate(f * 0.01F, f * 0.01F, 0.0F);
			GlStateManager.matrixMode(5888);
			GlStateManager.enableBlend();
			//float f1 = 0.5F;
			float r = ((float) ModGlobals.RED + 1) / 256F;
			float g = ((float) ModGlobals.GREEN + 1) / 256F;
			float b = ((float) ModGlobals.BLUE + 1) / 256F;
			//GlStateManager.color(0.5F, 0.5F, 0.5F, 1.0F);
			GlStateManager.color(r, g, b, 1.0F);
			GlStateManager.disableLighting();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
			if (entitylivingbaseIn instanceof EntitySlime) {
				GlStateManager.scale(1.3F, 1.5F, 1.3F);
				GlStateManager.translate(0.0F, -0.4F, 0.0F);
			}
			else {
				GlStateManager.scale(1.1F, 1.1F, 1.1F);
			}
			entityModel.setModelAttributes(entityRenderer.getMainModel());
			entityModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
			//GlStateManager.translate(f / 0.01F, f / 0.01F, 0.0F);
			GlStateManager.translate(-0.5F, -0.5F, -0.5F);
			GlStateManager.matrixMode(5890);
			GlStateManager.loadIdentity();
			GlStateManager.matrixMode(5888);
			GlStateManager.enableLighting();
			GlStateManager.disableBlend();
			GlStateManager.depthMask(flag);
		}
	}

	@Override
	public boolean shouldCombineTextures() {
		return true;
	}

}
