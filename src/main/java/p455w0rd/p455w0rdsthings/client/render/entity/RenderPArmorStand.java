package p455w0rd.p455w0rdsthings.client.render.entity;

import net.minecraft.client.model.ModelArmorStandArmor;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.client.renderer.entity.layers.LayerCustomHead;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.client.model.ModelPArmorStand;
import p455w0rd.p455w0rdsthings.entity.EntityPArmorStand;
import p455w0rdslib.util.EasyMappings;

@SideOnly(Side.CLIENT)
public class RenderPArmorStand extends RenderLivingBase<EntityPArmorStand> {
	public static final ResourceLocation TEXTURE_ARMOR_STAND = new ResourceLocation("p455w0rdsthings", "textures/entity/armorstand/metal.png");

	public RenderPArmorStand(RenderManager manager) {
		super(manager, new ModelPArmorStand(), 0.0F);
		LayerBipedArmor layerbipedarmor = new LayerBipedArmor(this) {
			@Override
			protected void initArmor() {
				modelLeggings = new ModelArmorStandArmor(0.5F);
				modelArmor = new ModelArmorStandArmor(1.0F);
			}
		};
		addLayer(layerbipedarmor);
		addLayer(new LayerHeldItem(this));
		addLayer(new LayerCustomHead(getMainModel().bipedHead));
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityPArmorStand entity) {
		return TEXTURE_ARMOR_STAND;
	}

	@Override
	public ModelPArmorStand getMainModel() {
		return (ModelPArmorStand) super.getMainModel();
	}

	@Override
	protected void applyRotations(EntityPArmorStand entityLiving, float p_77043_2_, float p_77043_3_, float partialTicks) {
		if (EasyMappings.world(entityLiving) == null) {
			return;
		}
		GlStateManager.rotate(180.0F - p_77043_3_, 0.0F, 1.0F, 0.0F);
		float f = EasyMappings.world(entityLiving).getTotalWorldTime() - entityLiving.punchCooldown + partialTicks;
		if (f < 5.0F) {
			GlStateManager.rotate(MathHelper.sin(f / 1.5F * 3.1415927F) * 3.0F, 0.0F, 1.0F, 0.0F);
		}
	}

	@Override
	protected boolean canRenderName(EntityPArmorStand entity) {
		return entity.getAlwaysRenderNameTag();
	}

	@Override
	public void doRender(EntityPArmorStand entity, double x, double y, double z, float entityYaw, float partialTicks) {
		if (entity.hasMarker()) {
			renderMarker = true;
		}
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		if (entity.hasMarker()) {
			renderMarker = false;
		}
	}
}
