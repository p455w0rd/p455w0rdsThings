package p455w0rd.p455w0rdsthings.client.render;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.vecmath.Matrix4f;

import org.apache.commons.lang3.tuple.Pair;
import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;

import codechicken.lib.render.item.IItemRenderer;
import codechicken.lib.util.TransformUtils;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IPerspectiveAwareModel;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger;
import p455w0rd.p455w0rdsthings.client.model.ModelInterchanger;
import p455w0rd.p455w0rdsthings.init.ModBlocks;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.GuiUtils;

/**
 * @author p455w0rd
 *
 */
@SideOnly(Side.CLIENT)
public class TESRInterchanger extends TileEntitySpecialRenderer<TileInterchanger> implements IItemRenderer, IPerspectiveAwareModel {

	public static final ResourceLocation INTERCHANGER_TEXTURE = new ResourceLocation(ModGlobals.MODID, "textures/models/interchanger.png");
	public static final ResourceLocation INTERCHANGER_SPRITE = new ResourceLocation(ModGlobals.MODID, "models/interchanger_sprite");
	public static final ModelInterchanger MODEL = new ModelInterchanger();
	private static final ResourceLocation END_SKY_TEXTURE = new ResourceLocation("textures/environment/end_sky.png");
	private static final ResourceLocation END_PORTAL_TEXTURE = new ResourceLocation("textures/entity/end_portal.png");
	private static final Random RANDOM = new Random(31100L);
	FloatBuffer buffer = GLAllocation.createDirectFloatBuffer(16);
	protected boolean hasBlending;
	protected boolean hasLight;

	@Override
	public void renderItem(ItemStack item) {
		if (Block.getBlockFromItem(item.getItem()) == ModBlocks.INTERCHANGER) {

			//GlStateManagerHelper.pushState();
			GlStateManager.pushMatrix();
			//GlStateManager.translate(-0.5f, -0.5f, 0.0f);
			renderPortal(0, 0 + 0.1d, 0);
			//GlStateManager.translate(0.5f, 0.5f, 0.5f);
			GlStateManager.popMatrix();
			GuiUtils.bindTexture(INTERCHANGER_TEXTURE);
			GlStateManager.enableLighting();
			GlStateManager.rotate(180, 0, 0, 180);
			GlStateManager.translate(-0.5, -1.5, 0.5);
			MODEL.render(0.0625F);
			GlStateManager.translate(0.5, 1.5, -0.5);
			GlStateManager.disableLighting();
			GlStateManager.disableDepth();
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			renderChannelCode(item, -1.0, -1.85, 0);
			//GlStateManagerHelper.popState();
		}
	}

	@Override
	public void renderTileEntityAt(TileInterchanger te, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.enableDepth();
		GlStateManager.depthFunc(515);
		GlStateManager.depthMask(true);

		if (destroyStage >= 0) {
			bindTexture(DESTROY_STAGES[destroyStage]);
			GlStateManager.matrixMode(5890);
			GlStateManager.pushMatrix();
			GlStateManager.scale(4.0F, 4.0F, 1.0F);
			GlStateManager.translate(0.0625F, 0.0625F, 0.0625F);
			GlStateManager.matrixMode(5888);
		}
		else {
			GuiUtils.bindTexture(INTERCHANGER_TEXTURE);
		}
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.translate((float) x, (float) y + 1.0F, (float) z + 1.0F);
		GlStateManager.scale(1.0F, -1.0F, -1.0F);

		GlStateManager.translate(0.5F, -0.5F, 0.5F);

		if (destroyStage < 0) {
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		}

		MODEL.render(0.0625F);

		GlStateManager.translate(-0.5F, 0.5F, -0.5F);

		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

		if (destroyStage >= 0) {
			GlStateManager.matrixMode(5890);
			GlStateManager.popMatrix();
			GlStateManager.matrixMode(5888);
		}
		if (te.isRedstoneRequirementMet()) {
			renderPortal(x, y + 0.1d, z);
		}
		renderChannelCode(te, x, y, z);

	}

	protected void saveGLState() {
		hasBlending = GL11.glGetBoolean(GL11.GL_BLEND);
		hasLight = GL11.glGetBoolean(GL11.GL_LIGHTING);
		GL11.glPushAttrib(GL11.GL_CURRENT_BIT);
		GL11.glPushMatrix();
	}

	protected void loadGLState() {
		if (hasBlending) {
			GL11.glEnable(GL11.GL_BLEND);
		}
		else {
			GL11.glDisable(GL11.GL_BLEND);
		}
		if (hasLight) {
			GL11.glEnable(GL11.GL_LIGHTING);
		}
		else {
			GL11.glDisable(GL11.GL_LIGHTING);
		}
		GL11.glPopMatrix();
		GL11.glPopAttrib();
	}

	public void renderChannelCode(ItemStack stack, double x, double y, double z) {
		List<ItemStack> channelID = Lists.newArrayList();
		if (stack.hasTagCompound() && stack.getTagCompound().hasKey("BlockEntityTag")) {
			NBTTagCompound tag = stack.getTagCompound().getCompoundTag("BlockEntityTag").getCompoundTag(TileInterchanger.TAG_NAME);
			NBTTagList list = tag.getTagList(TileInterchanger.TAG_CHANNELID, 10);
			if (tag == null || list == null || list.hasNoTags() || list.tagCount() < 6) {
				channelID = TileInterchanger.getDefaultChannelID();
			}
			else {
				for (int i = 0; i < list.tagCount(); i++) {
					channelID.add(ItemStack.loadItemStackFromNBT(list.getCompoundTagAt(i)));
				}
			}
		}
		else {
			channelID = TileInterchanger.getDefaultChannelID();
		}

		int[] angle = new int[] {
				45,
				-45,
				-135,
				45
		};
		for (int h = 0; h < 4; ++h) {
			for (int i = 0; i < channelID.size(); ++i) {

				saveGLState();
				if (h < 2) {
					GlStateManager.translate(h % 2 == 0 ? (float) x - 0.215 : x - 0.025, h % 2 == 0 ? y + 0.965 : (float) y + 1, h % 2 == 0 ? (float) z : (float) z - 0.02);
				}
				else {
					GlStateManager.translate(h % 2 == 0 ? (float) x + 1.215 : x + 1.0, (float) y + 0.975, h % 2 == 0 ? (float) z + 0.9 : (float) z + 1.02);
				}
				GlStateManager.scale(0.065D, 0.065D, 0.065D);
				GlStateManager.enableDepth();
				GlStateManager.enableLighting();
				GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				GlStateManager.rotate(angle[h], h % 2 == 0 ? 1 : 0, 0, h % 2 == 0 ? 0 : 1);
				if (h < 2) {
					GlStateManager.translate(h % 2 != 0 ? 1.5 : -((i - 10) + (i - 6)), 0, h % 2 != 0 ? -((i - 10) + (i - 3)) : 1);
				}
				else {
					GlStateManager.translate(-(h % 2 != 0 ? 1 : -((i - 10) + (i - 6))), 0, -(h % 2 != 0 ? -((i - 10) + (i - 3)) : 1));
				}
				Minecraft.getMinecraft().getItemRenderer().renderItem(EasyMappings.player(), channelID.get(h % 2 == 0 ? i : 5 - i), ItemCameraTransforms.TransformType.NONE);
				//GlStateManager.disableLighting();
				GlStateManager.disableBlend();
				GlStateManager.disableDepth();
				GlStateManager.enableLighting();
				//GlStateManager.enableTexture2D();
				loadGLState();

			}
		}
	}

	public void renderChannelCode(TileInterchanger tile, double x, double y, double z) {
		List<ItemStack> channelID = tile.getChannelID();
		int[] angle = new int[] {
				45,
				-45,
				-135,
				45
		};
		for (int h = 0; h < 4; ++h) {
			for (int i = 0; i < channelID.size(); ++i) {
				GlStateManager.pushMatrix();
				if (h < 2) {
					GlStateManager.translate(h % 2 == 0 ? (float) x - 0.215 : x - 0.025, h % 2 == 0 ? y + 0.965 : (float) y + 1, h % 2 == 0 ? (float) z : (float) z - 0.02);
				}
				else {
					GlStateManager.translate(h % 2 == 0 ? (float) x + 1.215 : x + 1.0, (float) y + 0.975, h % 2 == 0 ? (float) z + 0.9 : (float) z + 1.02);
				}
				GlStateManager.scale(0.065D, 0.065D, 0.065D);
				GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				GlStateManager.rotate(angle[h], h % 2 == 0 ? 1 : 0, 0, h % 2 == 0 ? 0 : 1);
				if (h < 2) {
					GlStateManager.translate(h % 2 != 0 ? 1.5 : -((i - 10) + (i - 6)), 0, h % 2 != 0 ? -((i - 10) + (i - 3)) : 1);
				}
				else {
					GlStateManager.translate(-(h % 2 != 0 ? 1 : -((i - 10) + (i - 6))), 0, -(h % 2 != 0 ? -((i - 10) + (i - 3)) : 1));
				}
				Minecraft.getMinecraft().getItemRenderer().renderItem(EasyMappings.player(), channelID.get(h % 2 == 0 ? i : 5 - i), ItemCameraTransforms.TransformType.NONE);
				//GlStateManager.translate(h % 2 == 0 ? 1 + i : -0.5 + i, 0, h % 2 == 0 ? -0.5 + i : 1 + i);

				GlStateManager.popMatrix();
			}
		}
	}

	public void renderPortal(double x, double y, double z) {
		float f = (float) x;
		float f1 = (float) y;
		float f2 = (float) z;
		if (rendererDispatcher != null) {
			f = (float) rendererDispatcher.entityX;
			f1 = (float) rendererDispatcher.entityY;
			f2 = (float) rendererDispatcher.entityZ;
		}
		GlStateManager.disableLighting();
		GlStateManager.disableCull();
		RANDOM.setSeed(31100L);

		for (int i = 0; i < 16; ++i) {
			GlStateManager.pushMatrix();
			float f4 = 16 - i;
			float f5 = 0.0625F;
			float f6 = 1.0F / (f4 + 1.0F);

			if (i == 0) {
				GuiUtils.bindTexture(END_PORTAL_TEXTURE);
				f6 = 0.1F;
				f4 = 65.0F;
				f5 = 0.5F;
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			}

			if (i >= 1) {
				GuiUtils.bindTexture(END_PORTAL_TEXTURE);
			}

			if (i == 1) {
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
				f5 = 0.5F;
			}

			float f7 = (float) (+(y + 0.75D));
			float f8 = f7 + (float) ActiveRenderInfo.getPosition().yCoord;
			float f9 = f7 + f4 + (float) ActiveRenderInfo.getPosition().yCoord;
			float f10 = f8 / f9;
			f10 = (float) (y + 0.75D) + f10;
			GlStateManager.translate(f, f10, f2);
			GlStateManager.texGen(GlStateManager.TexGen.S, 9217);
			GlStateManager.texGen(GlStateManager.TexGen.T, 9217);
			GlStateManager.texGen(GlStateManager.TexGen.R, 9217);
			GlStateManager.texGen(GlStateManager.TexGen.Q, 9216);
			GlStateManager.texGen(GlStateManager.TexGen.S, 9473, getBuffer(1.0F, 0.0F, 0.0F, 0.0F));
			GlStateManager.texGen(GlStateManager.TexGen.T, 9473, getBuffer(0.0F, 0.0F, 1.0F, 0.0F));
			GlStateManager.texGen(GlStateManager.TexGen.R, 9473, getBuffer(0.0F, 0.0F, 0.0F, 1.0F));
			GlStateManager.texGen(GlStateManager.TexGen.Q, 9474, getBuffer(0.0F, 1.0F, 0.0F, 0.0F));
			GlStateManager.enableTexGenCoord(GlStateManager.TexGen.S);
			GlStateManager.enableTexGenCoord(GlStateManager.TexGen.T);
			GlStateManager.enableTexGenCoord(GlStateManager.TexGen.R);
			GlStateManager.enableTexGenCoord(GlStateManager.TexGen.Q);
			GlStateManager.popMatrix();
			GlStateManager.matrixMode(5890);
			GlStateManager.pushMatrix();
			GlStateManager.loadIdentity();
			GlStateManager.translate(0.0F, (Minecraft.getSystemTime() % 700000L / 700000.0F) * 25, 0.0F);
			GlStateManager.scale(f5, f5, f5);
			GlStateManager.translate(0.5F, 0.5F, 0.0F);
			GlStateManager.rotate((i * i * 4321 + i * 9) * 2.0F, 0.0F, 0.0F, 1.0F);
			GlStateManager.translate(-0.5F, -0.5F, 0.0F);
			GlStateManager.translate(-f, -f2, -f1);
			f8 = f7 + (float) ActiveRenderInfo.getPosition().yCoord;
			GlStateManager.translate((float) ActiveRenderInfo.getPosition().xCoord * f4 / f8, (float) ActiveRenderInfo.getPosition().zCoord * f4 / f8, -f1);
			Tessellator tessellator = Tessellator.getInstance();
			VertexBuffer vertexbuffer = tessellator.getBuffer();
			vertexbuffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
			float f11 = (RANDOM.nextFloat() * 0.001F) * f6 + 0.85F;
			float f12 = (RANDOM.nextFloat() * 0.9F) * f6;
			float f13 = (RANDOM.nextFloat() * 0.001F) * f6 + 0.85F;
			vertexbuffer.pos(x + 0.15D, y + 0.78D, z + 0.15D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.15D, y + 0.78D, z + 0.85D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.85D, y + 0.78D, z + 0.85D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.85D, y + 0.78D, z + 0.15D).color(f11, f12, f13, 1.0F).endVertex();

			vertexbuffer.pos(x + 0.05D, y, z + 0.05D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.05D, y, z + 0.95D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.95D, y, z + 0.95D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.95D, y, z + 0.05D).color(f11, f12, f13, 1.0F).endVertex();

			vertexbuffer.pos(x + 0.1D, y + 0.01D, z + 0.125D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.1D, y + 0.01D, z + 0.875D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.1D, y + 0.85D, z + 0.875D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.1D, y + 0.85D, z + 0.125D).color(f11, f12, f13, 1.0F).endVertex();

			vertexbuffer.pos(x + 0.125D, y + 0.01D, z + 0.1D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.875D, y + 0.01D, z + 0.1D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.875D, y + 0.85D, z + 0.1D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.125D, y + 0.85D, z + 0.1D).color(f11, f12, f13, 1.0F).endVertex();

			vertexbuffer.pos(x + 0.9D, y + 0.01D, z + 0.125D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.9D, y + 0.01D, z + 0.875D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.9D, y + 0.85D, z + 0.875D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.9D, y + 0.85D, z + 0.125D).color(f11, f12, f13, 1.0F).endVertex();

			vertexbuffer.pos(x + 0.125D, y + 0.01D, z + 0.9D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.875D, y + 0.01D, z + 0.9D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.875D, y + 0.85D, z + 0.9D).color(f11, f12, f13, 1.0F).endVertex();
			vertexbuffer.pos(x + 0.125D, y + 0.85D, z + 0.9D).color(f11, f12, f13, 1.0F).endVertex();
			tessellator.draw();
			GlStateManager.popMatrix();
			GlStateManager.matrixMode(5888);
			GuiUtils.bindTexture(END_SKY_TEXTURE);
		}

		GlStateManager.disableBlend();
		GlStateManager.disableTexGenCoord(GlStateManager.TexGen.S);
		GlStateManager.disableTexGenCoord(GlStateManager.TexGen.T);
		GlStateManager.disableTexGenCoord(GlStateManager.TexGen.R);
		GlStateManager.disableTexGenCoord(GlStateManager.TexGen.Q);
		GlStateManager.enableLighting();
	}

	private FloatBuffer getBuffer(float p_147525_1_, float p_147525_2_, float p_147525_3_, float p_147525_4_) {
		buffer.clear();
		buffer.put(p_147525_1_).put(p_147525_2_).put(p_147525_3_).put(p_147525_4_);
		buffer.flip();
		return buffer;
	}

	@Override
	public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
		return MapWrapper.handlePerspective(this, TransformUtils.DEFAULT_BLOCK.getTransforms(), cameraTransformType);
	}

	@Override
	public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
		return new ArrayList<BakedQuad>();
	}

	@Override
	public boolean isAmbientOcclusion() {
		return false;
	}

	@Override
	public boolean isGui3d() {
		return false;
	}

	@Override
	public boolean isBuiltInRenderer() {
		return true;
	}

	@Override
	public TextureAtlasSprite getParticleTexture() {
		return Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(INTERCHANGER_SPRITE.toString());
	}

	@Override
	public ItemCameraTransforms getItemCameraTransforms() {
		return ItemCameraTransforms.DEFAULT;
	}

	@Override
	public ItemOverrideList getOverrides() {
		return ItemOverrideList.NONE;
	}

}
