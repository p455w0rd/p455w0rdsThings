package p455w0rd.p455w0rdsthings.client.render;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.client.model.pipeline.LightUtil;
import net.minecraftforge.client.model.pipeline.QuadGatheringTransformer;

public class RenderModel {
	public static void render(IBakedModel model, ItemStack stack) {
		render(model, -1, stack);
	}

	public static void render(IBakedModel model, int color) {
		render(model, color, (ItemStack) null);
	}

	public static void render(IBakedModel model, int color, ItemStack stack) {
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		vertexbuffer.begin(7, DefaultVertexFormats.ITEM);
		for (EnumFacing enumfacing : EnumFacing.values()) {
			renderQuads(vertexbuffer, model.getQuads((IBlockState) null, enumfacing, 0L), color, stack);
		}
		renderQuads(vertexbuffer, model.getQuads((IBlockState) null, (EnumFacing) null, 0L), color, stack);
		tessellator.draw();
	}

	public static void renderQuads(VertexBuffer renderer, List<BakedQuad> quads, int color, ItemStack stack) {
		boolean flag = (color == -1) && (stack != null);
		int i = 0;
		for (int j = quads.size(); i < j; i++) {
			BakedQuad bakedquad = quads.get(i);
			int k = color;
			if ((flag) && (bakedquad.hasTintIndex())) {
				ItemColors itemColors = Minecraft.getMinecraft().getItemColors();
				k = itemColors.getColorFromItemstack(stack, bakedquad.getTintIndex());
				if (EntityRenderer.anaglyphEnable) {
					k = TextureUtil.anaglyphColor(k);
				}
				k |= 0xFF000000;
			}
			LightUtil.renderQuadColor(renderer, bakedquad, k);
		}
	}

	// =========

	public static void render(ModelPlayer model, int color, EntityPlayer player, float partialTicks, boolean multiPass) {
		RenderManager rm = Minecraft.getMinecraft().getRenderManager();
		rm.renderEntityStatic(player, partialTicks, false);
		//rm.doRenderEntity(player, player.posX, player.posY, player.posZ, player.cameraYaw, partialTicks, multiPass);
	}

	/**
	 * @author tterrag
	 */
	public static void renderModelColored(IBakedModel model, int color, ItemStack stack) {
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer bufferbuilder = tessellator.getBuffer();

		List<BakedQuad> allquads = new ArrayList<>();

		for (EnumFacing enumfacing : EnumFacing.values()) {
			allquads.addAll(model.getQuads((IBlockState) null, enumfacing, 0L));
		}

		allquads.addAll(model.getQuads((IBlockState) null, (EnumFacing) null, 0L));

		Deque<Pair<List<BakedQuad>, int[]>> segmentedQuads = new ArrayDeque<>();

		for (BakedQuad q : allquads) {
			Pair<List<BakedQuad>, int[]> tail = segmentedQuads.peekLast();
			int[] light = {
					0,
					0
			};
			if (q.getFormat() != DefaultVertexFormats.ITEM && q.getFormat().getElements().contains(DefaultVertexFormats.TEX_2S)) {
				int lmapIndex = q.getFormat().getElements().indexOf(DefaultVertexFormats.TEX_2S);

				QuadGatheringTransformer qgt = new QuadGatheringTransformer() {

					@Override
					public void setTexture(TextureAtlasSprite texture) {
					}

					@Override
					public void setQuadTint(int tint) {
					}

					@Override
					public void setQuadOrientation(EnumFacing orientation) {
					}

					@Override
					public void setApplyDiffuseLighting(boolean diffuse) {
					}

					@Override
					protected void processQuad() {
						int[] totalLight = new int[2];
						for (int i = 0; i < 4; i++) {
							float blight = (quadData[lmapIndex][i][0] * 0xFFFF) / 0x20;
							float slight = (quadData[lmapIndex][i][1] * 0xFFFF) / 0x20;
							totalLight[0] += (int) blight;
							totalLight[1] += (int) slight;
						}
						light[0] = totalLight[0] / 4;
						light[1] = totalLight[1] / 4;
					}
				};
				qgt.setVertexFormat(q.getFormat());
				q.pipe(qgt);
			}

			if (tail == null || !Arrays.equals(light, tail.getValue())) {
				segmentedQuads.add(Pair.of(Lists.newArrayList(q), light));
			}
			else {
				tail.getLeft().add(q);
			}
		}

		for (Pair<List<BakedQuad>, int[]> segment : segmentedQuads) {
			bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM);

			float emissive = segment.getRight()[1] / 15f;

			GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, RenderHelper.setColorBuffer(emissive, emissive, emissive, 1));

			float bl = segment.getRight()[0] * 16, sl = segment.getRight()[1] * 16;
			float lastBl = OpenGlHelper.lastBrightnessX, lastSl = OpenGlHelper.lastBrightnessY;
			if (bl > lastBl && sl > lastSl) {
				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, bl, sl);
			}
			else if (bl > lastBl) {
				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, bl, lastSl);
			}
			else if (sl > lastSl) {
				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lastBl, sl);
			}

			renderQuads(bufferbuilder, segment.getLeft(), color, stack);
			tessellator.draw();

			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lastBl, lastSl);

			GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, RenderHelper.setColorBuffer(0, 0, 0, 1));
		}
	}

}
