package p455w0rd.p455w0rdsthings.client.render;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4f;

import org.apache.commons.lang3.tuple.Pair;

import codechicken.lib.model.SimpleBakedItemModel;
import codechicken.lib.render.item.IItemRenderer;
import codechicken.lib.util.TransformUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IPerspectiveAwareModel;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.items.ItemMagnet;

/**
 * @author p455w0rd
 *
 */
@SideOnly(Side.CLIENT)
public class MagnetItemRenderer implements IItemRenderer, IPerspectiveAwareModel {

	public static MagnetItemRenderer INSTANCE = new MagnetItemRenderer();

	public static MagnetItemRenderer getInstance() {
		return INSTANCE;
	}

	@Override
	public void renderItem(ItemStack item) {
		if (!(item.getItem() instanceof ItemMagnet)) {
			return;
		}

		String internalName = item.getItem().getRegistryName().getResourcePath();
		IBakedModel model = new SimpleBakedItemModel(new ResourceLocation(ModGlobals.MODID, "items/" + internalName));

		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.enableLighting();
		GlStateManager.enableBlend();
		GlStateManager.enableRescaleNormal();
		if (item.isOnItemFrame()) {
			GlStateManager.scale(1.25D, 1.25D, 1.25D);
			GlStateManager.translate(-0.1D, -0.1D, -0.25D);
		}

		GlStateManager.pushMatrix();

		RenderModel.render(model, item);
		if (item.getItemDamage() > 0) {
			GlintEffectRenderer.apply(model, ((ItemMagnet) item.getItem()).tempDisable ? 6 : 7);
		}

		GlStateManager.popMatrix();

		GlStateManager.disableRescaleNormal();
	}

	@Override
	public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
		return new ArrayList<BakedQuad>();
	}

	@Override
	public boolean isAmbientOcclusion() {
		return false;
	}

	@Override
	public boolean isGui3d() {
		return false;
	}

	@Override
	public boolean isBuiltInRenderer() {
		return true;
	}

	@Override
	public TextureAtlasSprite getParticleTexture() {
		return null;
	}

	@Override
	public ItemCameraTransforms getItemCameraTransforms() {
		return ItemCameraTransforms.DEFAULT;
	}

	@Override
	public ItemOverrideList getOverrides() {
		return ItemOverrideList.NONE;
	}

	@Override
	public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
		return MapWrapper.handlePerspective(this, TransformUtils.DEFAULT_ITEM.getTransforms(), cameraTransformType);
	}

}
