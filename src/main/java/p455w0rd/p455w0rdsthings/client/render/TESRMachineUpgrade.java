package p455w0rd.p455w0rdsthings.client.render;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4f;

import org.apache.commons.lang3.tuple.Pair;

import codechicken.lib.render.item.IItemRenderer;
import codechicken.lib.render.state.GlStateManagerHelper;
import codechicken.lib.util.TransformUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IPerspectiveAwareModel;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.IMachineInWorldUpgradable;
import p455w0rd.p455w0rdsthings.api.IMachineUpgradeBlock;
import p455w0rd.p455w0rdsthings.blocks.BlockMachineUpgrade;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineUpgrade;
import p455w0rd.p455w0rdsthings.client.model.ModelMachineUpgrade;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rdslib.util.RenderUtils;
import p455w0rdslib.util.Vector3;
import p455w0rdslib.util.WorldUtils;

/**
 * @author p455w0rd
 *
 */
@SideOnly(Side.CLIENT)
public class TESRMachineUpgrade extends TileEntitySpecialRenderer<TileMachineUpgrade> implements IItemRenderer, IPerspectiveAwareModel {

	public static final ResourceLocation MACHINEUPGRADE_TEXTURE = new ResourceLocation(ModGlobals.MODID, "textures/models/machineupgrade.png");
	public static final ResourceLocation MACHINEUPGRADE_SPRITE = new ResourceLocation(ModGlobals.MODID, "models/machineupgrade_sprite");
	public static final ModelMachineUpgrade MODEL = new ModelMachineUpgrade();
	public static final ResourceLocation SPEED_BEAM = new ResourceLocation(ModGlobals.MODID, "textures/entity/speed_beam.png");
	public static final ResourceLocation HEX_BEAM = new ResourceLocation(ModGlobals.MODID, "textures/entity/hexbeam.png");
	public static final ResourceLocation HEX_BEAM2 = new ResourceLocation(ModGlobals.MODID, "textures/entity/hexbeam2.png");
	public static final ResourceLocation BEAM_HIT = new ResourceLocation(ModGlobals.MODID, "textures/entity/glow_sphere.png");

	private TextureAtlasSprite alt_sprite = null;
	private float ticks = 0;
	private boolean rev = false;

	@Override
	public void renderItem(ItemStack item) {
		if (Block.getBlockFromItem(item.getItem()) instanceof BlockMachineUpgrade) {
			GlStateManagerHelper.pushState();
			GlStateManager.pushMatrix();
			ResourceLocation texture = MACHINEUPGRADE_TEXTURE;
			if (Block.getBlockFromItem(item.getItem()) != null && Block.getBlockFromItem(item.getItem()) instanceof IMachineUpgradeBlock) {
				IMachineUpgradeBlock block = (IMachineUpgradeBlock) Block.getBlockFromItem(item.getItem());
				if (block.getType() != null && block.getType().getTexture() != null) {
					texture = block.getType().getTexture();
				}
			}
			RenderUtils.bindTexture(texture);
			GlStateManager.enableLighting();
			GlStateManager.rotate(180, 0, 0, 180);
			GlStateManager.translate(-0.5, -1.5, 0.5);
			MODEL.render(0.0625F);
			GlStateManager.translate(0.5, 1.5, -0.5);
			//GlStateManager.disableLighting();
			GlStateManager.disableBlend();
			GlStateManager.popMatrix();
			GlStateManagerHelper.popState();
		}
	}

	@Override
	public void renderTileEntityAt(TileMachineUpgrade te, double x, double y, double z, float partialTicks, int destroyStage) {
		if (!Minecraft.getMinecraft().isGamePaused()) {
			doTick();
		}
		GlStateManager.enableDepth();
		GlStateManager.depthFunc(515);
		GlStateManager.depthMask(true);

		if (destroyStage >= 0) {
			bindTexture(DESTROY_STAGES[destroyStage]);
			GlStateManager.matrixMode(5890);
			GlStateManager.pushMatrix();
			GlStateManager.scale(4.0F, 4.0F, 1.0F);
			GlStateManager.translate(0.0625F, 0.0625F, 0.0625F);
			GlStateManager.matrixMode(5888);
		}
		else {
			ResourceLocation texture = MACHINEUPGRADE_TEXTURE;
			if (te.getWorld().getBlockState(te.getPos()).getBlock() != null && te.getWorld().getBlockState(te.getPos()).getBlock() instanceof IMachineUpgradeBlock) {
				IMachineUpgradeBlock block = (IMachineUpgradeBlock) te.getWorld().getBlockState(te.getPos()).getBlock();
				if (block.getType() != null && block.getType().getTexture() != null) {
					texture = block.getType().getTexture();
					alt_sprite = block.getType().getParticleSprite();
				}
			}
			RenderUtils.bindTexture(texture);
		}
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.enableLighting();
		GlStateManager.translate((float) x, (float) y + 1.0F, (float) z + 1.0F);
		GlStateManager.scale(1.0F, -1.0F, -1.0F);

		GlStateManager.translate(0.5F, -0.5F, 0.5F);

		if (destroyStage < 0) {
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		}
		GlStateManager.rotate(45.0F, 0, 1.0F, 0);
		MODEL.render(0.0625F);

		GlStateManager.translate(-0.5F, 0.5F, -0.5F);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

		if (destroyStage >= 0) {
			GlStateManager.matrixMode(5890);
			GlStateManager.popMatrix();
			GlStateManager.matrixMode(5888);
		}
		if (te.getEnergy() < 1000) {
			return;
		}
		if (te != null) {
			for (EnumFacing facing : EnumFacing.HORIZONTALS) {
				TileEntity currentTile = WorldUtils.getWorld().getTileEntity(te.getPos().offset(facing, 2).offset(facing.rotateY(), 2));
				if (currentTile != null && currentTile instanceof IMachineInWorldUpgradable) {
					if (WorldUtils.getWorld().getBlockState(te.getPos().offset(facing).offset(facing.rotateY())).getBlock() instanceof BlockAir) {
						boolean active = ((IMachineInWorldUpgradable) currentTile).isUpgradeActive();
						if (!active) {
							continue;
						}
						float yPos = active ? te.getPos().getY() + ticks / 1000 : te.getPos().getY() + 0.5F;
						Vector3[] endPositions = new Vector3[] {
								new Vector3(currentTile.getPos().getX() + 1, yPos, currentTile.getPos().getZ()),
								new Vector3(currentTile.getPos().getX() + 1, yPos, currentTile.getPos().getZ() + 1),
								new Vector3(currentTile.getPos().getX(), yPos, currentTile.getPos().getZ() + 1),
								new Vector3(currentTile.getPos().getX(), yPos, currentTile.getPos().getZ())
						};

						Vector3 startPos = new Vector3(te.getPos().getX() + 0.5D, te.getPos().getY() + 0.9D, te.getPos().getZ() + 0.5D);
						Vector3 endPos = new Vector3(endPositions[facing.getHorizontalIndex()]);
						RenderUtils.renderCircleBeamPoint2Point(startPos, endPos, partialTicks, 0, 255, 0, (te != null && active) ? 255 : 255, 0.035D, SPEED_BEAM);
						RenderUtils.renderBeamHit(BEAM_HIT, endPos, partialTicks, 0.125F);
					}
				}
			}
		}
	}

	private void doTick() {
		if (!rev) {
			if (ticks < 1000) {
				ticks += 8;
			}
			else {
				rev = true;
			}
		}
		else {
			if (ticks > 0) {
				ticks -= 8;
			}
			else {
				rev = false;
			}
		}
	}

	@Override
	public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
		return MapWrapper.handlePerspective(this, TransformUtils.DEFAULT_BLOCK.getTransforms(), cameraTransformType);
	}

	@Override
	public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
		return new ArrayList<BakedQuad>();
	}

	@Override
	public boolean isAmbientOcclusion() {
		return false;
	}

	@Override
	public boolean isGui3d() {
		return false;
	}

	@Override
	public boolean isBuiltInRenderer() {
		return true;
	}

	@Override
	public TextureAtlasSprite getParticleTexture() {
		return alt_sprite != null ? alt_sprite : Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(MACHINEUPGRADE_SPRITE.toString());
	}

	@Override
	public ItemCameraTransforms getItemCameraTransforms() {
		return ItemCameraTransforms.DEFAULT;
	}

	@Override
	public ItemOverrideList getOverrides() {
		return ItemOverrideList.NONE;
	}

}
