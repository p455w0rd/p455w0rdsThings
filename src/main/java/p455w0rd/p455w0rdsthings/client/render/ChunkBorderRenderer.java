package p455w0rd.p455w0rdsthings.client.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModKeyBindings;
import p455w0rdslib.util.EasyMappings;

public class ChunkBorderRenderer {

	private int chunkEdgeState = 0;

	@SubscribeEvent
	public void resetOverlay(WorldEvent.Load event) {
		chunkEdgeState = 0;
	}

	@SubscribeEvent
	public void getKeyPress(TickEvent.ClientTickEvent event) {
		if (EasyMappings.world() == null) {
			return;
		}
		if (ModKeyBindings.KEYBIND_CHUNKOVERLAY.isPressed()) {
			chunkEdgeState = (chunkEdgeState + 1) % 2;
		}
	}

	@SuppressWarnings("unused")
	@SubscribeEvent
	public void renderChunkEdges(RenderWorldLastEvent event) {
		if (chunkEdgeState != 0) {
			EntityPlayerSP entity = EasyMappings.player();
			Tessellator tessellator = Tessellator.getInstance();
			VertexBuffer worldrenderer = tessellator.getBuffer();
			GL11.glPushMatrix();
			float frame = event.getPartialTicks();
			double inChunkPosX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * frame;
			double inChunkPosY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * frame;
			double inChunkPosZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * frame;
			GL11.glTranslated((-inChunkPosX), (-inChunkPosY), (-inChunkPosZ));
			GL11.glDisable(3553);
			GL11.glEnable(3042);
			GL11.glBlendFunc(770, 771);
			GL11.glLineWidth(3.0f);
			worldrenderer.begin(1, DefaultVertexFormats.POSITION_COLOR);
			GL11.glTranslatef(entity.chunkCoordX * 16, 0.0f, entity.chunkCoordZ * 16);
			double x = 0.0;
			double z = 0.0;
			float redColourR = 0.9f;
			float redColourG = 0.0f;
			float redColourB = 0.0f;
			float redColourA = 1.0f;
			float greenColourR = 0.0f;
			float greenColourG = 0.9f;
			float greenColourB = 0.0f;
			float greenColourA = 0.4f;
			/*
			 * for (int chunkZ = -2; chunkZ <= 2; ++chunkZ) { for (int chunkX =
			 * -2; chunkX <= 2; ++chunkX) { if (Math.abs(chunkX) == 2 &&
			 * Math.abs(chunkZ) == 2) continue; x = chunkX * 16; z = chunkZ *
			 * 16; redColourA = (float)Math.round(Math.pow(1.25, - chunkX *
			 * chunkX + chunkZ * chunkZ) * 6.0) / 6.0f; worldrenderer.pos(x,
			 * 0.0, z).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x, 256.0,
			 * z).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x + 16.0, 0.0,
			 * z).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x + 16.0, 256.0,
			 * z).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x, 0.0, z +
			 * 16.0).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x, 256.0, z +
			 * 16.0).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x + 16.0, 0.0, z +
			 * 16.0).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); worldrenderer.pos(x + 16.0, 256.0, z
			 * + 16.0).color(Globals.RED, Globals.GREEN, Globals.BLUE,
			 * Globals.ALPHA).endVertex(); } }
			 */
			z = 0.0;
			x = 0.0;
			if (chunkEdgeState == 1) {
				float eyeHeight = (float) (entity.getEyeHeight() + entity.posY);
				int eyeHeightBlock = (int) Math.floor(eyeHeight);
				int minY = Math.max(0, eyeHeightBlock - 16);
				int maxY = Math.min(256, eyeHeightBlock + 16);
				boolean renderedSome = false;
				for (int y = 0; y < 257; ++y) {
					greenColourA = 0.4f;
					if (y < minY) {
						greenColourA = (float) (greenColourA - Math.pow(minY - y, 2.0) / 500.0);
					}
					if (y > maxY) {
						greenColourA = (float) (greenColourA - Math.pow(y - maxY, 2.0) / 500.0);
					}
					if (greenColourA < 0.01f) {
						if (!renderedSome) {
							continue;
						}
						break;
					}

					if (y < 256) {
						for (int n = 0; n <= 16; ++n) {
							worldrenderer.pos(n, y, z).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(n, y + 1, z).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(x, y, n).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(x, y + 1, n).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(n, y, z + 16.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(n, y + 1, z + 16.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(x + 16.0, y, n).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
							worldrenderer.pos(x + 16.0, y + 1, n).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
						}
					}

					worldrenderer.pos(0.0, y, 0.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(16.0, y, 0.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(0.0, y, 0.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(0.0, y, 16.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(16.0, y, 0.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(16.0, y, 16.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(0.0, y, 16.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					worldrenderer.pos(16.0, y, 16.0).color(ModGlobals.RED, ModGlobals.GREEN, ModGlobals.BLUE, ModGlobals.ALPHA).endVertex();
					renderedSome = true;
				}
				/*
				 * float eyeHeight = (float)((double)entity.getEyeHeight() +
				 * entity.posY); int eyeHeightBlock =
				 * (int)Math.floor(eyeHeight); int minY = Math.max(0,
				 * eyeHeightBlock - 16); int maxY = Math.min(256, eyeHeightBlock
				 * + 16); boolean renderedSome = false; for (int y = 0; y < 257;
				 * ++y) { greenColourA = 0.4f; if (y < minY) { greenColourA =
				 * (float)((double)greenColourA - Math.pow(minY - y, 2.0) /
				 * 500.0); } if (y > maxY) { greenColourA =
				 * (float)((double)greenColourA - Math.pow(y - maxY, 2.0) /
				 * 500.0); } if (greenColourA < 0.01f) { if (!renderedSome)
				 * continue; break; }
				 *
				 * if (y < 256) { for (int n = 1; n < 16; ++n) {
				 * worldrenderer.pos((double)n, (double)y,
				 * z).color(greenColourR, greenColourG, greenColourB,
				 * greenColourA).endVertex(); worldrenderer.pos((double)n,
				 * (double)(y + 1), z).color(greenColourR, greenColourG,
				 * greenColourB, greenColourA).endVertex(); worldrenderer.pos(x,
				 * (double)y, (double)n).color(greenColourR, greenColourG,
				 * greenColourB, greenColourA).endVertex(); worldrenderer.pos(x,
				 * (double)(y + 1), (double)n).color(greenColourR, greenColourG,
				 * greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos((double)n, (double)y, z +
				 * 16.0).color(greenColourR, greenColourG, greenColourB,
				 * greenColourA).endVertex(); worldrenderer.pos((double)n,
				 * (double)(y + 1), z + 16.0).color(greenColourR, greenColourG,
				 * greenColourB, greenColourA).endVertex(); worldrenderer.pos(x
				 * + 16.0, (double)y, (double)n).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(x + 16.0, (double)(y + 1),
				 * (double)n).color(greenColourR, greenColourG, greenColourB,
				 * greenColourA).endVertex(); } }
				 *
				 * worldrenderer.pos(0.0, (double)y, 0.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(16.0, (double)y, 0.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(0.0, (double)y, 0.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(0.0, (double)y, 16.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(16.0, (double)y, 0.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(16.0, (double)y, 16.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(0.0, (double)y, 16.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * worldrenderer.pos(16.0, (double)y, 16.0).color(greenColourR,
				 * greenColourG, greenColourB, greenColourA).endVertex();
				 * renderedSome = true; }
				 */
			}
			tessellator.draw();
			GL11.glPopMatrix();
			GL11.glEnable(3553);
			GL11.glDisable(3042);
		}
	}

	/*
		public static void renderChunkView(TileChunkLoader te) {
			if (getWorld().isRemote) {
				int radius = te.getRadius();
				if (((int) ModGlobals.TIME % 4) == 0) {
					ChunkPos chunkPos = ChunkUtils.getChunkPos(te.getWorld(), te.getPos());
					int chunkLeastXBlock = chunkPos.getXStart() - (16 * (radius - 1));
					int chunkLeastZBlock = chunkPos.getZStart() - (16 * (radius - 1));
					int chunkMaxXBlock = chunkPos.getXEnd() + (16 * (radius - 1)) + 1;
					int chunkMaxZBlock = chunkPos.getZEnd() + (16 * (radius - 1)) + 1;
	
					for (int y1 = -1; y1 < 16; y1++) {
						for (int x = chunkLeastXBlock; x <= chunkMaxXBlock; ++x) {
							for (int z = chunkLeastZBlock; z <= chunkMaxZBlock; ++z) {
								if ((x == chunkLeastXBlock && z >= chunkLeastZBlock) || (x >= chunkLeastXBlock && z == chunkLeastZBlock) || (x == chunkMaxXBlock && z <= chunkMaxZBlock) || (x <= chunkMaxXBlock && z == chunkMaxZBlock)) {
									double color1 = 0.6;
									double color2 = 1;
									if ((x == chunkLeastXBlock && z == chunkLeastZBlock) || (x == chunkMaxXBlock && z == chunkMaxZBlock) || (x == chunkLeastXBlock && z == chunkMaxZBlock) || (x == chunkMaxXBlock && z == chunkLeastZBlock)) {
										color1 = 0;
										color2 = 0;
									}
									getWorld().spawnParticle(EnumParticleTypes.REDSTONE, true, x, (double) (te.getPos().getY() + 1) + y1, z, color1, color2, 0, new int[0]);
								}
							}
						}
					}
				}
			}
		}
	*/

}
