/*
 * This file is part of p455w0rd's Things. Copyright (c) 2016, p455w0rd (aka
 * TheRealp455w0rd), All rights reserved unless otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify it
 * under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
 *
 * You should have received a copy of the MIT License along with p455w0rd's
 * Things. If not, see <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.client.gui;

import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rdslib.client.gui.GuiModular;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.GuiUtils;

/**
 * @author p455w0rd
 *
 */
public class GuiTrashCan extends GuiModular {

	public GuiTrashCan(Container inventorySlotsIn) {
		super(inventorySlotsIn);
		xSize = 210;
		ySize = 204;
		guiLeft = ((width - xSize) / 2);
		guiTop = ((height - ySize) / 2);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		mc.getTextureManager().bindTexture(new ResourceLocation(ModGlobals.MODID, "textures/gui/gui_trash_can.png"));
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableLighting();
		GlStateManager.disableBlend();
		int fontColor = 16777215;
		int yOffset = 123;

		mc.fontRendererObj.drawString(I18n.format("tile.trash_can.name", new Object[0]), 7, 6, 0x99A74CFF, true);

		mc.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 7, ySize - yOffset, fontColor);
		GlStateManager.enableBlend();
		GlStateManager.enableLighting();
	}

	@Override
	protected void renderToolTip(ItemStack stack, int x, int y) {
		List<String> list = stack.getTooltip(EasyMappings.player(), mc.gameSettings.advancedItemTooltips);

		for (int i = 0; i < list.size(); ++i) {
			if (i == 0) {
				list.set(i, stack.getRarity().rarityColor + list.get(i));
			}
			else {
				list.set(i, TextFormatting.GRAY + list.get(i));
			}
		}

		net.minecraftforge.fml.client.config.GuiUtils.preItemToolTip(stack);
		GuiUtils.drawToolTipWithBorderColor(this, list, x, y, 0xFF6E03C8, 0x996E03C8);
		net.minecraftforge.fml.client.config.GuiUtils.postItemToolTip();
	}

}
