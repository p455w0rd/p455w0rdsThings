package p455w0rd.p455w0rdsthings.client.gui;

import java.io.IOException;
import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileCompressor;
import p455w0rd.p455w0rdsthings.container.ContainerCompressor;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.integration.JEI;
import p455w0rdslib.client.gui.GuiModular;
import p455w0rdslib.util.EnergyUtils;
import p455w0rdslib.util.GuiUtils;
import p455w0rdslib.util.MathUtils;
import p455w0rdslib.util.ReadableNumberConverter;

/**
 * @author p455w0rd
 *
 */
public class GuiCompressor extends GuiModular {
	//201x155
	public static final ResourceLocation BACKGROUND = new ResourceLocation(ModGlobals.MODID, "textures/gui/compressor.png");
	private int CAPACITY = 0;
	private int ENERGY = 0;
	private int PERCENT_COMPLETE = 0;

	public GuiCompressor(EntityPlayer player, TileCompressor tile) {
		super(new ContainerCompressor(player, tile));
		xSize = 201;
		ySize = 155;
		setTitle("Compressor");
		setTitleColor(0x17FF6D);
		setBackgroundTexture(BACKGROUND);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableLighting();
		GlStateManager.disableBlend();
		if (ENERGY > 0 && CAPACITY > 0) {
			drawEnergyBar();
		}
		if (PERCENT_COMPLETE > 0) {
			drawProgressBar();
		}
		if (Mods.JEI.isLoaded()) {
			GuiUtils.drawItem(new ItemStack(Blocks.CRAFTING_TABLE), 93, 42);
		}
		GlStateManager.enableBlend();
		GlStateManager.enableLighting();
	}

	private void drawEnergyBar() {
		GuiUtils.bindTexture(BACKGROUND);
		drawTexturedModalRect(9, 21 + 37 - MathUtils.ceil(((float) MathUtils.ceil(MathUtils.getPercent(ENERGY, CAPACITY)) / 100) * 37), 248, 0, 8, MathUtils.ceil(((float) MathUtils.ceil(MathUtils.getPercent(ENERGY, CAPACITY)) / 100) * 37));
	}

	private void drawProgressBar() {
		int width = MathUtils.floor(((float) PERCENT_COMPLETE / 100) * 21);
		drawTexturedModalRect(91, 23, 202, 1, width, 13);
	}

	private ContainerCompressor getContainer() {
		return (ContainerCompressor) inventorySlots;
	}

	private World getWorld() {
		return getContainer().getTile().getWorld();
	}

	private BlockPos getPos() {
		return getContainer().getTile().getPos();
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		if (isPointInRegion(9, 21, 8, 37, mouseX, mouseY)) {
			GuiUtils.drawToolTipWithBorderColor(this, Arrays.<String>asList(new String[] {
					ReadableNumberConverter.INSTANCE.toWideReadableForm(ENERGY) + "/" + ReadableNumberConverter.INSTANCE.toSlimReadableForm(CAPACITY) + " RF"
			}), mouseX, mouseY, 0xFF17FF6D, 0x9917FF6D);
		}
		if (PERCENT_COMPLETE > 0 && isPointInRegion(91, 23, 21, 13, mouseX, mouseY)) {
			GuiUtils.drawToolTipWithBorderColor(this, Arrays.<String>asList(new String[] {
					PERCENT_COMPLETE + "%"
			}), mouseX, mouseY, 0xFF17FF6D, 0x9917FF6D);
		}
		if (Mods.JEI.isLoaded()) {
			if (isPointInRegion(93, 42, 16, 16, mouseX, mouseY)) {
				drawJEIClickOverlay();
				GuiUtils.drawToolTipWithBorderColor(this, Arrays.<String>asList(new String[] {
						"Show Recipes"
				}), mouseX, mouseY, 0xFF17FF6D, 0x9917FF6D);
			}
		}
		/*
		if (isMouseOverSlot(inventorySlots.getSlot(3), mouseX, mouseY) && !inventorySlots.getSlot(3).getHasStack()) {
			GuiUtils.drawToolTipWithBorderColor(this, Arrays.<String>asList(new String[] {
					"Speed Upgrade Slot"
			}), mouseX, mouseY, 0xFF17FF6D, 0x9917FF6D);
		
		}
		*/
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		if (mouseButton == 0 && isPointInRegion(93, 42, 16, 16, mouseX, mouseY) && Mods.JEI.isLoaded()) {
			JEI.showRecipes(Arrays.<String>asList(new String[] {
					JEI.UID.COMPRESSOR
			}));
		}
		else {
			super.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}

	private void drawJEIClickOverlay() {
		int x = 93 + guiLeft;
		int y = 42 + guiTop;
		int width = 16;
		int height = 16;
		int red = 255;
		int green = 255;
		int blue = 255;
		int alpha = 80;
		GlStateManager.disableLighting();
		GlStateManager.disableDepth();
		GlStateManager.disableTexture2D();
		Tessellator tessellator1 = Tessellator.getInstance();
		VertexBuffer renderer = tessellator1.getBuffer();

		renderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
		renderer.pos(x + 0, y + 0, 0.0D).color(red, green, blue, alpha).endVertex();
		renderer.pos(x + 0, y + height, 0.0D).color(red, green, blue, alpha).endVertex();
		renderer.pos(x + width, y + height, 0.0D).color(red, green, blue, alpha).endVertex();
		renderer.pos(x + width, y + 0, 0.0D).color(red, green, blue, alpha).endVertex();
		Tessellator.getInstance().draw();

		GlStateManager.enableTexture2D();
		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
	}

	@Override
	public void updateScreen() {
		ENERGY = EnergyUtils.getRF(EnergyUtils.getRFTile(getWorld(), getPos()));
		CAPACITY = EnergyUtils.getRFCapacity(EnergyUtils.getRFTile(getWorld(), getPos()));
		TileCompressor compressor = getContainer().getTile();
		if (compressor.getCurrentRecipe() != null) {
			PERCENT_COMPLETE = compressor.getPercentComplete();
		}
		else {
			PERCENT_COMPLETE = 0;
		}
	}

}
