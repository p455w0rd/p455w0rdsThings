package p455w0rd.p455w0rdsthings.client.gui.guide;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.widgets.EntryButton;
import p455w0rd.p455w0rdsthings.client.gui.guide.widgets.TexturedButton;
import p455w0rd.p455w0rdsthings.guide.PlayerData;
import p455w0rd.p455w0rdsthings.guide.PlayerData.PlayerSave;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModGuide;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rd.p455w0rdsthings.util.TextEffects;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.MathUtils;

/**
 * @author p455w0rd
 *
 */
@SideOnly(Side.CLIENT)
public class GuiMainPage extends GuiGuide {

	private static final String[] QUOTES = new String[] {
			"I am super excited for it@Seyeght",
			"Yo dude...I hope u dont think i ditched you for covers@Morpheus1101",
			"That's so cool@Epiic_Thundercat",
			"Password you are a nice modder, you always try to find the best way of doing this both code wise and functionality. I love all your work especially p455w0rd's Things, I've used it a lot for some of its \"unique\" items and things many people have been wanting for a long while like the /dank/null storage blocks that clear your inventory and many others <3 you pass :) (fully honest)@sokratis12GR",
			"XD love it@juryjr",
			"it's like if Chuck Norris' beard gave birth to Bruce Lee's fist while riding John Wayne like a horse@Kiba",
			"What the heck is a Frienderman??@Hermy",
			"A world without a Wireless Crafting Terminal is a world I don't want to live in.@hypnotizd"
	};

	//private TexturedButton achievementButton;
	private TexturedButton configButton;

	private GuiButton tutorialButton;
	private boolean showTutorial;

	private String bookletName;

	private List<String> quote;
	private String quoteGuy;

	public GuiMainPage(GuiScreen previousScreen) {
		super(previousScreen, null);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void initGui() {
		super.initGui();

		int flavor = 1;
		if (EasyMappings.world().rand.nextFloat() <= 0.1) {
			flavor = MathUtils.getRandom(2, 7);
		}
		bookletName = ModGlobals.NAME + " " + I18n.translateToLocal("guide.manualName.2");

		String usedQuote = QUOTES[EasyMappings.world().rand.nextInt(QUOTES.length)];
		String[] quoteSplit = usedQuote.split("@");
		if (quoteSplit.length == 2) {
			quote = fontRendererObj.listFormattedStringToWidth(quoteSplit[0], 150);
			quoteGuy = quoteSplit[1];
		}

		PlayerSave data = PlayerData.getDataFromPlayer(EasyMappings.player());
		if (!data.didBookTutorial) {
			showTutorial = true;

			tutorialButton = new GuiButton(666666, guiLeft + 140 / 2 - 50, guiTop + 146, 100, 20, "Add bookmarks");
			buttonList.add(tutorialButton);
		}
		/*
		else {
			List<String> configText = new ArrayList<String>();
			configText.add(TextFormatting.GOLD + "Open Config GUI");
			configText.addAll(fontRendererObj.listFormattedStringToWidth("Press this to configure " + ModGlobals.NAME + " in-game. \nSome changes will require a game restart!", 200));
			configButton = new TexturedButton(RES_LOC_GADGETS, -388, guiLeft + 16, guiTop + ySize - 30, 188, 14, 16, 16, configText);
			//buttonList.add(configButton);
		}
		*/
		for (int i = 0; i < BUTTONS_PER_PAGE; i++) {
			if (ModGuide.GUIDE_ENTRIES.size() > i) {
				buttonList.add(new EntryButton(i, guiLeft + 156, guiTop + 11 + i * 13, 115, 10, "- " + ModGuide.GUIDE_ENTRIES.get(i).getLocalizedNameWithFormatting(), (ItemStack) null));
			}
			else {
				return;
			}
		}
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button instanceof EntryButton) {
			if (ModGuide.GUIDE_ENTRIES.size() > button.id) {
				IGuideEntry entry = ModGuide.GUIDE_ENTRIES.get(button.id);
				if (entry != null) {
					mc.displayGuiScreen(new GuiEntry(previousScreen, this, entry, 0, "", false));
				}
			}
		}
		//else if (button == achievementButton) {
		//	GuiScreen achievements = new GuiAAAchievements(EasyMappings.player().getStatFileWriter());
		//	mc.displayGuiScreen(achievements);
		//}
		//else if (button == configButton) {
		//	GuiScreen config = new GuiConfiguration(this);
		//	mc.displayGuiScreen(config);
		//}
		else if (showTutorial && button == tutorialButton) {
			if (hasBookmarkButtons()) {
				if (!isShiftKeyDown()) {
					for (int i = 0; i < ModGuide.INTRO_BOOKMARKS.size(); i++) {
						if (ModGuide.INTRO_BOOKMARKS.get(i) instanceof IGuidePage) {
							bookmarkButtons[i].assignedPage = (IGuidePage) ModGuide.INTRO_BOOKMARKS.get(i);
						}
						else if (ModGuide.INTRO_BOOKMARKS.get(i) instanceof IGuideEntry) {
							bookmarkButtons[i].assignedEntry = (IGuideEntry) ModGuide.INTRO_BOOKMARKS.get(i);
						}
					}
				}
				showTutorial = false;
				tutorialButton.visible = false;

				PlayerSave data = PlayerData.getDataFromPlayer(EasyMappings.player());
				data.didBookTutorial = true;
				GuideUtils.syncPlayerDataToServer();
			}
		}
		else {
			super.actionPerformed(button);
		}
	}

	@Override
	public void drawScreenPre(int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(mouseX, mouseY, partialTicks);

		//String strg = TextFormatting.DARK_GREEN + TextFormatting.getTextWithoutFormattingCodes(I18n.translateToLocal(bookletName));
		String strg = TextEffects.makeFabulous(bookletName);
		fontRendererObj.drawString(strg, guiLeft + 72 - fontRendererObj.getStringWidth(strg) / 2 - 3, guiTop + 19, 0, true);
		//strg = TextFormatting.DARK_GREEN + I18n.translateToLocal("guide.manualName.2");
		//fontRendererObj.drawString(strg, guiLeft + 72 - fontRendererObj.getStringWidth(strg) / 2 - 3, guiTop + 19 + fontRendererObj.FONT_HEIGHT, 0);

		String versionStrg = "2.0";
		UUID playerID = EasyMappings.player().getUniqueID();
		if (playerID.equals(UUID.fromString("3f9f4a94-95e3-40fe-8895-e8e3e84d1468")) || playerID.equals(UUID.fromString("7b794048-f144-40bb-83dd-4789a5cf7cc8"))) {
			//versionStrg = TextUtils.rainbow("Happy Birthday!", true);
			//versionStrg = TextEffects.makeFabulous("Happy Birthday!");
		}
		strg = TextFormatting.GREEN + TextFormatting.ITALIC.toString() + "-" + versionStrg + "-";
		fontRendererObj.drawString(strg, guiLeft + 72 - fontRendererObj.getStringWidth(strg) / 2 - 3, guiTop + 40, 0, true);

		if (showTutorial) {
			String text = TextFormatting.BLUE + "It looks like this is the first time you are using this manual. \nIf you click the button below, some useful bookmarks will be stored at the bottom of the GUI. You should definitely check them out to get started with " + ModGlobals.NAME + "! \nIf you don't want this, shift-click the button.";
			renderSplitScaledAsciiString(text, guiLeft + 11, guiTop + 55, 0, false, 0.75F, 120);
		}
		else if (quote != null && !quote.isEmpty() && quoteGuy != null) {
			int quoteSize = quote.size();

			for (int i = 0; i < quoteSize; i++) {
				renderScaledAsciiString(TextFormatting.ITALIC + quote.get(i), guiLeft + 17, guiTop + 50 + (i * 8), 0, false, 0.75F);
			}
			renderScaledAsciiString("- " + quoteGuy, guiLeft + 50, guiTop + 50 + quoteSize * 8, 0, false, 0.8F);
		}
	}

	@Override
	public void addOrModifyItemRenderer(ItemStack renderedStack, int x, int y, float scale, boolean shouldTryTransfer) {

	}
}