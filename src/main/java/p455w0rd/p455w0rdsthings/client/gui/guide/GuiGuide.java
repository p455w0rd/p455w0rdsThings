package p455w0rd.p455w0rdsthings.client.gui.guide;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.google.gson.JsonParseException;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.widgets.BookmarkButton;
import p455w0rd.p455w0rdsthings.client.gui.guide.widgets.TexturedButton;
import p455w0rd.p455w0rdsthings.guide.PlayerData;
import p455w0rd.p455w0rdsthings.guide.PlayerData.PlayerSave;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.StringUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public abstract class GuiGuide extends GuiGuideBase {

	public static final int BUTTONS_PER_PAGE = 12;
	public static final ResourceLocation RES_LOC_GUI = new ResourceLocation(ModGlobals.MODID, "textures/gui/guide/guide.png");
	public static final ResourceLocation RES_LOC_GADGETS = new ResourceLocation(ModGlobals.MODID, "textures/gui/guide/guide_widgets.png");
	protected final BookmarkButton[] bookmarkButtons = new BookmarkButton[12];
	public GuiScreen previousScreen;
	public GuiGuideBase parentPage;
	public GuiTextField searchField;
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	private GuiButton buttonLeft;
	private GuiButton buttonRight;
	private GuiButton buttonBack;

	public GuiGuide(GuiScreen previousScreen, GuiGuideBase parentPage) {
		this.previousScreen = previousScreen;
		this.parentPage = parentPage;

		xSize = 281;
		ySize = 180;
	}

	@Override
	public void initGui() {
		//super.initGui();

		guiLeft = (width - xSize) / 2;
		guiTop = (height - ySize) / 2;

		if (hasPageLeftButton()) {
			List<String> hoverText = Arrays.asList(TextFormatting.GOLD + "Previous Page", TextFormatting.ITALIC + "Or scroll up");
			buttonLeft = new TexturedButton(RES_LOC_GADGETS, -2000, guiLeft - 12, guiTop + ySize - 8, 18, 54, 18, 10, hoverText);
			buttonList.add(buttonLeft);
		}

		if (hasPageRightButton()) {
			List<String> hoverText = Arrays.asList(TextFormatting.GOLD + "Next Page", TextFormatting.ITALIC + "Or scroll down");
			buttonRight = new TexturedButton(RES_LOC_GADGETS, -2001, guiLeft + xSize - 6, guiTop + ySize - 8, 0, 54, 18, 10, hoverText);
			buttonList.add(buttonRight);
		}

		if (hasBackButton()) {
			List<String> hoverText = Arrays.asList(TextFormatting.GOLD + "Go Back", TextFormatting.ITALIC + "Or right-click", TextFormatting.ITALIC.toString() + TextFormatting.GRAY + "Hold Shift for Main Page");
			buttonBack = new TexturedButton(RES_LOC_GADGETS, -2002, guiLeft - 15, guiTop - 3, 36, 54, 18, 10, hoverText);
			buttonList.add(buttonBack);
		}

		if (hasSearchBar()) {
			searchField = new GuiTextField(-420, fontRendererObj, guiLeft + xSize + 2, guiTop + ySize - 40 + 2, 64, 12);
			searchField.setMaxStringLength(50);
			searchField.setEnableBackgroundDrawing(false);
		}

		if (hasBookmarkButtons()) {
			PlayerSave data = PlayerData.getDataFromPlayer(EasyMappings.player());

			int xStart = guiLeft + xSize / 2 - 16 * bookmarkButtons.length / 2;
			for (int i = 0; i < bookmarkButtons.length; i++) {
				bookmarkButtons[i] = new BookmarkButton(1337 + i, xStart + i * 16, guiTop + ySize, this);
				buttonList.add(bookmarkButtons[i]);

				if (data.bookmarks[i] != null) {
					if (data.bookmarks[i] instanceof IGuidePage) {
						bookmarkButtons[i].assignedPage = (IGuidePage) data.bookmarks[i];
					}
					else {
						bookmarkButtons[i].assignedEntry = (IGuideEntry) data.bookmarks[i];
					}
				}
			}
		}
	}

	@Override
	public void onGuiClosed() {
		super.onGuiClosed();

		//Don't cache the parent GUI, otherwise it opens again when you close the cached book!
		previousScreen = null;

		PlayerSave data = PlayerData.getDataFromPlayer(EasyMappings.player());
		data.lastOpenBooklet = this;
		boolean change = false;
		for (int i = 0; i < bookmarkButtons.length; i++) {
			if (data.bookmarks[i] == null || (data.bookmarks[i] != bookmarkButtons[i].assignedPage && data.bookmarks[i] != bookmarkButtons[i].assignedEntry)) {
				data.bookmarks[i] = bookmarkButtons[i].assignedPage == null ? bookmarkButtons[i].assignedEntry : bookmarkButtons[i].assignedPage;
				if (data.bookmarks[i] != null) {
					change = true;
				}
			}
		}

		if (change) {
			GuideUtils.syncPlayerDataToServer();
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawScreenPre(mouseX, mouseY, partialTicks);
		//super.drawScreen(mouseX, mouseY, partialTicks);
		for (int i = 0; i < buttonList.size(); ++i) {
			buttonList.get(i).drawButton(mc, mouseX, mouseY);
		}

		for (int j = 0; j < labelList.size(); ++j) {
			labelList.get(j).drawLabel(mc, mouseX, mouseY);
		}
		drawScreenPost(mouseX, mouseY, partialTicks);
	}

	public void drawScreenPre(int mouseX, int mouseY, float partialTicks) {
		GlStateManager.color(1F, 1F, 1F);
		mc.getTextureManager().bindTexture(RES_LOC_GUI);
		drawModalRectWithCustomSizedTexture(guiLeft, guiTop, 0, 0, xSize, ySize, 512, 512);

		if (hasSearchBar()) {
			mc.getTextureManager().bindTexture(RES_LOC_GADGETS);
			this.drawTexturedModalRect(guiLeft + xSize, guiTop + ySize - 40, 188, 0, 68, 14);

			boolean unicodeBefore = fontRendererObj.getUnicodeFlag();
			fontRendererObj.setUnicodeFlag(true);

			if (!searchField.isFocused() && (searchField.getText() == null || searchField.getText().isEmpty())) {
				fontRendererObj.drawString(TextFormatting.ITALIC + "Click to search...", guiLeft + xSize + 2, guiTop + ySize - 40 + 2, 0xFFFFFF, false);
			}

			searchField.drawTextBox();

			fontRendererObj.setUnicodeFlag(unicodeBefore);
		}

	}

	public void drawScreenPost(int mouseX, int mouseY, float partialTicks) {
		for (GuiButton button : buttonList) {
			if (button instanceof BookmarkButton) {
				((BookmarkButton) button).drawHover(mouseX, mouseY);
			}
			else if (button instanceof TexturedButton) {
				((TexturedButton) button).drawHover(mouseX, mouseY);
			}
		}
	}

	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);

		if (hasSearchBar()) {
			searchField.mouseClicked(mouseX, mouseY, mouseButton);
		}

		if (mouseButton == 1 && hasBackButton()) {
			onBackButtonPressed();
		}
		if (mouseButton == 0) {
			//ITextComponent itextcomponent = getClickedComponentAt(mouseX, mouseY);

			//if (itextcomponent != null && handleComponentClick(itextcomponent)) {
			//	return;
			//}
		}
	}

	@Override
	public void handleMouseInput() throws IOException {
		int wheel = Mouse.getEventDWheel();
		if (wheel != 0) {
			if (wheel < 0) {
				if (hasPageRightButton()) {
					onPageRightButtonPressed();
				}
			}
			else if (wheel > 0) {
				if (hasPageLeftButton()) {
					onPageLeftButtonPressed();
				}
			}
		}
		super.handleMouseInput();
	}

	@Override
	public void updateScreen() {
		//super.updateScreen();

		if (hasSearchBar()) {
			searchField.updateCursorCounter();
		}
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}

	public boolean hasPageLeftButton() {
		return false;
	}

	public void onPageLeftButtonPressed() {

	}

	public boolean hasPageRightButton() {
		return false;
	}

	public void onPageRightButtonPressed() {

	}

	public boolean hasBackButton() {
		return false;
	}

	public void onBackButtonPressed() {
		mc.displayGuiScreen(new GuiMainPage(previousScreen));
	}

	public boolean hasSearchBar() {
		return false;
	}

	public boolean hasBookmarkButtons() {
		return true;
	}

	public void onSearchBarChanged(String searchBarText) {
		GuiGuideBase parent = !(this instanceof GuiEntry) ? this : parentPage;
		mc.displayGuiScreen(new GuiEntry(previousScreen, parent, GuideUtils.allAndSearch, 0, searchBarText, true));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (hasPageLeftButton() && button == buttonLeft) {
			onPageLeftButtonPressed();
		}
		else if (hasPageRightButton() && button == buttonRight) {
			onPageRightButtonPressed();
		}
		else if (hasBackButton() && button == buttonBack) {
			onBackButtonPressed();
		}
		else if (hasBookmarkButtons() && button instanceof BookmarkButton) {
			int index = ArrayUtils.indexOf(bookmarkButtons, button);
			if (index >= 0) {
				bookmarkButtons[index].onPressed();
			}
		}
		else {
			super.actionPerformed(button);
		}
	}

	@Override
	protected void keyTyped(char typedChar, int key) throws IOException {
		if (key == Keyboard.KEY_ESCAPE || (key == mc.gameSettings.keyBindInventory.getKeyCode())) {
			mc.displayGuiScreen(previousScreen);
		}
		else {
			super.keyTyped(typedChar, key);
		}
	}

	@Override
	public void renderScaledAsciiString(String text, int x, int y, int color, boolean shadow, float scale) {
		String finalStr = text;
		try {
			ITextComponent txt = ITextComponent.Serializer.jsonToComponent(text);
			finalStr = txt.getFormattedText();
		}
		catch (JsonParseException var13) {
			finalStr = text;
		}
		StringUtils.renderScaledAsciiString(fontRendererObj, finalStr, x, y, color, shadow, scale);
	}

	@Override
	public void renderSplitScaledAsciiString(String text, int x, int y, int color, boolean shadow, float scale, int length) {
		String finalStr = text;
		try {
			ITextComponent txt = ITextComponent.Serializer.jsonToComponent(text);
			//finalStr = txt.getFormattedText();
			finalStr = txt.getUnformattedText();
		}
		catch (JsonParseException var13) {
			finalStr = text;
		}
		StringUtils.renderSplitScaledAsciiString(fontRendererObj, finalStr, x, y, color, shadow, scale, length);
	}

	@Override
	public List<GuiButton> getButtonList() {
		return buttonList;
	}

	@Override
	public int getGuiLeft() {
		return guiLeft;
	}

	@Override
	public int getGuiTop() {
		return guiTop;
	}

	@Override
	public int getSizeX() {
		return xSize;
	}

	@Override
	public int getSizeY() {
		return ySize;
	}

}
