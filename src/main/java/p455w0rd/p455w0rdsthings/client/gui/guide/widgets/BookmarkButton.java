package p455w0rd.p455w0rdsthings.client.gui.guide.widgets;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiEntry;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiGuide;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiPage;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.GuiUtils;

/**
 * @author Ellpeck -- used with explicit permission
 *
 */
@SideOnly(Side.CLIENT)
public class BookmarkButton extends GuiButton {

	private final GuiGuide booklet;
	public IGuidePage assignedPage;
	public IGuideEntry assignedEntry;
	private int pageTimer;
	private int index = 0;

	public BookmarkButton(int id, int x, int y, GuiGuide booklet) {
		super(id, x, y, 16, 16, "");
		this.booklet = booklet;
		pageTimer = 0;
	}

	public void onPressed() {
		if (assignedPage != null) {
			if (GuiScreen.isShiftKeyDown()) {
				assignedPage = null;
			}
			else if (!(booklet instanceof GuiPage) || ((GuiPage) booklet).pages[0] != assignedPage) {
				GuiPage gui = GuideUtils.createPageGui(booklet.previousScreen, booklet, assignedPage);
				Minecraft.getMinecraft().displayGuiScreen(gui);
			}
		}
		else if (assignedEntry != null) {
			if (GuiScreen.isShiftKeyDown()) {
				assignedPage = null;
				assignedEntry = null;
			}
			else if (!(booklet instanceof IGuideEntry) || (IGuideEntry) booklet != assignedEntry) {
				Minecraft.getMinecraft().displayGuiScreen(GuideUtils.createEntryGui(assignedEntry.getIdentifier()));
			}
		}
		else {
			if (booklet instanceof GuiPage) {
				assignedPage = ((GuiPage) booklet).pages[0];
			}
			else if (booklet instanceof GuiEntry) {
				assignedEntry = (IGuideEntry) booklet;
			}
		}
	}

	@Override
	public void drawButton(Minecraft minecraft, int x, int y) {
		if (visible) {
			minecraft.getTextureManager().bindTexture(GuiGuide.RES_LOC_GADGETS);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			hovered = x >= xPosition && y >= yPosition && x < xPosition + width && y < yPosition + height;
			int k = getHoverState(hovered);
			if (k == 0) {
				k = 1;
			}

			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
			GlStateManager.blendFunc(770, 771);
			int renderHeight = 25;
			this.drawTexturedModalRect(xPosition, yPosition, 224 + (assignedPage == null ? assignedEntry == null ? 0 : 16 : 16), 14 - renderHeight + k * renderHeight, width, renderHeight);
			mouseDragged(minecraft, x, y);

			if (assignedPage != null) {
				List<ItemStack> displayStacks = assignedPage.getChapter().getDisplayItemStacks();
				pageTimer++;
				if (pageTimer >= 100000) {
					pageTimer = 0;
				}
				if (pageTimer % 500 == 0) {
					cycleItem(displayStacks);
				}

				GlStateManager.pushMatrix();
				GuideUtils.renderStackToGui(displayStacks.get(index), xPosition + 2, yPosition + 1, 0.725F);
				GlStateManager.popMatrix();
			}
			else if (assignedEntry != null) {
				GlStateManager.pushMatrix();
				GuideUtils.renderStackToGui(new ItemStack(ModItems.GUIDE), xPosition + 2, yPosition + 1, 0.725F);
				GlStateManager.popMatrix();
			}
		}
	}

	private void cycleItem(List<ItemStack> stacks) {
		if (index + 1 < stacks.size()) {
			index++;
			return;
		}
		index = 0;
	}

	public void drawHover(int mouseX, int mouseY) {
		if (isMouseOver()) {
			List<String> list = new ArrayList<String>();

			if (assignedPage != null) {
				IGuideChapter chapter = assignedPage.getChapter();

				list.add(TextFormatting.GOLD + chapter.getLocalizedName() + TextFormatting.GOLD + ", Page " + (chapter.getPageIndex(assignedPage) + 1));
				list.add("Click to open");
				list.add(TextFormatting.ITALIC + "Shift-Click to remove");
			}
			else if (assignedEntry != null) {
				list.add(TextFormatting.GOLD + assignedEntry.getLocalizedName());
				list.add("Click to open");
				list.add(TextFormatting.ITALIC + "Shift-Click to remove");
			}
			else {
				list.add(TextFormatting.GOLD + "No Bookmark");

				if (booklet instanceof GuiPage) {
					list.add("Click to bookmark current page");
				}
				else {
					list.add("Open a page to bookmark it");
				}
			}

			//Minecraft mc = Minecraft.getMinecraft();
			//GuiUtils.drawHoveringText(list, mouseX, mouseY, mc.displayWidth, mc.displayHeight, -1, mc.fontRendererObj);
			GuiUtils.drawToolTipWithBorderColor(booklet, list, mouseX, mouseY, DankNullUtils.getColor(1, true), DankNullUtils.getColor(1, false));
		}
	}
}
