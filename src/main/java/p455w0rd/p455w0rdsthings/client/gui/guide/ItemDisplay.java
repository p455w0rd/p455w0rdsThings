package p455w0rd.p455w0rdsthings.client.gui.guide;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.GuiUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class ItemDisplay {
	public final int x;
	public final int y;
	public final float scale;
	private final GuiPage gui;
	private final IGuidePage page;
	public ItemStack stack;

	public ItemDisplay(GuiPage gui, int x, int y, float scale, ItemStack stack, boolean shouldTryTransfer) {
		this.gui = gui;
		this.x = x;
		this.y = y;
		this.scale = scale;
		this.stack = stack;
		page = shouldTryTransfer ? GuideUtils.findFirstPageForStack(stack) : null;
	}

	@SideOnly(Side.CLIENT)
	public void drawPre() {
		GuideUtils.renderStackToGui(stack, x, y, scale);
	}

	@SideOnly(Side.CLIENT)
	public void drawPost(int mouseX, int mouseY) {

		if (isHovered(mouseX, mouseY)) {
			Minecraft mc = gui.mc;
			boolean flagBefore = mc.fontRendererObj.getUnicodeFlag();
			mc.fontRendererObj.setUnicodeFlag(false);

			List<String> list = stack.getTooltip(EasyMappings.player(), mc.gameSettings.advancedItemTooltips);

			for (int k = 0; k < list.size(); ++k) {
				if (k == 0) {
					list.set(k, stack.getRarity().rarityColor + list.get(k));
				}
				else {
					list.set(k, TextFormatting.GRAY + list.get(k));
				}
			}

			if (page != null && page != gui.pages[0] && page != gui.pages[1]) {
				list.add(TextFormatting.GOLD + I18n.translateToLocal("guide.click_to_see_recipe"));
			}

			GuiUtils.drawToolTipWithBorderColor(gui, list, mouseX, mouseY, DankNullUtils.getColor(1, true), DankNullUtils.getColor(1, false));

			mc.fontRendererObj.setUnicodeFlag(flagBefore);
		}

	}

	public void onMousePress(int button, int mouseX, int mouseY) {

		if (button == 0 && isHovered(mouseX, mouseY)) {
			if (page != null && page != gui.pages[0] && page != gui.pages[1]) {
				gui.mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));

				GuiGuide gui = GuideUtils.createPageGui(this.gui.previousScreen, this.gui, page);
				this.gui.mc.displayGuiScreen(gui);
			}
		}

	}

	public boolean isHovered(int mouseX, int mouseY) {
		return mouseX >= x && mouseY >= y && mouseX < x + 16 * scale && mouseY < y + 16 * scale;
	}
}