package p455w0rd.p455w0rdsthings.client.gui.guide.widgets;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.GuiUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
@SideOnly(Side.CLIENT)
public class TexturedButton extends GuiButton {

	public final List<String> textList = Lists.<String>newArrayList();
	private final ResourceLocation resLoc;
	public int texturePosX;
	public int texturePosY;

	public TexturedButton(ResourceLocation resLoc, int id, int x, int y, int texturePosX, int texturePosY, int width, int height) {
		this(resLoc, id, x, y, texturePosX, texturePosY, width, height, Lists.<String>newArrayList());
	}

	public TexturedButton(ResourceLocation resLoc, int id, int x, int y, int texturePosX, int texturePosY, int width, int height, List<String> hoverTextList) {
		super(id, x, y, width, height, "");
		this.texturePosX = texturePosX;
		this.texturePosY = texturePosY;
		this.resLoc = resLoc;
		textList.addAll(hoverTextList);
	}

	@Override
	public void drawButton(Minecraft minecraft, int x, int y) {
		if (visible) {
			minecraft.getTextureManager().bindTexture(resLoc);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			hovered = x >= xPosition && y >= yPosition && x < xPosition + width && y < yPosition + height;
			int k = getHoverState(hovered);
			if (k == 0) {
				k = 1;
			}

			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
			GlStateManager.blendFunc(770, 771);
			this.drawTexturedModalRect(xPosition, yPosition, texturePosX, texturePosY - height + k * height, width, height);
			mouseDragged(minecraft, x, y);
		}
	}

	public void drawHover(int x, int y) {
		if (isMouseOver()) {
			//Minecraft mc = Minecraft.getMinecraft();
			//GuiUtils.drawHoveringText(this.textList, x, y, mc.displayWidth, mc.displayHeight, -1, mc.fontRendererObj);
			GuiUtils.drawToolTipWithBorderColor(EasyMappings.mc().currentScreen, textList, x, y, DankNullUtils.getColor(1, true), DankNullUtils.getColor(1, false));
		}
	}
}
