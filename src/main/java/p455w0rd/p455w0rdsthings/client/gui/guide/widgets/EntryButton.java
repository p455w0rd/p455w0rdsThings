package p455w0rd.p455w0rdsthings.client.gui.guide.widgets;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.StringUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
@SideOnly(Side.CLIENT)
public class EntryButton extends GuiButton {

	public List<ItemStack> stacksToRender = Lists.<ItemStack>newArrayList();
	private int pageTimer;
	private int index = 0;

	public EntryButton(int id, int x, int y, int width, int height, String text, @Nullable ItemStack displayImage) {
		this(id, x, y, width, height, text, displayImage == null ? Lists.<ItemStack>newArrayList() : Lists.<ItemStack>newArrayList(displayImage));
	}

	public EntryButton(int id, int x, int y, int width, int height, String text, @Nullable List<ItemStack> displayImages) {
		super(id, x, y, width, height, text);
		stacksToRender = displayImages;
	}

	@Override
	public void drawButton(Minecraft minecraft, int mouseX, int mouseY) {
		if (visible) {

			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			hovered = mouseX >= xPosition && mouseY >= yPosition && mouseX < xPosition + width && mouseY < yPosition + height;
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
			GlStateManager.blendFunc(770, 771);
			mouseDragged(minecraft, mouseX, mouseY);
			int textOffsetX = 0;
			if (stacksToRender.size() > 0) {
				pageTimer++;
				if (pageTimer >= 100000) {
					pageTimer = 0;
				}

				if (pageTimer % 500 == 0) {
					cycleItem();
				}
				if (stacksToRender.get(index) != null) {
					GlStateManager.pushMatrix();
					GuideUtils.renderStackToGui(stacksToRender.get(index), xPosition - 4, yPosition, 0.8F);
					GlStateManager.popMatrix();
					textOffsetX = 10;
				}
			}

			float scale = 0.75F;

			if (hovered) {
				GlStateManager.pushMatrix();
				GuideUtils.drawHorizontalGradientRect(xPosition + textOffsetX - 1, yPosition + height - 1, xPosition + (int) (minecraft.fontRendererObj.getStringWidth(displayString) * scale) + textOffsetX + 1, yPosition + height, 0x00 << 24 | DankNullUtils.getColor(1, true), DankNullUtils.getColor(1, false), zLevel);
				GlStateManager.popMatrix();
			}

			StringUtils.renderScaledAsciiString(minecraft.fontRendererObj, displayString, xPosition + textOffsetX, yPosition + 2 + (height - 8) / 2, 0, false, scale);
		}
	}

	private void cycleItem() {
		if (index + 1 < stacksToRender.size()) {
			index++;
			return;
		}
		index = 0;
	}
}
