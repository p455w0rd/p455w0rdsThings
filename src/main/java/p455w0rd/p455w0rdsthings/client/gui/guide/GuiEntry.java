package p455w0rd.p455w0rdsthings.client.gui.guide;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.widgets.EntryButton;
import p455w0rd.p455w0rdsthings.util.GuideUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
@SideOnly(Side.CLIENT)
public class GuiEntry extends GuiGuide {

	//The page in the entry. Say you have 2 more chapters than fit on one double page, then those 2 would be displayed on entryPage 1 instead.
	private final int entryPage;
	private final int pageAmount;
	private final IGuideEntry entry;
	private List<IGuideChapter> chapters = Lists.<IGuideChapter>newArrayList();
	private final String searchText;
	private final boolean focusSearch;

	public GuiEntry(GuiScreen previousScreen, GuiGuideBase parentPage, IGuideEntry entry, int entryPage, String search, boolean focusSearch) {
		super(previousScreen, parentPage);
		this.entryPage = entryPage;
		this.entry = entry;
		searchText = search;
		this.focusSearch = focusSearch;
		if (entry != null && search != null) {
			chapters = entry.getChaptersForDisplay(search);
		}
		if (!chapters.isEmpty()) {
			IGuideChapter lastChap = chapters.get(chapters.size() - 1);
			pageAmount = lastChap == null ? 1 : calcEntryPage(this.entry, lastChap, searchText) + 1;
		}
		else {
			pageAmount = 1;
		}
	}

	public GuiEntry(GuiScreen previousScreen, GuiGuideBase parentPage, IGuideEntry entry, IGuideChapter chapterForPageCalc, String search, boolean focusSearch) {
		this(previousScreen, parentPage, entry, calcEntryPage(entry, chapterForPageCalc, search), search, focusSearch);
	}

	public IGuideEntry getEntry() {
		return entry;
	}

	private static int calcEntryPage(IGuideEntry entry, IGuideChapter chapterForPageCalc, String search) {
		int index = entry.getChaptersForDisplay(search).indexOf(chapterForPageCalc);
		return index / (BUTTONS_PER_PAGE * 2);
	}

	@Override
	public void drawScreenPre(int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(mouseX, mouseY, partialTicks);

		String name = entry == null ? "" : entry.getLocalizedName();
		fontRendererObj.drawString(name, guiLeft + xSize / 2 - fontRendererObj.getStringWidth(name) / 2, guiTop - 1, 0xFFFFFF, true);

		for (int i = 0; i < 2; i++) {
			String pageStrg = "Page " + (entryPage * 2 + i + 1) + "/" + pageAmount * 2;
			renderScaledAsciiString(pageStrg, guiLeft + 25 + i * 136, guiTop + ySize - 7, 0xFFFFFF, false, 0.8F);
		}
	}

	@Override
	public void initGui() {
		super.initGui();

		if (hasSearchBar() && searchText != null) {
			searchField.setText(searchText);
			if (focusSearch) {
				searchField.setFocused(true);
			}
		}

		int idOffset = entryPage * (BUTTONS_PER_PAGE * 2);
		for (int x = 0; x < 2; x++) {
			for (int y = 0; y < BUTTONS_PER_PAGE; y++) {
				int id = y + x * BUTTONS_PER_PAGE;
				if (chapters.size() > id + idOffset) {
					IGuideChapter chapter = chapters.get(id + idOffset);
					buttonList.add(new EntryButton(id, guiLeft + 14 + x * 142, guiTop + 11 + y * 13, 115, 10, chapter.getLocalizedNameWithFormatting(), chapter.getDisplayItemStacks()));
				}
				else {
					return;
				}
			}
		}
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button instanceof EntryButton) {
			int actualId = button.id + entryPage * (BUTTONS_PER_PAGE * 2);

			if (chapters.size() > actualId) {
				IGuideChapter chapter = chapters.get(actualId);
				if (chapter != null) {
					IGuidePage[] pages = chapter.getAllPages();
					if (pages != null && pages.length > 0) {
						mc.displayGuiScreen(GuideUtils.createPageGui(previousScreen, this, pages[0]));
					}
				}
			}
		}
		else {
			super.actionPerformed(button);
		}
	}

	@Override
	public void addOrModifyItemRenderer(ItemStack renderedStack, int x, int y, float scale, boolean shouldTryTransfer) {

	}

	@Override
	public boolean hasPageLeftButton() {
		return entryPage > 0;
	}

	@Override
	public void onPageLeftButtonPressed() {
		mc.displayGuiScreen(new GuiEntry(previousScreen, parentPage, entry, entryPage - 1, searchText, searchField.isFocused()));
	}

	@Override
	public boolean hasPageRightButton() {
		return !chapters.isEmpty() && entryPage < pageAmount - 1;
	}

	@Override
	public void onPageRightButtonPressed() {
		mc.displayGuiScreen(new GuiEntry(previousScreen, parentPage, entry, entryPage + 1, searchText, searchField.isFocused()));
	}

	@Override
	public boolean hasBackButton() {
		return true;
	}

	@Override
	public void onBackButtonPressed() {
		if (!isShiftKeyDown()) {
			mc.displayGuiScreen(parentPage);
		}
		else {
			super.onBackButtonPressed();
		}
	}
}