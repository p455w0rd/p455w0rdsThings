package p455w0rd.p455w0rdsthings.client.gui.guide;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.widgets.TexturedButton;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rd.p455w0rdsthings.guide.page.PageEntity;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.EasyMappings;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
@SideOnly(Side.CLIENT)
public class GuiPage extends GuiGuide {

	public final IGuidePage[] pages = new IGuidePage[2];
	private final List<ItemDisplay> itemDisplays = new ArrayList<ItemDisplay>();
	public int pageTimer;

	private GuiButton buttonViewOnline;

	public GuiPage(GuiScreen previousScreen, GuiGuideBase parentPage, IGuidePage page1, IGuidePage page2) {
		super(previousScreen, parentPage);

		pages[0] = page1;
		pages[1] = page2;
	}

	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);

		for (ItemDisplay display : itemDisplays) {
			display.onMousePress(mouseButton, mouseX, mouseY);
		}

		for (IGuidePage page : pages) {
			if (page != null) {
				page.mouseClicked(this, mouseX, mouseY, mouseButton);
			}
		}
	}

	@Override
	public void mouseReleased(int mouseX, int mouseY, int state) {
		super.mouseReleased(mouseX, mouseY, state);

		for (IGuidePage page : pages) {
			if (page != null) {
				page.mouseReleased(this, mouseX, mouseY, state);
			}
		}
	}

	@Override
	public void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);

		for (IGuidePage page : pages) {
			if (page != null) {
				page.mouseClickMove(this, mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
			}
		}
	}

	@Override
	public void actionPerformed(GuiButton button) throws IOException {
		if (button == buttonViewOnline) {
			List<String> links = getWebLinks();
			if (Desktop.isDesktopSupported()) {
				for (String link : links) {
					try {
						Desktop.getDesktop().browse(new URI(link));
					}
					catch (Exception e) {
						ModLogger.error("Couldn't open website from Booklet page!");
					}
				}
			}
		}
		else {
			super.actionPerformed(button);

			for (IGuidePage page : pages) {
				if (page != null) {
					page.actionPerformed(this, button);
				}
			}
		}
	}

	@Override
	public void initGui() {
		itemDisplays.clear();
		super.initGui();

		List<String> links = getWebLinks();
		if (links != null && !links.isEmpty()) {
			buttonViewOnline = new TexturedButton(RES_LOC_GADGETS, -782822, guiLeft + xSize - 24, guiTop + ySize - 25, 0, 172, 16, 16, Collections.singletonList(TextFormatting.GOLD + "View Online"));
			buttonList.add(buttonViewOnline);
		}

		for (int i = 0; i < pages.length; i++) {
			IGuidePage page = pages[i];
			if (page != null) {
				page.initGui(this, guiLeft + 6 + i * 142, guiTop + 7);
			}
		}
	}

	private List<String> getWebLinks() {
		/*
		List<String> links = new ArrayList<String>();

		for (IGuidePage page : pages) {
			if (page != null) {
				String link = page.getWebLink();
				if (link != null && !links.contains(link)) {
					links.add(link);
				}
			}
		}

		return links;
		*/
		return null;
	}

	@Override
	public void updateScreen() {
		super.updateScreen();

		for (int i = 0; i < pages.length; i++) {
			IGuidePage page = pages[i];
			if (page != null) {
				page.updateScreen(this, guiLeft + 6 + i * 142, guiTop + 7, pageTimer);
			}
		}
		for (IGuidePage guide : pages) {
			if (guide instanceof PageEntity) {
				if (((PageEntity) guide).getCurrentEntity() instanceof EntityFrienderman) {
					EntityFrienderman frienderman = (EntityFrienderman) ((PageEntity) guide).getCurrentEntity();
					if (frienderman.getEntityWorld() == null) {
						frienderman.setWorld(EasyMappings.world());
					}
					if (frienderman.getEntityWorld() != null) {
						frienderman.onLivingUpdate();
					}
				}
			}
		}
		pageTimer++;
	}

	@Override
	public void drawScreenPre(int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(mouseX, mouseY, partialTicks);

		if (pages[0] != null) {
			IGuideChapter chapter = pages[0].getChapter();
			String name = chapter.getLocalizedName();
			//float scale = 4.0f;
			//float scaledWidth = fontRendererObj.getStringWidth(name) * scale;
			//float x = (width / 2f) - (scaledWidth / 2F);
			//float y = guiTop - 60;
			//GlStateManager.pushMatrix();
			//GlStateManager.translate(x, y, 0);
			//GlStateManager.scale(scale, scale, 0.0f);
			//fontRendererObj.drawString(name, x, guiTop - 60, 0xFFFFFF, true);
			//GlStateManager.translate(-x, -y, 0);
			//GlStateManager.popMatrix();
			fontRendererObj.drawString(name, guiLeft + xSize / 2 - fontRendererObj.getStringWidth(name) / 2, guiTop - 1, 0xFFFFFF, true);
		}

		for (int i = 0; i < pages.length; i++) {
			IGuidePage page = pages[i];
			if (page != null) {
				IGuideChapter chapter = pages[i].getChapter();
				String pageStrg = "Page " + (chapter.getPageIndex(pages[i]) + 1) + "/" + chapter.getAllPages().length;
				renderScaledAsciiString(pageStrg, guiLeft + 25 + i * 136, guiTop + ySize - 7, 0xFFFFFF, false, 0.8F);

				GlStateManager.color(1F, 1F, 1F);
				page.drawScreenPre(this, guiLeft + 6 + i * 142, guiTop + 7, mouseX, mouseY, partialTicks);
			}
		}
		for (ItemDisplay display : itemDisplays) {
			display.drawPre();
		}
	}

	@Override
	public void drawScreenPost(int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPost(mouseX, mouseY, partialTicks);

		for (int i = 0; i < pages.length; i++) {
			IGuidePage page = pages[i];
			if (page != null) {
				GlStateManager.color(1F, 1F, 1F);
				page.drawScreenPost(this, guiLeft + 6 + i * 142, guiTop + 7, mouseX, mouseY, partialTicks);
			}
		}
		for (ItemDisplay display : itemDisplays) {
			display.drawPost(mouseX, mouseY);
		}
	}

	@Override
	public void addOrModifyItemRenderer(ItemStack renderedStack, int x, int y, float scale, boolean shouldTryTransfer) {
		for (ItemDisplay display : itemDisplays) {
			if (display.x == x && display.y == y && display.scale == scale) {
				display.stack = renderedStack;
				return;
			}
		}

		itemDisplays.add(new ItemDisplay(this, x, y, scale, renderedStack, shouldTryTransfer));
	}

	@Override
	public boolean hasPageLeftButton() {
		IGuidePage page = pages[0];
		if (page != null) {
			IGuideChapter chapter = page.getChapter();
			if (chapter != null) {
				return chapter.getPageIndex(page) > 0;
			}
		}
		return false;
	}

	@Override
	public void onPageLeftButtonPressed() {
		IGuidePage page = pages[0];
		if (page != null) {
			IGuideChapter chapter = page.getChapter();
			if (chapter != null) {
				IGuidePage[] pages = chapter.getAllPages();

				int pageNumToOpen = chapter.getPageIndex(page) - 1;
				if (pageNumToOpen >= 0 && pageNumToOpen < pages.length) {
					mc.displayGuiScreen(GuideUtils.createPageGui(previousScreen, parentPage, pages[pageNumToOpen]));
				}
			}
		}
	}

	@Override
	public boolean hasPageRightButton() {
		IGuidePage page = pages[1];
		if (page != null) {
			IGuideChapter chapter = page.getChapter();
			if (chapter != null) {
				int pageIndex = chapter.getPageIndex(page);
				int pageAmount = chapter.getAllPages().length;
				return pageIndex + 1 < pageAmount;
			}
		}
		return false;
	}

	@Override
	public void onPageRightButtonPressed() {
		IGuidePage page = pages[1];
		if (page != null) {
			IGuideChapter chapter = page.getChapter();
			if (chapter != null) {
				IGuidePage[] pages = chapter.getAllPages();

				int pageNumToOpen = chapter.getPageIndex(page) + 1;
				if (pageNumToOpen >= 0 && pageNumToOpen < pages.length) {
					mc.displayGuiScreen(GuideUtils.createPageGui(previousScreen, parentPage, pages[pageNumToOpen]));
				}
			}
		}
	}

	@Override
	public boolean hasBackButton() {
		return true;
	}

	@Override
	public void onBackButtonPressed() {
		if (!isShiftKeyDown()) {
			mc.displayGuiScreen(parentPage);
		}
		else {
			super.onBackButtonPressed();
		}
	}
}