package p455w0rd.p455w0rdsthings.client.gui;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.EnergyMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.FluidMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileInterchanger.ItemMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.AccessMode;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileMachineBase.RedstoneMode;
import p455w0rd.p455w0rdsthings.container.ContainerInterchanger;
import p455w0rd.p455w0rdsthings.init.ModGlobals;
import p455w0rd.p455w0rdsthings.init.ModNetworking;
import p455w0rd.p455w0rdsthings.network.PacketSetAccessMode;
import p455w0rd.p455w0rdsthings.network.PacketSetInterchangerMode;
import p455w0rd.p455w0rdsthings.network.PacketSetRedstoneMode;
import p455w0rdslib.api.gui.IGuiElement;
import p455w0rdslib.api.gui.IGuiListItem;
import p455w0rdslib.client.gui.GuiModular;
import p455w0rdslib.client.gui.element.GuiButton2State;
import p455w0rdslib.client.gui.element.GuiButtonMultiState;
import p455w0rdslib.client.gui.element.GuiList;
import p455w0rdslib.client.gui.element.GuiPlayerListItem;
import p455w0rdslib.client.gui.element.GuiPos;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.GuiUtils;
import p455w0rdslib.util.PlayerUUIDUtils;
import p455w0rdslib.util.PlayerUtils;

/**
 * @author p455w0rd
 *
 */
public class GuiInterchanger extends GuiModular {
	//248x166
	public static final ResourceLocation BACKGROUND = new ResourceLocation(ModGlobals.MODID, "textures/gui/interchanger.png");

	public GuiInterchanger(EntityPlayer player, TileInterchanger tile) {
		super(new ContainerInterchanger(player, tile));
		xSize = 201;
		ySize = 136;
		setTitle("Interchanger");
		setTitleColor(0xFFA74CFF);
		setTitleScale(0.9F);
		setTitleOffsetY(7);
		setBackgroundTexture(BACKGROUND);
		UUID owner = tile.getOwner();
		List<IGuiListItem> playerList = Lists.<IGuiListItem>newArrayList();
		List<UUID> accessList = tile.getAccessList();
		List<UUID> serverPlayerList = PlayerUtils.getFullPlayerList();
		playerList.add(new GuiPlayerListItem(PlayerUUIDUtils.getPlayerName(owner) + " (Owner)", owner).setDisabled(true));
		serverPlayerList.remove(tile.getOwner());

		for (int j = 0; j < serverPlayerList.size(); j++) {
			for (int i = 0; i < accessList.size(); i++) {
				if (serverPlayerList.get(j) == accessList.get(i) || serverPlayerList.get(j).equals(tile.getOwner())) {
					if (!serverPlayerList.get(j).equals(tile.getOwner())) {
						playerList.add(new GuiPlayerListItem(PlayerUUIDUtils.getPlayerName(accessList.get(i)), accessList.get(i)) {
							@Override
							public IGuiListItem setHighlighted(boolean isSelected) {
								if (isSelected) {
									tile.addPlayerToAccessList(getID());
								}
								else {
									tile.removePlayerFromAccessList(getID());
								}
								tile.markDirty();
								//ModNetworking.INSTANCE.sendToServer(new PacketModifyAccessList(tile.pos(), isSelected, tile.getWorld().provider.getDimension(), getID()));
								return super.setHighlighted(isSelected);
							}
						}.setHighlighted());
					}
					serverPlayerList.remove(j);
				}
			}
		}
		for (UUID uuid : serverPlayerList) {
			playerList.add(new GuiPlayerListItem(PlayerUUIDUtils.getPlayerName(uuid), uuid) {
				@Override
				public IGuiListItem setHighlighted(boolean isSelected) {
					if (isSelected) {
						tile.addPlayerToAccessList(getID());
					}
					else {
						tile.removePlayerFromAccessList(getID());
					}
					tile.markDirty();
					//ModNetworking.INSTANCE.sendToServer(new PacketModifyAccessList(tile.pos(), isSelected, tile.getWorld().provider.getDimension(), getID()));
					return super.setHighlighted(isSelected);
				}
			});
		}
		/*
		playerList.add(new GuiPlayerListItem(PlayerUUIDUtils.getPlayerName(UUID.fromString("069a79f4-44e9-4726-a5be-fca90e38aaf5")), UUID.fromString("069a79f4-44e9-4726-a5be-fca90e38aaf5")) {
			@Override
			public IGuiListItem setHighlighted(boolean isSelected) {
				if (isSelected) {
					tile.addPlayerToAccessList(getID());
				}
				else {
					tile.removePlayerFromAccessList(getID());
				}
				tile.markDirty();
				//ModNetworking.INSTANCE.sendToServer(new PacketModifyAccessList(tile.pos(), isSelected, tile.getWorld().provider.getDimension(), getID()));
				return super.setHighlighted(isSelected);
			}
		}.setHighlighted(tile.getAccessList().contains(UUID.fromString("069a79f4-44e9-4726-a5be-fca90e38aaf5"))));
		*/
		//addElement(new GuiCheckbox(this, new GuiPos(6, 36), "Public Mode", true, 75));
		addElement(new GuiButton2State(this, new GuiPos(6, 56), 13, "Access Mode: Public", "Access Mode: Private") {
			@Override
			public boolean onClick(int x, int y) {
				if (x >= getX() && y >= getY() && x < getX() + getWidth() && y < getY() + getHeight()) {
					cycleState();
					tile.setAccessMode(getState() ? AccessMode.PUBLIC : AccessMode.PRIVATE);
					ModNetworking.INSTANCE.sendToServer(new PacketSetAccessMode(tile.pos(), getState(), tile.getDimension()));
					for (IGuiElement element : getElements()) {
						if (element instanceof GuiList) {
							element.setVisible(getState() ? false : true);
						}
					}
					initGui();
					return true;
				}
				return false;
			}
		}.setState(tile.getAccessMode() == AccessMode.PUBLIC ? true : false).setBGColor(0xFF6E03C8));

		addElement(new GuiButtonMultiState(this, new GuiPos(6, 72), 13, Lists.newArrayList("Energy Mode: Sending", "Energy Mode: Receiving", "Energy Mode: Both", "Energy Mode: Disabled")) {
			@Override
			public boolean onClick(int x, int y) {
				if (x >= getX() && y >= getY() && x < getX() + getWidth() && y < getY() + getHeight()) {
					cycleState();
					tile.setEnergyMode(EnergyMode.values()[getState()]);
					ModNetworking.INSTANCE.sendToServer(new PacketSetInterchangerMode(tile.pos(), 0, getState(), tile.getDimension()));
					return true;
				}
				return false;
			}
		}.setState(tile.getEnergyMode().ordinal()).setBGColor(0xFF6E03C8));

		addElement(new GuiButtonMultiState(this, new GuiPos(6, 88), 13, Lists.newArrayList("Item Mode: Sending", "Item Mode: Receiving", "Item Mode: Both", "Item Mode: Disabled")) {
			@Override
			public boolean onClick(int x, int y) {
				if (x >= getX() && y >= getY() && x < getX() + getWidth() && y < getY() + getHeight()) {
					cycleState();
					tile.setItemMode(ItemMode.values()[getState()]);
					ModNetworking.INSTANCE.sendToServer(new PacketSetInterchangerMode(tile.pos(), 1, getState(), tile.getDimension()));
					return true;
				}
				return false;
			}
		}.setState(tile.getItemMode().ordinal()).setBGColor(0xFF6E03C8));

		addElement(new GuiButtonMultiState(this, new GuiPos(6, 104), 13, Lists.newArrayList("Fluid Mode: Sending", "Fluid Mode: Receiving", "Fluid Mode: Both", "Fluid Mode: Disabled")) {
			@Override
			public boolean onClick(int x, int y) {
				if (x >= getX() && y >= getY() && x < getX() + getWidth() && y < getY() + getHeight()) {
					cycleState();
					tile.setFluidMode(FluidMode.values()[getState()]);
					ModNetworking.INSTANCE.sendToServer(new PacketSetInterchangerMode(tile.pos(), 2, getState(), tile.getDimension()));
					return true;
				}
				return false;
			}
		}.setState(tile.getFluidMode().ordinal()).setBGColor(0xFF6E03C8));

		addElement(new GuiButtonMultiState(this, new GuiPos(6, 120), 13, Lists.newArrayList("Redstone Mode: Required", "Redstone Mode: Active Without Signal", "Redstone Mode: Ignored")) {
			@Override
			public boolean onClick(int x, int y) {
				if (x >= getX() && y >= getY() && x < getX() + getWidth() && y < getY() + getHeight()) {
					cycleState();
					tile.setRedstoneMode(RedstoneMode.values()[getState()]);
					ModNetworking.INSTANCE.sendToServer(new PacketSetRedstoneMode(tile.pos(), getState(), tile.getDimension()));
					return true;
				}
				return false;
			}
		}.setState(tile.getRedstoneMode().ordinal()).setBGColor(0xFF6E03C8));

		setTooltipColor(0xFF6E03C8, true);
		setTooltipColor(0x996E03C8, false);
		addElement(new GuiList(this, new GuiPos(6, 145), 189, 68, playerList).setBorderColor(0xFF000000).setBackgroundColor(0x336E03C8).setSelectedTextColor(0xFFFFFFFF).setSelectedBackgroundColor(0xFF6E03C8).setDisabledTextColor(0xFFA74CFF).generateScrollbar().setTooltip(Lists.newArrayList(I18n.format("gui.cos.HellFirePVP.isSoFcking.ANAL.about.indev.screenshots.desc"))).setVisible(tile.getAccessMode() == AccessMode.PRIVATE ? true : false));
	}

	@Override
	public void initGui() {
		if (((ContainerInterchanger) inventorySlots).getTile().getAccessMode() == AccessMode.PRIVATE) {
			ySize = 136 + 80;
		}
		else {
			ySize = 136;
		}
		super.initGui();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1, 1, 1, 1);
		GuiUtils.bindTexture(BACKGROUND);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		GlStateManager.translate(0.0F, 0.0F, 0.0F);
		this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, 136);
		if (((ContainerInterchanger) inventorySlots).getTile().getAccessMode() == AccessMode.PRIVATE) {
			for (int i = 0; i <= 80; i++) {
				this.drawTexturedModalRect(guiLeft, guiTop + 136 + i, 0, 136, xSize, 3);
				i += 2;
			}
		}
		this.drawTexturedModalRect(guiLeft, guiTop + ySize, 0, 176, xSize, 6);
		GlStateManager.pushMatrix();
		GlStateManager.translate(guiLeft, guiTop, 0.0F);
		drawElements(partialTicks, x, y, false);
		GlStateManager.popMatrix();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableLighting();
		GlStateManager.disableBlend();
		//GuiUtils.getFontRenderer().drawStringWithShadow(getTitle(), 4, 6, titleColor);
		GuiUtils.drawScaledString("Channel Code", 100, 30, 0.75F, 0xFFFFFFFF);
		if (((ContainerInterchanger) inventorySlots).getTile().getAccessMode() == AccessMode.PRIVATE) {
			GuiUtils.drawScaledString("Access List", 106, 182, 0.75F, 0xFFFFFFFF);
		}
		GlStateManager.enableBlend();
		GlStateManager.enableLighting();
	}

	@Override
	protected void renderToolTip(ItemStack stack, int x, int y) {
		List<String> list = stack.getTooltip(EasyMappings.player(), mc.gameSettings.advancedItemTooltips);
		String formatted = (TextFormatting.getTextWithoutFormattingCodes(list.get(0).toLowerCase()).equals("wool") ? "White" : TextFormatting.getTextWithoutFormattingCodes(list.get(0)).replaceFirst(" Wool", ""));
		int numSpaces = ((16 - formatted.length()) / 2) + 1;
		String finalString = StringUtils.repeat(" ", numSpaces) + "" + formatted;
		net.minecraftforge.fml.client.config.GuiUtils.preItemToolTip(stack);
		GuiUtils.drawToolTipWithBorderColor(this, Lists.newArrayList(finalString, "Click to change"), x, y, getTooltipColors()[0], getTooltipColors()[1]);
		net.minecraftforge.fml.client.config.GuiUtils.postItemToolTip();
	}

}
