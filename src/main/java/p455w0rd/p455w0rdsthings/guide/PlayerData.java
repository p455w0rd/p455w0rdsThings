package p455w0rd.p455w0rdsthings.guide;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiGuide;
import p455w0rd.p455w0rdsthings.util.GuideUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class PlayerData {

	public static PlayerSave getDataFromPlayer(EntityPlayer player) {
		WorldData worldData = WorldData.get(player.getEntityWorld());
		ConcurrentHashMap<UUID, PlayerSave> data = worldData.playerSaveData;
		UUID id = player.getUniqueID();

		if (data.containsKey(id)) {
			PlayerSave save = data.get(id);
			if (save != null && save.id != null && save.id.equals(id)) {
				worldData.markDirty();
				return save;
			}
		}

		//Add Data if none is existant
		PlayerSave save = new PlayerSave(id);
		data.put(id, save);
		worldData.markDirty();
		return save;
	}

	public static class PlayerSave {

		public UUID id;
		public boolean bookGottenAlready;
		public boolean didBookTutorial;
		public Object[] bookmarks = new Object[12];

		@SideOnly(Side.CLIENT)
		public GuiGuide lastOpenBooklet;

		public PlayerSave(UUID id) {
			this.id = id;
		}

		public void readFromNBT(NBTTagCompound compound, boolean savingToFile) {
			bookGottenAlready = compound.getBoolean("PBookGotten");
			didBookTutorial = compound.getBoolean("PDidTutorial");
			NBTTagList bookmarks = compound.getTagList("PBookmarks", 8);
			loadBookmarks(bookmarks);
		}

		public void writeToNBT(NBTTagCompound compound, boolean savingToFile) {
			compound.setBoolean("PBookGotten", bookGottenAlready);
			compound.setBoolean("PDidTutorial", didBookTutorial);
			compound.setTag("PBookmarks", saveBookmarks());
		}

		public NBTTagList saveBookmarks() {
			NBTTagList bookmarks = new NBTTagList();
			for (Object bookmark : this.bookmarks) {
				if (bookmark == null) {
					continue;
				}
				String str = "";
				if (bookmark instanceof IGuidePage) {
					str = ((IGuidePage) bookmark).getIdentifier();
				}
				else {
					str = ((IGuideEntry) bookmark).getIdentifier();
				}
				bookmarks.appendTag(new NBTTagString(bookmark == null ? "" : str));
			}
			return bookmarks;
		}

		public void loadBookmarks(NBTTagList bookmarks) {
			for (int i = 0; i < bookmarks.tagCount(); i++) {
				String strg = bookmarks.getStringTagAt(i);
				if (strg != null && !strg.isEmpty()) {
					IGuidePage page = GuideUtils.getBookletPageById(strg);
					if (page != null) {
						this.bookmarks[i] = page;
					}
					else {
						this.bookmarks[i] = GuideUtils.createEntry(strg);
					}
				}
				else {
					this.bookmarks[i] = null;
				}
			}
		}
	}

}