package p455w0rd.p455w0rdsthings.guide.entry;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.util.GuideUtils;
import p455w0rdslib.util.EasyMappings;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class GuideEntry implements IGuideEntry {

	private final String identifier;
	private final List<IGuideChapter> chapters = new ArrayList<IGuideChapter>();
	private TextFormatting color;
	private final int priority;

	public GuideEntry(String identifier) {
		this(identifier, 0);
	}

	public GuideEntry(String identifier, int priority) {
		this.identifier = identifier;
		this.priority = priority;
		GuideUtils.addGuideEntry(this);

		color = TextFormatting.RESET;
	}

	@SideOnly(Side.CLIENT)
	private static boolean fitsFilter(IGuidePage page, String searchBarText) {
		Minecraft mc = Minecraft.getMinecraft();

		List<ItemStack> items = new ArrayList<ItemStack>();
		page.getItemStacksForPage(items);
		if (!items.isEmpty()) {
			for (ItemStack stack : items) {
				if (stack != null) {
					List<String> tooltip = stack.getTooltip(EasyMappings.player(), mc.gameSettings.advancedItemTooltips);
					for (String strg : tooltip) {
						if (strg != null && strg.toLowerCase(Locale.ROOT).contains(searchBarText)) {
							return true;
						}
					}
				}
			}
		}

		List<FluidStack> fluids = new ArrayList<FluidStack>();
		page.getFluidStacksForPage(fluids);
		if (!fluids.isEmpty()) {
			for (FluidStack stack : fluids) {
				if (stack != null) {
					String strg = stack.getLocalizedName();
					if (strg != null && strg.toLowerCase(Locale.ROOT).contains(searchBarText)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	@Override
	public List<IGuideChapter> getAllChapters() {
		return chapters;
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public String getLocalizedName() {
		return I18n.translateToLocal("guide.indexEntry." + getIdentifier() + ".name");
	}

	@Override
	public String getLocalizedNameWithFormatting() {
		return color + getLocalizedName();
	}

	@Override
	public void addChapter(IGuideChapter chapter) {
		chapters.add(chapter);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public List<IGuideChapter> getChaptersForDisplay(String searchBarText) {
		if (searchBarText != null && !searchBarText.isEmpty()) {
			String search = searchBarText.toLowerCase(Locale.ROOT);

			List<IGuideChapter> fittingChapters = new ArrayList<IGuideChapter>();
			for (IGuideChapter chapter : getAllChapters()) {
				if (chapter.getLocalizedName().toLowerCase(Locale.ROOT).contains(search)) {
					fittingChapters.add(chapter);
				}
				else {
					for (IGuidePage page : chapter.getAllPages()) {
						if (fitsFilter(page, search)) {
							fittingChapters.add(chapter);
							break;
						}
					}
				}
			}

			return fittingChapters;
		}
		else {
			return getAllChapters();
		}
	}

	public GuideEntry setImportant() {
		color = TextFormatting.DARK_GREEN;
		return this;
	}

	public GuideEntry setSpecial() {
		color = TextFormatting.DARK_PURPLE;
		return this;
	}

	@Override
	public int getSortingPriority() {
		return priority;
	}

}