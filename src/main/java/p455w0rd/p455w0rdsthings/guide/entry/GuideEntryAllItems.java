package p455w0rd.p455w0rdsthings.guide.entry;

import java.util.List;

import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.init.ModGuide;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class GuideEntryAllItems extends GuideEntry {

	public GuideEntryAllItems(String identifier) {
		super(identifier);
	}

	@Override
	public void addChapter(IGuideChapter chapter) {

	}

	@Override
	public List<IGuideChapter> getAllChapters() {
		return ModGuide.ALL_CHAPTERS;
	}
}