package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rdslib.util.RenderUtils;

/**
 * @author p455w0rd
 *
 */
public class PageCraftingEntity extends PageCrafting {

	Entity entity = null;
	int rotation = 0;
	int ticks = 0;
	float scale = 1.0f;
	int yOff = 0;

	public PageCraftingEntity(int localizationKey, Entity entityIn, List<IRecipe> recipes) {
		super(localizationKey, recipes);
		entity = entityIn;
	}

	public PageCraftingEntity(int localizationKey, Entity entityIn, IRecipe... recipes) {
		super(localizationKey, recipes);
		entity = entityIn;
	}

	public PageCraftingEntity(int localizationKey, Entity entityIn, int yOffset, IRecipe... recipes) {
		super(localizationKey, recipes);
		entity = entityIn;
		yOff = yOffset;
	}

	public PageCraftingEntity(int localizationKey, Entity entityIn, int yOffset, List<IRecipe> recipes) {
		super(localizationKey, recipes);
		entity = entityIn;
		yOff = yOffset;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPost(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPost(gui, startX, startY, mouseX, mouseY, partialTicks);
		float height = entity.height;
		height = (float) ((height - 1) * .7 + 1);
		float s = 1.5f * ((32 * 14.0f / 25) / height);
		RenderUtils.renderEntity(entity, startX + 50, (startY + 120) + yOff, (s * 2) * scale, getRotation());
		incrRotation();
	}

	public void incrRotation() {
		if (ticks < 250) {
			ticks++;
		}
		else {
			ticks = 0;
		}
		if ((ticks % 5) == 0) {
			if (!GuiScreen.isShiftKeyDown()) {
				rotation++;
				if (rotation >= 360) {
					rotation = 0;
				}
			}
		}
	}

	public PageCraftingEntity setScale(float s) {
		scale = s;
		return this;
	}

	public float getRotation() {
		return rotation;
	}

}
