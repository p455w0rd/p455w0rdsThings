package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.entity.EntityFrienderman;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.RenderUtils;

/**
 * @author p455w0rd
 *
 */
public class PageEntity extends GuidePage {

	List<Entity> entities = Lists.<Entity>newArrayList();
	int rotation = 0;
	int ticks = 0;
	float scale = 1.0f;
	int listIndex = 0;
	int yOff = -1;
	boolean posOff, posInit = false;

	public PageEntity(int locKey, Entity entityIn) {
		this(locKey, entityIn, 1.0F);
	}

	public PageEntity(int locKey, Entity entityIn, float scale) {
		this(locKey, 0, Lists.<Entity>newArrayList(entityIn), -1, scale);
	}

	public PageEntity(int locKey, Entity entityIn, int yOffset, float scale) {
		this(locKey, 0, Lists.<Entity>newArrayList(entityIn), yOffset, scale);
	}

	public PageEntity(int locKey, List<Entity> entitiesIn) {
		this(locKey, 0, entitiesIn, -1, 1.0F);
	}

	public PageEntity(int locKey, int priority, List<Entity> entitiesIn) {
		this(locKey, priority, entitiesIn, -1, 1.0F);
	}

	public PageEntity(int locKey, List<Entity> entitiesIn, float scaleIn) {
		this(locKey, 0, entitiesIn, -1, scaleIn);
	}

	public PageEntity(int locKey, int priority, List<Entity> entitiesIn, int yOffset, float scaleIn) {
		super(locKey, priority);
		entities = entitiesIn;
		scale = scaleIn;
		yOff = yOffset;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPost(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPost(gui, startX, startY, mouseX, mouseY, partialTicks);
		float height = getCurrentEntity().height;
		height = (float) ((height - 1) * .7 + 1);
		float s = 1.5f * ((32 * 14.0f / 25) / height);
		RenderUtils.renderEntity(getCurrentEntity(), startX + 50, (startY + 125) + yOff, s * 4, getRotation());
		incrRotation();
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {
		super.updateScreen(gui, startX, startY, pageTimer);
		if (getCurrentEntity() == null) {
			return;
		}
		else if (getCurrentEntity().getEntityWorld() == null) {
			getCurrentEntity().setWorld(EasyMappings.world());
		}

		if (GuiScreen.isAltKeyDown() && getCurrentEntity() != null && getCurrentEntity() instanceof EntityFrienderman) {
			((EntityFrienderman) getCurrentEntity()).doLidAnim();
		}

	}

	public void cycleEntity() {
		int newIndex = listIndex + 1;
		if (newIndex >= entities.size()) {
			listIndex = 0;
			return;
		}
		listIndex++;
		if (getCurrentEntity() != null && getCurrentEntity() instanceof EntityFrienderman) {
			((EntityFrienderman) getCurrentEntity()).doLidAnim();
		}
	}

	public void incrRotation() {
		if (ticks < 360) {
			ticks++;
		}
		else {
			ticks = 0;

		}
		if ((ticks % 5) == 0) {
			if (!GuiScreen.isShiftKeyDown()) {
				rotation++;
				if (rotation >= 360) {
					rotation = 0;
					cycleEntity();
				}
			}
		}

	}

	public Entity getCurrentEntity() {
		return entities.get(listIndex);
	}

	public float getRotation() {
		return rotation;
	}

}
