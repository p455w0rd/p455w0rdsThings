package p455w0rd.p455w0rdsthings.guide.page;

import javax.annotation.Nonnull;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;

/**
 * @author p455w0rd
 *
 */
public class PageImage extends GuidePage {

	final ResourceLocation image;

	public PageImage(int localizationKey, @Nonnull ResourceLocation location) {
		super(localizationKey);
		image = location;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);
		gui.mc.getTextureManager().bindTexture(image);
		//PngSizeInfo size = GuiUtils.drawTexturedModalRect(startX + 5, startY + 6, 0, 156, 88, 51, 0);

	}

}
