package p455w0rd.p455w0rdsthings.guide.page;

import java.util.Arrays;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiGuide;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rdslib.util.GuiUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class PageCrafting extends GuidePage {

	private final List<IRecipe> recipes;
	private int recipeAt;
	private String recipeTypeLocKey;
	private boolean isWildcard;

	public PageCrafting(int localizationKey, List<IRecipe> recipes) {
		super(localizationKey);
		this.recipes = recipes;
	}

	public PageCrafting(int localizationKey, IRecipe... recipes) {
		this(localizationKey, Arrays.asList(recipes));
	}

	public GuidePage setWildcard() {
		isWildcard = true;
		return this;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);

		gui.mc.getTextureManager().bindTexture(GuiGuide.RES_LOC_GADGETS);
		GuiUtils.drawTexturedModalRect(startX + 5, startY + 6, 20, 0, 116, 54, 0);

		gui.renderScaledAsciiString("(" + I18n.translateToLocal(recipeTypeLocKey) + ")", startX + 6, startY + 65, 0, false, 0.65F);

		PageTextOnly.renderTextToPage(gui, this, startX + 6, startY + 80);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {
		super.updateScreen(gui, startX, startY, pageTimer);

		if (pageTimer % 20 == 0) {
			findRecipe(gui, startX, startY);
		}
	}

	private void findRecipe(GuiGuideBase gui, int startX, int startY) {
		if (!recipes.isEmpty()) {
			IRecipe recipe = recipes.get(recipeAt);
			if (recipe != null) {
				setupRecipe(gui, recipe, startX, startY);
			}

			recipeAt++;
			if (recipeAt >= recipes.size()) {
				recipeAt = 0;
			}
		}
	}

	@Override
	public void initGui(GuiGuideBase gui, int startX, int startY) {
		super.initGui(gui, startX, startY);
		findRecipe(gui, startX, startY);
	}

	@Override
	public void getItemStacksForPage(List<ItemStack> list) {
		super.getItemStacksForPage(list);

		if (!recipes.isEmpty()) {
			for (IRecipe recipe : recipes) {
				if (recipe != null) {
					ItemStack output = recipe.getRecipeOutput();
					if (output != null) {
						ItemStack copy = output.copy();
						if (isWildcard) {
							copy.setItemDamage(OreDictionary.WILDCARD_VALUE);
						}
						list.add(copy);
					}
				}
			}
		}
	}

	private void setupRecipe(GuiGuideBase gui, IRecipe recipe, int startX, int startY) {
		ItemStack[] stacks = new ItemStack[9];
		int width = 3;
		int height = 3;

		if (recipe instanceof ShapedRecipes) {
			ShapedRecipes shaped = (ShapedRecipes) recipe;
			width = shaped.recipeWidth;
			height = shaped.recipeHeight;
			stacks = shaped.recipeItems;
			recipeTypeLocKey = "guide.shapedRecipe";
		}
		else if (recipe instanceof ShapelessRecipes) {
			ShapelessRecipes shapeless = (ShapelessRecipes) recipe;
			for (int i = 0; i < shapeless.recipeItems.size(); i++) {
				stacks[i] = shapeless.recipeItems.get(i);
			}
			recipeTypeLocKey = "guide.shapelessRecipe";
		}
		else if (recipe instanceof ShapedOreRecipe) {
			ShapedOreRecipe shaped = (ShapedOreRecipe) recipe;
			try {
				width = ReflectionHelper.getPrivateValue(ShapedOreRecipe.class, shaped, 4);
				height = ReflectionHelper.getPrivateValue(ShapedOreRecipe.class, shaped, 5);
			}
			catch (Exception e) {
				ModLogger.error("Something went wrong trying to get the Crafting Recipe in the booklet to display!");
			}
			for (int i = 0; i < shaped.getInput().length; i++) {
				Object input = shaped.getInput()[i];
				if (input != null) {
					stacks[i] = input instanceof ItemStack ? (ItemStack) input : (((List<ItemStack>) input).isEmpty() ? null : ((List<ItemStack>) input).get(0));
				}
			}
			recipeTypeLocKey = "guide.shapedOreRecipe";
		}
		else if (recipe instanceof ShapelessOreRecipe) {
			ShapelessOreRecipe shapeless = (ShapelessOreRecipe) recipe;
			for (int i = 0; i < shapeless.getInput().size(); i++) {
				Object input = shapeless.getInput().get(i);
				stacks[i] = input instanceof ItemStack ? (ItemStack) input : (((List<ItemStack>) input).isEmpty() ? null : ((List<ItemStack>) input).get(0));
			}
			recipeTypeLocKey = "guide.shapelessOreRecipe";
		}

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				ItemStack stack = stacks[y * width + x];
				if (stack != null) {
					ItemStack copy = stack.copy();
					copy.stackSize = 1;
					if (copy.getItemDamage() == OreDictionary.WILDCARD_VALUE) {
						copy.setItemDamage(0);
					}

					gui.addOrModifyItemRenderer(copy, startX + 6 + x * 18, startY + 7 + y * 18, 1F, true);
				}
			}
		}

		gui.addOrModifyItemRenderer(recipe.getRecipeOutput(), startX + 100, startY + 25, 1F, false);
	}
}