package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import p455w0rd.p455w0rdsthings.api.ICompressorRecipe;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.client.gui.GuiCompressor;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiGuide;
import p455w0rd.p455w0rdsthings.init.ModLogger;
import p455w0rdslib.util.GuiUtils;

/**
 * @author p455w0rd
 *
 */
public class PageCompressingCrafting extends GuidePage {

	private final ICompressorRecipe compressorRecipe;
	private final List<IRecipe> recipes;
	private int recipeAt;
	private String[] recipeTypeLocKey = new String[2];
	int progress = 0;
	private boolean isWildcard;

	public PageCompressingCrafting(int localizationKey, ICompressorRecipe compRecipe, IRecipe recipe) {
		this(localizationKey, compRecipe, Lists.<IRecipe>newArrayList(recipe));
	}

	public PageCompressingCrafting(int localizationKey, ICompressorRecipe compRecipe, List<IRecipe> recipes) {
		super(localizationKey);
		compressorRecipe = compRecipe;
		this.recipes = recipes;
		recipeTypeLocKey[0] = "guide.compressorRecipe";
	}

	public GuidePage setWildcard() {
		isWildcard = true;
		return this;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);

		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GlStateManager.pushMatrix();
		GuiUtils.drawTexturedModalRect(startX + 5, startY + 6, 0, 156, 88, 51, 0);
		gui.mc.fontRendererObj.drawStringWithShadow("RF: " + compressorRecipe.getEnergyRequired(), startX + 11, startY + 35, 0x17FF6D);
		if (compressorRecipe.getSecondOutput() != null) {
			GlStateManager.scale(0.5, 0.5, 0.5);
			gui.mc.fontRendererObj.drawStringWithShadow("2nd Item Chance: " + (int) (compressorRecipe.getSecondOutputChance() * 100) + "%", startX * 2 + 21, startY * 2 + 92, 0x17FF6D);
			GlStateManager.scale(2f, 2f, 2f);
		}
		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GuiUtils.drawTexturedModalRect(startX + 41, startY + 15, 203, 1, progress, 13, 0);
		gui.renderScaledAsciiString("(" + I18n.translateToLocal(recipeTypeLocKey[0]) + ")", startX + 6, startY + 61, 0, false, 0.65F);
		GlStateManager.color(1F, 1F, 1F, 1F);
		GuiUtils.bindTexture(GuiGuide.RES_LOC_GADGETS);
		GuiUtils.drawTexturedModalRect(startX + 5, startY + 81, 20, 0, 116, 54, 0);
		gui.renderScaledAsciiString("(" + I18n.translateToLocal(recipeTypeLocKey[1]) + ")", startX + 6, startY + 140, 0, false, 0.65F);
		GlStateManager.popMatrix();

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {
		super.updateScreen(gui, startX, startY, pageTimer);
		if (pageTimer % 5 == 0) {
			progress++;
		}
		if (progress > 20) {
			progress = 0;
		}
		if (pageTimer % 20 == 0) {
			findRecipe(gui, startX, startY);
		}
	}

	private void findRecipe(GuiGuideBase gui, int startX, int startY) {
		if (!recipes.isEmpty()) {
			IRecipe recipe = recipes.get(recipeAt);
			if (recipe != null) {
				setupRecipe(gui, recipe, startX, startY + 75);
			}

			recipeAt++;
			if (recipeAt >= recipes.size()) {
				recipeAt = 0;
			}
		}
	}

	@Override
	public void initGui(GuiGuideBase gui, int startX, int startY) {
		super.initGui(gui, startX, startY);
		setupRecipe(gui, compressorRecipe, startX, startY);
		findRecipe(gui, startX, startY);
	}

	@Override
	public void getItemStacksForPage(List<ItemStack> list) {
		super.getItemStacksForPage(list);
		if (compressorRecipe != null) {
			//list.add(compressorRecipe.getInput().copy());
			list.add(compressorRecipe.getOutput().copy());
			if (compressorRecipe.hasSecondOutput()) {
				list.add(compressorRecipe.getSecondOutput().copy());
			}
		}
		if (!recipes.isEmpty()) {
			for (IRecipe recipe : recipes) {
				if (recipe != null) {
					ItemStack output = recipe.getRecipeOutput();
					if (output != null) {
						ItemStack copy = output.copy();
						if (isWildcard) {
							copy.setItemDamage(OreDictionary.WILDCARD_VALUE);
						}
						list.add(copy);
					}
				}
			}
		}
	}

	private void setupRecipe(GuiGuideBase gui, ICompressorRecipe recipe, int startX, int startY) {
		gui.addOrModifyItemRenderer(recipe.getInput(), startX + 12, startY + 13, 1F, true);
		gui.addOrModifyItemRenderer(recipe.getOutput(), startX + 70, startY + 13, 1F, false);
		if (recipe.hasSecondOutput()) {
			gui.addOrModifyItemRenderer(recipe.getSecondOutput(), startX + 70, startY + 35, 1F, false);
		}
	}

	private void setupRecipe(GuiGuideBase gui, IRecipe recipe, int startX, int startY) {
		ItemStack[] stacks = new ItemStack[9];
		int width = 3;
		int height = 3;

		if (recipe instanceof ShapedRecipes) {
			ShapedRecipes shaped = (ShapedRecipes) recipe;
			width = shaped.recipeWidth;
			height = shaped.recipeHeight;
			stacks = shaped.recipeItems;
			recipeTypeLocKey[1] = "guide.shapedRecipe";
		}
		else if (recipe instanceof ShapelessRecipes) {
			ShapelessRecipes shapeless = (ShapelessRecipes) recipe;
			for (int i = 0; i < shapeless.recipeItems.size(); i++) {
				stacks[i] = shapeless.recipeItems.get(i);
			}
			recipeTypeLocKey[1] = "guide.shapelessRecipe";
		}
		else if (recipe instanceof ShapedOreRecipe) {
			ShapedOreRecipe shaped = (ShapedOreRecipe) recipe;
			try {
				width = ReflectionHelper.getPrivateValue(ShapedOreRecipe.class, shaped, 4);
				height = ReflectionHelper.getPrivateValue(ShapedOreRecipe.class, shaped, 5);
			}
			catch (Exception e) {
				ModLogger.error("Something went wrong trying to get the Crafting Recipe in the booklet to display!");
			}
			for (int i = 0; i < shaped.getInput().length; i++) {
				Object input = shaped.getInput()[i];
				if (input != null) {
					stacks[i] = input instanceof ItemStack ? (ItemStack) input : (((List<ItemStack>) input).isEmpty() ? null : ((List<ItemStack>) input).get(0));
				}
			}
			recipeTypeLocKey[1] = "guide.shapedOreRecipe";
		}
		else if (recipe instanceof ShapelessOreRecipe) {
			ShapelessOreRecipe shapeless = (ShapelessOreRecipe) recipe;
			for (int i = 0; i < shapeless.getInput().size(); i++) {
				Object input = shapeless.getInput().get(i);
				stacks[i] = input instanceof ItemStack ? (ItemStack) input : (((List<ItemStack>) input).isEmpty() ? null : ((List<ItemStack>) input).get(0));
			}
			recipeTypeLocKey[1] = "guide.shapelessOreRecipe";
		}

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				ItemStack stack = stacks[y * width + x];
				if (stack != null) {
					ItemStack copy = stack.copy();
					copy.stackSize = 1;
					if (copy.getItemDamage() == OreDictionary.WILDCARD_VALUE) {
						copy.setItemDamage(0);
					}

					gui.addOrModifyItemRenderer(copy, startX + 6 + x * 18, startY + 7 + y * 18, 1F, true);
				}
			}
		}

		gui.addOrModifyItemRenderer(recipe.getRecipeOutput(), startX + 100, startY + 25, 1F, false);
	}
}