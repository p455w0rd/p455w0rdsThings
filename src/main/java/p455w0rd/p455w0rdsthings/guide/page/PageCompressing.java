package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.ICompressorRecipe;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.client.gui.GuiCompressor;
import p455w0rdslib.util.GuiUtils;

/**
 * @author p455w0rd
 *
 */
public class PageCompressing extends GuidePage {

	private final ICompressorRecipe recipe;
	private final String recipeTypeLocKey = "guide.compressorRecipe";
	int progress = 0;

	public PageCompressing(int localizationKey, ICompressorRecipe recipe) {
		super(localizationKey);
		this.recipe = recipe;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);

		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GuiUtils.drawTexturedModalRect(startX + 5, startY + 6, 0, 156, 88, 51, 0);
		gui.mc.fontRendererObj.drawStringWithShadow("RF: " + recipe.getEnergyRequired(), startX + 11, startY + 35, 0x17FF6D);
		if (recipe.getSecondOutput() != null) {
			GlStateManager.pushMatrix();
			GlStateManager.scale(0.5, 0.5, 0.5);
			gui.mc.fontRendererObj.drawStringWithShadow("2nd Item Chance: " + (int) (recipe.getSecondOutputChance() * 100) + "%", startX * 2 + 21, startY * 2 + 92, 0x17FF6D);
			GlStateManager.scale(-0.5f, -0.5f, -0.5f);
			GlStateManager.popMatrix();
		}
		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GuiUtils.drawTexturedModalRect(startX + 41, startY + 15, 203, 1, progress, 13, 0);
		gui.renderScaledAsciiString("(" + I18n.translateToLocal(recipeTypeLocKey) + ")", startX + 6, startY + 61, 0, false, 0.65F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {
		super.updateScreen(gui, startX, startY, pageTimer);
		if (pageTimer % 5 == 0) {
			progress++;
		}
		if (progress > 20) {
			progress = 0;
		}
	}

	@Override
	public void initGui(GuiGuideBase gui, int startX, int startY) {
		super.initGui(gui, startX, startY);
		setupRecipe(gui, recipe, startX, startY);
	}

	@Override
	public void getItemStacksForPage(List<ItemStack> list) {
		super.getItemStacksForPage(list);
		if (recipe != null) {
			//list.add(recipe.getInput().copy());
			list.add(recipe.getOutput().copy());
			if (recipe.hasSecondOutput()) {
				list.add(recipe.getSecondOutput().copy());
			}
		}
	}

	private void setupRecipe(GuiGuideBase gui, ICompressorRecipe recipe, int startX, int startY) {
		gui.addOrModifyItemRenderer(recipe.getInput(), startX + 12, startY + 13, 1F, true);
		gui.addOrModifyItemRenderer(recipe.getOutput(), startX + 70, startY + 13, 1F, false);
		if (recipe.hasSecondOutput()) {
			gui.addOrModifyItemRenderer(recipe.getSecondOutput(), startX + 70, startY + 35, 1F, false);
		}
	}
}