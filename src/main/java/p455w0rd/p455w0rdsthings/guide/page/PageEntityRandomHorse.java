package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.HorseType;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.init.ModIntegration.Mods;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.integration.TiC;
import p455w0rdslib.util.EasyMappings;
import p455w0rdslib.util.RenderUtils;

/**
 * @author p455w0rd
 *
 */
public class PageEntityRandomHorse extends GuidePage {

	List<ItemStack> horseArmors = Lists.<ItemStack>newArrayList(new ItemStack(ModItems.HORSE_ARMOR_REDSTONE), new ItemStack(ModItems.HORSE_ARMOR_LAPIS), new ItemStack(Items.IRON_HORSE_ARMOR), new ItemStack(Items.GOLDEN_HORSE_ARMOR), new ItemStack(Items.DIAMOND_HORSE_ARMOR), new ItemStack(ModItems.HORSE_ARMOR_EMERALD), new ItemStack(ModItems.HORSE_ARMOR_CARBON));
	int rotation = 0;
	int ticks = 0;
	int listIndex = 0;
	int yOff = -1;
	int horseVariant = 0;
	boolean posOff, posInit = false;

	public PageEntityRandomHorse(int locKey, int yOffset, float scale) {
		this(locKey, 0, yOffset);
	}

	public PageEntityRandomHorse(int locKey) {
		this(locKey, 0, -1);
	}

	public PageEntityRandomHorse(int locKey, int priority) {
		this(locKey, priority, -1);
	}

	public PageEntityRandomHorse(int locKey, float scaleIn) {
		this(locKey, 0, -1);
	}

	public PageEntityRandomHorse(int locKey, int priority, int yOffset) {
		super(locKey, priority);
		yOff = yOffset;
		horseVariant = EasyMappings.world().rand.nextInt(7);
		if (Mods.TINKERS.isLoaded()) {
			TiC.addHorseArmorToList(horseArmors);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPost(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPost(gui, startX, startY, mouseX, mouseY, partialTicks);
		float height = getHorse().height;
		height = (float) ((height - 1) * .7 + 1);
		float s = 1.5f * ((32 * 14.0f / 25) / height);
		RenderUtils.renderEntity(getHorse(), startX + 50, (startY + 125) + yOff, s * 2, getRotation());
		incrRotation();
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {
		super.updateScreen(gui, startX, startY, pageTimer);
		if (getCurrentHorseArmor() == null) {
			return;
		}

	}

	public void cycleArmor() {
		int newIndex = listIndex + 1;
		if (newIndex >= horseArmors.size()) {
			listIndex = 0;
			return;
		}
		listIndex++;
	}

	public void incrRotation() {
		if (ticks < 360) {
			ticks++;
		}
		else {
			ticks = 0;

		}
		if ((ticks % 5) == 0) {
			if (!GuiScreen.isShiftKeyDown()) {
				rotation++;
				if (rotation >= 360) {
					rotation = 0;
					cycleArmor();
					horseVariant = EasyMappings.world().rand.nextInt(7);
				}
			}
		}
	}

	public EntityHorse getHorse() {
		if (FMLCommonHandler.instance().getSide().isClient()) {

			EntityHorse horse = new EntityHorse(EasyMappings.world());

			horse.setNoAI(true);
			horse.setType(HorseType.HORSE);
			horse.setHorseVariant(horseVariant);
			horse.setRotationYawHead(0f);
			horse.setHorseTamed(true);
			horse.setHorseSaddled(true);
			horse.setHorseArmorStack(horseArmors.get(listIndex));

			return horse;
		}
		return null;
	}

	public ItemStack getCurrentHorseArmor() {
		return horseArmors.get(listIndex);
	}

	public float getRotation() {
		return rotation;
	}

}
