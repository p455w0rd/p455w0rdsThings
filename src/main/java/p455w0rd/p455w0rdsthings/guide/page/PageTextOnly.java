package p455w0rd.p455w0rdsthings.guide.page;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class PageTextOnly extends GuidePage {

	public PageTextOnly(int localizationKey) {
		super(localizationKey);
	}

	@SideOnly(Side.CLIENT)
	public static void renderTextToPage(GuiGuideBase gui, GuidePage page, int x, int y) {
		String text = page.getInfoText();
		if (text != null && !text.isEmpty()) {
			gui.renderSplitScaledAsciiString(text, x, y, 0, false, 0.75F, 120);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);
		renderTextToPage(gui, this, startX + 6, startY + 5);
	}
}
