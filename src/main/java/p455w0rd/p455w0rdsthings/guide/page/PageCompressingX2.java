package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.ICompressorRecipe;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.client.gui.GuiCompressor;
import p455w0rdslib.util.GuiUtils;

/**
 * @author p455w0rd
 *
 */
public class PageCompressingX2 extends GuidePage {

	private final ICompressorRecipe[] recipes = new ICompressorRecipe[2];
	private final String recipeTypeLocKey = "guide.compressorRecipe";
	int progress = 0;

	public PageCompressingX2(int localizationKey, ICompressorRecipe... recipesIn) {
		super(localizationKey);
		recipes[0] = recipesIn[0];
		recipes[1] = recipesIn[1];
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);

		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GlStateManager.pushMatrix();

		GuiUtils.drawTexturedModalRect(startX + 5, startY + 6, 0, 156, 88, 51, 0);
		gui.mc.fontRendererObj.drawStringWithShadow("RF: " + recipes[0].getEnergyRequired(), startX + 11, startY + 35, 0x17FF6D);
		if (recipes[0].getSecondOutput() != null) {
			GlStateManager.scale(0.5, 0.5, 0.5);
			gui.mc.fontRendererObj.drawStringWithShadow("2nd Item Chance: " + (int) (recipes[0].getSecondOutputChance() * 100) + "%", startX * 2 + 21, startY * 2 + 92, 0x17FF6D);
			GlStateManager.scale(2f, 2f, 2f);
		}

		GlStateManager.color(1F, 1F, 1F, 1F);
		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GuiUtils.drawTexturedModalRect(startX + 5, startY + 81, 0, 156, 88, 51, 0);
		gui.mc.fontRendererObj.drawStringWithShadow("RF: " + recipes[1].getEnergyRequired(), startX + 11, startY + 110, 0x17FF6D);
		if (recipes[1].getSecondOutput() != null) {
			GlStateManager.scale(0.5, 0.5, 0.5);
			gui.mc.fontRendererObj.drawStringWithShadow("2nd Item Chance: " + (int) (recipes[1].getSecondOutputChance() * 100) + "%", startX * 2 + 21, startY * 2 + 167, 0x17FF6D);
			GlStateManager.scale(2f, 2f, 2f);
		}

		GlStateManager.color(1F, 1F, 1F, 1F);
		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GuiUtils.drawTexturedModalRect(startX + 41, startY + 15, 203, 1, progress, 13, 0);
		gui.mc.getTextureManager().bindTexture(GuiCompressor.BACKGROUND);
		GuiUtils.drawTexturedModalRect(startX + 41, startY + 90, 203, 1, progress, 13, 0);
		gui.renderScaledAsciiString("(" + I18n.translateToLocal(recipeTypeLocKey) + ")", startX + 6, startY + 61, 0, false, 0.65F);
		gui.renderScaledAsciiString("(" + I18n.translateToLocal(recipeTypeLocKey) + ")", startX + 6, startY + 136, 0, false, 0.65F);
		GlStateManager.popMatrix();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {
		super.updateScreen(gui, startX, startY, pageTimer);
		if (pageTimer % 5 == 0) {
			progress++;
		}
		if (progress > 20) {
			progress = 0;
		}
	}

	@Override
	public void initGui(GuiGuideBase gui, int startX, int startY) {
		super.initGui(gui, startX, startY);
		setupRecipe(gui, recipes[0], startX, startY);
		setupRecipe(gui, recipes[1], startX, startY + 75);
	}

	@Override
	public void getItemStacksForPage(List<ItemStack> list) {
		super.getItemStacksForPage(list);
		if (recipes[0] != null) {
			//list.add(recipe.getInput().copy());
			list.add(recipes[0].getOutput().copy());
			if (recipes[0].hasSecondOutput()) {
				list.add(recipes[0].getSecondOutput().copy());
			}
		}
		if (recipes[1] != null) {
			//list.add(recipe.getInput().copy());
			list.add(recipes[1].getOutput().copy());
			if (recipes[1].hasSecondOutput()) {
				list.add(recipes[1].getSecondOutput().copy());
			}
		}
	}

	private void setupRecipe(GuiGuideBase gui, ICompressorRecipe recipe, int startX, int startY) {
		gui.addOrModifyItemRenderer(recipe.getInput(), startX + 12, startY + 13, 1F, true);
		gui.addOrModifyItemRenderer(recipe.getOutput(), startX + 70, startY + 13, 1F, false);
		if (recipe.hasSecondOutput()) {
			gui.addOrModifyItemRenderer(recipe.getSecondOutput(), startX + 70, startY + 35, 1F, false);
		}
	}
}