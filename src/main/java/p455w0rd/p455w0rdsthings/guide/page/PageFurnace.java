package p455w0rd.p455w0rdsthings.guide.page;

import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.client.gui.guide.GuiGuide;
import p455w0rdslib.util.GuiUtils;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class PageFurnace extends GuidePage {

	private final ItemStack input;
	private final ItemStack output;

	public PageFurnace(int localizationKey, ItemStack output) {
		super(localizationKey);
		this.output = output;
		input = getInputForOutput(output);
	}

	private static ItemStack getInputForOutput(ItemStack output) {
		for (Map.Entry<ItemStack, ItemStack> entry : FurnaceRecipes.instance().getSmeltingList().entrySet()) {
			ItemStack stack = entry.getValue();
			if (stack != null) {
				if (stack.isItemEqual(output)) {
					return entry.getKey();
				}
			}
		}
		return null;
	}

	@Override
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);

		gui.mc.getTextureManager().bindTexture(GuiGuide.RES_LOC_GADGETS);
		GuiUtils.drawTexturedModalRect(startX + 23, startY + 10, 0, 146, 80, 26, 0);

		gui.renderScaledAsciiString("(Furnace Recipe)", startX + 32, startY + 42, 0, false, 0.65F);

		PageTextOnly.renderTextToPage(gui, this, startX + 6, startY + 57);
	}

	@Override
	public void initGui(GuiGuideBase gui, int startX, int startY) {
		super.initGui(gui, startX, startY);

		gui.addOrModifyItemRenderer(input, startX + 23 + 1, startY + 10 + 5, 1F, true);
		gui.addOrModifyItemRenderer(output, startX + 23 + 59, startY + 10 + 5, 1F, false);
	}

	@Override
	public void getItemStacksForPage(List<ItemStack> list) {
		super.getItemStacksForPage(list);

		list.add(output);
	}
}
