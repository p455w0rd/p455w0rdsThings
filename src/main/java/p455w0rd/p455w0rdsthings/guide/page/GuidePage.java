package p455w0rd.p455w0rdsthings.guide.page;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Splitter;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;
import p455w0rd.p455w0rdsthings.init.ModLogger;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
@SideOnly(Side.CLIENT)
public class GuidePage implements IGuidePage {

	protected final HashMap<String, String> textReplacements = new HashMap<String, String>();

	protected final int localizationKey;
	private final List<ItemStack> itemsForPage = new ArrayList<ItemStack>();
	private final List<FluidStack> fluidsForPage = new ArrayList<FluidStack>();
	protected IGuideChapter chapter;
	protected boolean hasNoText;
	private final int priority;

	private Minecraft mc;
	private static final Splitter P_NEWLINE_SPLITTER = Splitter.on('\n');

	public GuidePage(int localizationKey) {
		this(localizationKey, 0);
	}

	public GuidePage(int localizationKey, int priority) {
		this.localizationKey = localizationKey;
		this.priority = priority;
		mc = Minecraft.getMinecraft();
	}

	@Override
	public void getItemStacksForPage(List<ItemStack> list) {
		list.addAll(itemsForPage);
	}

	@Override
	public void getFluidStacksForPage(List<FluidStack> list) {
		list.addAll(fluidsForPage);
	}

	@Override
	public IGuideChapter getChapter() {
		return chapter;
	}

	@Override
	public void setChapter(IGuideChapter chapter) {
		this.chapter = chapter;
	}

	@Override
	public String getInfoText() {
		if (hasNoText) {
			return null;
		}

		String base = I18n.translateToLocal("guide.chapter." + chapter.getIdentifier() + ".text." + localizationKey);

		base = base.replaceAll("<imp>", TextFormatting.BLUE + "" + TextFormatting.BLUE);
		base = base.replaceAll("<item>", TextFormatting.BLUE + "");
		base = base.replaceAll("<r>", TextFormatting.BLACK + "");
		base = base.replaceAll("<n>", "\n");
		base = base.replaceAll("<i>", TextFormatting.ITALIC + "");
		base = base.replaceAll("<tifisgrin>", TextFormatting.DARK_RED + "" + TextFormatting.UNDERLINE); //This is fucking important so go read it now

		for (Map.Entry<String, String> entry : textReplacements.entrySet()) {
			base = base.replaceAll(entry.getKey(), entry.getValue());
		}
		return base;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void mouseClicked(GuiGuideBase gui, int mouseX, int mouseY, int mouseButton) {
		/*
		ITextComponent itextcomponent1 = getClickedComponentAt(gui, mouseX, mouseY);
		
		if (itextcomponent1 != null) {
			handleComponentHover(gui, itextcomponent1, mouseX, mouseY);
		}
		*/

	}

	protected void drawHoveringText(GuiGuideBase gui, List<String> text, int mouseX, int mouseY) {
		Method m = ReflectionHelper.findMethod(GuiScreen.class, gui, new String[] {
				"drawHoveringText"
		}, Void.class);
		try {
			m.invoke(gui, text, mouseX, mouseY);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			ModLogger.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	protected void drawCreativeTabHoveringText(GuiGuideBase gui, String tabName, int mouseX, int mouseY) {
		drawHoveringText(gui, Arrays.<String>asList(new String[] {
				tabName
		}), mouseX, mouseY);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void mouseReleased(GuiGuideBase gui, int mouseX, int mouseY, int state) {

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void mouseClickMove(GuiGuideBase gui, int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void actionPerformed(GuiGuideBase gui, GuiButton button) {

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void initGui(GuiGuideBase gui, int startX, int startY) {

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateScreen(GuiGuideBase gui, int startX, int startY, int pageTimer) {

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawScreenPost(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {

	}

	@Override
	public boolean shouldBeOnLeftSide() {
		return (chapter.getPageIndex(this) + 1) % 2 != 0;
	}

	@Override
	public String getIdentifier() {
		return chapter.getIdentifier() + "." + chapter.getPageIndex(this);
	}

	@Override
	public String getWebLink() {
		return "http://ellpeck.de/actaddmanual#" + chapter.getIdentifier();
	}

	public GuidePage setNoText() {
		hasNoText = true;
		return this;
	}

	public GuidePage addFluidToPage(Fluid fluid) {
		fluidsForPage.add(new FluidStack(fluid, 1));
		return this;
	}

	public GuidePage addItemToPage(ItemStack stack) {
		itemsForPage.add(stack);
		return this;
	}

	@Override
	public GuidePage addTextReplacement(String key, String value) {
		textReplacements.put(key, value);
		return this;
	}

	@Override
	public GuidePage addTextReplacement(String key, float value) {
		return this.addTextReplacement(key, Float.toString(value));
	}

	@Override
	public GuidePage addTextReplacement(String key, int value) {
		return this.addTextReplacement(key, Integer.toString(value));
	}

	@Override
	public int getSortingPriority() {
		return priority;
	}
}
