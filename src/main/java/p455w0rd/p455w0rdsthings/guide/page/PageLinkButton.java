package p455w0rd.p455w0rdsthings.guide.page;

import java.awt.Desktop;
import java.net.URI;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.text.translation.I18n;
import p455w0rd.p455w0rdsthings.api.guide.GuiGuideBase;
import p455w0rd.p455w0rdsthings.init.ModLogger;

/**
 * @author Ellpeck - used with explicit permission
 *
 */
public class PageLinkButton extends GuidePage {

	private static final int BUTTON_ID = -12782;
	private final String link;

	public PageLinkButton(int localizationKey, String link) {
		super(localizationKey);
		this.link = link;
	}

	@Override
	public void initGui(GuiGuideBase gui, int startX, int startY) {
		super.initGui(gui, startX, startY);

		gui.getButtonList().add(new GuiButton(BUTTON_ID, startX + 125 / 2 - 50, startY + 130, 100, 20, I18n.translateToLocal("guide.chapter." + chapter.getIdentifier() + ".button." + localizationKey)));
	}

	@Override
	public void drawScreenPre(GuiGuideBase gui, int startX, int startY, int mouseX, int mouseY, float partialTicks) {
		super.drawScreenPre(gui, startX, startY, mouseX, mouseY, partialTicks);
		PageTextOnly.renderTextToPage(gui, this, startX + 6, startY + 5);
	}

	@Override
	public void actionPerformed(GuiGuideBase gui, GuiButton button) {
		if (button.id == BUTTON_ID) {
			if (Desktop.isDesktopSupported()) {
				try {
					Desktop.getDesktop().browse(new URI(link));
				}
				catch (Exception e) {
					ModLogger.error("Couldn't open website from Link Button page!");
				}
			}
		}
		else {
			super.actionPerformed(gui, button);
		}
	}
}