package p455w0rd.p455w0rdsthings.guide.chapter;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import p455w0rd.p455w0rdsthings.api.guide.IGuideChapter;
import p455w0rd.p455w0rdsthings.api.guide.IGuideEntry;
import p455w0rd.p455w0rdsthings.api.guide.IGuidePage;

/**
 * @author Ellpeck -- used with explicit permission
 *
 */
public class GuideChapter implements IGuideChapter {

	public final IGuidePage[] pages;
	public final IGuideEntry entry;
	public List<ItemStack> displayStacks = Lists.<ItemStack>newArrayList();
	private final String identifier;
	private final int priority;
	public TextFormatting color;

	public GuideChapter(String identifier, IGuideEntry entry, ItemStack displayImage, IGuidePage... pages) {
		this(identifier, entry, Lists.<ItemStack>newArrayList(displayImage), pages);
	}

	public GuideChapter(String identifier, IGuideEntry entry, ItemStack displayImage, int priority, IGuidePage... pages) {
		this(identifier, entry, Lists.<ItemStack>newArrayList(displayImage), priority, pages);
	}

	public GuideChapter(String identifier, IGuideEntry entry, List<ItemStack> displayImages, IGuidePage... pages) {
		this(identifier, entry, displayImages, 0, pages);
	}

	public GuideChapter(String identifier, IGuideEntry entry, List<ItemStack> displayImages, int priority, IGuidePage... pages) {
		this.pages = pages;
		this.identifier = identifier;
		this.entry = entry;
		this.priority = priority;
		displayStacks = displayImages;
		color = TextFormatting.RESET;
		this.entry.addChapter(this);
		for (IGuidePage page : this.pages) {
			page.setChapter(this);
		}
	}

	@Override
	public IGuidePage[] getAllPages() {
		return pages;
	}

	@Override
	public String getLocalizedName() {
		return I18n.translateToLocal("guide.chapter." + getIdentifier() + ".name");
	}

	@Override
	public String getLocalizedNameWithFormatting() {
		return color + getLocalizedName();
	}

	@Override
	public IGuideEntry getEntry() {
		return entry;
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public int getPageIndex(IGuidePage page) {
		for (int i = 0; i < pages.length; i++) {
			if (pages[i] == page) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int getSortingPriority() {
		return priority;
	}

	public GuideChapter setImportant() {
		color = TextFormatting.DARK_GREEN;
		return this;
	}

	public GuideChapter setSpecial() {
		color = TextFormatting.DARK_PURPLE;
		return this;
	}

	@Override
	public List<ItemStack> getDisplayItemStacks() {
		return displayStacks;
	}
}
