/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.recipe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import p455w0rd.p455w0rdsthings.api.IArmorUpgrade;
import p455w0rd.p455w0rdsthings.init.ModItems;
import p455w0rd.p455w0rdsthings.items.ItemEmeraldCarbonArmor;
import p455w0rd.p455w0rdsthings.items.ItemRFArmor;
import p455w0rd.p455w0rdsthings.util.ArmorUtils;
import p455w0rd.p455w0rdsthings.util.ArmorUtils.Upgrades;
import p455w0rdslib.util.ItemUtils;

/**
 * @author p455w0rd
 *
 */
public class ArmorUpgradeRecipe implements IRecipe {

	private ItemStack output = null;
	private List<ItemStack> input = new ArrayList<ItemStack>();

	public ArmorUpgradeRecipe() {
	}

	public ArmorUpgradeRecipe(ItemStack out, ItemStack... inputs) {
		output = out;
		if (output.getItem() == ModItems.EMERALD_CARBON_JETPLATE) {
			output = ArmorUtils.enableUpgrade(output, EntityEquipmentSlot.CHEST, Upgrades.FLIGHT);
		}
		input = Arrays.asList(inputs);
	}

	@Override
	public boolean matches(InventoryCrafting inv, World worldIn) {
		boolean hasArmorItem = false;
		boolean hasUpgradeItem = false;
		int armorItemCount = 0;
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) == null) {
				continue;
			}
			if (inv.getStackInSlot(i).getItem() instanceof ItemEmeraldCarbonArmor) {
				hasArmorItem = true;
				++armorItemCount;
				continue;
			}
			if (inv.getStackInSlot(i).getItem() instanceof IArmorUpgrade) {
				hasUpgradeItem = true;
			}
		}
		// Allow only one piece of armor, but multiple upgrades
		return hasArmorItem && hasUpgradeItem && armorItemCount == 1;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {

		ItemStack craftingResult = null;
		ItemStack armorItemStack = null;
		int armorItemCount = 0;
		NBTTagCompound nbtTC = new NBTTagCompound();
		boolean containsOtherItem = false;
		boolean isUpgradeFlight = false;
		boolean isUpgradeBattery = false;
		int batteryAmount = 0;
		List<ItemStack> upgradeItemStacks = new ArrayList<ItemStack>();

		// First we ensure there is only 1 type of upgradeable armor in the grid
		// We also do this first because we want to ensure later that only applicable upgrades
		// are present in the grid
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) != null && inv.getStackInSlot(i).getItem() instanceof ItemEmeraldCarbonArmor) {
				armorItemStack = inv.getStackInSlot(i);
				++armorItemCount;
			}
		}
		if (armorItemStack == null || armorItemCount > 1) {
			return null;
		}

		// Now we build a list of upgrades and ensure they are applicable to the armor item
		// we are attempting to upgrade
		// It also doesn't hurt to use the same loop to ensure there aren't any random
		// items thrown in
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) != null && inv.getStackInSlot(i).getItem() instanceof IArmorUpgrade && inv.getStackInSlot(i).getItem() != ModItems.ARMOR_UPGRADE_BASE) {
				IArmorUpgrade upgradeItem = (IArmorUpgrade) inv.getStackInSlot(i).getItem();
				Upgrades upgrade = upgradeItem.getUpgradeType();
				EntityEquipmentSlot armorType = ((ItemArmor) armorItemStack.getItem()).armorType;
				if (!ArmorUtils.isApplicableUpgrade(armorType, upgrade)) {
					return null;
				}
				if (upgradeItem == ModItems.ARMOR_UPGRADE_FLIGHT) {
					isUpgradeFlight = true;
				}
				if (upgradeItem == ModItems.ADVANCED_BATTERY) {
					isUpgradeBattery = true;
					batteryAmount = ((IEnergyContainerItem) upgradeItem).getEnergyStored(inv.getStackInSlot(i));
				}
				upgradeItemStacks.add(inv.getStackInSlot(i));
			}

			else if (inv.getStackInSlot(i) != null && ((!(inv.getStackInSlot(i).getItem() instanceof ItemEmeraldCarbonArmor) && !(inv.getStackInSlot(i).getItem() instanceof IArmorUpgrade)) || inv.getStackInSlot(i).getItem() == ModItems.ARMOR_UPGRADE_BASE)) {
				containsOtherItem = true;
			}

		}

		if (upgradeItemStacks.size() <= 0 || containsOtherItem) {
			return null;
		}
		ItemEmeraldCarbonArmor armorItem = (ItemEmeraldCarbonArmor) armorItemStack.getItem();

		craftingResult = armorItemStack.copy();

		if (craftingResult.hasTagCompound()) {
			nbtTC = craftingResult.getTagCompound();
		}
		for (int i = 0; i < upgradeItemStacks.size(); ++i) {
			IArmorUpgrade upgradeItem = (IArmorUpgrade) upgradeItemStacks.get(i).getItem();
			if (upgradeItem.getUpgradeType().getArmorTypes().contains(armorItem.armorType)) {
				nbtTC.setBoolean(upgradeItem.getUpgradeType().getNBTKey(), true);
				input.add(upgradeItemStacks.get(i));
			}
		}
		craftingResult.setTagCompound(nbtTC);
		//make sure we aren't just inputting what we're getting out -- not needed anymore?
		if (ItemStack.areItemStacksEqual(armorItemStack, craftingResult)) {
			craftingResult = null;
		}

		// a special-case scenario for the jetplate since we actually change items
		if (craftingResult != null && craftingResult.getItem() == ModItems.EMERALD_CARBON_CHESTPLATE && isUpgradeFlight) {
			ItemUtils.setItem(craftingResult, ModItems.EMERALD_CARBON_JETPLATE);
		}
		if (isUpgradeBattery && batteryAmount > 0) {
			((ItemRFArmor) craftingResult.getItem()).setEnergyStored(craftingResult, batteryAmount + ((ItemRFArmor) craftingResult.getItem()).getEnergyStored(craftingResult));
		}
		output = craftingResult;
		return craftingResult;
	}

	@Override
	public int getRecipeSize() {
		return 9;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return output;
	}

	public List<ItemStack> getInput() {
		return input;
	}

	@Override
	public ItemStack[] getRemainingItems(InventoryCrafting inv) {
		return ForgeHooks.defaultRecipeGetRemainingItems(inv);
	}

}
