package p455w0rd.p455w0rdsthings.recipe;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.item.ItemStack;
import p455w0rd.p455w0rdsthings.api.ICompressorRecipe;
import p455w0rdslib.util.ItemUtils;

/**
 * @author p455w0rd
 *
 */
public class RecipeCompressor implements ICompressorRecipe {

	private static final List<RecipeCompressor> REGISTRY = Lists.<RecipeCompressor>newArrayList();

	private ItemStack input;
	private ItemStack output;
	private ItemStack output2;
	private int energy;
	private double chance;

	public RecipeCompressor(ItemStack in, ItemStack out, @Nullable ItemStack out2, double chance2, int energyIn) {
		if (!isRecipeRegistered(this)) {
			input = in;
			output = out;
			energy = energyIn;
			if (out != null) {
				output2 = out2;
				chance = chance2;
			}
		}
	}

	public static List<RecipeCompressor> getRecipes() {
		return REGISTRY;
	}

	public static ICompressorRecipe addRecipe(RecipeCompressor recipe) {
		if (!isRecipeRegistered(recipe)) {
			getRecipes().add(recipe);
		}
		return recipe;
	}

	public static ICompressorRecipe addRecipe(ItemStack in, ItemStack out, @Nullable ItemStack out2, double chance2, int energy) {
		return addRecipe(new RecipeCompressor(in, out, out2, chance2, energy));
	}

	public static boolean isRecipeRegistered(RecipeCompressor recipe) {
		ItemStack currentInput = recipe.getInput();
		ItemStack currentOutput = recipe.getOutput();
		for (RecipeCompressor recipe2 : getRecipes()) {
			if (ItemUtils.compareStacks(currentInput, recipe2.getInput()) && ItemUtils.compareStacks(currentOutput, recipe2.getOutput())) {
				return true;
			}
		}
		return false;
	}

	public static boolean isValidInput(ItemStack stack) {
		for (ItemStack currentStack : getInputList()) {
			if (ItemUtils.compareStacks(stack, currentStack)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isValidOutput(ItemStack stack) {
		for (ItemStack currentStack : getOutputList()) {
			if (ItemUtils.compareStacks(stack, currentStack)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isValidSecondaryOutput(ItemStack stack) {
		for (ItemStack currentStack : getSecondaryOutputList()) {
			if (ItemUtils.compareStacks(stack, currentStack)) {
				return true;
			}
		}
		return false;
	}

	public static RecipeCompressor getRecipeFromInput(ItemStack stack) {
		for (RecipeCompressor recipe : getRecipes()) {
			if (ItemUtils.compareStacks(stack, recipe.getInput())) {
				return recipe;
			}
		}
		return null;
	}

	public static RecipeCompressor getRecipeFromOutput(ItemStack stack) {
		for (RecipeCompressor recipe : getRecipes()) {
			if (ItemUtils.compareStacks(stack, recipe.getOutput())) {
				return recipe;
			}
		}
		return null;
	}

	public static List<ItemStack> getInputList() {
		List<ItemStack> list = Lists.<ItemStack>newArrayList();
		for (RecipeCompressor recipe : getRecipes()) {
			list.add(recipe.getInput());
		}
		return list;
	}

	public static List<ItemStack> getOutputList() {
		List<ItemStack> list = Lists.<ItemStack>newArrayList();
		for (RecipeCompressor recipe : getRecipes()) {
			list.add(recipe.getOutput());
		}
		return list;
	}

	public static List<ItemStack> getSecondaryOutputList() {
		List<ItemStack> list = Lists.<ItemStack>newArrayList();
		for (RecipeCompressor recipe : getRecipes()) {
			list.add(recipe.getSecondOutput());
		}
		return list;
	}

	@Override
	public ItemStack getInput() {
		return input;
	}

	@Override
	public ItemStack getOutput() {
		return output;
	}

	@Override
	public ItemStack getSecondOutput() {
		return output2;
	}

	@Override
	public int getEnergyRequired() {
		return energy;
	}

	@Override
	public boolean hasSecondOutput() {
		return output2 != null;
	}

	@Override
	public boolean hasEnergyRegistered() {
		return energy > 0;
	}

	@Override
	public double getSecondOutputChance() {
		return chance;
	}

	@Override
	public int getInputCount() {
		return getInput().stackSize;
	}

	@Override
	public int getOutputCount() {
		return getOutput().stackSize;
	}

	@Override
	public int getSecondOutputCount() {
		return hasSecondOutput() ? getSecondOutput().stackSize : -1;
	}

}