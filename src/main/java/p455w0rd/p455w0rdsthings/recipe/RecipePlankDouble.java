/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.recipe;

import java.util.List;

import net.minecraft.block.*;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.*;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.oredict.OreDictionary;

/**
 * @author p455w0rd
 *
 */
public class RecipePlankDouble implements IRecipe {

	@Override
	public boolean matches(InventoryCrafting inv, World worldIn) {
		int axeCount = 0;
		int logCount = 0;
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) == null) {
				continue;
			}
			if (inv.getStackInSlot(i).getItem() instanceof ItemAxe) {
				axeCount++;
			}

			if (Block.getBlockFromItem(inv.getStackInSlot(i).getItem()) instanceof BlockLog) {
				logCount++;
			}
		}
		List<ItemStack> list = OreDictionary.getOres("plankWood");
		return axeCount == 1 && logCount == 1;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		ItemStack axeStack = null;
		ItemStack plankStack = null;
		int axeCount = 0;
		int logCount = 0;
		int logType = 0;

		// First we get the axe and ensure there is only one axe in the grid
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) != null && inv.getStackInSlot(i).getItem() instanceof ItemAxe) {
				axeCount++;
				if (axeCount > 1) {
					return null;
				}
				axeStack = inv.getStackInSlot(i);
			}
		}

		// Now we ensure there is only 1 stack of planks in the grid
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) != null && Block.getBlockFromItem(inv.getStackInSlot(i).getItem()) instanceof BlockLog) {
				logCount++;
				if (logCount > 1) {
					return null;
				}
				plankStack = inv.getStackInSlot(i);
			}
		}

		// ensure we have at least 1 axe and 1 plank
		if (axeStack == null || plankStack == null) {
			return null;
		}

		return new ItemStack(Blocks.PLANKS, 8, plankStack.getItemDamage());
	}

	@Override
	public int getRecipeSize() {
		return 2;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return null;
	}

	@Override
	public ItemStack[] getRemainingItems(InventoryCrafting inv) {
		ItemStack[] remainingItems = new ItemStack[inv.getSizeInventory()];
		ItemStack axeStack = null;
		ItemStack newStack = null;
		ItemStack plankStack = null;
		for (int i = 0; i < inv.getSizeInventory(); ++i) {
			if (inv.getStackInSlot(i) != null && inv.getStackInSlot(i).getItem() instanceof ItemAxe) {
				axeStack = inv.getStackInSlot(i);
				int currentDamage = axeStack.getItemDamage();
				int maxDamage = axeStack.getMaxDamage();
				if (currentDamage + 1 < maxDamage) {
					newStack = axeStack.copy();
					newStack.setItemDamage(currentDamage + 1);
					remainingItems[i] = newStack;
				}
				else {
					axeStack = null;
				}
			}
			if (inv.getStackInSlot(i) != null && OreDictionary.getOres("plankWood").contains(inv.getStackInSlot(i).getItem())) {
				plankStack = inv.getStackInSlot(i);
				if (plankStack.stackSize > 1) {
					plankStack.stackSize--;
					remainingItems[i] = plankStack;
				}
				else {
					plankStack = null;
				}
			}
		}
		if (newStack == null && plankStack == null) {
			remainingItems = null;
		}
		return remainingItems != null ? remainingItems : ForgeHooks.defaultRecipeGetRemainingItems(inv);
	}

}
