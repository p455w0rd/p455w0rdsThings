/*
 * This file is part of p455w0rd's Things.
 * Copyright (c) 2016, p455w0rd (aka TheRealp455w0rd), All rights reserved
 * unless
 * otherwise stated.
 *
 * p455w0rd's Things is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 *
 * p455w0rd's Things is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * MIT License for more details.
 *
 * You should have received a copy of the MIT License
 * along with p455w0rd's Things. If not, see
 * <https://opensource.org/licenses/MIT>.
 */
package p455w0rd.p455w0rdsthings.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileDankNullDock;
import p455w0rd.p455w0rdsthings.blocks.tileentities.TileDankNullDock.ExtractionMode;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;
import p455w0rdslib.util.ItemUtils;

/**
 * @author p455w0rd
 *
 */
public class DankNulItemHandler implements IItemHandlerModifiable, ICapabilitySerializable<NBTBase> {

	int maxSize = 0;
	ItemStack dankNull;
	TileDankNullDock dankDock;
	protected ItemStack[] stacks;

	public DankNulItemHandler(ItemStack stack) {
		if (stack != null) {
			dankNull = stack;
		}
		stacks = new ItemStack[54];
		//maxSize = stack.getItemDamage() + 1 == 6 ? Integer.MAX_VALUE : ((stack.getItemDamage() + 1) * 128) * (stack.getItemDamage() + 1);
	}

	public void setTile(TileDankNullDock dock) {
		dankDock = dock;
	}

	public TileDankNullDock getTile() {
		return dankDock;
	}

	public void setSize(int size) {
		stacks = new ItemStack[size];
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		validateSlotIndex(slot);
		if (ItemStack.areItemStacksEqual(stacks[slot], stack)) {
			return;
		}
		stacks[slot] = stack;
		onContentsChanged(slot);
	}

	protected void validateSlotIndex(int slot) {
		if (slot < 0 || slot >= stacks.length) {
			throw new RuntimeException("Slot " + slot + " not in valid range - [0," + stacks.length + ")");
		}
	}

	protected void onLoad() {

	}

	protected void onContentsChanged(int slot) {

	}

	@Override
	public int getSlots() {
		return stacks.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		validateSlotIndex(slot);
		return stacks[slot];
	}

	protected int getStackLimit(int slot, ItemStack stack) {
		return Integer.MAX_VALUE;
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		if (stack == null || stack.stackSize == 0) {
			return null;
		}

		validateSlotIndex(slot);

		ItemStack existing = stacks[slot];

		if (existing != null && !ItemUtils.compareStacks(stack, stacks[slot])) {
			return null;
		}

		if (!simulate) {
			if (existing == null) {
				stacks[slot] = stack;
			}
			else {
				existing.stackSize += stack.stackSize;
			}
			//onContentsChanged(slot);
			serializeNBT();
		}

		return stacks[slot];
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		ItemStack ret = null;
		if (getTile() != null) {
			if (getTile().getExtractionMode() == ExtractionMode.NORMAL) {
				ret = doExtract(slot, amount, simulate);
			}
			else if (getTile().getExtractionMode() == ExtractionMode.SELECTED) {
				if (ItemUtils.compareStacks(getStackInSlot(slot), getTile().getSelectedStack())) {
					ret = doExtract(slot, amount, simulate);
				}
			}
		}
		else {
			ret = doExtract(slot, amount, simulate);
		}
		//DankNullUtils.reArrangeStacks(dankNull);
		return ret;
	}

	private ItemStack doExtract(int slot, int amount, boolean simulate) {
		if (amount == 0) {
			return null;
		}

		validateSlotIndex(slot);

		ItemStack existing = stacks[slot];

		if (existing == null) {
			return null;
		}

		int toExtract = Math.min(amount, existing.getMaxStackSize());

		if (existing.stackSize <= toExtract) {
			if (!simulate) {
				stacks[slot] = null;
				onContentsChanged(slot);
			}
			return existing;
		}
		else {
			if (!simulate) {
				stacks[slot] = ItemHandlerHelper.copyStackWithSize(existing, existing.stackSize - toExtract);
				onContentsChanged(slot);
			}

			return ItemHandlerHelper.copyStackWithSize(existing, toExtract);
		}
	}

	/*
	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		if (stack == null || stack.stackSize == 0) {
			return null;
		}
		validateSlotIndex(slot);
		ItemStack existing = stacks[slot];
		int limit = getStackLimit(slot, stack);
		if (existing != null) {
			if (!ItemHandlerHelper.canItemStacksStack(stack, existing)) {
				return stack;
			}
			limit -= existing.stackSize;
		}
		if (limit <= 0) {
			return stack;
		}
		boolean reachedLimit = stack.stackSize > limit;
		if (!simulate) {
			if (existing == null) {
				stacks[slot] = reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, limit) : stack;
			}
			else {
				existing.stackSize += reachedLimit ? limit : stack.stackSize;
			}
			onContentsChanged(slot);
		}
		return reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, stack.stackSize - limit) : null;
	}
	*/

	@Override
	public NBTBase serializeNBT() {
		NBTTagList nbtTagList = new NBTTagList();
		for (int i = 0; i < stacks.length; i++) {
			if (stacks[i] != null) {
				NBTTagCompound itemTag = new NBTTagCompound();
				itemTag.setInteger("Slot", i);
				int size = stacks[i].stackSize;
				int max = DankNullUtils.getDankNullMaxStackSize(dankNull);
				if (size > max) {
					stacks[i].stackSize = max;
				}
				itemTag.setInteger("RealCount", stacks[i].stackSize);
				stacks[i].writeToNBT(itemTag);
				nbtTagList.appendTag(itemTag);
			}
		}
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setTag(getName(), nbtTagList);
		nbt.setInteger("Size", stacks.length);
		return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.writeNBT(this, null);
	}

	@Override
	public void deserializeNBT(NBTBase nbtIn) {
		if (nbtIn instanceof NBTTagCompound) {
			NBTTagCompound nbt = (NBTTagCompound) nbtIn;
			setSize(nbt.hasKey("Size", Constants.NBT.TAG_INT) ? nbt.getInteger("Size") : stacks.length);
			NBTTagList tagList = nbt.getTagList(getName(), Constants.NBT.TAG_COMPOUND);
			for (int i = 0; i < tagList.tagCount(); i++) {
				NBTTagCompound itemTags = tagList.getCompoundTagAt(i);
				int slot = itemTags.getInteger("Slot");

				if (slot >= 0 && slot < stacks.length) {
					stacks[slot] = ItemStack.loadItemStackFromNBT(itemTags);
					stacks[slot].stackSize = itemTags.getInteger("RealCount");
				}
			}
			CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.readNBT(this, null, tagList);
			onLoad();
		}
	}

	private String getName() {
		return "danknull-inventory";
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return hasCapability(capability, facing) ? CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(this) : null;
	}

}
