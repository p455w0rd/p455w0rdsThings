package p455w0rd.p455w0rdsthings.inventory.slot;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

/**
 * @author p455w0rd
 *
 */
public class SlotItemHandlerDankNull extends SlotItemHandler {

	public SlotItemHandlerDankNull(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
	}

	@Override
	public int getItemStackLimit(ItemStack stack) {
		return Integer.MAX_VALUE;
	}

	/**
	@Override
	public boolean isItemValid(ItemStack stack)
	{
	    if (stack == null)
	        return false;
	
	    IItemHandler handler = this.getItemHandler();
	    ItemStack remainder;
	    if (handler instanceof IItemHandlerModifiable)
	    {
	        IItemHandlerModifiable handlerModifiable = (IItemHandlerModifiable) handler;
	        ItemStack currentStack = handlerModifiable.getStackInSlot(index);
	
	        handlerModifiable.setStackInSlot(index, null);
	
	        remainder = handlerModifiable.insertItem(index, stack, true);
	
	        handlerModifiable.setStackInSlot(index, currentStack);
	    }
	    else
	    {
	        remainder = handler.insertItem(index, stack, true);
	    }
	    return remainder == null || remainder.stackSize < stack.stackSize;
	}
	*/

}
