package p455w0rd.p455w0rdsthings.inventory;

import java.util.Iterator;

import javax.annotation.Nullable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.util.Constants;
import p455w0rd.p455w0rdsthings.items.ItemDankNull;
import p455w0rd.p455w0rdsthings.util.DankNullUtils;

public class InventoryDankNull implements IInventory, Iterable<ItemStack> {

	public static final String INVENTORY_NAME = "danknull-inventory";
	public static final String TAG_SLOT = "Slot";
	public static final String TAG_COUNT = "RealCount";

	private int size = 54;
	private final ItemStack[] stackArray;
	private final int[] sizesArray;
	private ItemStack dankNullStack;
	private int numRows = 0;
	private EntityPlayer player;

	public InventoryDankNull(ItemStack dankNull) {
		dankNullStack = dankNull;
		numRows = (dankNullStack.getItemDamage() + 1);
		size = (numRows * 9);
		stackArray = new ItemStack[size];
		sizesArray = new int[size];
		if (!dankNull.hasTagCompound()) {
			dankNull.setTagCompound(new NBTTagCompound());
		}
		readFromNBT(dankNull.getTagCompound());
	}

	@Override
	public int getSizeInventory() {
		return size;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return index < getSizeInventory() ? stackArray[index] : null;
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {
		if (getStackInSlot(index) != null) {
			if (getStackInSlot(index).stackSize <= amount) {
				ItemStack itemstack = getStackInSlot(index);
				setInventorySlotContents(index, null);
				setSizeForSlot(index, 0);
				markDirty();
				return itemstack;
			}
			ItemStack itemstack1 = getStackInSlot(index).splitStack(amount);
			setSizeForSlot(index, getSizeForSlot(index) - amount);
			if (getStackInSlot(index).stackSize == 0) {
				setInventorySlotContents(index, null);
			}
			markDirty();
			return itemstack1;
		}
		else {
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack itemStack) {
		getStacks()[index] = itemStack;
		markDirty();
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack stack) {
		return !(stack.getItem() instanceof ItemDankNull);
	}

	@Override
	public String getName() {
		return INVENTORY_NAME;
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	@Override
	public ITextComponent getDisplayName() {
		return new TextComponentString(getName());
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = getStackInSlot(index);
		if (stack != null) {
			setInventorySlotContents(index, null);
		}
		return stack;
	}

	@Override
	public int getInventoryStackLimit() {
		return Integer.MAX_VALUE;
	}

	@Override
	public void openInventory(EntityPlayer player) {
		if (player != null) {
			this.player = player;
		}
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
	}

	@Override
	public Iterator<ItemStack> iterator() {
		return new InvIterator(this);
	}

	public ItemStack[] getStacks() {
		return stackArray;
	}

	public int getSizeForSlot(int index) {
		return sizesArray[index];
	}

	public void setSizeForSlot(int index, int size) {
		sizesArray[index] = size < 0 ? 0 : size;
	}

	public long getMaxStackSize() {
		return DankNullUtils.getDankNullMaxStackSize(getDankNull());
	}

	public boolean isEmpty() {
		for (int x = 0; x < size; x++) {
			if (getStackInSlot(x) != null) {
				return false;
			}
		}
		return true;
	}

	public ItemStack getDankNull() {
		return dankNullStack != null ? dankNullStack : null;
	}

	public static boolean isSameItem(@Nullable ItemStack left, @Nullable ItemStack right) {
		return (left != null) && (right != null) && (left.isItemEqual(right));
	}

	@Override
	public void markDirty() {
		if (getDankNull() == null) {
			return;
		}
		if (!getDankNull().hasTagCompound()) {
			getDankNull().setTagCompound(new NBTTagCompound());
		}
		writeToNBT(getDankNull().getTagCompound());
	}

	public void writeToNBT(NBTTagCompound itemTC) {
		NBTTagList nbtTL = new NBTTagList();
		for (int i = 0; i < getSizeInventory(); i++) {
			if (getStackInSlot(i) != null) {
				NBTTagCompound nbtTC = new NBTTagCompound();
				nbtTC.setInteger(TAG_SLOT, i);
				nbtTC.setInteger(TAG_COUNT, getStackInSlot(i).stackSize <= DankNullUtils.getDankNullMaxStackSize(this) ? getStackInSlot(i).stackSize : DankNullUtils.getDankNullMaxStackSize(this));
				getStackInSlot(i).writeToNBT(nbtTC);
				nbtTL.appendTag(nbtTC);
			}
		}
		itemTC.setTag(getName(), nbtTL);
	}

	public void readFromNBT(NBTTagCompound compound) {
		NBTTagList nbtTL = compound.hasKey(getName(), Constants.NBT.TAG_LIST) ? compound.getTagList(getName(), Constants.NBT.TAG_COMPOUND) : new NBTTagList();
		for (int i = 0; i < nbtTL.tagCount(); i++) {
			NBTTagCompound nbtTC = nbtTL.getCompoundTagAt(i);
			if (nbtTC != null) {
				int slot = nbtTC.getInteger(TAG_SLOT);
				ItemStack stack = ItemStack.loadItemStackFromNBT(nbtTC);
				if (nbtTC.hasKey(TAG_COUNT)) {
					stack.stackSize = nbtTC.getInteger(TAG_COUNT);
					setSizeForSlot(slot, nbtTC.getInteger(TAG_COUNT));
				}
				setInventorySlotContents(slot, stack);
			}
		}
	}

	public EntityPlayer getPlayer() {
		return player;
	}

}
