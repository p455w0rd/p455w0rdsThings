package p455w0rd.p455w0rdsthings.events;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.Event;

/**
 * @author p455w0rd
 *
 */
public class ItemCompressedEvent extends Event {

	private final ItemStack compressedItem;
	private final EntityPlayer player;

	public ItemCompressedEvent(EntityPlayer player, ItemStack compressedItem) {
		this.player = player;
		this.compressedItem = compressedItem;
	}

	public EntityPlayer getPlayer() {
		return player;
	}

	public ItemStack getStack() {
		return compressedItem;
	}

	public Item getItem() {
		return getStack().getItem();
	}

}
